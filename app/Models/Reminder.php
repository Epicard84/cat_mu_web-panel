<?php

namespace App\Models;

use App\Models\MasterUser\Client;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Reminder extends Model
{
    use HasFactory;
    protected $fillable = [
        'reminder',
        'status',
        'masteruser_id',
        'date'
    ];

    public function getReminderDetails(){
        return $this->hasOne(ReminderDetail::class, 'id', 'reminder_id');
    }

    public static function medReminder($clientMasterUser,$clientName,$medName,$today,$time){
        Reminder::create([
            'masteruser_id' => $clientMasterUser,
            'date' => $today,
            'reminder' => $clientName." missed ".$medName." ".date('Hi',strtotime($time)),
            'status' => 0
        ]);
    }

    public static function mfdReminder($master_user,$mfd){

        // $year = date('Y');
        // $month = date('m');
        // $mfdMonth = $month+1;

        // Get the current date
        $currentDate = date('Y-m', strtotime("+1 month"));
        // Add one month to the current date
        // $currentDate->modify('+1 month');
        // $nextMonthDate = $currentDate->format('Y-m-d');
        // dd($currentDate);

        $mfdDate = $currentDate."-01";
        Reminder::create([
            'masteruser_id' => $master_user,
            'date' => $mfdDate,
            'reminder' => $mfd,
            'status' => 0
        ]);
    }

    
}
