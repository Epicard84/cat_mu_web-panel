<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guardian extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'guardian_name',
        'guardian_phone_number',
        'guardian_address',
    ];
}
