<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class Notification extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'sender_id',
        'receiver_id',
        'notification_id',
        'status'
    ];

    public function getNotiDetails(){
        return $this->hasOne(NotificationDetail::class, 'id', 'notification_id');
    }

    public static function medComInfo($notId,$muId){
        $masterU = $muId;
        $subU = User::where('master_user',$muId)->get();
        $new_medComNote = Notification::create([
            'receiver_id' => $muId,
            'notification_id' => $notId,
        ]);
        foreach($subU as $su){
            $new_medComNote = Notification::create([
                'receiver_id' => $su->id,
                'notification_id' => $notId,
            ]); 
        }
    }

    public static function newMedDetails($notId,$muId,$suId){
        if($muId != null){
            $subU = User::where('master_user',$muId)->get();
            foreach($subU as $su){
                $newmedDetails = Notification::create([
                    'sender_id'	=> $muId,
                    'receiver_id' => $su->id,
                    'notification_id' => $notId,
                ]); 
            }
        }
        else{
            $MasterUId = User::where('id',$suId)->first()->master_user;
            $subU = User::where('master_user',$MasterUId)->get();
            $newmedDetails = Notification::create([
                'sender_id'	=> $suId,
                'receiver_id' =>  $MasterUId,
                'notification_id' => $notId,
            ]);
            foreach($subU as $su){
                if($su->id != $suId){
                $newmedDetails = Notification::create([
                    'sender_id'	=> $muId,
                    'receiver_id' => $su->id,
                    'notification_id' => $notId,
                ]); 
            }
            }
        }

    }
}
