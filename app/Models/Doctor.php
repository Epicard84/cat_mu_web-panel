<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Doctor extends Model
{
    use HasFactory;
    protected $fillable = [
        'client_id',
        'doctor_name',
        'doctor_phone_number',
        'doctor_address',
    ];
}
