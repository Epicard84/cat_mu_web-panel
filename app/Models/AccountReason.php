<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AccountReason extends Model
{
    use HasFactory;
    protected $fillable = [
        'masteruser_id',
        'reason',
        'date',
        'status',
    ];

    public function getMasterUserDetails(){
        return $this->hasOne(User::class, 'id', 'masteruser_id')->withTrashed();
    }
}
