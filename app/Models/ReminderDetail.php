<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\MasterUser\Client;

class ReminderDetail extends Model
{
    use HasFactory;
    protected $fillable = [
        'patient_id',
        'reminder',
        // 'date',
        'medicine_id',
    ];

//     public function getPatientName(){
//         return $this->hasOne(Client::class, 'id', 'patient_id');
//     }

public function getClientrDetails(){
    return $this->hasOne(Client::class, 'id', 'patient_id');
}
}
