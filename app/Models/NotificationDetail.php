<?php

namespace App\Models;

use App\Models\MasterUser\Client;
use App\Models\MasterUser\Medicine;
use App\Models\MasterUser\MedicineSchedule;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Notification;

class NotificationDetail extends Model
{
    use HasFactory, SoftDeletes;

    protected $fillable = [
        'creator_id',
        'patient_id',
        'notification'
    ];


    
    public static function medComNote($medId,$date)
    {
        $date = date('m-d-Y',strtotime($date));
        $medDetails = Medicine::where('id',$medId)->first();
        $patientDetails = Client::where('id',$medDetails->client_id)->first();
        $new_medComNote = NotificationDetail::create([
            'patient_id' => $patientDetails->id,
            'notification' => "Patient name: ".$patientDetails->first_name." ".$patientDetails->last_name.", Medicine name: ".$medDetails->medicine_name." has been completed for ".$date,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        // dd("Notification_id: ".$new_medComNote->id." Client_id: ".$patientDetails->id);
        // dd("Notification_id: ".$new_medComNote->id." & MU_id: ".$patientDetails->master_user);
        
        Notification::medComInfo($new_medComNote->id,$patientDetails->master_user);
    }

    public static function addDetails($creator_id,$patient_id,$date,$type,$medSchId)
    {
        $dtype = "";
        if($type = 0){
            $dtype = "Note";
        }
        elseif($type = 1){
            $dtype = "Chart";
        }
        else{
            $dtype = "Narrative";
        }
        $creatorDetails = User::where('id',$creator_id)->first();
        $patientDetails = Client::where('id',$patient_id)->first();
        $date = date('m-d-Y',strtotime($date));
        $notification = "";
        if($medSchId != null){
            $medId = MedicineSchedule::where('id',$medSchId)->first();
            $medDetails = Medicine::where('id',$medId->medicine_id)->first();
            $notification = $creatorDetails->first_name." ".$creatorDetails->last_name." added a ".$dtype." for ".$patientDetails->first_name." ".$patientDetails->last_name." for ".$medDetails->medicine_name." for ".$date;
        }
        else{
            $notification = $creatorDetails->first_name." ".$creatorDetails->last_name." added a ".$dtype." for ".$patientDetails->first_name." ".$patientDetails->last_name." for ".$date;
        }

        $newMedDetails = NotificationDetail::create([
            'creator_id	' => "1",
            'notification' => $notification,
            'created_at' => date('Y-m-d H:i:s'),
        ]);
        // dd($newMedDetails);
        $muId = "";
        $suId = "";
        if($creatorDetails->user_type = 1){
            $muId = $creatorDetails->id;
        }
        else{
            $suId = $creatorDetails->master_user;
        }
        Notification::newMedDetails($newMedDetails->id,$muId,$suId);
    }
}
