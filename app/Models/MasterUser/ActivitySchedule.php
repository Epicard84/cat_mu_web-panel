<?php

namespace App\Models\MasterUser;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ActivitySchedule extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $fillable = [
        'activity_id',
        'date',
        'complete_details',
        'status',
        'scheduled_time'
    ];
}
