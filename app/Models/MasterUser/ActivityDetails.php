<?php

namespace App\Models\MasterUser;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class ActivityDetails extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $fillable = [
        'client_id',
        'type',
        'descriptions',
        'date',
        'images',
        'patient_id',
        'schedule_id',
        'creator_id'
    ];

    public function getName(){
        return $this->hasOne(User::class, 'id', 'creator_id');
    }
}
