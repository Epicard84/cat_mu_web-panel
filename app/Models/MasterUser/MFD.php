<?php

namespace App\Models\MasterUser;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\User;

class MFD extends Model
{
    use HasFactory;
    protected $table = 'mfds';
    protected $fillable = [
        'master_user_id',
        'date',
        'message'
    ];

    public function getName(){
        return $this->hasOne(User::class, 'id', 'master_user_id');
    }
}
