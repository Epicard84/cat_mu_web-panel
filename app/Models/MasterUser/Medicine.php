<?php

namespace App\Models\MasterUser;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Medicine extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'client_id',
        'date',
        'medicine_name',
        'start_date',
        'end_date',
        'time_days',
        'status',
        'descriptions',
        'docs',
        'scheduled_time'
    ];
    
    public function getClientName(){

        $this->belongsTo(MedicineSchedule::class,'medicine_id')->get();
    }

    public function getMedicineSchedule(){
        return $this->hasMany(MedicineSchedule::class, 'medicine_id', 'id');
    }
    
}
