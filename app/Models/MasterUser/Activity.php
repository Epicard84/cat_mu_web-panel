<?php

namespace App\Models\MasterUser;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MasterUser\ActivitySchedule;

class Activity extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'client_id',
        'date',
        'activity_name',
        'start_date',
        'end_date',
        'time_days',
        'scheduled_time',
        'status',
        'descriptions',
        'docs',
        'scheduled_time'
    ];

    public function getClientName(){

        $this->belongsTo(ActivitySchedule::class,'activity_id')->get();
    }

    public function getActivitySchedule(){
        return $this->hasMany(ActivitySchedule::class, 'activity_id', 'id');
    }
}
