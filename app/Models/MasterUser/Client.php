<?php

namespace App\Models\MasterUser;

use App\Models\ClientDocument;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User;

class Client extends Model
{
    use HasFactory;
    use SoftDeletes;
    protected $fillable = [
        'first_name',
        'last_name',
        'dob',
        'gender',
        'guardian_name',
        'guardian_phone_number',
        'guardian_address',
        'doctor_name',
        'doctor_phone_number',
        'doctor_address',
        'outher_contact_information',
        'additional_misc_information',
        'master_user',
        'status',
        'documents',
        'some_information_for_emergency',
        'discharged_date'
    ];

    public function getMedicines(){
        return $this->hasMany(Medicine::class, 'client_id', 'id');
    }
  
    //ForSuperAdmin Panel Tables
    public function getMasterDetails(){
        return $this->hasOne(User::class,'id','master_user')->withTrashed();
    }

    //Get Activities
    public function getActivities(){
        return $this->hasMany(Activity::class, 'client_id', 'id');
    }
}
