<?php

namespace App\Models\MasterUser;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class MedicineSchedule extends Model
{
    use HasFactory;
    use SoftDeletes;
    
    protected $fillable = [
        'medicine_id',
        'date',
        'complete_details',
        'status',
        'scheduled_time'
    ];

    public function getMedName(){
        return $this->hasOne(Medicine::class, 'id', 'medicine_id')->withTrashed();
    }
}
