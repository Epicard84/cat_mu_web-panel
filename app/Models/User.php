<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\MasterUser\Client;
class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'city',
        'country',
        'state',
        'password',
        'phone_number',
        'permanent_address',
        'mailing_address',
        'facility_address',
        'status',
        'profile_image_name',
        'company_name',
        'zip_code',
        'city',
        'user_type',
        'master_user',
        'fax_number',
        'home_phone_number',
        'total_beds',
        'operators_license',
        'tb_clearance',
        'physical_checkup',
        'misc_doc'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    

    public function getClents(){
        return $this->belongsTo(Client::class,);
    }

    //For SuperAdmin panel tables
    public function MasterDetails(){
        return $this->hasOne(User::class,'id','master_user')->withTrashed();
    }
}   
