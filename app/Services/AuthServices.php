<?php

namespace App\Services;

use App\Models\MasterUser\Membership;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class AuthServices{
//Login
public static function UserLogin($request)
{
    try {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $error =  $validator->errors();
            return ['error' => $error];
        } else {

            $credentials = [
                'email' => $request->email,
                'password' => $request->password,
            ];

            if (Auth::attempt($credentials)) {
                if (Auth::user()->user_type != 1) {
                    Auth::logout();
                    return ['error' => 'not authorized'];
                } else {
                    $membership_details = Membership::where('user_id', Auth::user()->id)->first();
                    if ($membership_details) {
                        if ($membership_details->status == 0) {
                            Auth::logout();
                            return ['error' => 'plan expired'];
                        }
                        $token = Auth::user()->createToken('AuthToken')->plainTextToken;
                        $user_details = User::where('id', Auth::user()->id)->first();
                        $user_details->status = 'online';
                        $user_details->update();
                        return  ['success' => Auth::user(), 'token' => $token];
                    } else {
                        Auth::logout();
                        return ['error' => 'Subscrptions not found'];
                    }
                }
            } else {
                return [
                    'error' => "invaild credentials"
                ];
            }
        }
    } catch (\Throwable $th) {
        return [
            'error' => $th->getMessage()
        ];
    }
}
}