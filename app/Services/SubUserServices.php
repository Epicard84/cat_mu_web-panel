<?php

namespace App\Services;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Faker\Factory as Faker;
use App\Models\PasswordReset;
use Illuminate\Support\Facades\Mail;
use App\Mail\SubUser\PasswordResetMail;
use App\Models\MasterUser\Client;
use App\Models\MasterUser\Medicine;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

class SubUserServices
{
    //Login
    public static function subUserLogin($request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                $error =  $validator->errors();
                return ['error' => $error];
            } else {

                $credentials = [
                    'email' => $request->email,
                    'password' => $request->password,
                ];

                if (Auth::attempt($credentials)) {
                    if (Auth::user()->user_type != 2) {
                        Auth::logout();
                        return ['error' => 'not authorized'];
                    } else {
                        
                        // $membership_details = Membership::where('user_id', Auth::user()->id)->first();
                        // if ($membership_details) {
                        //     if ($membership_details->status == 0) {
                        //         Auth::logout();
                        //         return ['error' => 'plan expired'];
                        //     }
                            $token = Auth::user()->createToken('AuthToken')->plainTextToken;
                            $user_details = User::where('id', Auth::user()->id)->first();
                        //     $user_details->status = 'online';
                            $user_details->update();
                            return  ['success' => Auth::user(), 'token' => $token];
                        // } else {
                        //     Auth::logout();
                        //     return ['error' => 'Subscrptions not found'];
                        // }
                    }
                } else {
                    return [
                        'error' => "invaild credentials"
                    ];
                }
            }
        } catch (\Throwable $th) {
            return [
                'error' => $th->getMessage()
            ];
        }
    }

    //Forgot password
    public static function forgotPassword($request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                $user_data = User::where('email', $request->email)
                    ->where('user_type', 2)
                    ->first();
                if ($user_data) {
                    $faker = Faker::create();
                    $otp = $faker->numerify('####');

                    $password_reset_data = PasswordReset::all();
                    foreach ($password_reset_data as  $value) {
                        if ($value->created_at == date('Y-m-d H:i:s', strtotime("+5 min"))) {
                            $password_reset_data->delete();
                        }
                    }

                    $mail = Mail::to($request->email)->send(new PasswordResetMail($otp, $user_data));
                    if ($mail) {
                        $otp_details = PasswordReset::where('email', $request->email)->first();

                        if ($otp_details) {
                            $otp_details->otp = Hash::make($otp);
                            $otp_details->created_at = date('Y-m-d H:i:s');
                            $otp_details->update();
                        } else {

                            PasswordReset::Insert([
                                'email' => $request->email,
                                'otp' => Hash::make($otp),
                                'created_at' => date('Y-m-d H:i:s')
                            ]);
                        }
                        return ['success' => "otp send successfuly", 'email' => $request->email];
                    }
                } else {
                    return ['error' => "user not found"];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //OTP verification
    static function otpVerification($request)
    {
        try {
            $password_reset_data = PasswordReset::all();
            foreach ($password_reset_data as  $value) {
                if ($value->created_at == date('Y-m-d H:i:s', strtotime("+5 min"))) {
                    $password_reset_data->delete();
                }
            }

            $validator = Validator::make($request->all(), [
                'otp' => 'required',
                'email' => 'required|email'
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                $email = $request->email;
                $otp_details = PasswordReset::where('email', $email)->first();
                // return ['success'=>Hash::check($request->otp, $otp_details->otp)];
                if($otp_details){
                    if (Hash::check($request->otp, $otp_details->otp)) {
                        $otp_details->delete();
                        return ['success' => "OTP verified successfuly"];
                    } else {
                        return ['error' => "Invalid OTP"];
                    }
                }
                else{
                    return ['error' => "OTP has expired"];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //Create new Password
    static function newPassword($request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'password' => 'required|min:6',
                'confirm_password' => 'required|min:6|same:password',
                'email' => 'required|email'
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                $user_data = User::where('email', $request->email)->first();
                if ($user_data) {
                    $user_data->password = Hash::make($request->password);
                    if ($user_data->update()) {
                        return ['success' => "password updated successfuly"];
                    }
                } else {
                    return ['error' => "user not found"];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //SubUser Dashboard
    public static function dasboard($id){
        try {
            $subUser = User::where('id',$id)->first();
            $masterUser = $subUser->master_user;
            $clients['total_clients'] = $total_clients =  Client::where('master_user',$masterUser)->where('status',1)->count();
            $clients['details'] = Client::select('id','first_name','last_name')->where('master_user',$masterUser)->where('status',1)->get();
            // dd ($total_clients);
            // $clients = Client::select('id', 'first_name', 'last_name')->where('master_user', $id)->get();
            if ($total_clients == 0) {
                return  ['success' => 'No clients found', 'data' => []];
            } else {
                return  ['success' => 'Clients found successfully','data' => $clients];
            }
        } catch (\Throwable $th) {
            return [
                'error' => $th->getMessage()
            ];
        }
    }

    //Client Details
    public static function client_details($id){
        try{
            $client = Client::where('id',$id)->first();
            $client['medicine'] = Medicine::where('client_id',$id)->get();
            if ($client == null) {
                return  ['success' => 'No clients found', 'data' => []];
            } else {
                return  ['success' => 'Client data found successfully','data' => $client];
            }

        } catch (\Throwable $th) {
            return [
                'error' => $th->getMessage()
            ];
        }    
    }
}