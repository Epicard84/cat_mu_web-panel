<?php

namespace App\Services;

use App\Mail\PasswordResetMail;
use App\Mail\WelcomeMail;
use App\Models\Doctor;
use App\Models\Guardian;
use App\Models\MasterUser\ClientDocument;
use App\Models\MasterUser\Client;
use App\Models\MasterUser\MFD;
use App\Models\MasterUser\Medicine;
use App\Models\MasterUser\MedicineDetails;
use App\Models\MasterUser\MedicineSchedule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Models\MasterUser\Membership;
use App\Models\MasterUser\PasswordReset as MasterUserPasswordReset;
use App\Models\User;
use Faker\UniqueGenerator;
use Illuminate\Support\Facades\Mail;
use Illuminate\Validation\Rules\Unique;
use Faker\Factory as Faker;
use App\Models\PasswordReset;
use App\Models\PasswordReset as ModelsPasswordReset;
use App\Models\Record;
use DateTime;
use GuzzleHttp\Psr7\Request;
use Illuminate\Auth\Events\PasswordReset as EventsPasswordReset;
use Illuminate\Support\Facades\Hash;
use Ramsey\Uuid\Type\Integer;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;
use PhpParser\Comment\Doc;
/* use Stevebauman\Location\Facades\Location as FacadesLocation; */
use App\Models\Reminder;
use App\Models\ReminderDetail;
use App\Models\UserDocument;
use Exception;

use function PHPUnit\Framework\isNull;

class MasterUserServices
{
    //Get Live Location
    /* public static function location()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';


        $ip = $ipaddress; //Dynamic IP address 
        $location = FacadesLocation::get($ip);
        // return $location->cityName . ', ' . $location->countryName;
        return $location;
    } */



    //Master User Registration 
    public static function masterUserRegister($request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'company_name' => 'required|string',
                // 'city' => 'required|string',
                // 'country' => 'required|string',
                // 'state' => 'required|string',
                'facility_address' => 'required',
                // 'zip_code' => 'required',
                'email' => 'required|unique:users,email',
                'phone_number' => 'required|unique:users,phone_number',
                'password' => 'required',
                'total_beds' => 'required',
                'operators_license' => 'required'
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                //Profile Image
                if ($request->hasFile('profile_image')) {
                    if ($request->file('profile_image')->isValid()) {

                        $file = $request->file('profile_image');
                        $profile_image_name = uniqid() . '.' . $file->getClientOriginalExtension();
                        $request->file('profile_image')->move("images/MasterUserProfile/", $profile_image_name);
                    }
                } else {
                    $profile_image_name = null;
                }

                //Operator's License
                if ($request->hasFile('operators_license')) {
                    if ($request->file('operators_license')->isValid()) {

                        $file = $request->file('operators_license');
                        $operators_license_name = uniqid() . '.ol.' . $file->getClientOriginalExtension();
                        $request->file('operators_license')->move("images/MasterUserProfile/", $operators_license_name);
                    }
                }

                //Tb Clearance Image
                if ($request->hasFile('tb_clearance')) {
                    if ($request->file('tb_clearance')->isValid()) {

                        $file = $request->file('tb_clearance');
                        $tb_clearance_name = uniqid() . '.tbc.' . $file->getClientOriginalExtension();
                        $request->file('tb_clearance')->move("images/MasterUserProfile/", $tb_clearance_name);
                    }
                } else {
                    $tb_clearance_name = null;
                }

                //Physical Checkup Image
                if ($request->hasFile('physical_checkup')) {
                    if ($request->file('physical_checkup')->isValid()) {

                        $file = $request->file('physical_checkup');
                        $physical_checkup_name = uniqid() . '.pc.' . $file->getClientOriginalExtension();
                        $request->file('physical_checkup')->move("images/MasterUserProfile/", $physical_checkup_name);
                    }
                } else {
                    $physical_checkup_name = null;
                }

                 //Miscellaneous Document Image
                 if ($request->hasFile('misc_doc')) {
                    if ($request->file('misc_doc')->isValid()) {

                        $file = $request->file('misc_doc');
                        $misc_doc_name = uniqid() . '.md.' . $file->getClientOriginalExtension();
                        $request->file('misc_doc')->move("images/MasterUserProfile/", $misc_doc_name);
                    }
                } else {
                    $misc_doc_name = null;
                }

                $user = User::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'email' => $request->email,
                    'phone_number' => $request->phone_number,
                    'company_name' => $request->company_name,
                    // 'zip_code' => $request->zip_code,
                    // 'city' => $request->city,
                    // 'country' => $request->country,
                    // 'state' => $request->state,
                    'status' => 'online',
                    // 'permanent_address' => $request->permanent_address,
                    'facility_address' => $request->facility_address,
                    'profile_image_name' => $profile_image_name,
                    'operators_license' => $operators_license_name,
                    'tb_clearance' => $tb_clearance_name,
                    'physical_checkup' => $physical_checkup_name,
                    'misc_doc' => $misc_doc_name,
                    'password' => Hash::make($request->password),
                    'user_type' => 1,
                    'fax_number' => $request->fax_number,
                    'home_phone_number' => $request->home_phone_number,
                    'total_beds' => $request->total_beds,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

                if ($user) {
                    Mail::to($request->email)
                        ->send(new WelcomeMail($user, $request->password));
                    return ['success', 'data' => $user];
                    return ['success', 'data' => $user];
                }
            }
        } catch (\Throwable $th) {
            // ($th->getMessage());
            return ['exception' => $th->getMessage()];
        }
    }

    //Master User Details
    public static function userDetails()
    {
        try {
            $user = User::where('id', Auth::user()->id)->first();

            if ($user) {
                $user->profile_image_name = $user->profile_image_name;
                $user->archive_clients = Client::select('first_name', 'last_name')->where('master_user', Auth::user()->id)->where('status', 0)->get();
                return ['success' => "master user found", 'data' => $user];
            } else {
                return ['error' => "no data found"];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //Edit Master USer
    public static function editMasterUser($request)
    {
        try {
            $user = User::where('id', $request->id)->first();

            if ($user->phone_number == $request->phone_number) {
                $phone_number = 'required|numeric';
            } else {
                $phone_number = 'required|numeric|unique:users,phone_number';
            }

            $validator = Validator::make($request->all(), [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'company_name' => 'required|string',
                // 'zip_code' => 'required',
                'phone_number' => $phone_number,
                // 'permanent_address' => 'required|string',
                'facility_address' => 'required|string',
                // 'city' => 'required|string',
                // 'state' => 'required|string',
                // 'country' => 'required|string',
            ]);


            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                if ($request->hasFile('image')) {
                    // if ($request->file('image')->isValid()) {

                        if (is_null($user->profile_image_name)) {
                            $file = $request->file('image');
                            $profile_image_name = uniqid() . '.' . $file->getClientOriginalExtension();
                            $request->file('image')->move("images/MasterUserProfile/", $profile_image_name);
                        } else {

                            $imagePath = public_path('images/MasterUserProfile/' . $user->profile_image_name);
                            if (File::exists($imagePath)) {

                                unlink($imagePath);
                            }
                            $file = $request->file('image');
                            $profile_image_name = uniqid() . '.' . $file->getClientOriginalExtension();
                            $request->file('image')->move("images/MasterUserProfile/", $profile_image_name);
                        }
                    // }
                } else {
                    if ($user->profile_image_name) {
                        $profile_image_name = $user->profile_image_name;
                    } else {
                    $profile_image_name = null;
                    }
                }

                if ($request->hasfile('operatorLicense')) {
                    if ($user->operators_license) {

                        $imagePath = public_path('images/MasterUserProfile/' . $user->operators_license);
                        if (File::exists($imagePath)) {

                            unlink($imagePath);
                        }
                    }
                    $file = $request->file('operatorLicense');
                    $operators_license_name = uniqid() . '.ol.' . $file->getClientOriginalExtension();
                    $request->file('operatorLicense')->move("images/MasterUserProfile/", $operators_license_name);
                } else {
                    if ($user->operators_license) {
                        $operators_license_name = $user->operators_license;
                    } else {
                        $operators_license_name = null;
                    }
                }

                if ($request->hasfile('tbClearance')) {
                    if ($user->tb_clearance) {

                        $imagePath = public_path('images/MasterUserProfile/' . $user->tb_clearance);
                        if (File::exists($imagePath)) {

                            unlink($imagePath);
                        }
                    }
                    $file = $request->file('tbClearance');
                    $tb_clearance_name = uniqid() . '.ol.' . $file->getClientOriginalExtension();
                    $request->file('tbClearance')->move("images/MasterUserProfile/", $tb_clearance_name);
                } else {
                    if ($user->tb_clearance) {
                        $tb_clearance_name = $user->tb_clearance;
                    } else {
                        $tb_clearance_name = null;
                    }
                }

                if ($request->hasfile('physicalCheckup')) {
                    if ($user->physical_checkup) {

                        $imagePath = public_path('images/MasterUserProfile/' . $user->physical_checkup);
                        if (File::exists($imagePath)) {

                            unlink($imagePath);
                        }
                    }
                    $file = $request->file('physicalCheckup');
                    $physical_checkup_name = uniqid() . '.ol.' . $file->getClientOriginalExtension();
                    $request->file('physicalCheckup')->move("images/MasterUserProfile/", $physical_checkup_name);
                } else {
                    if ($user->physical_checkup) {
                        $physical_checkup_name = $user->physical_checkup;
                    } else {
                        $physical_checkup_name = null;
                    }
                }
                if ($request->hasfile('misc_doc')) {
                    if ($user->misc_doc) {

                        $imagePath = public_path('images/MasterUserProfile/' . $user->misc_doc);
                        if (File::exists($imagePath)) {

                            unlink($imagePath);
                        }
                    }
                    $file = $request->file('misc_doc');
                    $misc_doc_name = uniqid() . '.md.' . $file->getClientOriginalExtension();
                    $request->file('misc_doc')->move("images/MasterUserProfile/", $misc_doc_name);
                } else {
                    if ($user->misc_doc) {
                        $misc_doc_name = $user->misc_doc;
                    } else {
                        $misc_doc_name = null;
                    }
                }

                $user->first_name = $request->first_name;
                $user->last_name = $request->last_name;
                $user->company_name = $request->company_name;
                $user->zip_code = $request->zip_code;
                $user->phone_number = $request->phone_number;
                $user->permanent_address = $request->permanent_address;
                $user->facility_address = $request->facility_address;
                $user->city = $request->city;
                $user->state = $request->state;
                $user->country = $request->country;
                $user->profile_image_name = $profile_image_name;
                $user->operators_license = $operators_license_name;
                $user->tb_clearance = $tb_clearance_name;
                $user->physical_checkup = $physical_checkup_name;
                $user->misc_doc = $misc_doc_name;

                if ($user->update()) {
                    return ['success' => "master user updated successfuly", 'data' => $user];
                }
            }
        } catch (\Throwable $th) {

            return ['error' => $th->getMessage()];
        }
    }
    //Login
    public static function masterUserLogin($request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email',
                'password' => 'required'
            ]);

            if ($validator->fails()) {
                $error =  $validator->errors();
                return ['error' => $error];
            } else {

                $credentials = [
                    'email' => $request->email,
                    'password' => $request->password,
                ];

                if (Auth::attempt($credentials)) {
                    if (Auth::user()->user_type != 1) {
                        Auth::logout();
                        return ['error' => 'not authorized'];
                    } else {
                        $membership_details = Membership::where('user_id', Auth::user()->id)->first();
                        // if ($membership_details) {
                        // if ($membership_details->status == 0) {
                        //     Auth::logout();
                        //     return ['error' => 'plan expired'];
                        // }
                        $token = Auth::user()->createToken('AuthToken')->plainTextToken;
                        $user_details = User::where('id', Auth::user()->id)->first();
                        $user_details->status = 'online';
                        $user_details->update();
                        return  ['success' => Auth::user(), 'token' => $token];
                        // } else {
                        //     Auth::logout();
                        //     return ['error' => 'Subscrptions not found'];
                        // }
                    }
                } else {
                    $blockeduser = User::where('email', $request->email)->withTrashed()->first();
                    if (empty($blockeduser->deleted_at)) {
                        return [
                            'error' => "Invaild credentials"
                        ];
                    } else {
                        return [
                            'error' => "Your account has been blocked. Please contact with Admin"
                        ];
                    }
                }
            }
        } catch (\Throwable $th) {
            return [
                'error' => $th->getMessage()
            ];
        }
    }

    public static function dashboard($id)
    {
        try {
            $clients = Client::select('id', 'first_name', 'last_name')->where('master_user', $id)->get();
            if (is_null($clients)) {
                return  ['success' => 'clients not found', 'data' => []];
            } else {
                return  ['success' => 'clients found successfully', 'data' => $clients];
            }
        } catch (\Throwable $th) {
            return [
                'error' => $th->getMessage()
            ];
        }
    }


    //Forgot password
    public static function forgotPassword($request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'email' => 'required|email'
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                $user_data = User::where('email', $request->email)
                    ->where('user_type', 1)
                    ->first();


                if ($user_data) {
                    $faker = Faker::create();
                    $otp = $faker->numerify('####');

                    $password_reset_data = PasswordReset::all();
                    foreach ($password_reset_data as  $value) {
                        if ($value->created_at == date('Y-m-d H:i:s', strtotime("+5 min"))) {
                            $password_reset_data->delete();
                        }
                    }

                    $mail = Mail::to($request->email)->send(new PasswordResetMail($otp, $user_data));

                    if ($mail) {
                        $otp_details = PasswordReset::where('email', $request->email)->first();

                        if ($otp_details) {
                            $otp_details->otp = Hash::make($otp);
                            $otp_details->created_at = date('Y-m-d H:i:s');
                            $otp_details->update();
                        } else {

                            PasswordReset::Insert([
                                'email' => $request->email,
                                'otp' => Hash::make($otp),
                                'created_at' => date('Y-m-d H:i:s')
                            ]);
                        }
                        return ['success' => "otp send successfuly", 'email' => $request->email, 'otp' => $otp];
                    }
                } else {
                    return ['error' => "user not found"];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //OTP verification
    static function otpVerification($request)
    {
        try {
            $password_reset_data = PasswordReset::all();
            foreach ($password_reset_data as  $value) {
                if ($value->created_at == date('Y-m-d H:i:s', strtotime("+5 min"))) {
                    $password_reset_data->delete();
                }
            }

            $validator = Validator::make($request->all(), [
                'otp' => 'required',
                'email' => 'required|email'
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                $email = $request->email;
                $otp_details = PasswordReset::where('email', $email)->first();
                // return ['success'=>Hash::check($request->otp, $otp_details->otp)];
                if (Hash::check($request->otp, $otp_details->otp)) {
                    $otp_details->delete();
                    return ['success' => "otp verified successfuly"];
                } else {
                    return ['error' => "Invalid OTP"];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //Create new Password
    static function newPassword($request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'password' => 'required|min:6',
                'confirm_password' => 'required|min:6|same:password',
                'email' => 'required|email'
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                $user_data = User::where('email', $request->email)->first();
                if ($user_data) {
                    $user_data->password = Hash::make($request->password);
                    if ($user_data->update()) {
                        return ['success' => "password updated successfuly"];
                    }
                } else {
                    return ['error' => "user not found"];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //Update password
    static function updatePassword($request)
    {

        try {
            $validator = Validator::make($request->all(), [
                'old_password' => 'required',
                'password' => 'required|min:6',
                'confirm_password' => 'required|min:6|same:password',
            ]);
            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                $data = User::where('id', Auth::user()->id)->first();



                if (Hash::check($request->old_password, $data->password)) {
                    $data->password = Hash::make($request->password);


                    if ($data->update()) {
                        return ['success' => "Password updated successfully"];
                    }
                } else {

                    return ['wrong_error' => "*Please enter correct Old Password"];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }





    //Create new client

    static function clientRegister($request)
    {

        try {

            $count = Client::where('master_user', Auth::user()->id)->where('status', 1)->count();
            if ($count < 5) {
                $validator = Validator::make($request->all(), [
                    'first_name' => 'required|string',
                    'last_name' => 'required|string',
                    'dob' => 'required|date',
                    'gender' => 'required|string',
                ]);

                if ($validator->fails()) {
                    return ['error' => $validator->errors()];
                } else {
                    $date  = date('Y-m-d H:i:s');
                    if ($request->hasfile('documents')) {
                        $files = $request->file('documents');
                        $count = 0;

                        foreach ($files as $file) {
                            $file_extention = $file->getClientOriginalExtension();

                            if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                            } else {
                                $count++;
                            }
                        }

                        if ($count ==  0) {
                            foreach ($files as $file) {
                                $name =  preg_replace('/\s+/', '', $request->first_name) . uniqid() . '.' . $file->getClientOriginalExtension();
                                $file->move(public_path() . '/documents/Client/', $name);
                                $client_docs[] = $name;
                            }
                            $client_docs = implode(',', $client_docs);
                        } else {
                            return ['error' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                        }
                    } else {
                        $client_docs = null;
                    }
                    $client =  Client::create([
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'dob' => $request->dob,
                        'gender' => $request->gender,
                        'additional_misc_information' => $request->additional_misc_information,
                        'status' => 1,
                        'documents' => $client_docs,
                        'master_user' => Auth::user()->id,
                        'created_at' => $date
                    ]);

                    $doctor = json_decode($request->doctor, true);

                    foreach ($doctor as  $key => $doctor) {

                        $doctor = Doctor::create([
                            'client_id' => $client->id,
                            'doctor_name' => $doctor['name'],
                            'doctor_phone_number' => $doctor['phone_number'],
                            'doctor_address' => $doctor['address']
                        ]);
                    }
                    $guardian = json_decode($request->guardian, true);


                    foreach ($guardian as  $key => $guardian) {

                        $Guardian = Guardian::create([
                            'client_id' => $client->id,
                            'guardian_name' => $guardian['name'],
                            'guardian_phone_number' => $guardian['phone_number'],
                            'guardian_address' => $guardian['address']
                        ]);
                    }
                    $guardian = Guardian::where('client_id', $client->id)->get();
                    $doctor = Doctor::where('client_id', $client->id)->get();

                    $client['doctor'] = $doctor;
                    $client['guardian'] = $guardian;

                    return ['success' => "Client created successfuly", 'data' => $client];
                }
            } else {
                return ['error' => "you can create maximum 5 clients"];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }



    public static function clientMedicineSchedule($id)
    {
        try {
            $medicine  =  Medicine::where('client_id', $id)->get();
            $client_details = Client::select('first_name', 'last_name')->where('id', $id)->first();
            $subuser_id = Client::select('sub_user')->where('id', $id)->first();
            $subuser_details = User::select('first_name', 'last_name')->where('id', $subuser_id->sub_user)->first();

            foreach ($medicine as $value) {
                $medicaine_schedule = MedicineSchedule::where('medicine_id', $value->id)->get();
                $data = [];

                foreach ($medicaine_schedule as $val) {
                    $data[] = [
                        'id' => $val->id,
                        'date' => $val->date,
                        'times' => $value->time_days,
                        'status' => $val->status,
                        'complete_status' => $val->complete_details
                    ];
                }

                $value->medicine_data = $data;
            }

            if (is_null($medicine)) {
                return ['success' => "clients not found", 'data' => []];
            } else {
                $data = [
                    $medicine,
                    'client_details' => $client_details,
                    'subuser_details' => ['name' => strtoupper(substr($subuser_details->first_name, 0, 1)) . strtoupper(substr($subuser_details->last_name, 0, 1))]
                ];

                return ['success' => "clients found successfuly", 'data' => $data];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }


    public static function updateMedicineSchedule($request)
    {

        try {

            $medicaine_schedule = MedicineSchedule::where('id', $request->medicine_id)->first();
            $medicaine_schedule->status = 1;
            $medicaine_schedule->complete_details = implode(',', $request->compelete_details);

            if ($medicaine_schedule->update()) {

                return ['success' => "medicine schedule updated successfuly", 'data' => $medicaine_schedule];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //notes details 
    public static function notesDetails($id)
    {
        try {
            $notes_details = MedicineDetails::where('client_id', $id)->where('type', 0)->get();
            $subuser_id = Client::where('id', $id)->first();
            $subuser_details = User::where('id', $subuser_id->sub_user)->first();
            $subuser_name = strtoupper(substr($subuser_details->first_name, 0, 1)) . strtoupper(substr($subuser_details->last_name, 0, 1));



            $data = [
                'notes_details' => $notes_details,
                'subusername' => $subuser_name
            ];
            return ['success' => "notes found successfuly", 'data' => $data];
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //chats details 
    public static function chatsDetails($id)
    {
        try {
            $chats_details = MedicineDetails::where('client_id', $id)->where('type', 1)->get();
            $subuser_id = Client::where('id', $id)->first();
            $subuser_details = User::where('id', $subuser_id->sub_user)->first();
            $subuser_name = strtoupper(substr($subuser_details->first_name, 0, 1)) . strtoupper(substr($subuser_details->last_name, 0, 1));



            $data = [
                'chats_details' => $chats_details,
                'subusername' => $subuser_name
            ];
            return ['success' => "chats found successfuly", 'data' => $data];
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //narrates details 
    public static function narratesDetails($id)
    {
        try {
            $narrates_details = MedicineDetails::where('client_id', $id)->where('type', 2)->get();
            $subuser_id = Client::where('id', $id)->first();
            $subuser_details = User::where('id', $subuser_id->sub_user)->first();
            $subuser_name = strtoupper(substr($subuser_details->first_name, 0, 1)) . strtoupper(substr($subuser_details->last_name, 0, 1));



            $data = [
                'narrates_details' => $narrates_details,
                'subusername' => $subuser_name
            ];
            return ['success' => "narrates found successfuly", 'data' => $data];
        } catch (\Throwable $th) {
            //throw $th;
        }
    }

    //create add medicine for client

    public static function addNewMedicine($request)
    {
        try {

            //Medicine Creation 

            $medicine_docs[] = "";
            if ($request->hasFile('docs')) {
                $files = $request->file('docs');
                foreach ($files as $key => $file) {
                    $image = $request->medicine_name . uniqid() . "." . $file->getClientOriginalExtension();
                    $file->move(public_path('documents/Medicine_Docs/'), $image);
                    $medicine_docs[] = $image;
                }
            }

            $medicine = Medicine::create([
                'client_id' => $request->client_id,
                'medicine_name' => $request->medicine_name,
                'start_date' => $request->start_date,
                'end_date' => $request->start_date,
                'time_days' => $request->time_days,
                'status' => 0,
                'docs' => implode(',', $medicine_docs),
                'descriptions' => $request->description
            ]);


            if ($medicine) {

                $medicine_data = Medicine::where('id', $medicine->id)->first();

                // get from and using date

                $earlier = new DateTime($medicine_data->start_date);
                $later = new DateTime($medicine_data->end_date);

                $diff = (int) $later->diff($earlier)->format("%a"); //3

                $diff = $diff + 1;

                $date  = date('Y-m-d', strtotime($medicine_data->start_date . ' + 1 days'));
                for ($i = 0; $i < $diff; $i++) {
                    if ($i == 0) {
                        $date = $medicine_data->start_date;
                    }

                    MedicineSchedule::create([
                        'medicine_id' => $medicine_data->id,
                        'date' => $date,
                        'status' => 0
                    ]);

                    $date  = date('Y-m-d', strtotime($date . ' + 1 days'));
                }


                return ['success' => "medicine created successfuly"];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //Get Client all Medicine Details
    public static function clientMedicineDetails($id)
    {
        try {
            $medicine  =  Medicine::where('client_id', $id)->get();

            if (is_null($medicine)) {
                return ['success' => "clients not found", 'data' => []];
            } else {

                foreach ($medicine as $key => $value) {
                    $v = [];
                    foreach (explode(',', $value->docs) as  $val) {
                        $v[] = public_path('docs/Medicine_Docs/') . $val;
                    }
                    $value->docs = implode(',', $v);
                }

                return ['success' => "clients found successfuly", 'data' => $medicine];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //Client Medicine Delete
    public static function medicineDelete($id)
    {
        try {
            $medicine_details = Medicine::where('id', $id)->first();
            if ($medicine_details) {
                // foreach(explode(',',$medicine_details->docs) as $value){
                //     $image_path = public_path('docs/Medicine_Docs/').$value;
                //     if (File::exists($image_path)) {
                //         unlink($image_path);
                //     }
                // }
                $medicaine_schedule = MedicineSchedule::where('medicine_id', $medicine_details->id)->delete();
                $medicine_details->delete();
                if ($medicaine_schedule) {
                    return ['success' => "medicine deleted successfuly"];
                }
            } else {
                return ['error' => "No medicine found"];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    // //client archive
    // public static function clientArchive($request)
    // {
    //     $count = 0;
    //     try {
    //         foreach ($request->clients_id as $key => $value) {
    //             $client = Client::find($value);
    //             $client->status = 0;
    //             $client->update();
    //             $count = (int)$count + 1;
    //         }
    //         if ($count > 0) {
    //             return ['success' => "clients archive successfuly"];
    //         }
    //     } catch (\Throwable $th) {
    //         return ['error' => $th->getMessage()];
    //     }
    // }

    public static function archiveClients($request)
    {
        try {
            $archive_client = Client::where('master_user', $request)->onlyTrashed()->orderByDesc('id')->get();
            $count = Client::where('master_user', $request)->onlyTrashed()->count();
            if ($count >  0) {
                return ['success' => "archive clients found successfuly", 'data' => $archive_client];
            } else {
                return ['error' => 'no clients found'];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }


    //create add MDF 
    public static function createMfd($request)
    {
        try {

            $validator = Validator::make($request->all(), [
                'date' => 'required',
                'message' => 'required',
            ]);
            if ($validator->fails()) {
                return ['error' => 'Details & Date required to proceed.'];
            } else {
                $date = date('m-d-Y', strtotime($request->date));
                $time = date('Hi') . "hrs";
                MFD::create([
                    'master_user_id' => Auth::user()->id,
                    'date' => $date . " " . $time,
                    'message' => $request->message
                ]);

                Reminder::mfdReminder(Auth::user()->id, $request->message);

                return ['success' => "MFD created successfuly"];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }


    //Create Subuser
    public static function subUserRegister($request)
    {
        try {
            //code...
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                // 'company_name' => 'required|string',
                'email' => 'required|email|unique:users',
                // 'phone_number' => 'required|unique:users',
                'password' => 'required|same:confirm_password|min:4|max:4',
                // 'address' => 'required|string',
                // 'mailing_address' => 'required|string',
                // 'city' => 'required|string',
                // 'state' => 'required|string',
                // 'country' => 'required|string',
                // 'image' => 'required'
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {
                $subUsers = User::where('master_user', Auth::user()->id)->get();
                // dd($subUsers);
                $count = 0;
                foreach ($subUsers as $s) {
                    if (Hash::check($request->password, $s->password)) {
                        $count++;
                    }
                }
                if ($count == 0) {
                    if ($request->hasFile('profile_image')) {
                        $image = uniqid() . "." . $request->file('profile_image')->getClientOriginalExtension();
                        $request->file('profile_image')->move(public_path('images/SubUserProfile/'), $image);
                    } else {
                        $image = null;
                    }
                    $document = null;
                    if ($request->hasfile('document')) {
                        $files = $request->file('document');
                        $count = 0;

                        foreach ($files as $file) {
                            $file_extention = $file->getClientOriginalExtension();

                            if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                            } else {
                                $count++;
                            }
                        }

                        if ($count ==  0) {
                            foreach ($files as $file) {
                                $name =  $request->first_name . '_' . $request->last_name . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                                $file->move(public_path('/documents/SubUser_Docs/'), $name);
                                $document[] = $name;
                            }
                            $document = implode(',', $document);
                        } else {
                            return ['documenterror' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                        }
                    }

                    //Operator's License
                if ($request->hasFile('operators_license')) {
                    if ($request->file('operators_license')->isValid()) {

                        $file = $request->file('operators_license');
                        $operators_license_name = uniqid() . '.ol.' . $file->getClientOriginalExtension();
                        $request->file('operators_license')->move("images/SubUserProfile/", $operators_license_name);
                    }
                } else {
                    $operators_license_name = null;
                }

                //Tb Clearance Image
                if ($request->hasFile('tb_clearance')) {
                    if ($request->file('tb_clearance')->isValid()) {

                        $file = $request->file('tb_clearance');
                        $tb_clearance_name = uniqid() . '.tbc.' . $file->getClientOriginalExtension();
                        $request->file('tb_clearance')->move("images/SubUserProfile/", $tb_clearance_name);
                    }
                } else {
                    $tb_clearance_name = null;
                }

                //Physical Checkup Image
                if ($request->hasFile('physical_checkup')) {
                    if ($request->file('physical_checkup')->isValid()) {

                        $file = $request->file('physical_checkup');
                        $physical_checkup_name = uniqid() . '.pc.' . $file->getClientOriginalExtension();
                        $request->file('physical_checkup')->move("images/SubUserProfile/", $physical_checkup_name);
                    }
                } else {
                    $physical_checkup_name = null;
                }

                 //Miscellaneous Document Image
                 if ($request->hasFile('misc_doc')) {
                    if ($request->file('misc_doc')->isValid()) {

                        $file = $request->file('misc_doc');
                        $misc_doc_name = uniqid() . '.md.' . $file->getClientOriginalExtension();
                        $request->file('misc_doc')->move("images/SubUserProfile/", $misc_doc_name);
                    }
                } else {
                    $misc_doc_name = null;
                }

                    $date = date('Y-m-d H:i:s');
                    $user = User::create([
                        'first_name' => $request->first_name,
                        'last_name' => $request->last_name,
                        'company_name' => $request->company_name,
                        'email' => $request->email,
                        'phone_number' => $request->phone_number,
                        'password' => Hash::make($request->password),
                        'permanent_address' => $request->address,
                        'mailing_address' => $request->mailing_address,
                        // 'city' => $request->city,
                        // 'state' => $request->state,
                        // 'country' => $request->country,
                        'master_user' => Auth::user()->id,
                        'user_type' => 2,
                        'profile_image_name' => $image,
                        'operators_license' => $operators_license_name,
                        'tb_clearance' => $tb_clearance_name,
                        'physical_checkup' => $physical_checkup_name,
                        'misc_doc' => $misc_doc_name,
                        'created_at' => $date,
                    ]);
                    $docs = UserDocument::create([
                        'user_id' => $user->id,
                        'document' => $document,
                    ]);

                    // $user_details = User::where('created_at', $date)->first();
                    try {
                        Mail::to($request->email)
                            ->cc(Auth::user()->email)
                            ->send(new WelcomeMail($user, $request->password));
                    } catch (Exception $e) {
                        if ($e->getMessage()) {
                            return ['success' => "Subuser created successfuly", 'data' => $user];
                        }
                    }
                    return ['success' => "Subuser created successfuly", 'data' => $user];
                } else {
                    return ['passcodeerror' => " Passcode is not available! Please try with another passcode"];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    //Edit Sub User
    public static function editSubUser($request)
    {
        try {
            $user_details = User::where('id', $request->id)->first();

            if ($user_details->phone_number == $request->phone_number) {

                $phone_number = 'required';
            } else {
                $phone_number = 'required|unique:users,phone_number';
            }
            $validator = Validator::make($request->all(), [
                // 'phone_number' => $phone_number,
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                // 'company_name' => 'required',
                // 'city' => 'required|string',
                // 'state' => 'required|string',
                // 'country' => 'required|string',
                // 'address' => 'required|string',
                // 'mailing_address' => 'required|string',
            ]);

            if ($validator->fails()) {
                return ['error' => $validator->errors()];
            } else {

                if ($request->hasFile('profile_image')) {
                    $image = uniqid() . "." . $request->file('profile_image')->getClientOriginalExtension();
                    if ($user_details->profile_image_name) {
                        unlink(public_path('images/SubUserProfile/' . $user_details->profile_image_name));
                    }
                    $request->file('profile_image')->move(public_path('images/SubUserProfile/'), $image);
                } else {
                    if ($user_details->profile_image_name) {
                        $image = $user_details->profile_image_name;
                    } else {
                        $image = null;
                    }
                }

                if ($request->hasfile('operatorLicense')) {
                    if ($user_details->operators_license) {

                        $imagePath = public_path('images/SubUserProfile/' . $user_details->operators_license);
                        if (File::exists($imagePath)) {

                            unlink($imagePath);
                        }
                    }
                    $file = $request->file('operatorLicense');
                    $operators_license_name = uniqid() . '.ol.' . $file->getClientOriginalExtension();
                    $request->file('operatorLicense')->move("images/SubUserProfile/", $operators_license_name);
                } else {
                    if ($user_details->operators_license) {
                        $operators_license_name = $user_details->operators_license;
                    } else {
                        $operators_license_name = null;
                    }
                }

                if ($request->hasfile('tbClearance')) {
                    if ($user_details->tb_clearance) {

                        $imagePath = public_path('images/SubUserProfile/' . $user_details->tb_clearance);
                        if (File::exists($imagePath)) {

                            unlink($imagePath);
                        }
                    }
                    $file = $request->file('tbClearance');
                    $tb_clearance_name = uniqid() . '.ol.' . $file->getClientOriginalExtension();
                    $request->file('tbClearance')->move("images/SubUserProfile/", $tb_clearance_name);
                } else {
                    if ($user_details->tb_clearance) {
                        $tb_clearance_name = $user_details->tb_clearance;
                    } else {
                        $tb_clearance_name = null;
                    }
                }

                if ($request->hasfile('physicalCheckup')) {
                    if ($user_details->physical_checkup) {

                        $imagePath = public_path('images/SubUserProfile/' . $user_details->physical_checkup);
                        if (File::exists($imagePath)) {

                            unlink($imagePath);
                        }
                    }
                    $file = $request->file('physicalCheckup');
                    $physical_checkup_name = uniqid() . '.ol.' . $file->getClientOriginalExtension();
                    $request->file('physicalCheckup')->move("images/SubUserProfile/", $physical_checkup_name);
                } else {
                    if ($user_details->physical_checkup) {
                        $physical_checkup_name = $user_details->physical_checkup;
                    } else {
                        $physical_checkup_name = null;
                    }
                }
                if ($request->hasfile('misc_doc')) {
                    if ($user_details->misc_doc) {

                        $imagePath = public_path('images/SubUserProfile/' . $user_details->misc_doc);
                        if (File::exists($imagePath)) {

                            unlink($imagePath);
                        }
                    }
                    $file = $request->file('misc_doc');
                    $misc_doc_name = uniqid() . '.md.' . $file->getClientOriginalExtension();
                    $request->file('misc_doc')->move("images/SubUserProfile/", $misc_doc_name);
                } else {
                    if ($user_details->misc_doc) {
                        $misc_doc_name = $user_details->misc_doc;
                    } else {
                        $misc_doc_name = null;
                    }
                }

                $user_details->first_name = $request->first_name;
                $user_details->last_name = $request->last_name;
                $user_details->company_name = $request->company_name;
                $user_details->phone_number = $request->phone_number;
                $user_details->permanent_address = $request->address;
                $user_details->mailing_address = $request->mailing_address;
                $user_details->city = $request->city;
                $user_details->state = $request->state;
                $user_details->status = 'online';
                $user_details->country = $request->country;
                $user_details->profile_image_name = $image;
                $user_details->operators_license = $operators_license_name;
                $user_details->tb_clearance = $tb_clearance_name;
                $user_details->physical_checkup = $physical_checkup_name;
                $user_details->misc_doc = $misc_doc_name;
                $user_details->updated_at = date('Y-m-d H:i:s');

                if ($user_details->update()) {

                    return ['success' => 'SubUser Updated Successfully'];
                }
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }






    public static function changeUserDetails($id)
    {
        try {
            $client = Client::where('id', $id)->first();
            $all_sub_users = User::select('first_name', 'last_name', 'id')->where('master_user', Auth::user()->id)->get();
            $sub_user = User::select('first_name', 'last_name', 'profile_image_name')->where('id', $client->sub_user)->first();
            if ($sub_user) {
                $sub_user['profile_image_name'] =  public_path('images/SubUserProfile/') . $sub_user->profile_image_name;
                $data['sub_user'] = $sub_user;
                $data['all_sub_user'] = $all_sub_users;
                return ['success' => "sub user found", 'data' => $data];
            } else {

                return ['success' => "sub user not found", 'data' => $sub_user];
            }
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }

    public static function updateSubUserDetails($request)
    {
        try {
            return ['success' => "sub user not found"];
        } catch (\Throwable $th) {
            return ['error' => $th->getMessage()];
        }
    }
}
