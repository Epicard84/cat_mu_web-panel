<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Mail\PasswordResetMail;
use App\Models\MasterUser\Client;
use App\Models\PasswordReset;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\MasterUserServices;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Stevebauman\Location\Facades\Location as FacadesLocation;
use App\Models\Location;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{

    public function location()
    {
        $ipaddress = '';
        if (isset($_SERVER['HTTP_CLIENT_IP']))
            $ipaddress = $_SERVER['HTTP_CLIENT_IP'];
        else if (isset($_SERVER['HTTP_X_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_X_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_X_FORWARDED'];
        else if (isset($_SERVER['HTTP_FORWARDED_FOR']))
            $ipaddress = $_SERVER['HTTP_FORWARDED_FOR'];
        else if (isset($_SERVER['HTTP_FORWARDED']))
            $ipaddress = $_SERVER['HTTP_FORWARDED'];
        else if (isset($_SERVER['REMOTE_ADDR']))
            $ipaddress = $_SERVER['REMOTE_ADDR'];
        else
            $ipaddress = 'UNKNOWN';


        $ip = $ipaddress; //Dynamic IP address
        $location = FacadesLocation::get($ip);
        return $location;
    }

    //MasterUser Signup
    public function signUp()
    {
        return view('signup');
    }

    //Master Register
    public function userRegister(Request $request)
    {
        $resposne = MasterUserServices::masterUserRegister($request);

        // dd($resposne);
        if (array_key_exists('error', $resposne)) {
            return back()->with(['error' => $resposne['error']]);
        } else {
            Session::flash('success', 'User Created successfully');
            return redirect('/');
        }
    }



    //User Login page
    public function userLogin()
    {
        return view('login');
    }

    //User Login Authentication
    public function userloginAuth(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            $array = json_decode(json_encode($errors), true);

            return back()->with(['errors' => $array]);
            // if(array_key_exists('email', $array)){
            //     dd($array['email'][0]);
            // }
        } else {

            $credentials = [
                'email' => $request->email,
                'password' => $request->password,
            ];

            if (Auth::attempt($credentials)) {
                if (Auth::user()->user_type == "1") {

                    // $current_location = $this->location();
                    // $user_location = Location::where("user_id", Auth::user()->id)->first();

                    // if (!empty($user_location)) {
                    //     // dd($user_location->location);
                    //     $user_location->location = $current_location->cityName . ', ' . $current_location->countryName;
                    //     $user_location->latitude = $current_location->latitude;
                    //     $user_location->longitude = $current_location->longitude;
                    //     // $user_location->location = "NA";
                    //     $user_location->update();
                    // } else {
                    //     // dd("Hi");
                    //     Location::create([
                    //         'user_id' => Auth::user()->id,
                    //         'location' => $current_location->cityName . ', ' . $current_location->countryName,
                    //         'latitude' => $current_location->latitude,
                    //         'longitude' => $current_location->longitude,
                    //         'technical_status' => 'online',
                    //         'created_at' => Carbon::now()
                    //     ]);
                    // }

                    return redirect('master_dashboard');
                } elseif (Auth::user()->user_type == 2) {
                    // Auth::logout();
                    // return back()->with(['msg' => 'You are not authorized']);
                    // $current_location = $this->location();
                    // $user_location = Location::where("user_id", Auth::user()->id)->first();

                    // if (!empty($user_location)) {
                    //     // dd($user_location->location);
                    //     $user_location->location = $current_location->cityName . ', ' . $current_location->countryName;
                    //     $user_location->latitude = $current_location->latitude;
                    //     $user_location->longitude = $current_location->longitude;
                    //     // $user_location->location = "NA";
                    //     $user_location->update();
                    // } else {
                    //     // dd("Hi");
                    //     Location::create([
                    //         'user_id' => Auth::user()->id,
                    //         'location' => $current_location->cityName . ', ' . $current_location->countryName,
                    //         'latitude' => $current_location->latitude,
                    //         'longitude' => $current_location->longitude,
                    //         'technical_status' => 'online',
                    //         'created_at' => Carbon::now()
                    //     ]);
                    // }
                    return redirect('su_dashboard');

                } else {
                    Auth::logout();
                    return back()->with(['msg' => 'You are not authorized']);
                }
            } else {
                $blockeduser = User::where('email', $request->email)->withTrashed()->first();
                if (empty($blockeduser->deleted_at)) {
                    return back()->with(['msg' => 'Wrong Credentials']);
                } else {
                    return back()->with(['msg' => 'Your account has been blocked. Please contact with Admin']);
                }
            }
        }
    }

    //Logout
    public function userLogout()
    {
        Auth::logout();
        return redirect(route('/'));
    }

    //forgot password
    public function forgotPassword()
    {
        return view('forgotpassword');
    }

    public function verifyEmail(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();

            $array = json_decode(json_encode($errors), true);

            return back()->with(['errors' => $array]);
            // if(array_key_exists('email', $array)){
            //     dd($array['email'][0]);
            // }
        } else {
            $data = User::where('email', $request->email)->first();

            if ($data) {
                if ($data->user_type == 0) {
                    return back()->with(['msg' => 'You are not authorized']);
                } elseif ($data->user_type == 1) {
                    $validator = Validator::make($request->all(), [
                        'email' => 'required|email'
                    ]);

                    if ($validator->fails()) {
                        $errors = $validator->errors();
                        $array = json_decode(json_encode($errors), true);

                        return back()->with(['errors' => $array]);
                    } else {
                        $user_data = User::where('email', $request->email)
                            ->first();
                        if ($user_data) {
                            $faker = Faker::create();
                            $otp = $faker->numerify('####');

                            $password_reset_data = PasswordReset::all();
                            foreach ($password_reset_data as  $value) {
                                if ($value->created_at == date('Y-m-d H:i:s', strtotime("+5 min"))) {
                                    $password_reset_data->delete();
                                }
                            }


                            $mail = Mail::to($request->email)->send(new PasswordResetMail($otp, $user_data));

                            if ($mail) {
                                $otp_details = PasswordReset::where('email', $request->email)->first();

                                if ($otp_details) {
                                    $otp_details->otp = Hash::make($otp);
                                    $otp_details->created_at = date('Y-m-d H:i:s');
                                    $otp_details->update();
                                } else {

                                    PasswordReset::Insert([
                                        'email' => $request->email,
                                        'otp' => Hash::make($otp),
                                        'created_at' => date('Y-m-d H:i:s')
                                    ]);
                                }
                                $email =  Crypt::encryptString($request->email);
                                return redirect('/otp/' . $email);
                            }
                        }
                    }
                } else {
                    // return redirect('su_forget_password');
                    return view("subuser_forgetpassword", ['user' => $data]);
                }
            } else {
                return back()->with(['msg' => 'No user found']);
            }
        }
    }

    public function otp($email)
    {
        $email = Crypt::decryptString($email);
        $data = PasswordReset::where('email', $email)->first();
        if ($data) {
            return view('forgotpassword2', ['email' => $email]);
        } else {
            return back()->with('msg', 'Please try again');
        }
    }

    public function verifyOtp(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'digit1' => 'required',
            'digit2' => 'required',
            'digit3' => 'required',
            'digit4' => 'required',
        ]);

        if ($validator->fails()) {


            return back()->with(['msg' => 'Please enter a vaild code']);
        } else {

            $otp_details = PasswordReset::where('email', $request->email)->first();
            $otp = $request->digit1 . $request->digit2 . $request->digit3 . $request->digit4;

            $date = $otp_details->created_at;
            $newDate = date("Y-m-d H:i:s", strtotime($date . " +5 minutes"));

            if ($newDate >= Carbon::now()) {

                if (Hash::check($otp, $otp_details->otp)) {
                    $email = Crypt::encryptString($request->email);
                    $otp_details->delete();
                    return redirect('/new_password/' . $email);
                } else {
                    // dd('wrong');
                    // $otp_details->delete();
                    return back()->with('msg', 'Please enter a vaild code');
                }
            } else {
                return redirect('/forgot_password', $request->email)->with('msg', 'Please try again.');
            }


            // return ['success'=>Hash::check($request->otp, $otp_details->otp)];
        }
    }

    public function newPassword($email)
    {
        $email = Crypt::decryptString($email);
        return view('forgotpassword3', ['email' => $email]);
    }

    public function verifyPassword(Request $request)
    {
        $email = $request->email;
        $data = User::where('email', $email)->first();
        $data->password = Hash::make($request->password);

        if ($data->update()) {
            Session::flash('password');
            return redirect('/');
        }
    }

    //SubUser Login Post
    public function suLoginPost(Request $request)
    {
        // dd($request->all());
        // if($request){

        $validator = Validator::make($request->all(), [
            'digit1' => 'required',
            'digit2' => 'required',
            'digit3' => 'required',
            'digit4' => 'required',
        ]);

        if ($validator->fails()) {
            return back()->with(['msg' => 'Please enter a vaild code']);
        } else {
            $passcode = $request->digit1 . $request->digit2 . $request->digit3 . $request->digit4;
            $subs = User::where('master_user', $request->muID)->get();
            // dd($subs);
            $muID = $request->muID;
            // dd($muID);

            $found = 0;
            foreach ($subs as $sub) {
                if ($sub->master_user == $muID && Hash::check($passcode, $sub->password)) {
                    $found++;
                    $subU = $sub->id;
                }
            }
            if ($found > 0) {
                Auth::logout();
                // dd("Hello");
                $credentials = [
                    'id' => $subU,
                    'password' => $passcode,
                ];

                if (Auth::attempt($credentials)) {

                    // dd(Auth::user());

                    // $current_location = $this->location();
                    $user_location = Location::where("user_id", Auth::user()->id)->first();

                    if ($user_location) {
                        // $user_location->location = $current_location->cityName . ', ' . $current_location->countryName;
                        $user_location->location = "Kolkata" . ', ' . "India";
                        $user_location->update();
                    } else {
                        Location::create([
                            'user_id' => Auth::user()->id,
                            'location' => "Kolkata" . ', ' . "Kolkata",
                            'technical_status' => 'online',
                            'created_at' => Carbon::now()
                        ]);
                    }

                    // return redirect('master_dashboard');
                    // dd(Auth::user());
                    return redirect()->route('su_dashboard');
                } else {
                    dd($credentials);
                }
            } else {
                return back()->with(['msg' => 'Wrong Credentials']);
            }
        }
        // }else{
        //     dd(Auth::user());
        // }
    }

    //SubUser Reset Password
    public function suResetPasscode(Request $request)
    {
        // dd($request->digit1);
        $validator = Validator::make($request->all(), [
            'digit1' => 'required',
            'digit2' => 'required',
            'digit3' => 'required',
            'digit4' => 'required',
            'digit5' => 'required',
            'digit6' => 'required',
            'digit7' => 'required',
            'digit8' => 'required',

        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'All fields required'];
        } else {
            // dd("hello");
            $newPasscode = $request->digit1 . $request->digit2 . $request->digit3 . $request->digit4;
            $confirmPasscode = $request->digit5 . $request->digit6 . $request->digit7 . $request->digit8;

            if ($newPasscode == $confirmPasscode) {
                // if($newPasscode == $oldPasscode){
                //     return ['error' => "Old Passcode and New Passcode should be different"];
                // }else{
                // dd($request->suID);
                // if($sub->master_user == $muID && Hash::check($passcode, $sub->password)){
                //     $found=1;
                // }
                // $suDetail = User::where('id',$request->suID)->first();
                // if(Hash::check($oldPasscode, Auth::user()->password)){
                // dd('Correct Old Passcode');
                $user = User::where('id', $request->suID)->first();
                $subUsers = User::where('master_user', $user->master_user)->get();
                // dd($subUsers);
                $count = 0;
                foreach ($subUsers as $s) {
                    if (Hash::check($newPasscode, $s->password)) {
                        $count++;
                    }
                }
                // dd($count);
                if ($count == 0) {
                    $suDetail = User::where('id', $user->id)->first();
                    $suDetail->password = Hash::make($newPasscode);
                    $suDetail->update();
                    return ['success' => 'Passcode reset successful!'];
                } else {
                    return ['error' => 'Passcode is not available! Please try with another passcode'];
                }
                // }else{
                //     return ['error' => 'Wrong Old Passcode! Please enter correct passcode'];
                // }
                // }
            } else {
                return ['error' => 'New Passcode and Confirm Passcode should be matched'];
            }
        }
    }

    //T&C page
    public function tandc()
    {
        return view('terms-of-service');
    }

    //Privacy Policy page
    public function privacyPolicy()
    {
        return view('privacy-and-cookie-policy');
    }

    //About Us
    public function aboutUs()
    {
        return view('about-us');
    }
}
