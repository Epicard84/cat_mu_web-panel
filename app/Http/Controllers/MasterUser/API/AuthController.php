<?php

namespace App\Http\Controllers\MasterUser\API;

use App\Http\Controllers\Controller;
use App\Models\Location;
use App\Services\AuthServices;
use Illuminate\Http\Request;
use App\Services\MasterUserServices;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function loginAuth(Request $request){
        $response = MasterUserServices::masterUserLogin($request);

        if(array_key_exists('error',$response)){
            return response()->json([
                'success' => false,
                'message' => $response['error'],
                'data' => []
            
            ]);
        }else{
            $user_location = Location::where("user_id",Auth::user()->id)->first();

            if($user_location){
                $user_location->location = $request->location;
                $user_location->latitude = $request->latitude;
                $user_location->longitude = $request->longitude;
                $user_location->update();
            }else{
                $user_location = Location::create([
                    'user_id' => Auth::user()->id,
                    'location' => $request->location,
                    'latitude' => $request->latitude,
                    'longitude' => $request->longitude,
                    'technical_status' =>'online',
                    'created_at' =>Carbon::now()
                ]);
            }

            return response()->json([
                'success' => true,
                'message' => "login Successfuly",
                'auth_token' => $response['token'],
                'data' => $response['success'], 
                'location' =>  $user_location->location
            ]);
        }
    }

    public function forgotPassword(Request $request){
        $response = MasterUserServices::forgotPassword($request);
        if(array_key_exists('error',$response)){
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' =>[
                'email'=>$response['email'],
                'otp'=>$response['otp']
            ]
        ]);

    }

    public function otpVerification(Request $request){
        $response = MasterUserServices::otpVerification($request);
        if(array_key_exists('error',$response)){
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            
        ]);
    }

    public function newPassword(Request $request){
        $response = MasterUserServices::newPassword($request);
        if(array_key_exists('error',$response)){
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
        ]);
    }

    
}
