<?php

namespace App\Http\Controllers\MasterUser\API;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Guardian;
use App\Models\Location;
use App\Models\MasterUser\Client;
use App\Models\MasterUser\MFD;
use App\Models\User;
use Illuminate\Http\Request;
use App\Services\MasterUserServices;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PhpParser\Node\Stmt\Foreach_;
use App\Models\MasterUser\MedicineDetails;
use App\Models\MasterUser\Medicine;
use App\Models\MasterUser\MedicineSchedule;
use Illuminate\Support\Facades\Validator;
use App\Models\NotificationDetail;
use App\Models\Notification;
use App\Models\Reminder;
use App\Models\ReminderDetail;
use App\Models\AccountReason;
use App\Models\UserDocument;
use App\Models\MasterUser\Activity;
use App\Models\MasterUser\ActivitySchedule;
use App\Models\MasterUser\ActivityDetails;

use function PHPUnit\Framework\isNull;

class MasterUserApiController extends Controller
{
    //Master user details
    public function masterUserDashboard()
    {
        try {
            //code...
            $master_user_details = User::select('first_name', 'last_name', 'profile_image_name')->find(Auth::user()->id);
            $clients_list = Client::select('id', 'first_name', 'last_name', 'sub_user')->where('master_user', Auth::user()->id)->orderBy('id', 'desc')->take(3)->get();
            foreach ($clients_list as $key => $value) {
                $sub_user = User::where('id', $value->sub_user)->first();
                $value->sub_users = $sub_user->first_name . ' ' . $sub_user->last_name;
            }
            $number_of_clients = Client::where('master_user', Auth::user()->id)->count();
            $subuser_list = User::select('id', 'first_name', 'last_name')->where('master_user', Auth::user()->id)->orderBy('id', 'desc')->take(3)->get();
            foreach ($subuser_list as $key => $value) {
                $clients = Client::where('sub_user', $value->id)->count();
                $value->clients_count = $clients;
            }

            $number_of_subuser = User::where('master_user', Auth::user()->id)->count();
            // $master_user_location = Location::where('user_id',Auth::user()->id)->first();

            return response()->json([
                'success' => true,
                'message' => "data found",
                'data' => [
                    'master_user_details' => $master_user_details,
                    'last_clients_list' => $clients_list,
                    'number_of_clients' => $number_of_clients,
                    'last_subuser_list' => $subuser_list,
                    'number_of_subuser' => $number_of_subuser,
                    'location' => ''
                ]
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Master user Dashboard
    public function dashboard()
    {
        $id = Auth::user()->id;
        $response = MasterUserServices::dashboard($id);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data']
        ]);
    }


    //Client details
    public function clientsDetails()
    {
        try {
            $all_clients = Client::where('master_user', Auth::user()->id)->where('status', 1)->get();
            foreach ($all_clients as $value) {

                $doctor = Doctor::where('client_id', $value->id)->get();
                $guardian = Guardian::where('client_id', $value->id)->get();

                $value['doctor'] = $doctor;
                $value['guardian'] = $guardian;
                $value['documents'] = explode(',', $value->documents);
            }


            $count = Client::where('master_user', Auth::user()->id)->where('status', 1)->count();
            if ($count <= 0) {
                return response()->json([
                    'success' => false,
                    'message' => "clients not found",
                    'data' => []
                ]);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "clients found successfuly",
                    'data' => $all_clients
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Client Archived Medications
    public function archivedMedicationList($id)
    {
        try {
            $archivedMeds = Medicine::where('client_id', $id)->where('status', 1)->get();
            foreach ($archivedMeds as $archMeds) {
                $archMeds['start_date'] = date('m-d-Y', strtotime($archMeds->start_date));
                $archMeds['end_date'] = date('m-d-Y', strtotime($archMeds->end_date));
            }

            return response()->json([
                'success' => true,
                'data' => $archivedMeds
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Create new client
    public function clientRegister(Request $request)
    {
        $response = MasterUserServices::clientRegister($request);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data']
        ]);
    }

    //Update Client 
    public function updateClient(Request $request)
    {
        try {
            $previousDocs = [];
            $detailsImage = [];

            if ($request->hasfile('file')) {
                $files = $request->file('file');
                foreach ($files as $file) {
                    $name =  preg_replace('/\s+/', '', $request->first_name) . uniqid() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path() . '/documents/Client/', $name);
                    $client_docs[] = $name;
                }
                $detailsImage = implode(',', $client_docs);
            }


            $client_details = Client::where('id', $request->client_id)->first();
            // dd($request->client_id);

            if ($client_details->documents) {
                $detailsImg = explode(',', $client_details->documents);
            }
            if ($request->docs) {

                $docs = explode(',', $request->docs);

                $previousDocs = implode(',', $docs);
            }
            if ($request->docs != "" && $detailsImg != "") {
                $newDocs = explode(',', $request->docs);
                $oldDocs = $detailsImg;
                // dd(array_diff($oldDocs , $newDocs));
                $diffDocs = array_diff($oldDocs, $newDocs);
                // dd($diffDocs);
                foreach ($diffDocs as $diffDoc) {
                    unlink(public_path('/documents/Client/' . $diffDoc));
                }
            } elseif ($client_details->documents != "" && $request->docs == "") {
                foreach ($detailsImg as $deImg) {
                    unlink(public_path('/documents/Client/' . $deImg));
                }
            }

            if ($previousDocs && $detailsImage == null) {
                $client_details->documents = $previousDocs;
            } elseif ($detailsImage && $previousDocs == null) {
                $client_details->documents = $detailsImage;
            } elseif ($detailsImage && $previousDocs) {
                $client_details->documents = $previousDocs . "," . $detailsImage;
            } else {
                $client_details->documents = null;
            }

            //docs update end


            // if (is_null($client_details->documents)) {
            //     if (is_null($file)) {
            //         $file = null;
            //     }
            // } else {
            //     if (is_null($file)) {
            //         $file = $client_details->documents;
            //     } else {
            //         $file = $client_details->documents . ',' . $file;
            //     }
            // }

            // dd($client_details);
            $client_details->first_name = $request->first_name;
            $client_details->last_name = $request->last_name;
            $client_details->dob = date('Y-m-d', strtotime($request->dob));
            $client_details->gender = $request->gender;
            $client_details->additional_misc_information = $request->additional_information;
            // $client_details->documents = $file;
            $client_details->update();


            $guardain_data = Guardian::where('client_id', $request->client_id)->delete();
            $doctor_data = Doctor::where('client_id', $request->client_id)->delete();

            $doctor = json_decode($request->doctor, true);
            // dd($doctor->name);
            // die;
            foreach ($doctor as  $key => $doctor) {

                Doctor::create([
                    'client_id' => $request->client_id,
                    'doctor_name' => $doctor['name'],
                    'doctor_phone_number' => $doctor['phone_number'],
                    'doctor_address' => $doctor['address']
                ]);
            }

            $guardian = json_decode($request->guardian, true);
            foreach ($guardian as $guardian) {
                Guardian::create([
                    'client_id' => $request->client_id,
                    'guardian_name' => $guardian['name'],
                    'guardian_phone_number' => $guardian['phone_number'],
                    'guardian_address' => $guardian['address']
                ]);
            }

            $client_details['documents'] = explode(',', $client_details->documents);


            $guardian_details = Guardian::where('client_id', $request->client_id)->get();
            $doctor_details = Doctor::where('client_id', $request->client_id)->get();

            $client_details['guardian'] = $guardian_details;
            $client_details['doctor'] = $doctor_details;




            return response()->json([
                'success' => true,
                'message' => 'updated successfull',
                'data' => $client_details

            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }




    //Add new medicines for clients
    public function clientMedicineDetails($id)
    {
        // dd($request->all());
        $response = MasterUserServices::clientMedicineDetails($id);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data'],
        ]);
    }

    //Get medicine Schedule for client SumonK
    public function clientMedicineSchedule($id, $date)
    {
        // // dd($request->all());
        // $response = MasterUserServices::clientMedicineSchedule($id);
        // if (array_key_exists('error', $response)) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => $response['error'],
        //     ]);
        // }

        // return response()->json([
        //     'success' => true,
        //     'message' => $response['success'],
        //     'data' => $response['data'],
        // ]);
        try {

            $client_data = Client::where('id', $id)->first();

            $fro = date('Y-m-d', strtotime($date . '-2 day'));
            $to = date('Y-m-d', strtotime($date));

            if ($date == 0) {
                $date = date('d F Y');
                $fro = date('Y-m-d', strtotime($date . '-2 day'));
                $to = date('Y-m-d');
            }

            $chart = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 1)->select('id', 'date')->get();
            $narrative = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 2)->select('id', 'date')->get();
            // dd($nach);
            $note = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->select('id', 'schedule_id')->get();
            // dd($note);
            // return view('MasterUser/client-medication', ['data' => $client_data, 'date' => $date, 'patient_id' => $id, 'nach' => $nach, 'note' => $note]);
            // dd('data->'.$client_data.' date->'.$date.' patient_id->'.$id.' nach->'.$nach.' note->'.$note);
            $client_medicine = $client_data->getMedicines;
            foreach ($client_medicine as $cm) {
                $medicine_schedule[] = $cm->getMedicineSchedule;
                $cm->docs = explode(',', $cm->docs);
            }
            // dd($medicine_schedule);
            return response()->json([
                'success' => true,
                'client_data' => $client_data,
                // 'client_medicine' => $client_medicine,
                // 'medicine_schedule' => $cm1,
                'date' => $date,
                'chart' => $chart,
                'narrative' => $narrative,
                'note' => $note
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Get medicine Schedule for Archived client SumonK
    public function archivedClientMedicineSchedule($id, $date)
    {
        try {

            $client_data = Client::where('id', $id)->withTrashed()->first();

            $fro = date('Y-m-d', strtotime($date . '-2 day'));
            $to = date('Y-m-d', strtotime($date));

            if ($date == 0) {
                $date = date('d F Y', strtotime($client_data->created_at));
                $fro = date('Y-m-d', strtotime($date . '-2 day'));
                $to = date('Y-m-d');
            }

            $chart = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 1)->select('id', 'date')->get();
            $narrative = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 2)->select('id', 'date')->get();
            // dd($nach);
            $note = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->select('id', 'schedule_id')->get();
            // dd($note);
            // return view('MasterUser/client-medication', ['data' => $client_data, 'date' => $date, 'patient_id' => $id, 'nach' => $nach, 'note' => $note]);
            // dd('data->'.$client_data.' date->'.$date.' patient_id->'.$id.' nach->'.$nach.' note->'.$note);
            $client_medicine = $client_data->getMedicines;
            foreach ($client_medicine as $cm) {
                $medicine_schedule[] = $cm->getMedicineSchedule;
                $cm->docs = explode(',', $cm->docs);
            }
            // dd($medicine_schedule);
            return response()->json([
                'success' => true,
                'client_data' => $client_data,
                // 'client_medicine' => $client_medicine,
                // 'medicine_schedule' => $cm1,
                'date' => $date,
                'chart' => $chart,
                'narrative' => $narrative,
                'note' => $note
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Complete medicine schedule SumonK
    public function clientMedicationUpdate($id, $index)
    {
        try {
            // dd($id);
            $medSch = MedicineSchedule::where('id', $id)->first();
            $today = date('Y-m-d');
            $fromDate = date('Y-m-d', strtotime($today . '-3 day'));

            if ($medSch->date >= $fromDate || $medSch->date == $today && $medSch->status == 0) {

                $time = $medSch->complete_details;

                // $current_time = date("h:i");
                $current_time = strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1));
                if ($time == NULL) {
                    $time = explode(',', $time);
                    for ($i = 0; $i < $index; $i++) {
                        $time[$i] = "";
                    }
                    $time[$index] = $current_time;
                    $updateSchedule = implode(',', $time);
                } else {
                    $time = explode(',', $time);
                    $time[$index] = $current_time;
                    $updateSchedule = implode(',', $time);
                }

                $medSch->complete_details = $updateSchedule;
                $medSch->update();
                $med = explode(',', $medSch->complete_details); //String to Array Conversion of medicine complete timing
                // $medCompleteCount = count($med); // Count how may time are completed
                $medCompleteCount = 0;
                foreach ($med as $m) {
                    if ($m != "" || $m != null) {
                        $medCompleteCount = (int)$medCompleteCount + 1;
                    }
                }
                $medSchCount = Medicine::where('id', $medSch->medicine_id)->first()->time_days; //Find how many times are scheduled
                if ($medCompleteCount == $medSchCount) {
                    $medSch->status = "1"; //if the count matched status will be updated.
                    $medSch->update();
                    NotificationDetail::medComNote($medSch->medicine_id, $today);
                }

                return response()->json([
                    'success' => true,
                    'message' => "Request Processed",
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Request can't be Processed",
                ], 200);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Complete medicine schedule SumonK
    public function clientMedicationUndo($id, $index)
    {
        try {
            // dd($id);
            $medSch = MedicineSchedule::where('id', $id)->first();
            $today = date('Y-m-d');
            $fromDate = date('Y-m-d', strtotime($today . '-3 day'));
            if ($medSch->date >= $fromDate || $medSch->date == $today) {
                if ($medSch->complete_details != "") {

                    $medComDetails = $medSch->complete_details;
                    $medComDetails = explode(',', $medComDetails);
                    $medComDetails[$index] = null;
                    $medComUpdate = implode(',', $medComDetails);
                    $medSch->complete_details = $medComUpdate;
                    $medSch->status = "0";
                    $medSch->update();

                    return response()->json([
                        'success' => true,
                        'message' => "Request Processed",
                    ], 200);
                } else {
                    return response()->json([
                        'success' => true,
                        'message' => "Request Can't be Processed",
                    ], 200);
                }
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "Request Can't be Processed",
                ], 200);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Add new medicine chart narrative note SumonK
    public function newDetails(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'description' => 'required|string',
                'patient_id' => 'required',
                'type' => 'required',
            ]);

            if ($validator->fails()) {
                return ['error' => 'Description required'];
            } else {
                $detailsImage = null;
                $signature = null;
                $schedule_id = null;
                $date = $request->date;
                if ($request->hasfile('detailsImage')) {
                    $files = $request->file('detailsImage');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "DNG" || $file_extention == "dng" || $file_extention == "jpg" || $file_extention == "JPG" || $file_extention == "png" || $file_extention == "PNG" || $file_extention == "jpeg" || $file_extention == "JPEG" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }

                    if ($count ==  0) {
                        foreach ($files as $file) {
                            $name =  $request->patient_id . '_' . $request->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path() . '/documents/Schedule_Details/', $name);
                            $detailsImage[] = $name;
                        }
                        $detailsImage = implode(',', $detailsImage);
                    } else {
                        return response()->json([
                            'success' => false,
                            'message' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted",
                        ], 200);
                    }
                }
                if ($request->hasfile('signature')) {
                    $file = $request->file('signature');
                    $signature =  $request->patient_id . '_' . $request->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path() . '/documents/Schedule_Details/', $signature);
                }
                if ($request->schedule_id) {
                    $schedule_id = $request->schedule_id;
                    $date = MedicineSchedule::where('id', $request->schedule_id)->first()->date;
                }
                // $date = date('Y-m-d H:i:s');
                $creator_id = Auth::user()->id;

                $client =  MedicineDetails::create([
                    'descriptions' => $request->description,
                    'patient_id' => $request->patient_id,
                    'schedule_id' => $schedule_id,
                    'type' => $request->type,
                    'date' => $date,
                    'images' => $detailsImage,
                    'creator_id' => $creator_id,
                    'signature' => $signature,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

                return response()->json([
                    'success' => true,
                    'message' => "Added successfuly",
                ], 200);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Edit Note Chart Narrative
    public function editDetails(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'description' => 'required|string',
            ]);

            if ($validator->fails()) {
                return ['error' => 'Description required'];
            } else {
                // dd($request)
                $details = MedicineDetails::where('id', $request->details_id)->first();
                $details->descriptions = $request->description;
                $previousDocs = [];
                $detailsImage = [];
                if ($details->images) {
                    $detailsImg = explode(',', $details->images);
                }
                if ($request->docs) {
                    // $detailsImg = explode(',', $details->images);
                    $docs = explode(',', $request->docs);
                    // foreach ($detailsImg as $dimg) {
                    //     $delete = 0;
                    //     foreach ($docs as $d) {
                    //         if ($d != $dimg) {
                    //             $delete = 1;
                    //         }
                    //     }
                    //     if ($delete == 1) {
                    //         // unlink(public_path('/documents/Schedule_Details/' . $dimg));
                    //     }
                    // }
                    $previousDocs = implode(',', $docs);
                }

                if ($request->docs != "" && $detailsImg != "") {
                    $newDocs = $docs;
                    $oldDocs = $detailsImg;
                    // dd(array_diff($oldDocs , $newDocs));
                    $diffDocs = array_diff($oldDocs, $newDocs);
                    // dd($diffDocs);
                    foreach ($diffDocs as $diffDoc) {
                        unlink(public_path('/documents/Schedule_Details/' . $diffDoc));
                    }
                } elseif ($details->images != "" && $request->docs == "") {
                    foreach ($detailsImg as $deImg) {
                        unlink(public_path('/documents/Schedule_Details/' . $deImg));
                    }
                }

                if ($request->hasfile('detailsImage')) {
                    $files = $request->file('detailsImage');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "DNG" || $file_extention == "dng" || $file_extention == "jpg" || $file_extention == "JPG" || $file_extention == "png" || $file_extention == "PNG" || $file_extention == "jpeg" || $file_extention == "JPEG" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }
                    if ($count ==  0) {
                        foreach ($files as $file) {
                            $name =  $details->patient_id . '_' . $details->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path('/documents/Schedule_Details/'), $name);
                            $detailsImage[] = $name;
                        }
                        $detailsImage = implode(',', $detailsImage);
                        // $finalDocs = $detailsImage.",".$previousDocs;
                    } else {
                        return response()->json([
                            'success' => false,
                            'message' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted",
                        ], 200);
                    }
                }

                if ($previousDocs && $detailsImage == null) {
                    $details->images = $previousDocs;
                } elseif ($detailsImage && $previousDocs == null) {
                    $details->images = $detailsImage;
                } elseif ($detailsImage && $previousDocs) {
                    $details->images = $previousDocs . "," . $detailsImage;
                } else {
                    $details->images = null;
                }
                $details->update();


                return response()->json([
                    'success' => true,
                    'message' => "Updated successfuly",
                ]);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Delete Details
    public function deleteDetails($id)
    {
        try {
            $details = MedicineDetails::where('id', $id)->delete();

            return response()->json([
                'success' => true,
                'message' => "Deleted successfuly",
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //All Medication Note Chart Narrative SumonK
    public function medicationAllChart($id)
    {
        try {
            $patDetails = Client::where('id', $id)->first();
            $patName = $patDetails->first_name . " " . $patDetails->last_name;
            // dd($date." ".$id);
            $date = date('Y-m-d');
            $endDate = date('Y-m-d', strtotime('-7 days'));
            $date_chart = MedicineDetails::where('patient_id', $id)->where('date', '>=', $endDate)->orderByDesc('id')->get();
            foreach ($date_chart as $mn) {
                if ($mn->type == 0) {
                    $mn['type'] = "Note";
                } elseif ($mn->type == 1) {
                    $mn['type'] = "Prog Note";
                } else {
                    $mn['type'] = "Dr's Order";
                }
                $mn['time'] = date('m-d-Y Hi', strtotime($mn->created_at)) . 'hrs';
                $mn['creator_id'] = $mn->getName;
                $noteImg = explode(',', $mn->images);
                $note_img = [];
                if ($mn->images) {
                    foreach ($noteImg as $nIm) {
                        $note_img[] = url('public/documents/Schedule_Details/' . $nIm);
                    }
                }
                $mn['images'] = $note_img;
                $signature = null;
                // $mn['signature'] = $signature;
                if ($mn->signature) {
                    $signature = url('public/documents/Schedule_Details/' . $mn->signature);
                }
                $mn['signature'] = $signature;
            }
            return response()->json([
                'success' => true,
                'details' => $date_chart,
                'patient_id' => $id,
            ], 200);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Filtered All Notes/Charts/Dr Orders
    public function filterMedicationAllChart($id, $type)
    {
        try {
            $patDetails = Client::where('id', $id)->first();
            $patName = $patDetails->first_name . " " . $patDetails->last_name;
            // dd($date." ".$id);
            $date = date('Y-m-d');
            $endDate = date('Y-m-d', strtotime('-7 days'));
            if ($type == "4") {
                $date_chart = MedicineDetails::where('patient_id', $id)->where('date', '>=', $endDate)->orderByDesc('id')->get();
            } else {
                $date_chart = MedicineDetails::where('patient_id', $id)->where('date', '>=', $endDate)->where('type', $type)->orderByDesc('id')->get();
            }
            foreach ($date_chart as $mn) {
                if ($mn->type == 0) {
                    $mn['type'] = "Note";
                } elseif ($mn->type == 1) {
                    $mn['type'] = "Prog Note";
                } else {
                    $mn['type'] = "Dr's Order";
                }
                $mn['time'] = date('m-d-Y Hi', strtotime($mn->created_at)) . 'hrs';
                $mn['creator_id'] = $mn->getName;
                $noteImg = explode(',', $mn->images);
                $note_img = [];
                if ($mn->images) {
                    foreach ($noteImg as $nIm) {
                        $note_img[] = url('public/documents/Schedule_Details/' . $nIm);
                    }
                }
                $mn['images'] = $note_img;
                $signature = null;
                // $mn['signature'] = $signature;
                if ($mn->signature) {
                    $signature = url('public/documents/Schedule_Details/' . $mn->signature);
                }
                $mn['signature'] = $signature;
            }
            return response()->json([
                'success' => true,
                'details' => $date_chart,
                'patient_id' => $id,
            ], 200);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Get Medication Notes SumonK
    public function medicationNote($schid, $pid)
    {
        try {
            $med_shedule = MedicineSchedule::where('id', $schid)->first();
            $med_details = Medicine::where('id', $med_shedule->medicine_id)->first();
            $med_name = $med_details->medicine_name;
            $date_note = MedicineDetails::where('patient_id', $pid)->where('schedule_id', $schid)->where('type', 0)->orderByDesc('id')->get();
            foreach ($date_note as $mn) {
                $mn['time'] = date('Hi', strtotime($mn->created_at)) . 'hrs';
                $mn['creator_id'] = $mn->getName;
                $noteImg = explode(',', $mn->images);
                $note_img = [];
                if ($mn->images) {
                    foreach ($noteImg as $nIm) {
                        $note_img[] = url('public/documents/Schedule_Details/' . $nIm);
                    }
                }
                $mn['images'] = $note_img;
            }
            return response()->json([
                'success' => true,
                'med_name' => $med_name,
                'date_note' => $date_note,
                'patient_id' => $pid,
                'med_shedule' => $med_shedule
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Get Medication Chart SumonK
    public function medicationChart($date, $id)
    {
        try {
            // dd($date." ".$id);
            $date_chart = MedicineDetails::where('date', $date)->where('type', 1)->where('patient_id', $id)->orderByDesc('id')->get();
            foreach ($date_chart as $dc) {
                $dc['time'] = date('Hi', strtotime($dc->created_at)) . 'hrs';
                $dc['creator_id'] = $dc->getName;
                $chartImg = explode(',', $dc->images);
                $chart_img = [];
                if ($dc->images) {
                    foreach ($chartImg as $cIm) {
                        $chart_img[] = url('public/documents/Schedule_Details/' . $cIm);
                    }
                }
                $dc['images'] = $chart_img;
            }
            return response()->json([
                'success' => true,
                'date_chart' => $date_chart,
                'patient_id' => $id,
                // 'date' => $date
            ], 200);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Get Medication Narrative SumonK
    public function medicationNarrative($date, $id)
    {
        try {
            $date_narrative = MedicineDetails::where('date', $date)->where('type', 2)->where('patient_id', $id)->orderByDesc('id')->get();
            // dd($date_narrative);
            // return view('MasterUser/client-medication-narrative', ['date_narrative' => $date_narrative, 'patient_id' => $id, 'date' => $date]);
            foreach ($date_narrative as $dn) {
                $dn['time'] = date('Hi', strtotime($dn->created_at)) . 'hrs';
                $dn['creator_id'] = $dn->getName;
                $narrativeImg = explode(',', $dn->images);
                $narrative_img = [];
                if ($dn->images) {
                    foreach ($narrativeImg as $nIm) {
                        $narrative_img[] = url('public/documents/Schedule_Details/' . $nIm);
                    }
                }
                $dn['images'] = $narrative_img;
                $dn['signature'] = url('public/documents/Schedule_Details/' . $dn->signature);
            }
            return response()->json([
                'success' => true,
                'date_narrative' => $date_narrative,
                'patient_id' => $id,
                // 'date' => $date
            ], 200);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //client Medicine Shedule update
    public function updateMedicineSchedule(Request $request)
    {

        $response = MasterUserServices::updateMedicineSchedule($request);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data'],
        ]);
    }


    //get notes details
    public function notesDetails($id)
    {
        $response = MasterUserServices::notesDetails($id);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }

        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data'],
        ]);
    }

    //Add new medicines for clients SumonK
    public function addNewMedicine(Request $request)
    {

        // $response = MasterUserServices::addNewMedicine($request);
        // if (array_key_exists('error', $response)) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => $response['error'],
        //     ]);
        // }
        // return response()->json([
        //     'success' => true,
        //     'message' => $response['success']
        // ]);
        try {
            if (is_null($request->med_id)) {
                // dd($request);

                $medicine_docs = [];
                if ($request->hasFile('docs')) {
                    $files = $request->file('docs');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "DNG" || $file_extention == "dng" || $file_extention == "jpg" || $file_extention == "JPG" || $file_extention == "png" || $file_extention == "PNG" || $file_extention == "jpeg" || $file_extention == "JPEG" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }

                    if ($count ==  0) {
                        foreach ($files as $key => $file) {
                            $image = $request->medicine_name . uniqid() . "." . $file->getClientOriginalExtension();
                            $file->move(public_path('documents/Medicine_Docs/'), $image);
                            $medicine_docs[] = $image;
                        }
                    } else {
                        return response()->json([
                            'success' => false,
                            'message' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted",
                        ], 200);
                    }
                } //SumonK
                // $scheduled_time = $request->scheduled_time;
                $medicine = Medicine::create([
                    'client_id' => $request->client_id,
                    'medicine_name' => $request->medicine_name,
                    'start_date' => date('Y-m-d', strtotime($request->start_date)),
                    'end_date' => date('Y-m-d', strtotime($request->start_date)),
                    'time_days' => $request->time_days,
                    'scheduled_time' => $request->scheduled_time,
                    'status' => 0,
                    'docs' => implode(',', $medicine_docs),
                    'descriptions' => $request->description
                ]);

                if ($medicine) {

                    $medicine_data = Medicine::where('id', $medicine->id)->first();

                    MedicineSchedule::create([
                        'medicine_id' => $medicine_data->id,
                        'date' => date('Y-m-d', strtotime($request->start_date)),
                        'scheduled_time' => $medicine_data->scheduled_time,
                        'status' => 0
                    ]);
                    return response()->json([
                        'success' => true,
                        'message' => "Medicine Added successfuly",
                    ]);
                }
                // }
            } else {
                //Medicine Update

                $medicine_details = Medicine::where('id', $request->med_id)->first();
                $medicine_details->medicine_name = $request->medicine_name;
                $medicine_details->time_days = $request->time_days;
                $medicine_details->descriptions = $request->description;
                $medicine_details->status = $request->status;
                $medicine_details->scheduled_time = $request->scheduled_time;
                $medicine_details->update();

                return response()->json([
                    'success' => true,
                    'message' => "Medicine Updated successfuly",
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Get Medication Details SumonK
    public function medicationDetails($id)
    {
        try {
            // dd($id);
            $med_data = Medicine::where('id', $id)->first();
            $medDocs = explode(',', $med_data->docs);
            // $narrative_img = [];
            foreach ($medDocs as $md) {
                $med_docs[] = url('public/documents/Medicine_Docs/' . $md);
            }
            $med_data['docs'] = $med_docs;
            return response()->json([
                'success' => true,
                'data' => $med_data,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Client Medicine Delete
    public function medicineDelete($id)
    {
        $response = MasterUserServices::medicineDelete($id);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success']
        ]);
    }

    //Sub user Details
    public function subUserDetails()
    {
        try {
            $subuser_list = User::where('master_user', Auth::user()->id)->get();
            $count = User::where('master_user', Auth::user()->id)->count();
            if ($count <= 0) {
                return response()->json([
                    'success' => false,
                    'message' => "no sub users found",
                    'data' => $subuser_list
                ]);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "sub users found successfuly",
                    'data' => $subuser_list
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //SubUser profile data
    public function subUserProfile($id)
    {
        try {
            $subuserdata = User::where('id', $id)->first();
            $document = UserDocument::where('user_id', $id)->first();
            $document = explode(',', $document->document);
            $document = array_map(function ($doc) {
                return 'public/documents/SubUser_Docs/' . $doc;
            }, $document);
            return response()->json([
                'success' => true,
                'data' => ['profile' => $subuserdata, 'document' => $document]
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Add sub user
    public function subUserRegister(Request $request)
    {
        $response = MasterUserServices::subUserRegister($request);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        } elseif (array_key_exists('passcodeerror', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['passcodeerror'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data']
        ]);
    }

    public function subUserEdit(Request $request)
    {
        $response = MasterUserServices::editSubUser($request);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
        ]);
    }

    //Edit SubUser Document
    public function editDocsSubUser(Request $request)
    {
        try {

            $user = User::where('id', $request->details_id)->first();

            $details = UserDocument::where('user_id', $request->details_id)->first();

            $previousDocs = [];
            $detailsImage = [];

            if ($request->hasfile('detailsImage')) {
                $files = $request->file('detailsImage');
                $count = 0;

                foreach ($files as $file) {
                    $file_extention = $file->getClientOriginalExtension();

                    if ($file_extention == "DNG" || $file_extention == "dng" || $file_extention == "jpg" || $file_extention == "JPG" || $file_extention == "png" || $file_extention == "PNG" || $file_extention == "jpeg" || $file_extention == "JPEG" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                    } else {
                        $count++;
                    }
                }
                if ($count ==  0) {
                    foreach ($files as $file) {
                        $name =  $user->first_name . '_' . $user->last_name . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/documents/SubUser_Docs/'), $name);
                        $detailsImage[] = $name;
                    }
                    $detailsImage = implode(',', $detailsImage);
                    // $finalDocs = $detailsImage.",".$previousDocs;
                } else {
                    return ['error' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                }
            }

            if ($details->document) {
                $detailsImg = explode(',', $details->document);
            }
            if ($request->docs) {

                $docs = explode(',', $request->docs);

                $previousDocs = implode(',', $docs);
            }
            if ($request->docs != "" && $detailsImg != "") {
                $newDocs = explode(',', $request->docs);
                $oldDocs = $detailsImg;

                $diffDocs = array_diff($oldDocs, $newDocs);

                foreach ($diffDocs as $diffDoc) {
                    unlink(public_path('/documents/SubUser_Docs/' . $diffDoc));
                }
            } elseif ($details->document != "" && $request->docs == "") {
                foreach ($detailsImg as $deImg) {
                    unlink(public_path('/documents/SubUser_Docs/' . $deImg));
                }
            }

            if ($previousDocs && $detailsImage == null) {
                $details->document = $previousDocs;
            } elseif ($detailsImage && $previousDocs == null) {
                $details->document = $detailsImage;
            } elseif ($detailsImage && $previousDocs) {
                $details->document = $previousDocs . "," . $detailsImage;
            } else {
                $details->document = null;
            }
            $details->update();

            // return ['success' => "Updated successfuly"];
            return response()->json([
                'success' => true,
                'message' => "Documents updated successfuly",
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }



    public function clientArchive(Request $request)
    {
        try {

            $client = Client::where('id', $request->client_id)->first();
            // dd($client);
            if ($client) {
                $medicines = Medicine::where('client_id', $client->id)->get();
                foreach ($medicines as $med) {
                    $med->status = 1;
                    $med->update();
                }
                $client->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'client archive successfuly',
                ]);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => 'client not found',
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function clientRestore($id)
    {
        try {
            $client = Client::where('id', $id)->onlyTrashed()->first();
            // dd($client);
            if ($client) {
                $client->restore();
                $archiveMedicine = Medicine::where('client_id', $id)->get();
                foreach ($archiveMedicine as $am) {
                    $am->status = 0;
                    $am->update();
                }
                return response()->json([
                    'success' => true,
                    'message' => 'Client Restored Successfully',
                ]);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => 'client not found',
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function archiveClients()
    {
        $response = MasterUserServices::archiveClients(Auth::user()->id);

        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        } else {
            $client = $response['data'];

            foreach ($client as $value) {
                $value['documents'] = explode(',', $value['documents']);
                $value['guardian'] = Guardian::where('client_id', $value->id)->get();
                $value['doctor'] = Doctor::where('client_id', $value->id)->get();
            }
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' =>  $client
        ]);
    }

    public function createMfd(Request $request)
    {
        $response = MasterUserServices::createMfd($request);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
        ]);
    }

    public function mfdList()
    {
        try {
            $mfds = MFD::where('master_user_id', Auth::user()->id)->get();
            if (is_null($mfds)) {
                return response()->json([
                    'success' => false,
                    'message' => "no mfd found",
                    'data' => []
                ]);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "mfd found successfuly",
                    'data' => $mfds
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function editMfd($id, Request $request)
    {
        try {
            $mfd = MFD::find($id);
            $mfd->message = $request->mfd_details;
            $mfd->update();

            return response()->json([
                'success' => true,
                'message' => "MFD updated successfully"
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function userDetails()
    {

        $response = MasterUserServices::userDetails();
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
                'data' => []
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data']
        ]);
    }

    public function editMasterUser(Request $request)
    {
        $response = MasterUserServices::editMasterUser($request);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data']
        ]);
    }

    public function changeUserDetails($id)
    {
        $response = MasterUserServices::changeUserDetails($id);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
                'data' => []
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data']
        ]);
    }

    public function updateSubUserDetails(Request $request)
    {

        $response = MasterUserServices::updateSubUserDetails($request);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
        ]);
    }

    //Notification SumonK
    public function muNotifications()
    {
        try {
            $getNotifications = Notification::where('receiver_id', Auth::user()->id)->where('status', 0)->orderByDesc('id')->get();
            foreach ($getNotifications as $getNotify) {

                $Notify[] = $getNotify->getNotiDetails;
            }
            return response()->json([
                'success' => true,
                'data' => $getNotifications,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function notificationSeen($notiId)
    {
        try {
            $notificationDetails = Notification::where('id', $notiId)->first();
            $notificationDetails->status = 1;
            $notificationDetails->update();

            return response()->json([
                'success' => true,
                'message' => "Notification Seen Updated",
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Get Reminder
    public function muReminder($date)
    {
        try {
            $date = date('Y-m-d', strtotime($date));
            $reminders = Reminder::where('masteruser_id', Auth::user()->id)->where('date', $date)->orderByDesc('id')->get();

            // foreach($reminders as $rem)
            // {
            //     // $rems = $rem->getReminderDetails;
            //     $remp = $rem->getReminderDetails->getClientrDetails;
            //     $remp['documents'] = explode(',',$remp['documents']);
            // }
            return response()->json([
                'success' => true,
                'date' => $date,
                'data' => $reminders,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }
    //New Reminder SumonK
    public function newReminder(Request $request)
    {
        try {
            $reminderdetails =  Reminder::create([
                'masteruser_id' => Auth::user()->id,
                'reminder' => $request->reminder_details,
                'date' => date('Y-m-d', strtotime($request->reminder_date)),
                'created_at' => date('Y-m-d H:i:s'),
            ]);
            if ($reminderdetails) {
                return response()->json([
                    'success' => true,
                    'message' => 'New reminder addes successfully',
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Check Reminder
    public function checkReminder($rid)
    {
        try {
            $date = date('Y-m-d');
            $ReminderDetails = Reminder::where('id', $rid)->first();
            if ($ReminderDetails->date == $date) {
                $ReminderDetails->status = 1;
                $ReminderDetails->update();

                return response()->json([
                    'success' => true,
                    'message' => "Reminder checked successfully",
                ]);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "Sorry, Your request can't be processed",
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Delete Reminder
    public function deleteReminder($id)
    {
        try {
            Reminder::where('id', $id)->delete();

            return response()->json([
                'success' => true,
                'message' => "Reminder deleted successfully",
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }


    public function deleteUser($id)
    {
        try {
            $user = User::where('id', $id)->first();
            if ($user) {
                $user->delete();
                return response()->json([
                    'success' => true,
                    'message' => 'User Deleted Successfully'
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Restore SubUser
    public function subuserRestore($id)
    {
        try {
            $subuser = User::where('id', $id)->onlyTrashed()->first();
            // dd($client);
            if ($subuser) {
                $subuser->restore();
                return response()->json([
                    'success' => true,
                    'message' => 'SubUser Restored Successfully',
                ]);
            } else {
                return response()->json([
                    'success' => true,
                    'message' => 'SubUser not found',
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //permanent Delete SubUser SumonK
    public function deleteSubuser($id)
    {
        try {
            $subUser = User::where('id', $id)->onlyTrashed()->first();
            $subUser->forcedelete();
            return response()->json([
                'success' => true,
                'message' => 'Subuser Deleted Successfully',
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Archive SubUsers
    public function archiveSubuser()
    {
        try {
            $subusers = User::where('master_user', Auth::user()->id)->onlyTrashed()->orderByDesc('id')->get();
            return response()->json([
                'success' => true,
                'data' => $subusers,
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    public function nowDateTime()
    {
        try {
            $dateTime = date('Y-m-d H:i');
            return response()->json([
                'success' => true,
                'message' => $dateTime
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Account Disable Request SumonK
    public function disableReason(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'reason' => 'required',
            ]);

            if ($validator->fails()) {
                return ['error' => 'Reason required'];
            } else {
                // $creator_id = Auth::user()->id;

                $reasondetails =  AccountReason::create([
                    'masteruser_id' => Auth::user()->id,
                    'reason' => $request->reason,
                    'date' => date('m-d-Y'),
                    // 'status' => 0,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);
                return response()->json([
                    'success' => true,
                    'message' => "Your request has been submitted successfuly"
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Add new activity for clients SumonK
    public function addNewActivity(Request $request)
    {

        // $response = MasterUserServices::addNewMedicine($request);
        // if (array_key_exists('error', $response)) {
        //     return response()->json([
        //         'success' => false,
        //         'message' => $response['error'],
        //     ]);
        // }
        // return response()->json([
        //     'success' => true,
        //     'message' => $response['success']
        // ]);
        try {
            if (is_null($request->act_id)) {
                // dd($request);

                $activity_docs = [];
                if ($request->hasFile('docs')) {
                    $files = $request->file('docs');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "DNG" || $file_extention == "dng" || $file_extention == "jpg" || $file_extention == "JPG" || $file_extention == "png" || $file_extention == "PNG" || $file_extention == "jpeg" || $file_extention == "JPEG" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }

                    if ($count ==  0) {
                        foreach ($files as $key => $file) {
                            $image = $request->activity_name . uniqid() . "." . $file->getClientOriginalExtension();
                            $file->move(public_path('documents/Medicine_Docs/'), $image);
                            $activity_docs[] = $image;
                        }
                    } else {
                        return response()->json([
                            'success' => false,
                            'message' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted",
                        ], 200);
                    }
                } //SumonK
                // $scheduled_time = $request->scheduled_time;
                $activity = Activity::create([
                    'client_id' => $request->client_id,
                    'activity_name' => $request->activity_name,
                    'start_date' => date('Y-m-d', strtotime($request->start_date)),
                    'end_date' => date('Y-m-d', strtotime($request->start_date)),
                    'time_days' => $request->time_days,
                    'scheduled_time' => $request->scheduled_time,
                    'status' => 0,
                    'docs' => implode(',', $activity_docs),
                    'descriptions' => $request->description
                ]);

                if ($activity) {

                    $activity_data = Medicine::where('id', $activity->id)->first();

                    MedicineSchedule::create([
                        'activity_id' => $activity_data->id,
                        'date' => date('Y-m-d', strtotime($request->start_date)),
                        'scheduled_time' => $activity_data->scheduled_time,
                        'status' => 0
                    ]);
                    return response()->json([
                        'success' => true,
                        'message' => "Activity Added successfuly",
                    ]);
                }
                // }
            } else {
                //Medicine Update

                $medicine_details = Activity::where('id', $request->act_id)->first();
                $medicine_details->time_days = $request->time_days;
                $medicine_details->descriptions = $request->description;
                $medicine_details->status = $request->status;
                $medicine_details->scheduled_time = $request->scheduled_time;
                $medicine_details->update();

                return response()->json([
                    'success' => true,
                    'message' => "Activity Updated successfuly",
                ]);
            }
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Client Activity Archive
    public function archiveActivity($id)
    {
        try {
            $med = Activity::where('id', $id)->first();
            $med->status = 1;
            $med->update();

            return response()->json([
                'success' => true,
                'message' => "Activity archived successfully"
            ]);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Client Activity Delete
    public function activityDelete($id)
    {
        $response = MasterUserServices::activityDelete($id);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success']
        ]);
    }

    //Complete activity schedule SumonK
    public function clientActivityUpdate($id, $index)
    {
        try {
            // dd($id);
            $medSch = ActivitySchedule::where('id', $id)->first();
            $today = date('Y-m-d');

            if ($medSch->date == $today && $medSch->status == 0) {

                $time = $medSch->complete_details;

                // $current_time = date("h:i");
                $current_time = strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1));
                if ($time == NULL) {
                    $time = explode(',', $time);
                    for ($i = 0; $i < $index; $i++) {
                        $time[$i] = "";
                    }
                    $time[$index] = $current_time;
                    $updateSchedule = implode(',', $time);
                } else {
                    $time = explode(',', $time);
                    $time[$index] = $current_time;
                    $updateSchedule = implode(',', $time);
                }

                $medSch->complete_details = $updateSchedule;
                $medSch->update();
                $med = explode(',', $medSch->complete_details); //String to Array Conversion of medicine complete timing
                // $medCompleteCount = count($med); // Count how may time are completed

                // $medCompleteCount = 0;
                // foreach ($med as $m) {
                //     if ($m != "" || $m != null) {
                //         $medCompleteCount = (int)$medCompleteCount + 1;
                //     }
                // }
                // $medSchCount = Medicine::where('id', $medSch->medicine_id)->first()->time_days; //Find how many times are scheduled
                // if ($medCompleteCount == $medSchCount) {
                //     $medSch->status = "1"; //if the count matched status will be updated.
                //     $medSch->update();
                //     NotificationDetail::medComNote($medSch->medicine_id, $today);
                // }

                return response()->json([
                    'success' => true,
                    'message' => "Request Processed",
                ], 200);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => "Request can't be Processed",
                ], 200);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Complete Activity schedule SumonK
    public function clientActivityUndo($id, $index)
    {
        try {
            // dd($id);
            $medSch = ActivitySchedule::where('id', $id)->first();
            $today = date('Y-m-d');
            if ($medSch->date == $today) {
                if ($medSch->complete_details != "") {

                    $medComDetails = $medSch->complete_details;
                    $medComDetails = explode(',', $medComDetails);
                    $medComDetails[$index] = null;
                    $medComUpdate = implode(',', $medComDetails);
                    $medSch->complete_details = $medComUpdate;
                    $medSch->status = "0";
                    $medSch->update();

                    return response()->json([
                        'success' => true,
                        'message' => "Request Processed",
                    ], 200);
                } else {
                    return response()->json([
                        'success' => true,
                        'message' => "Request Can't be Processed",
                    ], 200);
                }
            } else {
                return response()->json([
                    'success' => true,
                    'message' => "Request Can't be Processed",
                ], 200);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Get activity Schedule for client SumonK
    public function clientActivitySchedule($id, $date)
    {
        try {

            $client_data = Client::where('id', $id)->first();

            $fro = date('Y-m-d', strtotime($date . '-2 day'));
            $to = date('Y-m-d', strtotime($date));

            if ($date == 0) {
                $date = date('d F Y');
                $fro = date('Y-m-d', strtotime($date . '-2 day'));
                $to = date('Y-m-d');
            }

            // $chart = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 1)->select('id', 'date')->get();
            // $narrative = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 2)->select('id', 'date')->get();
            // dd($nach);
            $note = ActivityDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->select('id', 'schedule_id')->get();
            // dd($note);

            // dd('data->'.$client_data.' date->'.$date.' patient_id->'.$id.' nach->'.$nach.' note->'.$note);
            $client_activity = $client_data->getActivities;
            foreach ($client_activity as $ca) {
                $activity_schedule[] = $ca->getActivitySchedule;
                $ca->docs = explode(',', $ca->docs);
            }
            // dd($medicine_schedule);
            return response()->json([
                'success' => true,
                'client_data' => $client_data,
                'date' => $date,
                // 'chart' => $chart,
                // 'narrative' => $narrative,
                'note' => $note
            ]);
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Get Activity Notes SumonK
    public function activityNote($schid, $pid)
    {
        try {
            $act_shedule = ActivitySchedule::where('id', $schid)->first();
            $med_details = Activity::where('id', $act_shedule->activity_id)->first();
            $act_name = $med_details->activity_name;
            $date_note = ActivityDetails::where('patient_id', $pid)->where('schedule_id', $schid)->where('type', 0)->orderByDesc('id')->get();
            foreach ($date_note as $mn) {
                $mn['time'] = date('Hi', strtotime($mn->created_at)) . 'hrs';
                $mn['creator_id'] = $mn->getName;
                $noteImg = explode(',', $mn->images);
                $note_img = [];
                if ($mn->images) {
                    foreach ($noteImg as $nIm) {
                        $note_img[] = url('public/documents/Schedule_Details/' . $nIm);
                    }
                }
                $mn['images'] = $note_img;
            }
            return response()->json([
                'success' => true,
                'act_name' => $act_name,
                'date_note' => $date_note,
                'patient_id' => $pid,
                'act_shedule' => $act_shedule
            ], 200);
        } catch (\Throwable $th) {
            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Add new activity chart narrative note SumonK
    public function newActivityDetails(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'description' => 'required|string',
                'patient_id' => 'required',
                'type' => 'required',
            ]);

            if ($validator->fails()) {
                return ['error' => 'Description required'];
            } else {
                $detailsImage = null;
                $schedule_id = null;
                $date = $request->date;
                if ($request->hasfile('detailsImage')) {
                    $files = $request->file('detailsImage');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "DNG" || $file_extention == "dng" || $file_extention == "jpg" || $file_extention == "JPG" || $file_extention == "png" || $file_extention == "PNG" || $file_extention == "jpeg" || $file_extention == "JPEG" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }

                    if ($count ==  0) {
                        foreach ($files as $file) {
                            $name =  $request->patient_id . '_' . $request->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path() . '/documents/Schedule_Details/', $name);
                            $detailsImage[] = $name;
                        }
                        $detailsImage = implode(',', $detailsImage);
                    } else {
                        return response()->json([
                            'success' => false,
                            'message' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted",
                        ], 200);
                    }
                }
                if ($request->schedule_id) {
                    $schedule_id = $request->schedule_id;
                    $date = ActivitySchedule::where('id', $request->schedule_id)->first()->date;
                }
                // $date = date('Y-m-d H:i:s');
                $creator_id = Auth::user()->id;

                $client =  ActivityDetails::create([
                    'descriptions' => $request->description,
                    'patient_id' => $request->patient_id,
                    'schedule_id' => $schedule_id,
                    'type' => $request->type,
                    'date' => $date,
                    'images' => $detailsImage,
                    'creator_id' => $creator_id,
                    'created_at' => date('Y-m-d H:i:s'),
                ]);

                return response()->json([
                    'success' => true,
                    'message' => "Added successfuly",
                ], 200);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }

    //Edit Activity Note Chart Narrative
    public function editActivityDetails(Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'description' => 'required|string',
            ]);

            if ($validator->fails()) {
                return ['error' => 'Description required'];
            } else {
                // dd($request)
                $details = ActivityDetails::where('id', $request->details_id)->first();
                $details->descriptions = $request->description;
                $previousDocs = [];
                $detailsImage = [];
                if ($details->images) {
                    $detailsImg = explode(',', $details->images);
                }
                if ($request->docs) {
                    // $detailsImg = explode(',', $details->images);
                    $docs = explode(',', $request->docs);
                    // foreach ($detailsImg as $dimg) {
                    //     $delete = 0;
                    //     foreach ($docs as $d) {
                    //         if ($d != $dimg) {
                    //             $delete = 1;
                    //         }
                    //     }
                    //     if ($delete == 1) {
                    //         // unlink(public_path('/documents/Schedule_Details/' . $dimg));
                    //     }
                    // }
                    $previousDocs = implode(',', $docs);
                }

                if ($request->docs != "" && $detailsImg != "") {
                    $newDocs = $docs;
                    $oldDocs = $detailsImg;
                    // dd(array_diff($oldDocs , $newDocs));
                    $diffDocs = array_diff($oldDocs, $newDocs);
                    // dd($diffDocs);
                    foreach ($diffDocs as $diffDoc) {
                        unlink(public_path('/documents/Schedule_Details/' . $diffDoc));
                    }
                } elseif ($details->images != "" && $request->docs == "") {
                    foreach ($detailsImg as $deImg) {
                        unlink(public_path('/documents/Schedule_Details/' . $deImg));
                    }
                }

                if ($request->hasfile('detailsImage')) {
                    $files = $request->file('detailsImage');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "DNG" || $file_extention == "dng" || $file_extention == "jpg" || $file_extention == "JPG" || $file_extention == "png" || $file_extention == "PNG" || $file_extention == "jpeg" || $file_extention == "JPEG" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }
                    if ($count ==  0) {
                        foreach ($files as $file) {
                            $name =  $details->patient_id . '_' . $details->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path('/documents/Schedule_Details/'), $name);
                            $detailsImage[] = $name;
                        }
                        $detailsImage = implode(',', $detailsImage);
                        // $finalDocs = $detailsImage.",".$previousDocs;
                    } else {
                        return response()->json([
                            'success' => false,
                            'message' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted",
                        ], 200);
                    }
                }

                if ($previousDocs && $detailsImage == null) {
                    $details->images = $previousDocs;
                } elseif ($detailsImage && $previousDocs == null) {
                    $details->images = $detailsImage;
                } elseif ($detailsImage && $previousDocs) {
                    $details->images = $previousDocs . "," . $detailsImage;
                } else {
                    $details->images = null;
                }
                $details->update();


                return response()->json([
                    'success' => true,
                    'message' => "Updated successfuly",
                ]);
            }
        } catch (\Throwable $th) {

            return response()->json([
                'success' => false,
                'message' => $th->getMessage(),
            ]);
        }
    }
}
