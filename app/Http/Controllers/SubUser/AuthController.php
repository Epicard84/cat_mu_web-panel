<?php

namespace App\Http\Controllers\SubUser;

use App\Http\Controllers\Controller;
use App\Services\SubUserServices;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function loginAuth(Request $request){
        $response = SubUserServices::subUserLogin($request);
        

        if(array_key_exists('error',$response)){
            return response()->json([
                'success' => false,
                'message' => $response['error'],
                'data' => []
            
            ]);
        }else{
            return response()->json([
                'success' => true,
                'message' => "login Successfuly",
                'auth_token' => $response['token'],
                'data' => $response['success'] 
            ]);
        }
    }

    public function forgotPassword(Request $request){
        $response = SubUserServices::forgotPassword($request);
        if(array_key_exists('error',$response)){
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' =>[
                'email'=>$response['email']
            ]
        ]);

    }

    public function otpVerification(Request $request){
        $response = SubUserServices::otpVerification($request);
        if(array_key_exists('error',$response)){
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            
        ]);
    }

    public function newPassword(Request $request){
        $response = SubUserServices::newPassword($request);
        if(array_key_exists('error',$response)){
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
        ]);
    }
}
