<?php

namespace App\Http\Controllers\SubUser;

use App\Http\Controllers\Controller;
use App\Services\SubUserServices;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubUserApiController extends Controller
{
    // SubUser Details Dashboard
    public function dashboard()
    {
        $id = Auth::user()->id;
        $response = SubUserServices::dasboard($id);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data']
        ]);
    }

    //Client Details
    public function client_details($id){
        $response = SubUserServices::client_details($id);
        if (array_key_exists('error', $response)) {
            return response()->json([
                'success' => false,
                'message' => $response['error'],
            ]);
        }
        return response()->json([
            'success' => true,
            'message' => $response['success'],
            'data' => $response['data']
        ]);
    }
}
