<?php

namespace App\Http\Controllers\SubUser;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Doctor;
use App\Models\Guardian;
use App\Models\Location;
use App\Models\MasterUser\Client;
use App\Models\MasterUser\Medicine;
use App\Models\MasterUser\MedicineSchedule;
use App\Models\MasterUser\MedicineDetails;
use App\Models\MasterUser\Membership;
use App\Models\MasterUser\MFD;
use App\Models\User;
use App\Services\MasterUserServices;
use Carbon\Carbon;
use DateTime;
// use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
/* use Stevebauman\Location\Facades\Location as FacadesLocation; */
use Stripe;
use Illuminate\Support\Facades\Validator;
use App\Models\NotificationDetail;
use App\Models\Notification;
use App\Models\Reminder;
use App\Models\ReminderDetail;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\View;
use App\Models\MasterUser\Activity;
use App\Models\MasterUser\ActivitySchedule;
use App\Models\MasterUser\ActivityDetails;

use function PHPUnit\Framework\isNull;

class SubUserController extends Controller
{
    //
    public function current_locations($id)
    {
        $user_location = Location::where("user_id", $id)->first(); // Get Current Location from the table
        return $user_location->location;
    }
    public function suDashboard()
    {
        // dd(Auth::user());
        $client = Client::where('master_user', Auth::user()->master_user)->where('status', 1)->get();
        // $location = $this->current_locations(Auth::user()->id);
        $location = User::select('facility_address')->where('id',Auth::user()->master_user)->first();
        $date = date('Y-m-d');
        $masterUser = User::where('id', Auth::user()->master_user)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/dashboard', ['location' => $location, 'client' => $client, 'date' => $date, 'masterUser' => $masterUser, 'reminder' => $reminder]);
    }

    //MasterUser Profile
    public function masterProfile($id)
    {
        $masterDetails = User::where('id', $id)->first();
        $muLocation = Location::where('user_id', $id)->orderByDesc('id')->first();
        $archivedClients = Client::where('master_user', $id)->onlyTrashed()->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/master-profile', ['masterDetails' => $masterDetails, 'muLocation' => $muLocation, 'archivedClients' => $archivedClients,'reminder'=>$reminder]);
    }

    //Client Medication Page
    public function suClientMedication($id, $date) //SumonK
    {
        $id = Crypt::decryptString($id);
        $client_data = Client::where('id', $id)->first();

        $fro = date('Y-m-d', strtotime($date . '-2 day'));
        $to = date('Y-m-d', strtotime($date));

        if ($date == 0) {
            $date = date('d F Y');
            $fro = date('Y-m-d', strtotime($date . '-2 day'));
            $to = date('Y-m-d');
        }

        $nach = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', "!=", 0)->get();
        // dd($nach);
        $note = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->get();
        // dd($note);
        $preClient = Client::select('id')->where('master_user',Auth::user()->master_user)->where('id', '<', $id)->orderByDesc('id')->first();
        $nxtClient = Client::select('id')->where('master_user',Auth::user()->master_user)->where('id', '>', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/client-medication', ['data' => $client_data, 'date' => $date, 'patient_id' => $id, 'nach' => $nach, 'note' => $note, 'preClient' => $preClient, 'nxtClient' => $nxtClient,'reminder'=>$reminder]);
    }

    public function suClientMedicationUpdate($id)
    {
        // dd($id);
        $id = explode('_', $id);
        $medSchId = $id[0];
        $index = $id[1];
        $medSch = MedicineSchedule::where('id', $id)->first();
        $today = date('Y-m-d');
        $fromDate = date('Y-m-d', strtotime($today . '-2 day'));

        if ($medSch->date >= $fromDate || $medSch->date == $today && $medSch->status == 0) {

            $time = $medSch->complete_details;

            // $current_time = date("h:i");
            $current_time = strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1));

            if ($time == NULL) {
                $time = explode(',', $time);
                for ($i = 0; $i < $index; $i++) {
                    $time[$i] = null;
                }
                $time[$index] = $current_time;
                $updateSchedule = implode(',', $time);
            } else {
                $time = explode(',', $time);
                $time[$index] = $current_time;
                $updateSchedule = implode(',', $time);
            }
            // if ($time) {
            //     $updateSchedule = $time . ',' . $current_time;
            // } else {
            //     $updateSchedule = $current_time;
            // }

            $medSch->complete_details = $updateSchedule;
            $medSch->update();
            // dd($updateSchedule);
            // return response()->json('success')
            // return ['success' => "Medicine Schedule updated successfuly"];
            // dd($medSch);
            $med = explode(',', $medSch->complete_details); //String to Array Conversion of medicine complete timing
            // $medCompleteCount = count($med); // Count how may time are completed
            $medCompleteCount = 0;
            foreach ($med as $m) {
                if ($m != "" || $m != null) {
                    $medCompleteCount = (int)$medCompleteCount + 1;
                }
            }
            $medSchCount = Medicine::where('id', $medSch->medicine_id)->first()->time_days; //Find how many times are scheduled
            if ($medCompleteCount == $medSchCount) {
                $medSch->status = "1"; //if the count matched status will be updated.
                $medSch->update();
                NotificationDetail::medComNote($medSch->medicine_id, $today);
            }
            // dd("medCompleteCount ".$medCompleteCount."/"."medSchCount ".$medSchCount);

            return ['success' => "Hold on! Request on process..."];
        } else {
            // return ['error' => "Sorry, You've missed Scheduled Medicine."];
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

    //Complete medicine schedule Undo SumonK
    public function suClientMedicationUndo($id)
    {
        // dd($id);
        $id = explode('_', $id);
        $medSchId = $id[0];
        $index = $id[1];
        $today = date('Y-m-d');
        $fromDate = date('Y-m-d', strtotime($today . '-2 day'));
        $medSch = MedicineSchedule::where('id', $medSchId)->first();
        if ($medSch->date >= $fromDate || $medSch->date == $today) {
            if ($medSch->complete_details != "") {
                $medComDetails = $medSch->complete_details;
                $medComDetails = explode(',', $medComDetails);
                $medComDetails[$index] = "";
                // //
                // $checkedBy= explode('_',$medComDetails[$index]);
                // // $checkedById = $checkedByInitialId[1];
                // if($checkedBy[1] == Auth::user()->id){
                //     dd("Delete");
                // }
                // else{
                //     return ['error' => "Sorry, Your are not authorised to uncheck."];
                // }
                // //
                $medComUpdate = implode(',', $medComDetails);
                $medSch->complete_details = $medComUpdate;
                $medSch->status = "0";
                $medSch->update();

                return ['success' => "Hold on! Request on process..."];
            } else {
                return ['error' => "Sorry, Your request can't be processed."];
            }
        } else {
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

    //Medication SumonK
    public function suMedicationDetails($id)
    {
        // dd($id);
        $med_data = Medicine::where('id', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/medication-details', ['med_data' => $med_data,'reminder' => $reminder]);
    }

    //Medication Note SumonK
    public function suMedicationNote($schid, $pid)
    {
        // dd($schid." ".$pid);
        $pid = Crypt::decryptString($pid);
        $clientArchive = Client::where('id', $pid)->first();
        $med_shedule = MedicineSchedule::where('id', $schid)->first();
        $med_details = Medicine::where('id', $med_shedule->medicine_id)->first();
        $med_name = $med_details->medicine_name;
        $date = $med_shedule->date;
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $pid)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        // dd($med_name);
        // $date_note = MedicineDetails::where('date',$med_date)->where('patient_id',$pid)->where('type',0)->get();
        $date_note = MedicineDetails::where('patient_id', $pid)->where('schedule_id', $schid)->where('type', 0)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        // dd($date_note);
        return view('SubUser/client-medication-notes', ['med_name' => $med_name, 'date_note' => $date_note, 'patient_id' => $pid, 'med_shedule' => $med_shedule, 'form' => $form, 'patName' => $patName,'reminder'=>$reminder]);
    }

    //Medication Chart SumonK
    public function suMedicationChart($date, $id)
    {
        $date = Crypt::decryptString($date);
        $id = Crypt::decryptString($id);
        $clientArchive = Client::where('id', $id)->first();
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        // dd($date." ".$id);
        $date_chart = MedicineDetails::where('date', $date)->where('type', 1)->where('patient_id', $id)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/client-medication-chart', ['date_chart' => $date_chart, 'patient_id' => $id, 'date' => $date, 'form' => $form, 'patName' => $patName,'reminder'=>$reminder]);
    }

    //Medication Narrative SumonK
    public function suMedicationNarrative($date, $id)
    {
        $date = Crypt::decryptString($date);
        $id = Crypt::decryptString($id);
        $clientArchive = Client::where('id', $id)->first();
        // dd($date." ".$id);
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        $date_narrative = MedicineDetails::where('date', $date)->where('type', 2)->where('patient_id', $id)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        // dd($date_narrative);
        return view('SubUser/client-medication-narrative', ['date_narrative' => $date_narrative, 'patient_id' => $id, 'date' => $date, 'form' => $form, 'patName' => $patName,'reminder'=>$reminder]);
    }

    //Medication Chart SumonK
    public function suMedicationAllChart($id)
    {
        $id = Crypt::decryptString($id);
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        // dd($date." ".$id);
        $date = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime('-1 year'));
        $date_chart = MedicineDetails::where('patient_id', $id)->where('date','>=',$endDate)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/client-all-chart', ['date_chart' => $date_chart, 'patient_id' => $id, 'date' => $date, 'patName' => $patName,'reminder'=>$reminder]);
    }

    //Add new medication details Charts Narratives Notes SumonK
    public function suNewDetails(Request $request)
    {
        // dd($request->detailsImage);

        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
            'patient_id' => 'required',
            'type' => 'required',
            // 'date' => 'required',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'Description required'];
        } else {
            $detailsImage = null;
            $schedule_id = null;
            $date = date('Y-m-d');
            if ($request->hasfile('detailsImage')) {
                $files = $request->file('detailsImage');
                $count = 0;

                foreach ($files as $file) {
                    $file_extention = $file->getClientOriginalExtension();

                    if($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx"){
                       
                    }
                    else{
                        $count++;
                    }
                }

                if ($count ==  0) {
                    foreach ($files as $file) {
                        $name =  $request->patient_id . '_' . $request->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/documents/Schedule_Details/'), $name);
                        $detailsImage[] = $name;
                    }
                    $detailsImage = implode(',', $detailsImage);
                } else {
                    return ['error' => "Invalid flie type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                }
            }
            if ($request->schedule_id) {
                $schedule_id = $request->schedule_id;
                $date = MedicineSchedule::where('id', $request->schedule_id)->first()->date;
            }
            // $date = date('Y-m-d H:i:s');
            $creator_id = Auth::user()->id;

            $clientdetails =  MedicineDetails::create([
                'descriptions' => $request->description,
                'patient_id' => $request->patient_id,
                'schedule_id' => $schedule_id,
                'type' => $request->type,
                'date' => $date,
                'images' => $detailsImage,
                'creator_id' => $creator_id,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            if ($clientdetails) {
                NotificationDetail::addDetails($creator_id, $request->patient_id, $date, $request->type, $schedule_id);
            }

            return ['success' => "Added successfuly"];
        }
    }

    //Edit Notes Charts Narratives
    public function suEditDetailsForm(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'Description required'];
        } else {
            $details = MedicineDetails::where('id', $request->details_id)->first();
            $details->descriptions = $request->description;
            $previousDocs = [];
            $detailsImage = [];
            if($details->images){
                $detailsImg = explode(',', $details->images);
                }
            if ($request->docs) {
                $previousDocs = implode(',', $request->docs);
            }
            if ($request->docs != "" && $detailsImg !="") {
                $newDocs = $request->docs;
                $oldDocs = $detailsImg;
                // dd(array_diff($oldDocs , $newDocs));
                $diffDocs = array_diff($oldDocs , $newDocs);
                // dd($diffDocs);
                foreach ($diffDocs as $diffDoc) {
                    unlink(public_path('/documents/Schedule_Details/' . $diffDoc));
                }
            } elseif($details->images != "" && $request->docs == "") {
                foreach ($detailsImg as $deImg) {
                    unlink(public_path('/documents/Schedule_Details/' . $deImg));
                }
            }

            if ($request->hasfile('detailsImage')) {
                $files = $request->file('detailsImage');
                $count = 0;

                foreach ($files as $file) {
                    $file_extention = $file->getClientOriginalExtension();

                    if($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx"){
                       
                    }
                    else{
                        $count++;
                    }
                }
                if ($count ==  0) {
                foreach ($files as $file) {
                    $name =  $details->patient_id . '_' . $details->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('/documents/Schedule_Details/'), $name);
                    $detailsImage[] = $name;
                }
                $detailsImage = implode(',', $detailsImage);
                // $finalDocs = $detailsImage.",".$previousDocs;
            } else {
                return ['error' => "Invalid flie type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
            }
            }

            if ($previousDocs && $detailsImage == null) {
                $details->images = $previousDocs;
            } elseif ($detailsImage && $previousDocs == null) {
                $details->images = $detailsImage;
            } elseif ($detailsImage && $previousDocs) {
                $details->images = $previousDocs . "," . $detailsImage;
            } else {
                $details->images = null;
            }
            $details->update();

            return ['success' => "Updated successfuly"];
        }
    }

    public function suMfd()
    {
        $masterUser = User::where('id', Auth::user()->id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();

        $mfds = MFD::where('master_user_id', Auth::user()->master_user)->orderBy('id', 'DESC')->get();
        // dd($mfds);
        $latest_mfd_date = 0;
        if (!$mfds) {
            $latest_mfd_date = date('Y-m', strtotime(MFD::where('master_user_id', Auth::user()->id)->orderBy('id', 'DESC')->first()->created_at));
            // dd($latest_mfd_date);
        }

        $today_date = date('Y-m');
        $mfd_btn = '0';

        if ($latest_mfd_date == $today_date) {
            $mfd_btn = '1';
        }
        return view('SubUser/mfd', ['masterUser' => $masterUser, 'mfds' => $mfds, 'mfd_btn' => $mfd_btn,'reminder'=>$reminder]);
    }


    //Notification SumonK
    public function suNotifications()
    {
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        $getNotifications = Notification::where('receiver_id', Auth::user()->id)->where('status', 0)->orderByDesc('id')->get();
        return view('SubUser/notifications', ['getNotifications' => $getNotifications,'reminder'=>$reminder]);
    }

    public function suNotificationSeen($notiId)
    {
        $notificationDetails = Notification::where('id', $notiId)->first();
        $notificationDetails->status = 1;
        $notificationDetails->update();

        return response()->json("seen");
    }

    //Get Reminder
    public function suReminder()
    {
        $date = date('Y-m-d');
        $check = 1;
        $reminders = Reminder::where('masteruser_id', Auth::user()->master_user)->where('date', $date)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/reminder', ['reminders' => $reminders, 'date' => $date, 'check' => $check,'reminder'=>$reminder]);
    }

    //Filter Reminder By Date
    public function suFilterReminder($date)
    {
        $date = date('Y-m-d', strtotime($date));
        $reminders = Reminder::where('masteruser_id', Auth::user()->master_user)->where('date', $date)->orderByDesc('id')->get();
        $check = 0;
        if ($date == date('Y-m-d')) {
            $check = 1;
        }
        $html = View::make('SubUser/reminder-render', compact("reminders", "check"))->render();
        return response()->json(['html' => $html]);
    }

    //Check Reminder
    public function suCheckReminder($rid)
    {
        $date = date('Y-m-d');
        $ReminderDetails = Reminder::where('id', $rid)->first();
        if ($ReminderDetails->date == $date) {
            $ReminderDetails->status = 1;
            $ReminderDetails->update();

            return ['success' => "Hold on! Request on process..."];
        } else {
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

    //Delete Reminder
    public function suDeleteReminder(Request $request)
    {
        Reminder::where('id', $request->id)->delete();

        response()->json(['success' => 'Reminder deleted successfully']);
    }

    //New Reminder
    public function suNewReminder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'reminder_details' => 'required',
            'reminder_date' => 'required',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'Details required'];
        } else {
            $creator_id = Auth::user()->master_user;

            $reminderdetails =  Reminder::create([
                'masteruser_id' => Auth::user()->master_user,
                'reminder' => $request->reminder_details,
                'date' => date('Y-m-d', strtotime($request->reminder_date)),
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            return ['success' => "Added successfuly"];
        }
    }

    //Change Passcode
    public function suChangePassword(){
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/change-password',['reminder'=>$reminder]);
    }

    //Update Passcode
    public function updatePasscode(Request $request){
        // dd($request->all());
        $validator = Validator::make($request->all(), [
            'digit1' => 'required',
            'digit2' => 'required',
            'digit3' => 'required',
            'digit4' => 'required',
            'digit5' => 'required',
            'digit6' => 'required',
            'digit7' => 'required',
            'digit8' => 'required',
            'digit9' => 'required',
            'digit10' => 'required',
            'digit11' => 'required',
            'digit12' => 'required',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'All fields required'];
        } else {
            // dd("hello");
            $oldPasscode = $request->digit1.$request->digit2.$request->digit3.$request->digit4;
            $newPasscode = $request->digit5.$request->digit6.$request->digit7.$request->digit8;
            $confirmPasscode = $request->digit9.$request->digit10.$request->digit11.$request->digit12;

            if($newPasscode == $confirmPasscode){
                if($newPasscode == $oldPasscode){
                    return ['error' => "Old Passcode and New Passcode should be different"];
                }else{
                    // dd($request->suID);
                    // if($sub->master_user == $muID && Hash::check($passcode, $sub->password)){
                    //     $found=1;
                    // }
                    // $suDetail = User::where('id',$request->suID)->first();
                    if(Hash::check($oldPasscode, Auth::user()->password)){
                        // dd('Correct Old Passcode');
                        $subUsers = User::where('master_user',Auth::user()->master_user)->get();
                        // dd($subUsers);
                        $count = 0;
                        foreach($subUsers as $s){
                            if(Hash::check($newPasscode, $s->password)){
                                $count++;
                            }
                        }
                        if($count == 0){
                            $suDetail = User::where('id',Auth::user()->id)->first();
                            $suDetail->password = Hash::make($newPasscode);
                            $suDetail->update();
                            return ['success' => 'Passcode updated successfully!'];
                        }
                        else{
                            return ['error' => 'Passcode is not available! Please try with another passcode'];
                        }
                    }else{
                        return ['error' => 'Wrong Old Passcode! Please enter correct passcode'];
                    }
                }
            }else{
                return ['error' => 'New Passcode and Confirm Passcode should be matched'];
            }
        }
    }
    //Client Activity Page
    public function suClientActivity($id, $date) //SumonK
    {
        $id = Crypt::decryptString($id);
        $client_data = Client::where('id', $id)->first();

        $fro = date('Y-m-d', strtotime($date . '-2 day'));
        $to = date('Y-m-d', strtotime($date));

        if ($date == 0) {
            $date = date('d F Y');
            $fro = date('Y-m-d', strtotime($date . '-2 day'));
            $to = date('Y-m-d');
        }

        // $nach = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', "!=", 0)->get();
        // dd($nach);
        $nach = [];
        $note = ActivityDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->get();
        // dd($note);
        // $note = [];
        $preClient = Client::select('id')->where('master_user',Auth::user()->master_user)->where('id', '<', $id)->orderByDesc('id')->first();
        $nxtClient = Client::select('id')->where('master_user',Auth::user()->master_user)->where('id', '>', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
        return view('SubUser/client-activity', ['data' => $client_data, 'date' => $date, 'patient_id' => $id, 'nach' => $nach, 'note' => $note, 'preClient' => $preClient, 'nxtClient' => $nxtClient,'reminder'=>$reminder]);
    }

    public function suClientActivityUpdate($id)
    {
        // dd($id);
        $id = explode('_', $id);
        $medSchId = $id[0];
        $index = $id[1];
        $medSch = ActivitySchedule::where('id', $medSchId)->first();
        $today = date('Y-m-d');

        if ($medSch->date == $today && $medSch->status == 0) {

            $time = $medSch->complete_details;

            // $current_time = date("h:i");
            $current_time = strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1));

            if ($time == NULL) {
                $time = explode(',', $time);
                for ($i = 0; $i < $index; $i++) {
                    $time[$i] = null;
                }
                $time[$index] = $current_time;
                $updateSchedule = implode(',', $time);
            } else {
                $time = explode(',', $time);
                $time[$index] = $current_time;
                $updateSchedule = implode(',', $time);
            }
            // if ($time) {
            //     $updateSchedule = $time . ',' . $current_time;
            // } else {
            //     $updateSchedule = $current_time;
            // }

            $medSch->complete_details = $updateSchedule;
            $medSch->update();
            // dd($updateSchedule);
            // return response()->json('success')
            // return ['success' => "Medicine Schedule updated successfuly"];
            // dd($medSch);
            // $med = explode(',', $medSch->complete_details); //String to Array Conversion of medicine complete timing
            // $medCompleteCount = count($med); // Count how may time are completed
            // $medCompleteCount = 0;
            // foreach ($med as $m) {
            //     if ($m != "" || $m != null) {
            //         $medCompleteCount = (int)$medCompleteCount + 1;
            //     }
            // }
            // $medSchCount = Medicine::where('id', $medSch->medicine_id)->first()->time_days; //Find how many times are scheduled
            // if ($medCompleteCount == $medSchCount) {
            //     $medSch->status = "1"; //if the count matched status will be updated.
            //     $medSch->update();
            //     NotificationDetail::medComNote($medSch->medicine_id, $today);
            // }
            // dd("medCompleteCount ".$medCompleteCount."/"."medSchCount ".$medSchCount);

            return ['success' => "Hold on! Request on process..."];
        } else {
            // return ['error' => "Sorry, You've missed Scheduled Medicine."];
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

        //Complete activity schedule Undo SumonK
public function suClientActivityUndo($id)
{
    // dd($id);
    $id = explode('_', $id);
    $medSchId = $id[0];
    $index = $id[1];
    $today = date('Y-m-d');
    $medSch = ActivitySchedule::where('id', $medSchId)->first();
    if ($medSch->date == $today) {
        if ($medSch->complete_details != "") {
            $medComDetails = $medSch->complete_details;
            $medComDetails = explode(',', $medComDetails);
            $medComDetails[$index] = "";
            // //
            // $checkedBy= explode('_',$medComDetails[$index]);
            // // $checkedById = $checkedByInitialId[1];
            // if($checkedBy[1] == Auth::user()->id){
            //     dd("Delete");
            // }
            // else{
            //     return ['error' => "Sorry, Your are not authorised to uncheck."];
            // }
            // //
            $medComUpdate = implode(',', $medComDetails);
            $medSch->complete_details = $medComUpdate;
            $medSch->status = "0";
            $medSch->update();

            return ['success' => "Hold on! Request on process..."];
        } else {
            return ['error' => "Sorry, Your request can't be processed."];
        }
    } else {
        return ['error' => "Sorry, Your request can't be processed."];
    }
}

    //Activity SumonK
    public function suActivityDetails($id)
    {
        // dd($id);
        $med_data = Activity::where('id', $id)->first();
        return view('SubUser/activity-details', ['med_data' => $med_data]);
    }

                //Activity Note SumonK
                public function suActivityNote($schid, $pid)
                {
                    // dd($schid." ".$pid);
                    $pid = Crypt::decryptString($pid);
                    $clientArchive = Client::where('id', $pid)->first();
                    $act_shedule = ActivitySchedule::where('id', $schid)->first();
                    $med_details = Activity::where('id', $act_shedule->activity_id)->first();
                    $act_name = $med_details->activity_name;
                    $date = $act_shedule->date;
                    $today = date('Y-m-d');
                    $patDetails = Client::where('id', $pid)->first();
                    $patName = $patDetails->first_name . " " . $patDetails->last_name;
                    $form = 0;
                    if ($date == $today && $clientArchive != null) {
                        $form = 1;
                    }
                    // dd($med_name);
                    // $date_note = MedicineDetails::where('date',$med_date)->where('patient_id',$pid)->where('type',0)->get();
                    $date_note = ActivityDetails::where('patient_id', $pid)->where('schedule_id', $schid)->where('type', 0)->orderByDesc('id')->get();
                    $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id',Auth::user()->master_user)->where('status', 0)->count();
                    // dd($date_note);
                    return view('SubUser/client-activity-notes', ['act_name' => $act_name, 'date_note' => $date_note, 'patient_id' => $pid, 'act_shedule' => $act_shedule, 'form' => $form, 'patName' => $patName,'reminder'=>$reminder]);
                }
            
                    //Add new activity details Charts Narratives Notes SumonK
                    public function suNewActivityDetails(Request $request)
                    {
                        // dd($request->detailsImage);
                
                        $validator = Validator::make($request->all(), [
                            'description' => 'required|string',
                            'patient_id' => 'required',
                            'type' => 'required',
                            // 'date' => 'required',
                        ]);
                
                        if ($validator->fails()) {
                            // return ['error' => $validator->errors()];
                            return ['error' => 'Description required'];
                        } else {
                            $detailsImage = null;
                            $schedule_id = null;
                            $date = date('Y-m-d');
                            if ($request->hasfile('detailsImage')) {
                                $files = $request->file('detailsImage');
                                $count = 0;
                
                                foreach ($files as $file) {
                                    $file_extention = $file->getClientOriginalExtension();
                
                                    if($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx"){
                                       
                                    }
                                    else{
                                        $count++;
                                    }
                                }
                
                                if ($count ==  0) {
                                    foreach ($files as $file) {
                                        $name =  $request->patient_id . '_' . $request->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                                        $file->move(public_path('/documents/Schedule_Details/'), $name);
                                        $detailsImage[] = $name;
                                    }
                                    $detailsImage = implode(',', $detailsImage);
                                } else {
                                    return ['error' => "Invalid flie type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                                }
                            }
                            if ($request->schedule_id) {
                                $schedule_id = $request->schedule_id;
                                $date = ActivitySchedule::where('id', $request->schedule_id)->first()->date;
                            }
                            // $date = date('Y-m-d H:i:s');
                            $creator_id = Auth::user()->id;
                
                            $clientdetails =  ActivityDetails::create([
                                'descriptions' => $request->description,
                                'patient_id' => $request->patient_id,
                                'schedule_id' => $schedule_id,
                                'type' => $request->type,
                                'date' => $date,
                                'images' => $detailsImage,
                                'creator_id' => $creator_id,
                                'created_at' => date('Y-m-d H:i:s'),
                            ]);
                
                            // if ($clientdetails) {
                            //     NotificationDetail::addDetails($creator_id, $request->patient_id, $date, $request->type, $schedule_id);
                            // }
                
                            return ['success' => "Added successfuly"];
                        }
                    }
            
                        //Edit Notes Charts Narratives
                public function suEditActivityDetailsForm(Request $request)
                {
                    // dd($request);
                    $validator = Validator::make($request->all(), [
                        'description' => 'required|string',
                    ]);
            
                    if ($validator->fails()) {
                        // return ['error' => $validator->errors()];
                        return ['error' => 'Description required'];
                    } else {
                        $details = ActivityDetails::where('id', $request->details_id)->first();
                        $details->descriptions = $request->description;
                        $previousDocs = [];
                        $detailsImage = [];
                        if($details->images){
                            $detailsImg = explode(',', $details->images);
                            }
                        if ($request->docs) {
                            $previousDocs = implode(',', $request->docs);
                        }
                        if ($request->docs != "" && $detailsImg !="") {
                            $newDocs = $request->docs;
                            $oldDocs = $detailsImg;
                            // dd(array_diff($oldDocs , $newDocs));
                            $diffDocs = array_diff($oldDocs , $newDocs);
                            // dd($diffDocs);
                            foreach ($diffDocs as $diffDoc) {
                                unlink(public_path('/documents/Schedule_Details/' . $diffDoc));
                            }
                        } elseif($details->images != "" && $request->docs == "") {
                            foreach ($detailsImg as $deImg) {
                                unlink(public_path('/documents/Schedule_Details/' . $deImg));
                            }
                        }
            
                        if ($request->hasfile('detailsImage')) {
                            $files = $request->file('detailsImage');
                            $count = 0;
            
                            foreach ($files as $file) {
                                $file_extention = $file->getClientOriginalExtension();
            
                                if($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx"){
                                   
                                }
                                else{
                                    $count++;
                                }
                            }
                            if ($count ==  0) {
                            foreach ($files as $file) {
                                $name =  $details->patient_id . '_' . $details->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                                $file->move(public_path('/documents/Schedule_Details/'), $name);
                                $detailsImage[] = $name;
                            }
                            $detailsImage = implode(',', $detailsImage);
                            // $finalDocs = $detailsImage.",".$previousDocs;
                        } else {
                            return ['error' => "Invalid flie type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                        }
                        }
            
                        if ($previousDocs && $detailsImage == null) {
                            $details->images = $previousDocs;
                        } elseif ($detailsImage && $previousDocs == null) {
                            $details->images = $detailsImage;
                        } elseif ($detailsImage && $previousDocs) {
                            $details->images = $previousDocs . "," . $detailsImage;
                        } else {
                            $details->images = null;
                        }
                        $details->update();
            
                        return ['success' => "Updated successfuly"];
                    }
                }
}
