<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller
{
    
    //Admin Login
    public function login(){
        return view('SuperAdmin.auth-login');
    }

    //Admin Login authentication
    public function loginAuth(Request $request){
            $login_credentials = [
                'email' => $request->email,
                'password' => $request->password
            ];

            if(Auth::attempt($login_credentials,$request->remember)){
                if(Auth::user()->user_type == 0){
                    return redirect(route('admin_dashboard'));
                }else{
                    Auth::logout();
                    return back()->with([
                        'error' => 'You are not authorized'
                     ]);
                }
            }else{
                return back()->with([
                    'error' => 'Invaid credentials'
                 ]);
                // $this->error('Invaid credentials');
            }
        }
 
    
    //Admin Logout
    public function logout(){
        Auth::logout();
        return redirect('superAdmin/admin');
    
    }
}
