<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use App\Mail\WelcomeMail;
use App\Models\MasterUser\Client;
use App\Models\Record;
use App\Models\User;
use GuzzleHttp\Promise\Create;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\DataTables;
/* use Stevebauman\Location\Facades\Location; */
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use App\Models\AccountReason;
use App\Models\Location as ModelsLocation;

class AdminController extends Controller
{
    //
    public function userDetails($id)
    {
        $user =  User::where('id', $id)->first();
        if ($user) {
            return $user;
        } else {
            return "no data found";
        }
    }

    //admin dashboard
    public function adminDashboard()
    {
        // dd("hello");
        $total_master_user = User::where('user_type', 1)->count();
        $total_sub_user = User::where('user_type', 2)->count();
        $total_client = Client::count();
        $masterUsers = User::where('user_type', 1)->orderByDesc('id')->limit(5)->get();
        $subUsers = User::where('user_type', 2)->orderByDesc('id')->limit(5)->get();
        $clients = Client::orderByDesc('id')->limit(5)->get();
        return view('SuperAdmin.dashboard', ['total_master_user' => $total_master_user, 'total_sub_user' => $total_sub_user, 'total_client' => $total_client, 'masterUsers' => $masterUsers, 'subUsers' => $subUsers, 'clients' => $clients]);
    }

    //master user 
    public function masterUsers(Request $request)
    {

        $master_users = User::where('user_type', 1)->orderBy('id', 'DESC')->get();

        if ($request->ajax()) {

            return Datatables::of($master_users)->addIndexColumn()


                ->addColumn('customstatus', function ($master_users) {
                    if ($master_users->status == 'online') {
                        if (is_null($master_users->profile_image_name)) {
                            $button = '<div class="avatar bg-warning me-3"><img src="https://mychartspace.com/public/images/MasterUserProfile/defult_image.png" alt="" srcset="">
                            <span class="avatar-status bg-success"></span></div>';
                        } else {

                            $button = '<div class="avatar bg-warning me-3"><img src="https://mychartspace.com/images/MasterUserProfile/' . $master_users->profile_image_name . '" alt="" srcset="">
                            <span class="avatar-status bg-success"></span></div>';
                        }
                    } else {
                        if (is_null($master_users->profile_image_name)) {
                            $button = '<div class="avatar bg-warning me-3"><img src="https://mychartspace.com/public/images/MasterUserProfile/defult_image.png" alt="" srcset="">
                        <span class="avatar-status bg-danger"></span> </div>';
                        } else {
                            $button = '<div class="avatar bg-warning me-3"><img src="https://mychartspace.com/images/MasterUserProfile/' . $master_users->profile_image_name . '" alt="" srcset="">
                        <span class="avatar-status bg-danger"></span> </div>';
                        }
                    }
                    return $button;
                })
                ->addColumn('customname', function ($master_users) {
                    $button = $master_users->first_name . ' ' . $master_users->last_name;

                    return $button;
                })
                ->addColumn('action', function ($master_users) {
                    $button = '<label for="" id="edit" class="btn icon btn-info round edit" data-id=' . $master_users->id . ' data-bs-toggle="modal" data-bs-target="#editmasteruser"><i class="fa-solid fa-user-pen"></i></label>';
                    $button .= '&nbsp;<label name ="delete" id="delete" data-id="' . $master_users->id . '" class="btn icon btn-danger round delete"><i class="fa-solid fa-trash-can"></i></label>';
                    $button .= '&nbsp; <a href="about_master_user/' . $master_users->id . '"><label name ="about" id="about" data-id="' . $master_users->id . '" class="btn icon btn-primary round about"><i class="fa-solid fa-eye"></i></label>';

                    return $button;
                })
                ->rawColumns(['action', 'customstatus'])
                ->make(true);
        }

        return view('SuperAdmin.master-users', ['master_users' => $master_users]);
    }

    //Sub user 
    public function subUsers(Request $request)
    {

        $sub_users = User::where('user_type', 2)->orderBy('id', 'DESC')->get();
        // dd($sub_users);

        if ($request->ajax()) {

            return Datatables::of($sub_users)->addIndexColumn()

                ->addColumn('subuser_name', function ($sub_users) {
                    $subusername = $sub_users->first_name . " " . $sub_users->last_name;

                    return $subusername;
                })
                ->addColumn('subuser_email', function ($sub_users) {
                    $subuseremail = $sub_users->email;

                    return $subuseremail;
                })
                ->addColumn('masteruser_name', function ($sub_users) {

                    $masterusername = $sub_users->MasterDetails->first_name . " " . $sub_users->MasterDetails->last_name;

                    return $masterusername;
                })
                ->addColumn('masteruser_email', function ($sub_users) {
                    // $mu = User::where('id', $sub_users->master_user)->first();
                    $masteruseremail = $sub_users->MasterDetails->email;

                    return $masteruseremail;
                })
                ->rawColumns(['subuser_name', 'subuser_email', 'masteruser_name', 'masteruser_email'])
                ->make(true);
        }

        return view('SuperAdmin.sub-users', ['sub_users' => $sub_users]);
    }

    //Clients
    public function clients(Request $request)
    {

        $clients = Client::orderByDesc('id')->get();
        // dd($clients);

        if ($request->ajax()) {

            return Datatables::of($clients)->addIndexColumn()

                ->addColumn('client_name', function ($clients) {
                    $clientname = $clients->first_name . " " . $clients->last_name;

                    return $clientname;
                })
                ->addColumn('client_dob', function ($clients) {
                    $clientdob = date('m-d-Y', strtotime($clients->dob));

                    return $clientdob;
                })
                ->addColumn('client_gender', function ($clients) {

                    $clientgender = ucfirst($clients->gender);
                    return $clientgender;
                })
                ->addColumn('client_status', function ($clients) {
                    if ($clients->status == 1) {
                        $clientstatus = "Active";
                    } else {
                        $clientstatus = "Discharged";
                    }
                    return $clientstatus;
                })
                ->addColumn('client_muName', function ($clients) {
                    // $mu = User::where('id',$sub_users->master_user)->first();
                    $clientmasterusername = $clients->getMasterDetails->first_name . " " . $clients->getMasterDetails->last_name;

                    return $clientmasterusername;
                })
                ->addColumn('client_muEmail', function ($clients) {
                    // $mu = User::where('id',$sub_users->master_user)->first();
                    $clientmasteruseremail = $clients->getMasterDetails->email;

                    return $clientmasteruseremail;
                })
                ->rawColumns(['client_name', 'client_dob', 'client_gender', 'client_status', 'client_muName', 'client_muEmail'])
                ->make(true);
        }

        return view('SuperAdmin.clients', ['clients' => $clients]);
    }

    public function masterUserValidator(Request $request)
    {
        // return response()->json([$request->email,$request->phone_number]);
        $validator = Validator::make($request->all(), [
            'email' => 'unique:users,email',
            'phone_number' => 'unique:users,phone_number',
            'password' => 'min:6|max:10'
        ]);

        if ($validator->fails()) {
            return response()->json(json_decode(json_encode($validator->errors()), true));
        } else {
            return response()->json("no errror");
        }
    }


    //Add master user 
    public function addMasterUser(Request $request)
    {

        if ($request->hasFile('profile_image')) {
            if ($request->file('profile_image')->isValid()) {

                $file = $request->file('profile_image');
                $profile_image_name = uniqid() . '.' . $file->getClientOriginalExtension();
                $request->file('profile_image')->move("images/MasterUserProfile/", $profile_image_name);
            }
        } else {
            $profile_image_name = null;
        }

        $user = User::create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'phone_number' => $request->phone_number,
            'company_name' => $request->company_name,
            'city' => $request->city,
            'country' => $request->country,
            'zip_code' => $request->zip_code,
            'state' => $request->state,
            'status' => 'offline',
            'permanent_address' => $request->permanent_address,
            'facility_address' => $request->facility_address,
            'profile_image_name' => $profile_image_name,
            'password' => Hash::make($request->password),
            'user_type' => 1,
            'created_at' => date('Y-m-d H:i:s'),
        ]);

        if ($user) {
            Mail::to($request->email)
                ->cc(Auth::user()->email)
                ->send(new WelcomeMail($user, $request->password));
            return response()->json("success");
        } else {
            return response()->json("error");
        }
    }

    //get user details
    public function getMasterUserDetails(Request $request)
    {
        $user = $this->userDetails($request->id);
        return response()->json($user);
    }

    //Edit master user details
    public function editMasterUser(Request $request)
    {
        $user = $this->userDetails($request->id);
        $array = [
            'first_name' => $request->edit_first_name,
            'last_name' => $request->edit_last_name,
            'company_name' => $request->edit_company_name,
            'city' => $request->edit_city,
            'country' => $request->edit_country,
            'zip_code' => $request->edit_zip_code,
            'state' => $request->edit_state,
            'permanent_address' => $request->edit_permanent_address,
            'facility_address' => $request->edit_facility_address,
            'subuser_limit' => $request->edit_subuser_limit,
            'updated_at' => date('Y-m-d H:i:s')
        ];

        if ($user->update($array)) {
            return response()->json('success');
        };
    }

    //delete master user 
    public function deleteMasterUser(Request $request)
    {
        $user = $this->userDetails($request->id);
        if ($user->delete()) {
            return response()->json('success');
        }
    }

    //Get user location testing function
    // public function location()
    // {
    //      $ip = '122.176.24.27'; /* Static IP address */
    //      $currentUserInfo = Location::get($ip);
    //     print_r($currentUserInfo);
    // }

    //Master user details
    public function aboutMasterUserDetails(Request $request, $id)
    {


        $master_user_data = $this->userDetails($id);
        // dd($request->ajax());

        $subUser_data = User::where('master_user', '=', $id)->get();

        $discharged_client = Client::where('master_user', '=', $id)->where('status', 0)->get();

        $client_data = Client::where('master_user', '=', $id)->where('status', 1)->get();
        // dd($table_data);
        if ($request->ajax()) {

            return Datatables::of($client_data)
                ->addIndexColumn()
                ->addColumn('clientName', function ($client_data) {
                    $clientname = $client_data->first_name . " " . $client_data->last_name;
                    return $clientname;
                })
                ->addColumn('dob', function ($client_data) {
                    $clientdob = date('m-d-Y', strtotime($client_data->dob));
                    return $clientdob;
                })
                ->addColumn('gender', function ($client_data) {
                    $clientgender = ucfirst($client_data->gender);
                    return $clientgender;
                })
                ->rawColumns(['clientName', 'dob', 'gender'])
                ->make(true);
        }

        $activeClientCount = Client::where('master_user', '=', $id)->count();
        $availableBeds = $master_user_data->total_beds - $activeClientCount;
        $location = ModelsLocation::where('user_id', $id)->first()->location;

        return view('SuperAdmin.master-user-details', ['master_user_data' => $master_user_data, 'subUsers' => $subUser_data, 'availableBeds' => $availableBeds, 'location' => $location, 'discharged_client' => $discharged_client]);
    }

    public function muLoginLocation($id)
    {
        $location = ModelsLocation::where('user_id', $id)->first();
        $lat = $location->latitude;
        $lon = $location->longitude;
        // $html = View::make('SuperAdmin/masteruser-location-map', compact("lat", "lon"))->render();
        // return response()->json(['html' => $html]);
        $user =  User::where('id', $id)->first();
        $userName = $user->first_name . " " . $user->last_name;
        return view('SuperAdmin/masteruser-location-map', ['lat' => $lat, 'lon' => $lon, 'userName' => $userName]);
    }

    public function allMuLoginLocation(Request $request)
    {
        // if ($request->ajax()) {
        //     $location = ModelsLocation::join('users', 'users.id', '=', 'locations.user_id')->select('locations.latitude', 'locations.longitude', 'locations.location', 'users.id', 'users.first_name', 'users.last_name')->where('users.user_type',1)->get();
        //     return response()->json(['success' => true, 'location' => $location]);
        // }
        $addresses = User::select('id','first_name','last_name','facility_address')->where('user_type',1)->get();
        // dd($addresses);
        return view('SuperAdmin/all-masteruser-location-map',compact('addresses'));
    }


    public function clientDetails(Request $request)
    {
        $master_users = User::where('user_type', 1)->get();

        if ($request->ajax()) {

            return Datatables::of($master_users)->addIndexColumn()


                ->addColumn('customname', function ($master_users) {
                    $button = $master_users->first_name . ' ' . $master_users->last_name;

                    return $button;
                })
                ->addColumn('action', function ($master_users) {
                    $button = '<label for="" id="edit" class="btn icon btn-info round edit" data-id=' . $master_users->id . ' data-bs-toggle="modal" data-bs-target="#editmasteruser"><i class="fa-solid fa-hospital-user"></i></label>';
                    return $button;
                })
                ->rawColumns(['action', 'customname'])
                ->make(true);
        }

        return view('SuperAdmin.client-details');
    }

    public function blockAccount(Request $request)
    {
        $requests = AccountReason::orderBydesc('id')->get();

        if ($request->ajax()) {

            return Datatables::of($requests)->addIndexColumn()


                ->addColumn('muname', function ($requests) {
                    $name = $requests->getMasterUserDetails->first_name . ' ' . $requests->getMasterUserDetails->last_name;
                    // $name = "Hello";
                    return $name;
                })
                ->addColumn('muemail', function ($requests) {
                    $email = $requests->getMasterUserDetails->email;
                    // $name = "Hello";
                    return $email;
                })
                ->addColumn('action', function ($requests) {
                    // $button = '<label for="" id="edit" class="btn icon btn-info round edit" data-id=' . $master_users->id . ' data-bs-toggle="modal" data-bs-target="#editmasteruser"><i class="fa-solid fa-hospital-user"></i></label>';
                    // $button = "Hi";

                    $button = $requests->getMasterUserDetails->deleted_at;
                    if ($requests->getMasterUserDetails->deleted_at == null) {
                        $button = "Active";
                        $button = '<button type="button" class="btn btn-success btn-sm activemu" data-id="' . $requests->masteruser_id . '">Active</button>';
                    } else {
                        $button = '<button type="button" class="btn btn-warning btn-sm blockedmu" data-id="' . $requests->masteruser_id . '">Blocked</button>';
                    }
                    return $button;
                })
                ->rawColumns(['action', 'muname', 'muemail'])
                ->make(true);
        }

        return view('SuperAdmin.block-account');
    }

    //delete master user 
    public function restoreMasterUser(Request $request)
    {
        // dd($request->id);
        $user = User::where('id', $request->id)->withTrashed()->first();
        // dd($user);
        if ($user->restore()) {
            return response()->json('success');
        }
    }
}
