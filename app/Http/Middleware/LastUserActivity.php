<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Models\User;
use App\Models\Location;
use App\Services\MasterUserServices;

class LastUserActivity
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */

    public function handle(Request $request, Closure $next)
    {
        if (Auth::check()) {
            if (Auth::user()->user_type != 0) {

                $location_details = Location::where('user_id', Auth::user()->id)->first();
                if ($location_details->location != MasterUserServices::location()) {
                    $location_details->location = MasterUserServices::location()->cityName . ', ' . MasterUserServices::location()->countryName;
                    $location_details->latitude = MasterUserServices::location()->latitude;
                    $location_details->longitude = MasterUserServices::location()->longitude;
                    //$location_details->update();
                }
            }
        }
        return $next($request);
    }
}
