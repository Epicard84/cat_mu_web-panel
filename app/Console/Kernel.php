<?php

namespace App\Console;
use App\Console\Commands\Medicine;
use App\Console\Commands\MakeReminder;
use App\Console\Commands\ReminderMedicine;
use App\Console\Commands\DeleteReminder;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    protected $commands = [
        Medicine::class,
        // DeleteReminder::class,
        // MedicineReminder::class,
        // ReminderMedicine::class,
    ];
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('Medicine')->everyMinute();  //Kushal
        // $schedule->command('medicine-reminder')->everyFiveMinutes();    //SumonK
        // $schedule->command('delete-reminder')->everyFiveMinutes();   //SumonK       //Check and Delete Reminder
        $schedule->command('Medicine')->dailyAt('23:50');  //SumonK     //Generate Medicine for next day
        $schedule->command('DeleteReminder')->dailyAt('00:15');  //SumonK     //Delete all reminders of pervious day
        // $schedule->command('Medicine')->dailyAt('11:59');  //SumonK     //Generate Medicine for next day
        // $schedule->command('medicine-reminder')->dailyAt('04:05'); //SumonK  // Generate Reminder for scheduled medicine
        // $schedule->command('MakeReminder')->dailyAt('04:15'); //SumonK // Generate Reminder for scheduled medicine
        $schedule->command('ReminderMedicine')->hourly()->storeOutput(storage_path('logs'));  //SumonK     //Generate Reminder for Medicine if uncheck
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
