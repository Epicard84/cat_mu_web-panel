<?php

namespace App\Console\Commands;

use App\Models\MasterUser\Medicine as MasterUserMedicine;
use App\Models\MasterUser\MedicineSchedule;
use Illuminate\Console\Command;
use App\Models\MasterUser\Activity as MasterUserActivity;
use App\Models\MasterUser\ActivitySchedule;

class Medicine extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'medicine';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $medicine_details = MasterUserMedicine::all();

        foreach ($medicine_details as  $value) {
            // if($value->end_date >= date('Y-m-d', strtotime(now())) && $value->status == 0){
            if($value->status == 0){
                $medicine_data = $value;
                // $end_date = date('Y-m-d', strtotime($medicine_data->end_date . ' + 1 days'));
                $end_date = date('Y-m-d', strtotime(now(). ' + 1 days'));
                $medicine_data->end_date = $end_date;
                $medicine_data->update();

                MedicineSchedule::create([
                    'medicine_id' => $value->id,
                    'scheduled_time' => $value->scheduled_time,
                    'date' => date('Y-m-d', strtotime($end_date)),
                    'status' => 0
                ]);
            }

        }

        //Activity scheduler
        $activity_details = MasterUserActivity::all();

        foreach ($activity_details as  $value) {
            // if($value->end_date >= date('Y-m-d', strtotime(now())) && $value->status == 0){
            if ($value->status == 0) {
                $activity_data = $value;
                // $end_date = date('Y-m-d', strtotime($medicine_data->end_date . ' + 1 days'));
                $end_date = date('Y-m-d', strtotime(now() . ' + 1 days'));
                $activity_data->end_date = $end_date;
                $activity_data->update();

                ActivitySchedule::create([
                    'activity_id' => $value->id,
                    'scheduled_time' => $value->scheduled_time,
                    'date' => date('Y-m-d', strtotime($end_date)),
                    'status' => 0
                ]);
            }
        }

        return Command::SUCCESS;
    }
}
