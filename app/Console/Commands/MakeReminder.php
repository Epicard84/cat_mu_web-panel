<?php

namespace App\Console\Commands;
use App\Models\MasterUser\Client;
use App\Models\MasterUser\Medicine;
use App\Models\Reminder;
use App\Models\ReminderDetail;
use App\Models\User;
use Illuminate\Support\Facades\DB;

use Illuminate\Console\Command;

class MakeReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'MakeReminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // return Command::SUCCESS;
        DB::table('reminders')->truncate();
        DB::table('reminder_details')->truncate();
        // return Command::SUCCESS;
        $date = date('Y-m-d');
        $activeMedicine = Medicine::where('end_date',$date)->get();

        foreach($activeMedicine as $med)
        {
            $patientDetails = Client::where('id',$med->client_id)->first();
            $patientName = $patientDetails->first_name." ".$patientDetails->last_name;
            $new_reminder = ReminderDetail::create([
                'patient_id' => $med->client_id,
                'medicine_id' => $med->id,
                'reminder' => $patientName."'s scheduled medicine ".$med->medicine_name." is scheduled for today.",
            ]);

            $mu = Client::where('id',$med->client_id)->first()->master_user;
            $su = User::where('master_user',$mu)->get();
            if($new_reminder){
                $reminderMU = Reminder::create([
                    'receiver_id' => $mu,
                    'reminder_id' => $new_reminder->id,
                ]);
                foreach($su as $s)
                {
                    $reminderSU = Reminder::create([
                        'receiver_id' => $s->id,
                        'reminder_id' => $new_reminder->id,
                    ]);
                }
            }
        }
    }
}
