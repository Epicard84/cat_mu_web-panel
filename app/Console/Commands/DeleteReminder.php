<?php

namespace App\Console\Commands;

use App\Models\MasterUser\MedicineSchedule;
use App\Models\Reminder;
use App\Models\ReminderDetail;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;

class DeleteReminder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'DeleteReminder';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $date = date('Y-m-d');
        $date = date('Y-m-d', strtotime(now() . ' - 1 days'));

        $reminders = Reminder::where('date', $date)->get();
        foreach ($reminders as $rc) {
            $rc->delete();
        }


        // DB::table("reminders")->truncate();
        // return Command::SUCCESS;
    }
}
