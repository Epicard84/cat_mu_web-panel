<?php

namespace App\Console\Commands;

use App\Models\MasterUser\Activity;
use App\Models\MasterUser\ActivitySchedule;
use App\Models\MasterUser\Client;
use App\Models\MasterUser\MedicineSchedule;
use App\Models\Reminder;
use Illuminate\Console\Command;
use App\Models\MasterUser\Medicine;

class ReminderMedicine extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ReminderMedicine';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $today = date('Y-m-d');
        $nowTime = date('H:i');

        $fromTime = date('H:i',strtotime( '-65 minutes') );
        $toTime = date('H:i',strtotime( '-55 minutes') ); 

        // $fromTime = "22:55";
        // $toTime = "23:05";

        $activeSchedules = MedicineSchedule::where('date',$today)->where('status',0)->get();

        if($nowTime == "00:00"){
            $activeSchedules = MedicineSchedule::where('date',date('Y-m-d', strtotime("-1 days")))->where('status',0)->get();
        }

        foreach($activeSchedules as $actSch){
            $schTimes = explode(',',$actSch->scheduled_time);
            $comTimes = explode(',',$actSch->complete_details);

            $noTimes = count($schTimes);

            for($i=0; $i<$noTimes; $i++){
                // if((strtotime($schTimes[$i]) == $fromTime) || (strtotime($schTimes[$i]) > $fromTime) && (strtotime($schTimes[$i])< $toTime)){
                    if( ($fromTime<=$schTimes[$i]) && ($toTime>$schTimes[$i]) ){
                    if(empty($comTimes[$i])){
                        $medDetails = Medicine::find($actSch->medicine_id);
                        $medName = $medDetails->medicine_name;
                        $clientId = $medDetails->client_id;
                        $clientDetails = Client::find($clientId);
                        $clientName = $clientDetails->first_name." ".$clientDetails->last_name;
                        $clientMasterUser = $clientDetails->master_user;

                        Reminder::medReminder($clientMasterUser,$clientName,$medName,$today,$schTimes[$i]);
                    } else{
                        continue;
                    }
                } else{
                    continue;
                }
            }
        }

        //Activity Reminder
        // $activitySchedules = ActivitySchedule::where('date', $today)->where('status', 0)->get();

        // if ($nowTime == "00:00") {
        //     $activitySchedules = ActivitySchedule::where('date', date('Y-m-d', strtotime("-1 days")))->where('status', 0)->get();
        // }

        // foreach ($activitySchedules as $actSch1) {
        //     $schTimes1 = explode(',', $actSch1->scheduled_time);
        //     $comTimes1 = explode(',', $actSch1->complete_details);

        //     $noTimes1 = count($schTimes1);

        //     for ($i = 0; $i < $noTimes1; $i++) {
        //         // if((strtotime($schTimes[$i]) == $fromTime) || (strtotime($schTimes[$i]) > $fromTime) && (strtotime($schTimes[$i])< $toTime)){
        //         if (($fromTime <= $schTimes1[$i]) && ($toTime > $schTimes1[$i])) {
        //             if (empty($comTimes1[$i])) {
        //                 $medDetails = Activity::find($actSch1->activity_id);
        //                 $actName = $medDetails->activity_name;
        //                 $clientId = $medDetails->client_id;
        //                 $clientDetails = Client::find($clientId);
        //                 $clientName = $clientDetails->first_name . " " . $clientDetails->last_name;
        //                 $clientMasterUser = $clientDetails->master_user;

        //                 Reminder::medReminder($clientMasterUser, $clientName, $actName, $today, $schTimes1[$i]);
        //             } else {
        //                 continue;
        //             }
        //         } else {
        //             continue;
        //         }
        //     }
        // }

        return Command::SUCCESS;
    }
}
