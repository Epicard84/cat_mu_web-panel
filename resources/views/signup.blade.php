
@include('layouts.auth.head')
<body onload="initAutocomplete()">
    <section class="loginContainer subuserLoginbg">
        <div class="commonLogoSection loginLogo">
            <img src="{{ asset('public/MasterUser/assets/images/logo.png') }}" alt="">
            <div>
                <h1>MyChartSpace</h1>
                <p>Save time, use your phone.</p>
            </div>
        </div>
        <h2>Welcome to MyChartSpace!</h2>
        {{-- <p>There are many variations of passages of Lorem Ipsum available.</p> --}}
        <br>
        <form action="{{ route('/sign_up/post') }}" method="post" enctype="multipart/form-data">
            @csrf
            <div class="row mt-3">
                <div class="col-md-6 mb-3"><input type="text" class="form-control" name="first_name"
                        placeholder="First Name" value="{{old('first_name')}}">
                    @if (Session::has('error'))
                        @if (array_key_exists('first_name', json_decode(json_encode(Session::get('error')), true)))
                            <p class="text-danger" align="left">
                                {{ json_decode(json_encode(Session::get('error')), true)['first_name'][0] }}</p>
                        @endif

                    @endif
                </div>
                <div class="col-md-6 mb-3"><input type="text" class="form-control" name="last_name"
                        placeholder="Last Name">
                    @if (Session::has('error'))
                        @if (array_key_exists('last_name', json_decode(json_encode(Session::get('error')), true)))
                            <p class="text-danger" align="left">
                                {{ json_decode(json_encode(Session::get('error')), true)['last_name'][0] }}</p>
                        @endif

                    @endif
                </div>

            </div>
            <div class="mb-3">
                <input type="text" class="form-control" name="company_name" placeholder="Company Name">
                @if (Session::has('error'))
                    @if (array_key_exists('company_name', json_decode(json_encode(Session::get('error')), true)))
                        <p class="text-danger" align="left">
                            {{ json_decode(json_encode(Session::get('error')), true)['company_name'][0] }}</p>
                    @endif

                @endif
            </div>
            <div class="mb-3">
                <input type="number" class="form-control phone_number" maxlength="10" name="phone_number" id="phone_number"
                    placeholder="Cell Number">
                @if (Session::has('error'))
                    @if (array_key_exists('phone_number', json_decode(json_encode(Session::get('error')), true)))
                        <p align="left" class="text-danger phone_number_error" id="phone_e">
                            {{ json_decode(json_encode(Session::get('error')), true)['phone_number'][0] }}</p>
                    @endif
                @endif
                <p align="left" class="text-danger phone_number_error"></p>
            </div>
            <div class="mb-3">
                <input type="number" class="form-control phone_number" maxlength="15" name="fax_number" id="fax_number"
                    placeholder="Fax Number">
                {{-- @if (Session::has('error'))
                    @if (array_key_exists('phone_number', json_decode(json_encode(Session::get('error')), true)))
                        <p align="left" class="text-danger phone_number_error" id="phone_e">
                            {{ json_decode(json_encode(Session::get('error')), true)['phone_number'][0] }}</p>
                    @endif
                @endif --}}
                {{-- <p align="left" class="text-danger phone_number_error"></p> --}}
            </div>
            <div class="mb-3">
                <input type="number" class="form-control phone_number" maxlength="15" name="home_phone_number" id="home_phone_number"
                    placeholder="Home Phone Number">
                {{-- @if (Session::has('error'))
                    @if (array_key_exists('phone_number', json_decode(json_encode(Session::get('error')), true)))
                        <p align="left" class="text-danger phone_number_error" id="phone_e">
                            {{ json_decode(json_encode(Session::get('error')), true)['phone_number'][0] }}</p>
                    @endif
                @endif --}}
                {{-- <p align="left" class="text-danger phone_number_error"></p> --}}
            </div>
            <div class="mb-3">
                <input type="email" class="form-control" name="email" placeholder="Enter email address">
                @if (Session::has('error'))
                    @if (array_key_exists('email', json_decode(json_encode(Session::get('error')), true)))
                        <p class="text-danger" align="left">
                            {{ json_decode(json_encode(Session::get('error')), true)['email'][0] }}</p>
                    @endif

                @endif
            </div>
            <!-- <div class="mb-3">
                <textarea class="form-control textareaheight" name="permanent_address" placeholder="Permanent address"></textarea>
                @if (Session::has('error'))
                    @if (array_key_exists('permanent_address', json_decode(json_encode(Session::get('error')), true)))
                        <p class="text-danger" align="left">
                            {{ json_decode(json_encode(Session::get('error')), true)['permanent_address'][0] }}</p>
                    @endif

                @endif
            </div> -->
            <div class="mb-3">
                <textarea class="form-control textareaheight" name="facility_address" placeholder="Facility address" id="addressInput"></textarea>
                @if (Session::has('error'))
                    @if (array_key_exists('facility_address', json_decode(json_encode(Session::get('error')), true)))
                        <p class="text-danger" align="left">
                            {{ json_decode(json_encode(Session::get('error')), true)['facility_address'][0] }}</p>
                    @endif
                @endif
            </div>
            <!-- <div class="row">
                <div class="col-md-6 mb-3"><input type="text" class="form-control" name="city" placeholder="City">
                    @if (Session::has('error'))
                        @if (array_key_exists('city', json_decode(json_encode(Session::get('error')), true)))
                            <p class="text-danger" align="left">
                                {{ json_decode(json_encode(Session::get('error')), true)['city'][0] }}</p>
                        @endif

                    @endif
                </div>
                <div class="col-md-6 mb-3"><input type="text" class="form-control" name="state" placeholder="State">
                    @if (Session::has('error'))
                        @if (array_key_exists('state', json_decode(json_encode(Session::get('error')), true)))
                            <p class="text-danger" align="left">
                                {{ json_decode(json_encode(Session::get('error')), true)['state'][0] }}</p>
                        @endif

                    @endif
                </div>
            </div> -->

            <!-- <div class="row">
                <div class="col-md-6 mb-3">
                    <input type="text" class="form-control" name="country" placeholder="Country">
                    @if (Session::has('error'))
                        @if (array_key_exists('country', json_decode(json_encode(Session::get('error')), true)))
                            <p class="text-danger" align="left">
                                {{ json_decode(json_encode(Session::get('error')), true)['country'][0] }}</p>
                        @endif

                    @endif
                </div>
                <div class="col-md-6 mb-3">
                    <input type="text" class="form-control col-md-6 mb-3 " id="zip_code" name="zip_code"
                        placeholder="Zip Code">
                    @if (Session::has('error'))
                        @if (array_key_exists('zip_code', json_decode(json_encode(Session::get('error')), true)))
                            <p class="text-danger zip_error" align="left">
                                {{ json_decode(json_encode(Session::get('error')), true)['zip_code'][0] }}</p>
                        @endif

                    @endif

                </div>
            </div> -->
            <div class="row">
                <div class="col-md-12 mb-3">
                    <input type="text" class="form-control" id="total_beds" name="total_beds"
                        placeholder="Total Beds">
                    @if (Session::has('error'))
                        @if (array_key_exists('total_beds', json_decode(json_encode(Session::get('error')), true)))
                            <p class="text-danger beds_error" align="left">
                                {{ json_decode(json_encode(Session::get('error')), true)['total_beds'][0] }}</p>
                        @endif

                    @endif

                </div>
            </div>

            <div class="form-group mb-3">
                <div class="fileUploadInput signupUpload">
                    <span>
                        <input type='file' class="form-control" name="profile_image" accept="image/*" id="aa"
                            onchange="pressed1()"><label id="fileLabel1">Upload profile image</label></span>
                    <button>Upload</button>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="fileUploadInput signupUpload">
                    <span>
                        <input type='file' class="form-control" name="operators_license" accept="image/*" id="operators_license"
                            onchange="pressed2()"><label id="fileLabel2">Upload Operators License*</label></span>
                    <button>Upload</button>
                </div>
                @if (Session::has('error'))
                    @if (array_key_exists('operators_license', json_decode(json_encode(Session::get('error')), true)))
                        <p class="text-danger beds_error" align="left">
                            {{ json_decode(json_encode(Session::get('error')), true)['operators_license'][0] }}</p>
                    @endif

                @endif
            </div>
            <div class="form-group mb-3">
                <div class="fileUploadInput signupUpload">
                    <span>
                        <input type='file' class="form-control" name="tb_clearance" accept="image/*" id="tb_clearance"
                            onchange="pressed3()"><label id="fileLabel3">Upload TB Clearance</label></span>
                    <button>Upload</button>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="fileUploadInput signupUpload">
                    <span>
                        <input type='file' class="form-control" name="physical_checkup" accept="image/*" id="physical_checkup"
                            onchange="pressed4()"><label id="fileLabel4">Upload Physical Checkup</label></span>
                    <button>Upload</button>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="fileUploadInput signupUpload">
                    <span>
                        <input type='file' class="form-control" name="misc_doc" accept="image/*" id="misc_doc"
                            onchange="pressed5()"><label id="fileLabel5">Miscellaneous Document</label></span>
                    <button>Upload</button>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="showhidePwd" id="show_hide_password">
                    <input class="form-control password" id="password" type="password" name="password"
                        placeholder="Password">
                    <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>
                </div>
            </div>
            <div class="form-group mb-3">
                <div class="showhidePwd" id="show_hide_password2">
                    <input class="form-control confirm_password" type="password" name="confirm_password"
                        id="confirm_password" placeholder="Confirm Password">
                    <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>
                </div>
                @if (Session::has('error'))
                    @if (array_key_exists('password', json_decode(json_encode(Session::get('error')), true)))
                        <p class=" text-danger" align="left" id="p_error">
                            {{ json_decode(json_encode(Session::get('error')), true)['password'][0] }}</p>
                    @endif

                @endif
                <p class="password_error" align="left" id="password_error"></p>
            </div>
            <p style="font-size: 16px">*By signing up , I am accepting <a href="{{route('terms-of-service')}}" style="font-size: 16px">Terms & Conditions</a> and <a href="{{route('privacy-and-cookie-policy')}}" style="font-size: 16px">Privacy Policies</a></p>

            <button type="submit">Sign Up</button>
        </form>
        <p class="mb-4 alreadyAccount">Already have an account? <a href="{{ route('/') }}">Login</a></p>
    </section>

    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });

            $("#show_hide_password2 a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password2 input').attr("type") == "text") {
                    $('#show_hide_password2 input').attr('type', 'password');
                    $('#show_hide_password2 i').addClass("fa-eye-slash");
                    $('#show_hide_password2 i').removeClass("fa-eye");
                } else if ($('#show_hide_password2 input').attr("type") == "password") {
                    $('#show_hide_password2 input').attr('type', 'text');
                    $('#show_hide_password2 i').removeClass("fa-eye-slash");
                    $('#show_hide_password2 i').addClass("fa-eye");
                }
            });
        });
    </script>
    <script>
        window.pressed1 = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel1.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel1.innerHTML = theSplit[theSplit.length - 1];
            }
        };
        window.pressed2 = function() {
            var b = document.getElementById('operators_license');
            if (b.value == "") {
                fileLabel2.innerHTML = "Upload image";
            } else {
                var theSplit = b.value.split('\\');
                fileLabel2.innerHTML = theSplit[theSplit.length - 1];
            }
        };
        window.pressed3 = function() {
            var c = document.getElementById('tb_clearance');
            if (c.value == "") {
                fileLabel3.innerHTML = "Upload image";
            } else {
                var theSplit = c.value.split('\\');
                fileLabel3.innerHTML = theSplit[theSplit.length - 1];
            }
        };
        window.pressed4 = function() {
            var d = document.getElementById('physical_checkup');
            if (d.value == "") {
                fileLabel4.innerHTML = "Upload image";
            } else {
                var theSplit = d.value.split('\\');
                fileLabel4.innerHTML = theSplit[theSplit.length - 1];
            }
        };
        window.pressed5 = function() {
            var e = document.getElementById('misc_doc');
            if (e.value == "") {
                fileLabel5.innerHTML = "Upload image";
            } else {
                var theSplit = e.value.split('\\');
                fileLabel5.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        $(document).ready(function() {

            // //phnumber Validation
            // function phone_number_validator(phnumber) {
            //     var filter =
            //     /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/g;

            //     if (filter.test(phnumber)) {
            //         return true;
            //     } else {
            //         return false;
            //     }

            // }



            // $('#phone_number').on('keyup', function() {
            //     $('#phone_e').hide();
            //     var response = phone_number_validator($(this).val());

            //     if ($(this).val().length == 0) {
            //         $('.phone_number_error').html("");
            //     } else if (response == false) {
            //         $('.phone_number_error').html("*Please Enter a valid Phone Number");
            //     } else {
            //         $('.guardian_phone_number_error').html("");
            //     }

            // })

            //password and confirm password checking
            $('#password, #confirm_password').on('keyup', function() {
                $('#p_error').hide();
                if ($('#password').val() == "" && $('#confirm_password').val() == "") {
                    $('#password_error').html('');
                } else {
                    if ($('#password').val() == $('#confirm_password').val()) {
                        $('#password_error').html('Matching').css({
                            "color": "green",
                            "font-size": "17px"
                        });
                    } else
                        $('#password_error').html('Not Matching').css({
                            "color": "red",
                            "font-size": "17px"

                        });

                }

            });
        });
    </script>
    <script>
    function initAutocomplete() {
      var input = document.getElementById('addressInput');
      var autocomplete = new google.maps.places.Autocomplete(input);

      autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        console.log(place.formatted_address);
        console.log(place);
      });
    }

    // function data(){
    //   var address = document.getElementById('addressInput').value;
    //   alert(address);
    // }
  </script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyASsOMW6tK9Qo981eUEHdcPi3WjNcJwFQ0&libraries=places"></script>

</body>
