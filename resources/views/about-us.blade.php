@extends('layouts.auth.app')

@section('title', 'Login')

@section('content')
    <section class="loginContainer nopdnnew">
        <div class="commonLogoSection loginLogo">
            <img src="{{ asset('public/MasterUser/assets/images/logo.png') }}" alt="">
            <div>
                <h1>MyChartSpace</h1>
                <p>Save time, use your phone.</p>
            </div>


        </div>
        {{-- <h2>About Us</h2> --}}

        <br>
        <div class="container">
            <p style="text-align: left">MyChartSpace.com has been specifically designed to enable charting on tablets, laptops, and desktops. This versatile tool offers customization options and grants you convenient and quick data entry into a reliable cloud storage by Amazon’s web services.</p>
            <p style="text-align: left">Leave behind the era of paper charting, allowing your yourself and your staff to operate at its highest level of efficiency.</p>
            <ul style="text-align: left">
                <li>Improve the flow of data logging processes and enhance overall productivity.</li>
                <li>Utilize mobility, speed, and adaptability to their fullest potential. </li>
                <li>Seamlessly file documents and information into patient’s folders in real-time. </li>
                <li>Optimize patient care and ensure utmost satisfaction.</li>
            </ul>

            <p style="color: black"><b>About us:
                </b></p>
                <p style="text-align: left">Golden Age Enterprises L.L.C. is an emerging startup based in Honolulu, Hawaii, dedicated to harnessing the power of AI and voice technology across various industries. Recognizing the immense potential of artificial intelligence, we approach its integration with a strong sense of responsibility.  Our team comprises highly skilled individuals who are committed to upholding safety standards and minimizing risks and liabilities, ensuring accountability at every step.</p>

                <p style="text-align: left">[Contact email and phone number and maybe address].</p>

                <p style="text-align: left">HST Pathways was developed with a deep understanding of the imperative to assist healthcare providers in enhancing efficiency, fostering growth, and delivering superior care to patients.</p>
                    <p style="text-align: left">We embarked on this journey by constructing a robust architecture and approach that enables us to create purpose-built cloud deployments of practice management capabilities. These deployments are designed to be unyielding and scalable, forming the foundation of all our software solutions—a framework we call the Health Systems and Technologies Foundation.</p>
                    <p style="text-align: left">Throughout more than 15 years, our company has continuously adapted to the evolving needs of ASCs. Our flagship solution, HST One, drives productivity and empowers our clients to provide exceptional patient experiences. We work closely in partnership with our clients to ensure they can consistently introduce innovations that meet their future needs.</p>
                        <p style="text-align: left">It's important to note that our solutions may not be the most economical in the market. However, cost-effectiveness has never been our sole objective. Instead, our focus lies in delivering exceptional return on investment (ROI), characterized by market-leading uptime, practical and user-friendly functionality, and an unparalleled client-retention rate of 97 percent.</p>
        </div>

        <div class="loginFooter">
            <div class="downloadicon">
                <span>Download from: </span>
                <img src="{{ asset('public/MasterUser/assets/images/android.png') }}" alt="">
                <img src="{{ asset('public/MasterUser/assets/images/ios.png') }}" alt="">
            </div>
            <div class="dnLink">
                <a href="{{route('/')}}">Home</a> |
                <a href="{{route('about-us')}}">About Us</a> |
                <a href="{{ route('terms-of-service') }}">Terms &amp; Condition</a> |
                <a href="{{ route('privacy-and-cookie-policy') }}">Privacy Policy</a>
            </div>
        </div>
    </section>

    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('public/MasterUser/assets/js/datepicker.js') }}"></script>



@endsection
