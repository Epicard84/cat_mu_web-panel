<!DOCTYPE html>
<html lang="en">

@include('SuperAdmin/components/head')
<meta name="csrf-token" content="{{ csrf_token() }}">

{{-- @foreach ($sub_users as $su)
    {{$su->getMasterDetail->id}}
@endforeach --}}

<body>
    <div id="app">
        @include('SuperAdmin/components/sidebar')
        <div id="main">
            @include('SuperAdmin/components/header')

            <div class="container-fluid">
                <div class="page-title">
                    <h3><b>Active Clients</b></h3>
                </div>

                <section class="section">
                    <div class="row">
                        <div class="card-body">

                            {{-- Datatable start --}}
                            <div class="card">

                                <div class="card-body">
                                    {{-- <div align="right" class="mb-3">
                                        <label for="" class="btn icon icon-left btn-success"
                                            data-bs-toggle="modal" data-bs-target="#addmasteruser"><i
                                                class="fa-solid fa-user-plus"></i></label>
                                    </div> --}}
                                    <table class='table table-striped table-light table-sm' id="querytable"
                                        width="100%">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Name</th>
                                                <th>Email</th>
                                                <th>Reason</th>
                                                <th width="10%">Date</th>
                                                <th width="8%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>


        @include('SuperAdmin/components/footer')

</body>
<script>
    $(document).ready(function() {

        // query DataTable ajax
        $('#querytable').DataTable({
            serverSide: true,
            ajax: "{{ route('block_account') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'muname',
                    name: 'muname'
                },
                {
                    data: 'muemail',
                    name: 'muemail'
                },
                {
                    data: 'reason',
                    name: 'reason'
                },
                {
                    data: 'date',
                    name: 'date'
                },
                // {
                //     data: 'mustatus',
                //     name: 'mustatus'
                // },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
        });

        //Master user delete
        $(document).on("click", ".activemu", function() {

            var id = $(this).attr("data-id");

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'mx-4 btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                // text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete user!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {


                    $.ajax({
                            type: "GET",
                            url: "{{ route('delete_master_user') }}",
                            data: {
                                'id': id
                            },
                            dataType: "json",
                            success: function(response) {
                                swalWithBootstrapButtons.fire(
                                    'Blocked!',
                                    'Your master user profile disabled.',
                                    'success', )
                            }
                        }),
                        setTimeout(() => {
                            window.location.reload()
                        }, 1000);

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your user profile is safe :)',
                        'error'
                    )
                }
            })

        });

        //Master user restore
        $(document).on("click", ".blockedmu", function() {

            var id = $(this).attr("data-id");

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'mx-4 btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                // text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, activate user!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {


                    $.ajax({
                            type: "GET",
                            url: "{{ route('restore_master_user') }}",
                            data: {
                                'id': id
                            },
                            dataType: "json",
                            success: function(response) {
                                swalWithBootstrapButtons.fire(
                                    'Activated!',
                                    'Your master user profile activated.',
                                    'success', )
                            }
                        }),
                        setTimeout(() => {
                            window.location.reload()
                        }, 1000);

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your current status is safe :)',
                        'error'
                    )
                }
            })

        });

    });
</script>
<style>


</style>

</html>
