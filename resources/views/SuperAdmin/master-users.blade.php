<!DOCTYPE html>
<html lang="en">

@include('SuperAdmin/components/head')
<meta name="csrf-token" content="{{ csrf_token() }}">

<body>
    <div id="app">
        @include('SuperAdmin/components/sidebar')
        <div id="main">
            @include('SuperAdmin/components/header')

            <div class="container-fluid">
                <div class="page-title">
                    <h3><b>Master Users</b></h3>
                </div>

                <section class="section">
                    <div class="row">
                        <div class="card-body">

                            {{-- Datatable start --}}
                            <div class="card">

                                <div class="card-body">
                                    <div align="right" class="mb-3">
                                        <label for="" class="btn icon icon-left btn-success"
                                            data-bs-toggle="modal" data-bs-target="#addmasteruser"><i
                                                class="fa-solid fa-user-plus"></i></label>
                                    </div>
                                    <table class='table table-striped table-light table-sm' id="querytable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Email address</th>
                                                {{-- <th>Phone number</th> --}}
                                                {{-- <th>Subscription Date</th> --}}
                                                {{-- <th>Last location</th> --}}
                                                <th>Status</th>
                                                {{-- <th>Technical Status</th> --}}
                                                <th style="width: 15%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            {{-- Add Master user modal --}}

                            <div class="modal fade" id="addmasteruser" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-info">
                                            <h5 class="modal-title white" id="myModalLabel130">Register Master User</h5>
                                            <button type="button" class="close" data-bs-dismiss="modal"
                                                aria-label="Close">
                                                <i data-feather="x"></i>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('master_user_validator') }}" method="POST"
                                                id="create_form" enctype="multipart/form-data">
                                                @csrf
                                                <label>Name: </label>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <input type="text" placeholder="First name"
                                                                name="first_name" class="form-control first_name"
                                                                required>
                                                        </div>
                                                        <div class="col-6">
                                                            <input type="text" placeholder="Last name"
                                                                name="last_name" class="form-control last_name"
                                                                required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <label>Company name: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="company_name"
                                                        class="form-control company_name" required>
                                                </div>
                                                <label>Phone number: </label>
                                                <div class="form-group">
                                                    <input type="number" placeholder="Number"
                                                        name="phone_number" class="form-control phone_number" required>
                                                    <p for="" class="phone_number_label text-danger mt-1"></p>
                                                </div>
                                                <label>Email: </label>
                                                <div class="form-group">
                                                    <input type="email" placeholder="" required name="email"
                                                        class="form-control email">
                                                    <p  class="email_label text-danger mt-1"></p>
                                                </div>
                                                {{-- <label>Permanent address: </label>
                                                <div class="form-group">
                                                    <textarea placeholder="" name="permanent_address" class="form-control permanent_address" required></textarea>
                                                </div> --}}
                                                <label>Facility address: </label>
                                                <div class="form-group">
                                                    <textarea type="text" placeholder="" name="facility_address" class="form-control facility_address" required></textarea>
                                                </div>
                                                <label>City: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="city"
                                                        class="form-control city" required>
                                                </div>
                                                <label>State: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="state"
                                                        class="form-control state" required>
                                                </div>
                                                <label>Country: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="country"
                                                        class="form-control country" required>
                                                </div>
                                                <label>Zip code: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="zip_code"
                                                        class="form-control zip_code" required>
                                                </div>
                                                <label>Profile image: </label>
                                                <div class="form-group">
                                                    <input type="file" placeholder="" accept="image/*"
                                                        name="profile_image" class="form-control profile_image"
                                                        >
                                                </div>

                                                {{-- <input type="email" class="form-control" name="email" id="email" required>
                                                    <div class="form-control-icon">
                                                        <i class="fa-solid fa-eye"></i>
                                                    </div> --}}








                                                <label>Password: </label>
                                                <div class="form-group">
                                                    <div class="showhidePwd" id="show_hide_password">
                                                        <input type="password" placeholder="" id="password"
                                                            name="password" class="form-control password" required>
                                                        <a class="showpass"><i class="fa-solid fa-eye"></i></a>
                                                    </div>
                                                </div>
                                                <label>Confirm Password: </label>
                                                <div class="form-group">
                                                    <div class="showhidePwd" id="show_hide_cnf_password">
                                                        <input type="text" placeholder="" id="confirm_password"
                                                            name="confirm_password"
                                                            class="form-control confirm_password" required>
                                                        <a class="showpass"><i class="fa-solid fa-eye-slash"></i></a>
                                                        <p id='message' class="message" class="mt-1"></p>
                                                    </div>
                                                </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light-secondary"
                                                data-bs-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block close">Close</span>
                                            </button>
                                            <button type="submit" class="btn btn-info ml-1 submit">
                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Register</span>
                                            </button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>

                        {{-- Edit Master user modal --}}

                        <div class="modal fade" id="editmasteruser" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel130">Edit Master User</h5>
                                        <button type="button" class="close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ route('edit_master_user') }}" method="POST" id="edit_form"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <input type="text" hidden name="id" class="id"
                                                id="id">
                                            <label>Name: </label>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <input type="text" placeholder="First name"
                                                            name="edit_first_name"
                                                            class="form-control edit_first_name" required>
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="text" placeholder="Last name"
                                                            name="edit_last_name" class="form-control edit_last_name"
                                                            required>
                                                    </div>
                                                </div>
                                            </div>
                                            <label>Company name: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_company_name"
                                                    class="form-control edit_company_name" required>
                                            </div>
                                            {{-- <label>Permanent address: </label>
                                            <div class="form-group">
                                                <textarea placeholder="" name="edit_permanent_address" class="form-control edit_permanent_address" required></textarea>
                                            </div> --}}
                                            <label>Facility address: </label>
                                            <div class="form-group">
                                                <textarea type="text" placeholder="" name="edit_facility_address" class="form-control edit_facility_address"
                                                    required></textarea>
                                            </div>
                                            <label>City: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_city"
                                                    class="form-control edit_city" required>
                                            </div>
                                            <label>State: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_state"
                                                    class="form-control edit_state" required>
                                            </div>
                                            <label>Country: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_country"
                                                    class="form-control edit_country" required>
                                            </div>
                                            <label>Zip code: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_zip_code"
                                                    class="form-control edit_zip_code" required>
                                            </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-secondary"
                                            data-bs-dismiss="modal">
                                            <i class="bx bx-x d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block edit_close">Close</span>
                                        </button>
                                        <button type="submit" class="btn btn-info ml-1 edit_submit">
                                            <i class="bx bx-check d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block">Update</span>
                                        </button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                    </div>
            </div>
            </section>
        </div>


        @include('SuperAdmin/components/footer')

</body>
<script>
    $(document).ready(function() {

        $("#show_hide_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_password input').attr("type") == "text") {
                $('#show_hide_password input').attr('type', 'password');
                $('#show_hide_password svg').addClass("fa-eye-slash");
                $('#show_hide_password svg').removeClass("fa-eye");
            } else if ($('#show_hide_password input').attr("type") == "password") {
                $('#show_hide_password input').attr('type', 'text');
                $('#show_hide_password svg').removeClass("fa-eye-slash");
                $('#show_hide_password svg').addClass("fa-eye");
            }
        });
        $("#show_hide_cnf_password a").on('click', function(event) {
            event.preventDefault();
            if ($('#show_hide_cnf_password input').attr("type") == "text") {
                $('#show_hide_cnf_password input').attr('type', 'password');
                $('#show_hide_cnf_password svg').addClass("fa-eye-slash");
                $('#show_hide_cnf_password svg').removeClass("fa-eye");
            } else if ($('#show_hide_cnf_password input').attr("type") == "password") {
                $('#show_hide_cnf_password input').attr('type', 'text');
                $('#show_hide_cnf_password svg').removeClass("fa-eye-slash");
                $('#show_hide_cnf_password svg').addClass("fa-eye");
            }
        });

        $(".masteruser").addClass("active");

        // query DataTable ajax
        $('#querytable').DataTable({
            serverSide: true,
            ajax: "{{ route('master_users') }}",
            columns: [{
                    data: 'id',
                    name: 'id'
                },
                {
                    data: 'customname',
                    name: 'customname'
                },
                {
                    data: 'email',
                    name: 'email'
                },
                // {data: 'phone_number', name: 'phone_number'},
                // {data: 'user_email', name: 'user_email',},
                // {data: 'subscription_date', name: 'subscription_date'},

                {
                    data: 'customstatus',
                    name: 'customstatus'
                },
                {
                    data: 'action',
                    name: 'action'
                },
            ],
            "order": [
                [0, "desc"]
            ]
        });

        function windowsReload() {
            setTimeout(function() {
                window.location.reload();
            }, 2500);

        }


        //Register Master user

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function error_empty() {
            $('.email_label').empty();
            $('.phone_number_label').empty();
            $('.message').html("");
        }

        $('.close').click(function(e) {
            e.preventDefault();
            $('.email_label').empty();
            error_empty()
        });

        function empty() {
            $('.first_name').val('');
            $('.last_name').val('');
            $('.company_name').val('');
            $('.phone_number').val('');
            $('.email').val('');
            $('.permanent_address').val('');
            $('.facility_address').val('');
            $('.city').val('');
            $('.state').val('');
            $('.country').val('');
            $('.profile_image').val('');

        };



        $('#create_form').submit(function(e) {

            e.preventDefault();
            error_empty();
            var email = $('.email').val();
            var phone_number = $('.phone_number').val();
            var password = $('#password').val();
            $.ajax({
                type: "GET",
                url: "{{ route('master_user_validator') }}",
                data: {
                    'email': email,
                    'phone_number': phone_number,
                    'password': password
                },
                dataType: "json",
                success: function(response) {
                    if (response != "no errror") {

                        let email = response.hasOwnProperty('email');
                        let phone_number = response.hasOwnProperty('phone_number');
                        let password = response.hasOwnProperty('password');

                        if (email) {
                            $('.email_label').append("*" + response.email[0]);
                        }

                        if (phone_number) {
                            $('.phone_number_label').append("*" + response.phone_number[0]);
                        }

                        if (password) {
                            $("#message").css({
                                "color": "red"
                            });
                            $('#message').html("*" + response.password[0]);
                        }
                    } else {
                        var form = $('#create_form')[0];
                        var formData = new FormData(form);
                        console.log(formData);
                        $.ajax({
                            url: "{{ route('add_master_user') }}",
                            type: 'post',
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            data: formData,
                            success: function(data) {
                                if (data == 'success') {
                                    empty();
                                    $('#addmasteruser').modal('hide')
                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2500,
                                        timerProgressBar: true,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })

                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Master User Created Successfully'
                                    })
                                    $('#querytable').DataTable().ajax.reload();

                                    // ... do something with the data...
                                }
                            }
                        });

                    }

                }
            });

        })

        //password and confirm password checking
        $('#password, #confirm_password').on('keyup', function() {
            if ($('#password').val() == "" && $('#confirm_password').val() == "") {
                $('#message').html('');
            } else {
                if ($('#password').val() == $('#confirm_password').val()) {
                    $('#message').html('Matching').css('color', 'green');
                } else
                    $('#message').html('Not Matching').css('color', 'red');

            }

        });




        let sub_user_limit;
        //Update Master User function
        $(document).on("click", ".edit", function() {
            var id = $(this).attr('data-id');
            $.ajax({
                type: "get",
                url: "{{ route('get_master_user_details') }}",
                data: {
                    'id': id
                },
                dataType: "json",
                success: function(response) {
                    $('.edit_first_name').val(response.first_name);
                    $('.edit_last_name').val(response.last_name);
                    $('.edit_company_name').val(response.company_name);
                    $('.edit_city').val(response.city);
                    $('.edit_country').val(response.country);
                    $('.edit_zip_code').val(response.zip_code);
                    $('.edit_permanent_address').val(response.permanent_address);
                    $('.edit_state').val(response.state);
                    $('.edit_facility_address').val(response.facility_address);
                    $('.edit_subuser_limit').val(response.subuser_limit);
                    $('.id').val(response.id);

                }
            });


        });


        $('#edit_form').submit(function(e) {
            e.preventDefault();

            var edit_form = $('#edit_form')[0];
            var edit_formData = new FormData(edit_form);
            $.ajax({
                type: "post",
                url: "{{ route('edit_master_user') }}",
                data: edit_formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(response) {
                    if (response == 'success') {
                        $('#editmasteruser').modal('hide')
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 2500,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener(
                                    'mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener(
                                    'mouseleave',
                                    Swal.resumeTimer
                                )
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Master User Updated Successfully'
                        })
                        $('#querytable').DataTable().ajax.reload();

                        // ... do something with the data...
                    }
                }
            });


        });


        //Master user delete
        $(document).on("click", ".delete", function() {

            var id = $(this).data('id');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'mx-4 btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {


                    $.ajax({
                            type: "GET",
                            url: "{{ route('delete_master_user') }}",
                            data: {
                                'id': id
                            },
                            dataType: "json",
                            success: function(response) {
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    'Your file has been deleted.',
                                    'success', )
                            }
                        }),
                        setTimeout(() => {
                            window.location.reload()
                        }, 1000);

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })

        });

        $(document).on("click", ".about", function() {
            console.log($(this).data('id'));
            $('.about_first_name').val(response.first_name);
            $('.about_last_name').val(response.last_name);
            $('.about_company_name').val(response.company_name);
            $('.about_city').val(response.city);
            $('.about_country').val(response.country);
            $('.about_permanent_address').val(response.permanent_address);
            $('.about_state').val(response.state);
            $('.about_facility_address').val(response.facility_address);
            $('.id').val(response.id);
        });


    });
</script>
<style>


</style>

</html>
