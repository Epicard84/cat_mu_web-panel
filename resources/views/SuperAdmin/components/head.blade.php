<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>MyChartSpace</title>
    
    <link rel="stylesheet" href="{{asset('public/SuperAdmin/assets/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset('public/SuperAdmin/assets/css/custom.css')}}">
    
    <link rel="stylesheet" href="{{asset('public/SuperAdmin/assets/vendors/chartjs/Chart.min.css')}}">

    <link rel="stylesheet" href="{{asset('public/SuperAdmin/assets/vendors/perfect-scrollbar/perfect-scrollbar.css')}}">
    <link rel="stylesheet" href="{{asset('public/SuperAdmin/assets/css/app.css')}}">
    <link rel="shortcut icon" href="{{asset('public/SuperAdmin/assets/images/favicon.png')}}" type="{{asset('SuperAdmin/assets/image/x-icon')}}">
   

   
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-GLhlTQ8iRABdZLl6O3oVMWSktQOp6b7In1Zl3/Jr59b6EGGoI1aFkw7cmDA6j6gD" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0-alpha1/dist/js/bootstrap.bundle.min.js" integrity="sha384-w76AqPfDkMBDXo30jS1Sgez6pr3x5MlQ1ZAGC+nuZB+EYdgRZgiwxhTBTkF7CXvN" crossorigin="anonymous"></script> --}}
   <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
   <link rel="stylesheet" href="//cdn.datatables.net/1.13.1/css/jquery.dataTables.min.css">
   <script src="//cdn.datatables.net/1.13.1/js/jquery.dataTables.min.js"></script>

   <script defer src="{{asset('public/SuperAdmin/assets/fontawesome/js/brands.js')}}"></script>
   <script defer src="{{asset('public/SuperAdmin/assets/fontawesome/js/solid.js')}}"></script>
   <script defer src="{{asset('public/SuperAdmin/assets/fontawesome/js/fontawesome.js')}}"></script>
   <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
</head>