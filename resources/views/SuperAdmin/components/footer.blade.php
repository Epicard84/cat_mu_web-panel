<footer>
    <div class="footer clearfix mb-0 text-muted">
        <div class="float-start">
            <p>2023 &copy;</p>
        </div>
        <div class="float-end">
            <p>Crafted with <span class='text-danger'>Team Laravel<i data-feather="heart"></i></span> by <a href="#">Brainium Infotech Solution</a></p>
        </div>
    </div>
</footer>
</div>
</div>
<script src="{{asset('public/SuperAdmin/assets/js/feather-icons/feather.min.js')}}"></script>
<script src="{{asset('public/SuperAdmin/assets/vendors/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
<script src="{{asset('public/SuperAdmin/assets/js/app.js')}}"></script>

<script src="{{asset('public/SuperAdmin/assets/vendors/chartjs/Chart.min.js')}}"></script>
<script src="{{asset('public/SuperAdmin/assets/vendors/apexcharts/apexcharts.min.js')}}"></script>
<script src="{{asset('public/SuperAdmin/assets/js/pages/dashboard.js')}}"></script>

<script src="{{asset('public/SuperAdmin/assets/js/main.js')}}"></script>
