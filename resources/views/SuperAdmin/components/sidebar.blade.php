<div id="sidebar" class='active'>
    <div class="sidebar-wrapper active">
        <div class="sidebar-header">
            <img src="{{ asset('public/SuperAdmin/assets/images/logo.jpg') }}" alt="" srcset="">
        </div>
        <div class="sidebar-menu">
            <ul class="menu">
                <li class="sidebar-item dashboard">

                    <a href="{{ route('admin_dashboard') }}" class='sidebar-link' style="text-decoration: none">
                        <i data-feather="home" width="20"></i>
                        <span>Dashboard</span>
                    </a>

                </li>

                <li class="sidebar-item masteruser">

                    <a href="{{ route('master_users') }}" class='sidebar-link' style="text-decoration: none">
                        <i data-feather="user" width="20"></i>
                        <span>Master Users</span>
                    </a>

                </li>

                <li class="sidebar-item discharged_clients clients">

                    <a href="{{route('all_sub_users')}}" class='sidebar-link ' style="text-decoration: none">
                        <i class="fa-solid fa-users" width="20"></i>             
                        <span>Sub Users</span>
                    </a>

                </li>

                <li class="sidebar-item discharged_clients clients">

                    <a href="{{route('clients')}}" class='sidebar-link ' style="text-decoration: none">
                        <i class="fa-solid fa-clipboard-user" width="20"></i>             
                        <span>Clients</span>
                    </a>

                </li>

                <li class="sidebar-item discharged_clients clients">

                    <a href="{{route('block_account')}}" class='sidebar-link ' style="text-decoration: none">
                        <i class="fas fa-exclamation-circle" width="20"></i>   
                        <span>Requests</span>
                    </a>

                </li>

            </ul>
        </div>
        <button class="sidebar-toggler btn x"><i data-feather="x"></i></button>
    </div>
</div>
