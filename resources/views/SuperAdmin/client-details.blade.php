<!DOCTYPE html>
<html lang="en">

@include('SuperAdmin/components/head')
<meta name="csrf-token" content="{{ csrf_token() }}">

<body>
    <div id="app">
        @include('SuperAdmin/components/sidebar')
        <div id="main">
            @include('SuperAdmin/components/header')

            <div class="container-fluid">
                <div class="page-title">
                    <h3><b>Master Users</b></h3>
                </div>

                <section class="section">
                    <div class="row">
                        <div class="card-body">

                            {{-- Datatable start --}}
                            <div class="card">

                                <div class="card-body">
                                    <div align="right" class="mb-3">
                                        <label for="" class="btn icon icon-left btn-success"
                                            data-bs-toggle="modal" data-bs-target="#addmasteruser"><i
                                                class="fa-solid fa-user-plus"></i></label>
                                    </div>
                                    <table class='table table-striped table-light table-sm' id="querytable">
                                        <thead>
                                            <tr>
                                                <th>Id</th>
                                                <th>Name</th>
                                                <th>Email address</th>
                                                <th style="width: 15%">Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                           

                           
                            

                        </div>
                    </div>
                </section>
            </div>


            @include('SuperAdmin/components/footer')

</body>
<script>
    $(document).ready(function() {

        $(".clients").addClass("active");

        // query DataTable ajax
        $('#querytable').DataTable({
            serverSide: true,
            ajax: "{{ route('client_details') }}",
            columns: [
                {
                    data: 'id',
                    name: 'id'
                },

                {
                    data: 'customname',
                    name: 'customname'
                },

                {
                    data: 'email',
                    name: 'email'
                },
               
                {
                    data: 'action',
                    name: 'action'
                },
            ]
        });

        function windowsReload() {
            setTimeout(function() {
                window.location.reload();
            }, 2500);

        }


        //Register Master user

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function error_empty() {
            $('.email_label').empty();
            $('.phone_number_label').empty();
            $('.message').html("");
        }

        $('.close').click(function(e) {
            e.preventDefault();
            $('.email_label').empty();
            error_empty()
        });

        $('#create_form').submit(function(e) {
            e.preventDefault();
            error_empty()
            var email = $('.email').val();
            var phone_number = $('.phone_number').val();
            var password = $('#password').val();
            $.ajax({
                type: "GET",
                url: "{{ route('master_user_validator') }}",
                data: {
                    'email': email,
                    'phone_number': phone_number,
                    'password': password
                },
                dataType: "json",
                success: function(response) {
                    if (response != "no errror") {

                        let email = response.hasOwnProperty('email');
                        let phone_number = response.hasOwnProperty('phone_number');
                        let password = response.hasOwnProperty('password');

                        if (email) {
                            $('.email_label').append("*" + response.email[0]);
                        }

                        if (phone_number) {
                            $('.phone_number_label').append("*" + response.phone_number[0]);
                        }

                        if (password) {
                            $("#message").css({
                                "color": "red"
                            });
                            $('#message').html("*" + response.password[0]);
                        }
                    } else {
                        var form = $('#create_form')[0];
                        var formData = new FormData(form);
                        $.ajax({
                            url: "{{ route('add_master_user') }}",
                            type: 'post',
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            data: formData,
                            success: function(data) {
                                if (data == 'success') {
                                    $('#addmasteruser').modal('hide')
                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2500,
                                        timerProgressBar: true,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })

                                    Toast.fire({
                                        icon: 'success',
                                        title: 'Master User Created Successfully'
                                    })
                                    $('#querytable').DataTable().ajax.reload();

                                    // ... do something with the data...
                                }
                            }
                        });

                    }

                }
            });

        })

        //password and confirm password checking
        $('#password, #confirm_password').on('keyup', function() {
            if ($('#password').val() == "" && $('#confirm_password').val() == "") {
                $('#message').html('');
            } else {
                if ($('#password').val() == $('#confirm_password').val()) {
                    $('#message').html('Matching').css('color', 'green');
                } else
                    $('#message').html('Not Matching').css('color', 'red');

            }

        });

        const minusButton = document.getElementById('minus');
        const plusButton = document.getElementById('plus');
        const inputField = document.getElementById('subuser_limit');

        minusButton.addEventListener('click', event => {
            event.preventDefault();
            const currentValue = Number(inputField.value) || 0;
            if (inputField.value != 0) {
                inputField.value = currentValue - 1;
            }
        });

        plusButton.addEventListener('click', event => {
            event.preventDefault();
            const currentValue = Number(inputField.value) || 0;
            inputField.value = currentValue + 1;
        });

        let sub_user_limit ;
        //Update Master User function
        $(document).on("click", ".edit", function() {
            var id = $(this).data('id');
            $.ajax({
                type: "get",
                url: "{{ route('get_master_user_details') }}",
                data: {
                    'id': id
                },
                dataType: "json",
                success: function(response) {
                    // console.log(response);
                    $('.edit_first_name').val(response.first_name);
                    $('.edit_last_name').val(response.last_name);
                    $('.edit_company_name').val(response.company_name);
                    $('.edit_city').val(response.city);
                    $('.edit_country').val(response.country);
                    $('.edit_permanent_address').val(response.permanent_address);
                    $('.edit_state').val(response.state);
                    $('.edit_mailing_address').val(response.mailing_address);
                    $('.edit_subuser_limit').val(response.subuser_limit);
                    $('.id').val(response.id);

                }
            });


        });

        //Subuser limit edit
        const editPlusButton = document.getElementById('edit_plus');
        const editInputField = document.getElementById('edit_subuser_limit');

       

        editPlusButton.addEventListener('click', event => {
            event.preventDefault();
            const currentValue = Number($('.edit_subuser_limit').val());
            editInputField.value = currentValue + 1;
        });


        $('#edit_form').submit(function(e) {
            e.preventDefault();

            var edit_form = $('#edit_form')[0];
            var edit_formData = new FormData(edit_form);
            $.ajax({
                type: "post",
                url: "{{ route('edit_master_user') }}",
                data: edit_formData,
                processData: false,
                contentType: false,
                dataType: "json",
                success: function(response) {
                    if (response == 'success') {
                        $('#editmasteruser').modal('hide')
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 2500,
                            timerProgressBar: true,
                            didOpen: (toast) => {
                                toast.addEventListener(
                                    'mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener(
                                    'mouseleave',
                                    Swal.resumeTimer
                                )
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Master User Updated Successfully'
                        })
                        $('#querytable').DataTable().ajax.reload();

                        // ... do something with the data...
                    }
                }
            });


        });



        //Master user delete
        $(document).on("click", ".delete", function() {

            var id = $(this).data('id');

            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'mx-4 btn btn-danger'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, delete it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true
            }).then((result) => {
                if (result.isConfirmed) {

                    swalWithBootstrapButtons.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success',
                        $.ajax({
                            type: "GET",
                            url: "{{ route('delete_master_user') }}",
                            data: {
                                'id': id
                            },
                            dataType: "json",
                            success: function(response) {
                                console.log(response);

                            }
                        }),
                        window.location.reload()
                    )

                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })

        });

        $(document).on("click", ".about", function() {
            console.log($(this).data('id'));
            $('.about_first_name').val(response.first_name);
            $('.about_last_name').val(response.last_name);
            $('.about_company_name').val(response.company_name);
            $('.about_city').val(response.city);
            $('.about_country').val(response.country);
            $('.about_permanent_address').val(response.permanent_address);
            $('.about_state').val(response.state);
            $('.about_mailing_address').val(response.mailing_address);
            $('.about_subuser_limit').val(response.subuser_limit);
            $('.id').val(response.id);
        });


    });
</script>
<style>


</style>

</html>
