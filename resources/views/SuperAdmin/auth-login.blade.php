<!DOCTYPE html>
<html lang="en">

<head>
   
    <title>Sign in</title>
    @include('SuperAdmin/components/head')
    
</head>

<body>
    <div id="auth">
        
<div class="container">
    <div class="row">
        <div class="col-md-5 col-sm-12 mx-auto">
            <div class="card pt-4">
                <div class="card-body">
                    <div class="text-center mb-2">
                        <img src="{{asset('public/SuperAdmin/assets/images/logo.jpg')}}" style="height: 50%; width: 50%" height="48" class='mb-4'>
                        <h3>Super Admin</h3>
                        <p>Please sign in to continue.</p>
                    </div>
                    <div class="text-center text-danger mb-3">

                        @if(Session::has('error'))
                            <p>{{Session::get('error')}}</p>
                        @endif
                     
                    </div>
                    <form action="{{route('admin_login_auth')}}" method="POST">
                        @csrf
                        <div class="form-group position-relative has-icon-left">
                            <label for="username">Email</label>
                            <div class="position-relative">
                                <input type="email" class="form-control" name="email" id="email" required>
                                <div class="form-control-icon">
                                    <i data-feather="user"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group position-relative has-icon-left">
                            <div class="clearfix">
                                <label for="password">Password</label>
                                {{-- <a href="{{route('forgotpassword')}}" class='float-end'>
                                    <small>Forgot password?</small>
                                </a> --}}
                            </div>
                            <div class="position-relative">
                                <input type="password" class="form-control" name="password" id="password" required>
                                <div class="form-control-icon">
                                    <i data-feather="lock"></i>
                                </div>
                            </div>
                        </div>

                        <div class='form-check clearfix my-4'>
                            <div class="checkbox float-start">
                                <input type="checkbox" id="checkbox1" class='form-check-input' name="remember" >
                                <label for="checkbox1">Remember me</label>
                            </div>
                            {{-- <div class="float-end">
                                <a href="{{route('register')}}">Don't have an account?</a>
                            </div> --}}
                        </div>
                        <div class="clearfix">
                            <button class="btn btn-primary float-end">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    </div>
    <script src="{{asset('public/SuperAdmin/assets/js/feather-icons/feather.min.js')}}"></script>
    <script src="{{asset('public/SuperAdmin/assets/js/app.js')}}"></script>
    
    <script src="{{asset('public/SuperAdmin/assets/js/main.js')}}"></script>
</body>

</html>
