<!DOCTYPE html>
<html lang="en">

@include('SuperAdmin/components/head')

<body>
    <div id="app">
        @include('SuperAdmin/components/sidebar')
        <div id="main">
            @include('SuperAdmin/components/header')

            <div class="container-fluid">
                <div class="page-title">
                    <h3>Master User Details</h3>
                </div>
                <section class="section">
                    <div class="row mb-2">
                        <div class="col-12">
                            <div class="card recent-sales overflow-auto">

                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-12">
                                            <div class="card">

                                                <div class="card-body">
                                                    <div class="muDetail">
                                                        <div class="muDetlImg">
                                                            @if ($master_user_data->profile_image_name)
                                                            <img src="{{ asset('images/MasterUserProfile/' . $master_user_data->profile_image_name) }}"
                                                                alt="" srcset="">
                                                        @else
                                                            <img src="{{ asset('public/images/MasterUserProfile/defult_image.png') }}"
                                                                alt="" srcset="">
                                                        @endif
                                                        </div>
                                                        <div class="muDetlTxt">
                                                            <span for="">Name::</span>
                                                                    <h5>{{ $master_user_data->first_name }}
                                                                        {{ $master_user_data->last_name }}</h5>
                                                        </div>
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <span for="">Email::</span>
                                                                <p class="form-control"><a href="mailto:{{ $master_user_data->email }}">{{ $master_user_data->email }}</a>
                                                                </p>
                                                            </div>
                                                            <div class="col-6">
                                                                <span for="">Cell Number::</span>
                                                                <p class="form-control">
                                                                    <a href="tel:"{{$master_user_data->phone_number }}"> {{$master_user_data->phone_number }}</a>
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-6">
                                                                <span for="">Home Phone Number::</span>
                                                                <p class="form-control">
                                                                    {{ $master_user_data->home_phone_number }}
                                                                </p>
                                                            </div>
                                                            <div class="col-6">
                                                                <span for="">Fax Number::</span>
                                                                <p class="form-control">
                                                                    {{ $master_user_data->fax_number }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <span>Company Name::</span>
                                                                <p class="form-control">
                                                                    {{ $master_user_data->company_name }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            
                                                            <div class="col-12">
                                                                <span>Facility Address::</span>
                                                                <p class="form-control">
                                                                    {{ $master_user_data->facility_address }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <span>Country::</span>
                                                                <p class="form-control">
                                                                    {{ $master_user_data->country }}</p>
                                                            </div>
                                                            <div class="col-3">
                                                                <span>Zip Code::</span>
                                                                <p class="form-control">
                                                                    {{ $master_user_data->zip_code }}</p>
                                                            </div>
                                                            <div class="col-3">
                                                                <span>State::</span>
                                                                <p class="form-control">{{ $master_user_data->state }}
                                                                </p>
                                                            </div>
                                                            <div class="col-3">
                                                                <span>city::</span>
                                                                <p class="form-control">{{ $master_user_data->city }}
                                                                </p>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-3">
                                                                <span>Date Enrolled::</span>
                                                                <p class="form-control">
                                                                    {{ date('m-d-Y', strtotime($master_user_data->created_at)) }}
                                                                </p>
                                                            </div>
                                                            <div class="col-3">
                                                                <span>Total Beds::</span>
                                                                <p class="form-control">
                                                                    {{ $master_user_data->total_beds }}</p>
                                                            </div>
                                                            <div class="col-3">
                                                                <span>Available Beds::</span>
                                                                <p class="form-control">
                                                                    {{ $availableBeds }}</p>
                                                            </div>
                                                            <div class="col-3 map" data-toggle="tooltip"
                                                                data-placement="top" title="Click to view on Map">
                                                                <span>Map Location::</span>
                                                                <p class="form-control"> <i
                                                                        class="fa-solid fa-map-pin"></i> &nbsp;
                                                                    {{ $location }}</p>
                                                            </div>
                                                        </div>
                                                        <input type="text" value="{{$master_user_data->id}}" hidden id="" name="id" class="id">
                                                    </div>


                                                </div>
                                            </div>

                                        </div>
                                        {{-- <div class="col-12">
                                            <div class="card">
                                                <div class="card-body">
                                                    
                                                </div>
                                            </div>
                                        </div> --}}
                                    </div>
                                </div>
                            </div>
                            <h3>Documents</h3>
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-4">
                                            <h6>Operators License</h6>
                                            @if ($master_user_data->operators_license)
                                            <a href="{{asset('images/MasterUserProfile/' . $master_user_data->operators_license)}}"><img src="{{ asset('images/MasterUserProfile/' . $master_user_data->operators_license) }}"
                                                    alt="" srcset="" class="docsimg"></a>
                                                    <div>
                                                        {{-- <a href="{{asset('images/MasterUserProfile/' . $master_user_data->operators_license)}}"><label
                                                                ><i class="fa-light fa-circle-down"></i></label></a> --}}
                                                    </div>
                                            @endif
                                        </div>
                                        <div class="col-4">
                                            <h6>TB Clearance</h6>
                                            @if ($master_user_data->tb_clearance)
                                                <a href="{{asset('images/MasterUserProfile/' . $master_user_data->tb_clearance)}}"><img src="{{ asset('images/MasterUserProfile/' . $master_user_data->tb_clearance) }}"
                                                    alt="" srcset="" class="docsimg"></a>
                                            @endif
                                        </div>
                                        <div class="col-4">
                                            <h6>Physical Checkup</h6>
                                            @if ($master_user_data->physical_checkup)
                                                <a href="{{asset('images/MasterUserProfile/' . $master_user_data->physical_checkup)}}"><img src="{{ asset('images/MasterUserProfile/' . $master_user_data->physical_checkup) }}"
                                                    alt="" srcset="" class="docsimg"></a>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <h3>Active SubUsers</h3>
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-borderless table-light" >
                                        <thead>
                                            <tr>
                                                <th scope="col">Sl No</th>
                                                <th scope="col">Sub User name</th>
                                                <th scope="col">Sub User email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $count=0    
                                            @endphp
                                            @foreach ($subUsers as $su)
                                            @php
                                                $count=$count+1
                                            @endphp
                                            <tr>
                                                <th>{{$count}}</th>
                                                <th>{{$su->first_name." ".$su->last_name}}</th>
                                                <th>{{$su->email}}</th>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <h3>Active Clients</h3>
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-borderless table-light" id="querytable">
                                        <thead>
                                            <tr>
                                                <th scope="col">Sl No</th>
                                                <th scope="col">Client name</th>
                                                <th scope="col">DOB</th>
                                                <th scope="col">Gender</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                           
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <h3>Discharged Clients</h3>
                            <div class="card">
                                <div class="card-body">
                                    <table class="table table-borderless table-light">
                                        <thead>
                                            <tr>
                                                <th scope="col">Sl No</th>
                                                <th scope="col">Client name</th>
                                                <th scope="col">DOB</th>
                                                <th scope="col">Gender</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                            $count=0    
                                            @endphp
                                            @foreach ($discharged_client as $dc)
                                            @php
                                                $count=$count+1
                                            @endphp
                                            <tr>
                                                <td>{{$count}}</td>
                                                <td>{{$dc->first_name." ".$dc->last_name}}</td>
                                                <td>{{$dc->dob}}</td>
                                                <td>{{ucfirst($dc->gender)}}</td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <center>
                                <div>
                                    <a href="{{ route('master_users') }}"><label for=""
                                            class="btn btn-primary btn-sm ">Back</label></a>
                                </div>
                            </center>
                        </div>


                    </div>

            </div>
        </div><!-- End Recent Sales -->
        </section>
    </div>






    @include('SuperAdmin/components/footer')

</body>
<script>
    $(document).ready(function() {
        $(".masteruser").addClass("active");
    
        let id = $('.id').val();
        console.log(id);

        // query DataTable ajax
        $('#querytable').DataTable({
               serverSide: true,
               ajax: `{{ url('superAdmin/about_master_user/${id}')}}`,
               columns: [
                   {
                       data: 'DT_RowIndex',
                       name: 'DT_RowIndex', 
                       orderable: false, 
                       searchable: false 
                   },
                
                   {
                       data: 'clientName',
                       name: 'clientName'
                   },

                   {
                       data: 'dob',
                       name: 'dob', 
                       orderable: false, 
                       searchable: false 
                   },
                   
                   {
                       data: 'gender',
                       name: 'gender', 
                       orderable: false, 
                       searchable: false 
                   },
               ]
           });
    });

</script>

<script>
    //Map
    $(".map").on({
        mouseenter: function() {
            $(this).css("cursor", "pointer");
        },

        mouseleave: function() {
            $(this).css("cursor", "auto");
        },

        click: function() {
            let user_id = $('.id').val();
            // alert(user_id);
            window.location.href = `{{ url('superAdmin/mu_login_location/${user_id}') }}`;
        }
    });
</script>

</html>
