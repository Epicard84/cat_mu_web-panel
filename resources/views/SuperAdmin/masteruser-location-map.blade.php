<html>
  <head>
    <title>Master User Map Location</title>
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script defer src="{{asset('public/SuperAdmin/assets/fontawesome/js/fontawesome.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="./style.css" />
    <script type="module" src="./index.js"></script>
    <style>
      /* 
 * Always set the map height explicitly to define the size of the div element
 * that contains the map. 
 */
      #map {
        height: 100%;
      }

      /* 
 * Optional: Makes the sample page fill the window. 
 */
      html,
      body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
    </style>
  </head>
  <body>
    <input hidden value="{{$lat}}" class="lat">
    <input hidden value="{{$lon}}" class="lon">
    {{-- <div>
      <button onclick="javascript:history.go(-1)"><i class="fas fa-long-arrow-alt-left"></i></button>
      <h3>{{$userName}}</h3>
    </div> --}}
    <div id="map"></div>

    <!-- prettier-ignore -->
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyASsOMW6tK9Qo981eUEHdcPi3WjNcJwFQ0&callback=initMap"></script>
  </body>
  <script>
    let map;
    let lat = "{{$lat}}";
    let lon = "{{$lon}}";
    let userName = "{{$userName}}";
    // console.log(parseFloat(lat),parseFloat(lon));

    async function initMap() {
      const { Map } = await google.maps.importLibrary("maps");

      map = new Map(document.getElementById("map"), {
        center: { lat: parseFloat(lat), lng: parseFloat(lon) },
        zoom: 15,
      });
      new google.maps.Marker({
        position: { lat: parseFloat(lat), lng: parseFloat(lon) },
        map,
        title: userName,
      });
    }

  initMap();
  </script>
</html>
