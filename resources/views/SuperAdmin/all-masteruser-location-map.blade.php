<html>

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Master User Map Location</title>
    <link rel="icon" href="{{ asset('public/MasterUser/assets/images/favicon.ico') }}" type="image/x-icon">
    <script src="https://polyfill.io/v3/polyfill.min.js?features=default"></script>
    <script src="https://code.jquery.com/jquery-3.6.3.min.js" integrity="sha256-pvPw+upLPUjgMXY0G+8O0xUf+/Im1MZjXxxgOcBQBXU=" crossorigin="anonymous"></script>
    <script defer src="{{ asset('public/SuperAdmin/assets/fontawesome/js/fontawesome.js') }}"></script>
    <link rel="stylesheet" type="text/css" href="./style.css" />
    <script type="module" src="./index.js"></script>
    <style>
        /*
 * Always set the map height explicitly to define the size of the div element
 * that contains the map.
 */
        #map {
            height: 100%;
        }

        /*
 * Optional: Makes the sample page fill the window.
 */
        html,
        body {
            height: 100%;
            margin: 0;
            padding: 0;
        }
    </style>
</head>

<body>
    {{-- <input hidden value="{{$lat}}" class="lat">
    <input hidden value="{{$lon}}" class="lon"> --}}
    {{-- <div>
      <button onclick="javascript:history.go(-1)"><i class="fas fa-long-arrow-alt-left"></i></button>
      <h3>{{$userName}}</h3>
    </div> --}}
    <div id="map"></div>

    <!-- prettier-ignore -->
    <!-- <script src="https://maps.google.com/maps/api/js?key=AIzaSyASsOMW6tK9Qo981eUEHdcPi3WjNcJwFQ0&callback=initMap"></script> -->

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASsOMW6tK9Qo981eUEHdcPi3WjNcJwFQ0&libraries=places"></script>
    <script>
        const addresses = @json($addresses);

        console.log(addresses);

        function initMap() {
            const map = new google.maps.Map(document.getElementById('map'), {
                center: {
                    lat: 0,
                    lng: 0
                },
                zoom: 2
            });

            addresses.forEach(address => {
                const geocoder = new google.maps.Geocoder();
                geocoder.geocode({
                    address: address.facility_address
                }, (results, status) => {
                    if (status === 'OK') {
                        const marker = new google.maps.Marker({
                            map: map,
                            position: results[0].geometry.location,
                            // title: results[0].formatted_address
                            title: `${address.first_name} ${address.last_name}`,
                            url: `https://mychartspace.com/superAdmin/about_master_user/${address.id}`,
                        });
                        marker.addListener('click', function() {
                            window.location.href = marker.url;
                        });

                    } else {
                        console.error('Geocode was not successful for the following reason: ' + status);
                    }
                });
            });
        }
    </script>

    <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyASsOMW6tK9Qo981eUEHdcPi3WjNcJwFQ0&libraries=places&callback=initMap"></script>

</body>
<!-- <script>
    let map;

    // console.log(parseFloat(lat),parseFloat(lon));

    async function initMap() {
        const {
            Map
        } = await google.maps.importLibrary("maps");

        map = new Map(document.getElementById("map"), {
            center: {
                lat: 22.518,
                lng: 88.3832
            },
            zoom: 15,
        });
        new google.maps.Marker({
            position: {
                lat: 22.518,
                lng: 88.3832
            },
            map,
            title: "Hello Kolkata",
        });
    }

    initMap();

    $(document).ready(function() {
        let map;

        // console.log(parseFloat(lat),parseFloat(lon));

        async function initMap() {
            const {
                Map
            } = await google.maps.importLibrary("maps");

            map = new Map(document.getElementById("map"), {
                center: {
                    lat: 21.3099,
                    lng: 157.8581
                },
                zoom: 3,
            });

        }

        initMap();
        // alert("Hello");
        $.ajax({
            type: "get",
            url: "{{ route('all_mu_login_location') }}",

            success: function(response) {
                console.log(response);
                $.each(response.location, function(indexInArray, value) {
                    var marker = new google.maps.Marker({
                        position: {
                            lat: parseFloat(value.latitude),
                            lng: parseFloat(value.longitude)
                        },
                        map,
                        title: `${value.first_name} ${value.last_name} , ${value.location}`,
                        url: `https://mychartspace.com/superAdmin/about_master_user/${value.id}`,
                        // url: route('https://mychartspace.com/superAdmin/admin'),
                    });

                    marker.addListener('click', function() {
                    window.location.href = marker.url;
                });

                });
            }
        });
    });
</script> -->



</html>