<!DOCTYPE html>
<html lang="en">

@include('SuperAdmin/components/head')

<body>
    <div id="app">
        @include('SuperAdmin/components/sidebar')
        <div id="main">
            @include('SuperAdmin/components/header')

            <div class="container-fluid">
                <div class="page-title">
                    <h3>Dashboard</h3>
                </div>
                <section class="section">
                    <div class="row mb-2">
                        <div class="col-12 col-md-4">
                            <div class="card card-statistic">
                                <div class="card-body p-2">
                                    <div class="d-flex flex-column">
                                        <div class='px-3 py-3 d-flex justify-content-between'>
                                            <h3 class='card-title'>Master Users</h3>
                                            <div class="card-right d-flex align-items-center">
                                                <p>{{ $total_master_user }}</p>
                                            </div>
                                        </div>
                                        {{-- <div class="chart-wrapper">
                                <canvas id="canvas1" style="height:100px !important"></canvas>
                            </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-4">
                            <div class="card card-statistic">
                                <div class="card-body p-2">
                                    <div class="d-flex flex-column">
                                        <div class='px-3 py-3 d-flex justify-content-between'>
                                            <h3 class='card-title'>Sub Users</h3>
                                            <div class="card-right d-flex align-items-center">
                                                <p>{{ $total_sub_user }}</p>
                                            </div>
                                        </div>
                                        {{-- <div class="chart-wrapper">
                                <canvas id="canvas2" style="height:100px !important"></canvas>
                            </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12 col-md-4">
                            <div class="card card-statistic">
                                <div class="card-body p-2">
                                    <div class="d-flex flex-column">
                                        <div class='px-3 py-3 d-flex justify-content-between'>
                                            <h3 class='card-title'>Active Clients</h3>
                                            <div class="card-right d-flex align-items-center">
                                                <p>{{ $total_client }}</p>
                                            </div>
                                        </div>
                                        {{-- <div class="chart-wrapper">
                                <canvas id="canvas4" style="height:100px !important"></canvas>
                            </div> --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Recent Sales -->
                    <div class="col-12">
                        <div class="card recent-sales overflow-auto">
                            <div class="card-body">
                                <div class="cardTop">
                                    <h5 class="card-title">Recent 5 Master Users</h5>
                                    <a href="{{ route('all_mu_login_location') }}" target="_blank" style="text-decoration: none; color:white"><span class="viewAll locations">Location Map</span></a>
                                    <span class="viewAll masterUsers">View All</span>
                                </div>
                                <table class="table table-borderless table-light">
                                    <thead>
                                        <tr>
                                            <th style="font-size: 10pt;" scope="col">Sl No</th>
                                            <th style="font-size: 10pt;" scope="col">Name</th>
                                            <th style="font-size: 10pt;" scope="col">Email</th>
                                            <th style="font-size: 10pt;" scope="col">Contact No</th>
                                            <th style="font-size: 10pt;" scope="col">Facility Address</th>
                                            <th style="font-size: 10pt;" scope="col">Joining Date</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $count1 = 0;
                                        @endphp
                                        @foreach ($masterUsers as $mu)
                                        @php
                                        $count1 = $count1 + 1;
                                        @endphp
                                        <tr>
                                            <th style="font-size: 10pt;" scope="row">{{ $count1 }}</th>
                                            <td style="font-size: 10pt;">{{ $mu->first_name . ' ' . $mu->last_name }}</td>
                                            <td style="font-size: 10pt;">{{ $mu->email }}</td>
                                            <td style="font-size: 10pt;">{{ $mu->phone_number }}</td>
                                            <!-- <td style="font-size: 10pt;">{{ $mu->city . ", " . $mu->country }}</td> -->
                                            <td style="font-size: 10pt;"><span data-bs-toggle="tooltip" data-bs-placement="top" title='{{ $mu->facility_address . ", " . $mu->city . ", " . $mu->country }}'>
                                                    {{$mu->country}}
                                                </span>
                                            </td>
                                            <td style="font-size: 10pt;">{{ date('m-d-Y', strtotime($mu->created_at)) }}</td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>

                            </div>
                            <div class="card-body">
                                <div class="cardTop">
                                    <h5 class="card-title">Recent 5 Sub Users</h5>
                                    <span class="viewAll subUsers">View All</span>
                                </div>
                                <table class="table table-borderless table-light">
                                    <thead>
                                        <tr>
                                            <th scope="col">Sl No</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Email</th>
                                            <th scope="col">Master User Name</th>
                                            <th scope="col">Master User Email</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $count2 = 0;
                                        @endphp
                                        @foreach ($subUsers as $su)
                                        @php
                                        $count2 = $count2 + 1;
                                        @endphp
                                        <tr>
                                            <th scope="row">{{ $count2 }}</th>
                                            <td>{{ $su->first_name . ' ' . $su->last_name }}</td>
                                            <td>{{ $su->email }}</td>
                                            <td>{{ $su->MasterDetails->first_name . ' ' . $su->MasterDetails->last_name }}
                                            </td>
                                            <td>{{ $su->MasterDetails->email }}</td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>

                            </div>
                            <div class="card-body">
                                <div class="cardTop">
                                    <h5 class="card-title">Recent 5 Clients</h5>
                                    <span class="viewAll clients">View All</span>
                                </div>
                                <table class="table table-borderless table-light">
                                    <thead>
                                        <tr>
                                            <th scope="col">Sl No</th>
                                            <th scope="col">Name</th>
                                            <th scope="col">Gender</th>
                                            <th scope="col">Master User Name</th>
                                            <th scope="col">Master User Email</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php
                                        $count3 = 0;
                                        @endphp
                                        @foreach ($clients as $c)
                                        @php
                                        $count3 = $count3 + 1;
                                        @endphp
                                        <tr>
                                            <th scope="row">{{ $count3 }}</th>
                                            <td>{{ $c->first_name . ' ' . $c->last_name }}</td>
                                            <td>{{ ucfirst($c->gender) }}</td>
                                            <td>{{ $c->getMasterDetails->first_name . ' ' . $c->getMasterDetails->last_name }}
                                            </td>
                                            <td>{{ $c->getMasterDetails->email }}</td>
                                        </tr>
                                        @endforeach


                                    </tbody>
                                </table>

                            </div>


                        </div>
                    </div><!-- End Recent Sales -->
                </section>

            </div>
            @include('SuperAdmin/components/footer')

</body>
<script>
    $(document).ready(function() {
        $(".dashboard").addClass("active");

        //All MasterUsers
        $(".masterUsers").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
            click: function() {
                // var patient_id = $(this).attr("data-id");

                // // alert(patient_id);
                // var date = "0";
                window.location.href = `{{ route('master_users') }}`;
            }
        });

        //All SubUsers
        $(".subUsers").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
            click: function() {
                // var patient_id = $(this).attr("data-id");

                // // alert(patient_id);
                // var date = "0";
                window.location.href = `{{ route('sub_users') }}`;
            }
        });

        //All SubUsers
        $(".clients").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
            click: function() {
                // var patient_id = $(this).attr("data-id");

                // // alert(patient_id);
                // var date = "0";
                window.location.href = `{{ route('clients') }}`;
            }
        });

        //All MasterUsers Locations
        $(".locations").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });
    });
</script>

</html>