<!DOCTYPE html>
<html lang="en">

@include('SuperAdmin/components/head')
<meta name="csrf-token" content="{{ csrf_token() }}">

{{-- @foreach ($sub_users as $su)
    {{$su->getMasterDetail->id}}
@endforeach --}}

<body>
    <div id="app">
        @include('SuperAdmin/components/sidebar')
        <div id="main">
            @include('SuperAdmin/components/header')

            <div class="container-fluid">
                <div class="page-title">
                    <h3><b>Active Sub Users</b></h3>
                </div>

                <section class="section">
                    <div class="row">
                        <div class="card-body">

                            {{-- Datatable start --}}
                            <div class="card">

                                <div class="card-body">
                                    {{-- <div align="right" class="mb-3">
                                        <label for="" class="btn icon icon-left btn-success"
                                            data-bs-toggle="modal" data-bs-target="#addmasteruser"><i
                                                class="fa-solid fa-user-plus"></i></label>
                                    </div> --}}
                                    <table class='table table-striped table-light table-sm' id="querytable">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Name</th>
                                                <th>Email</th>

                                                <th>MasterUser Name</th>
                                                {{-- <th style="width: 15%">Action</th> --}}
                                                <th>MasterUser Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>


                            {{-- Add Master user modal --}}

                            {{-- <div class="modal fade" id="addmasteruser" tabindex="-1" role="dialog"
                                aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-scrollable" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header bg-info">
                                            <h5 class="modal-title white" id="myModalLabel130">Register Master User</h5>
                                            <button type="button" class="close" data-bs-dismiss="modal"
                                                aria-label="Close">
                                                <i data-feather="x"></i>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{ route('master_user_validator') }}" method="POST"
                                                id="create_form" enctype="multipart/form-data">
                                                @csrf
                                                <label>Name: </label>
                                                <div class="form-group">
                                                    <div class="row">
                                                        <div class="col-6">
                                                            <input type="text" placeholder="First name"
                                                                name="first_name" class="form-control first_name"
                                                                required>
                                                        </div>
                                                        <div class="col-6">
                                                            <input type="text" placeholder="Last name"
                                                                name="last_name" class="form-control last_name"
                                                                required>
                                                        </div>
                                                    </div>
                                                </div>
                                                <label>Company name: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="company_name"
                                                        class="form-control company_name" required>
                                                </div>
                                                <label>Phone number: </label>
                                                <div class="form-group">
                                                    <input type="number" placeholder="Number"
                                                        name="phone_number" class="form-control phone_number" required>
                                                    <p for="" class="phone_number_label text-danger mt-1"></p>
                                                </div>
                                                <label>Email: </label>
                                                <div class="form-group">
                                                    <input type="email" placeholder="" required name="email"
                                                        class="form-control email">
                                                    <p  class="email_label text-danger mt-1"></p>
                                                </div>
                                                
                                                <label>Facility address: </label>
                                                <div class="form-group">
                                                    <textarea type="text" placeholder="" name="facility_address" class="form-control facility_address" required></textarea>
                                                </div>
                                                <label>City: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="city"
                                                        class="form-control city" required>
                                                </div>
                                                <label>State: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="state"
                                                        class="form-control state" required>
                                                </div>
                                                <label>Country: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="country"
                                                        class="form-control country" required>
                                                </div>
                                                <label>Zip code: </label>
                                                <div class="form-group">
                                                    <input type="text" placeholder="" name="zip_code"
                                                        class="form-control zip_code" required>
                                                </div>
                                                <label>Profile image: </label>
                                                <div class="form-group">
                                                    <input type="file" placeholder="" accept="image/*"
                                                        name="profile_image" class="form-control profile_image"
                                                        >
                                                </div>

                                                
                                                <label>Password: </label>
                                                <div class="form-group">
                                                    <div class="showhidePwd" id="show_hide_password">
                                                        <input type="password" placeholder="" id="password"
                                                            name="password" class="form-control password" required>
                                                        <a class="showpass"><i class="fa-solid fa-eye"></i></a>
                                                    </div>
                                                </div>
                                                <label>Confirm Password: </label>
                                                <div class="form-group">
                                                    <div class="showhidePwd" id="show_hide_cnf_password">
                                                        <input type="text" placeholder="" id="confirm_password"
                                                            name="confirm_password"
                                                            class="form-control confirm_password" required>
                                                        <a class="showpass"><i class="fa-solid fa-eye-slash"></i></a>
                                                        <p id='message' class="message" class="mt-1"></p>
                                                    </div>
                                                </div>
                                        </div>

                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-light-secondary"
                                                data-bs-dismiss="modal">
                                                <i class="bx bx-x d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block close">Close</span>
                                            </button>
                                            <button type="submit" class="btn btn-info ml-1 submit">
                                                <i class="bx bx-check d-block d-sm-none"></i>
                                                <span class="d-none d-sm-block">Register</span>
                                            </button>
                                        </div>
                                    </div>
                                    </form>
                                </div>
                            </div> --}}
                        </div>

                        {{-- Edit Master user modal --}}

                        {{-- <div class="modal fade" id="editmasteruser" tabindex="-1" role="dialog"
                            aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-scrollable" role="document">
                                <div class="modal-content">
                                    <div class="modal-header bg-primary">
                                        <h5 class="modal-title white" id="myModalLabel130">Edit Master User</h5>
                                        <button type="button" class="close" data-bs-dismiss="modal"
                                            aria-label="Close">
                                            <i data-feather="x"></i>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <form action="{{ route('edit_master_user') }}" method="POST" id="edit_form"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <input type="text" hidden name="id" class="id"
                                                id="id">
                                            <label>Name: </label>
                                            <div class="form-group">
                                                <div class="row">
                                                    <div class="col-6">
                                                        <input type="text" placeholder="First name"
                                                            name="edit_first_name"
                                                            class="form-control edit_first_name" required>
                                                    </div>
                                                    <div class="col-6">
                                                        <input type="text" placeholder="Last name"
                                                            name="edit_last_name" class="form-control edit_last_name"
                                                            required>
                                                    </div>
                                                </div>
                                            </div>
                                            <label>Company name: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_company_name"
                                                    class="form-control edit_company_name" required>
                                            </div>
                                           
                                            <label>Facility address: </label>
                                            <div class="form-group">
                                                <textarea type="text" placeholder="" name="edit_facility_address" class="form-control edit_facility_address"
                                                    required></textarea>
                                            </div>
                                            <label>City: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_city"
                                                    class="form-control edit_city" required>
                                            </div>
                                            <label>State: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_state"
                                                    class="form-control edit_state" required>
                                            </div>
                                            <label>Country: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_country"
                                                    class="form-control edit_country" required>
                                            </div>
                                            <label>Zip code: </label>
                                            <div class="form-group">
                                                <input type="text" placeholder="" name="edit_zip_code"
                                                    class="form-control edit_zip_code" required>
                                            </div>


                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-light-secondary"
                                            data-bs-dismiss="modal">
                                            <i class="bx bx-x d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block edit_close">Close</span>
                                        </button>
                                        <button type="submit" class="btn btn-info ml-1 edit_submit">
                                            <i class="bx bx-check d-block d-sm-none"></i>
                                            <span class="d-none d-sm-block">Update</span>
                                        </button>
                                    </div>
                                    </form>
                                </div>
                            </div>
                        </div> --}}


                    </div>
            </div>
            </section>
        </div>


        @include('SuperAdmin/components/footer')

</body>
<script>
    $(document).ready(function() {

        // $("#show_hide_password a").on('click', function(event) {
        //     event.preventDefault();
        //     if ($('#show_hide_password input').attr("type") == "text") {
        //         $('#show_hide_password input').attr('type', 'password');
        //         $('#show_hide_password svg').addClass("fa-eye-slash");
        //         $('#show_hide_password svg').removeClass("fa-eye");
        //     } else if ($('#show_hide_password input').attr("type") == "password") {
        //         $('#show_hide_password input').attr('type', 'text');
        //         $('#show_hide_password svg').removeClass("fa-eye-slash");
        //         $('#show_hide_password svg').addClass("fa-eye");
        //     }
        // });
        // $("#show_hide_cnf_password a").on('click', function(event) {
        //     event.preventDefault();
        //     if ($('#show_hide_cnf_password input').attr("type") == "text") {
        //         $('#show_hide_cnf_password input').attr('type', 'password');
        //         $('#show_hide_cnf_password svg').addClass("fa-eye-slash");
        //         $('#show_hide_cnf_password svg').removeClass("fa-eye");
        //     } else if ($('#show_hide_cnf_password input').attr("type") == "password") {
        //         $('#show_hide_cnf_password input').attr('type', 'text');
        //         $('#show_hide_cnf_password svg').removeClass("fa-eye-slash");
        //         $('#show_hide_cnf_password svg').addClass("fa-eye");
        //     }
        // });


        // query DataTable ajax
        $('#querytable').DataTable({
            serverSide: true,
            ajax: "{{ route('all_sub_users') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'subuser_name',
                    name: 'subuser_name'
                },
                {
                    data: 'subuser_email',
                    name: 'subuser_email'
                },
                // {data: 'phone_number', name: 'phone_number'},
                // {data: 'user_email', name: 'user_email',},
                // {data: 'subscription_date', name: 'subscription_date'},

                {
                    data: 'masteruser_name',
                    name: 'masteruser_name'
                },
                {
                    data: 'masteruser_email',
                    name: 'masteruser_email'
                },
            ],

        });


    });
</script>
<style>


</style>

</html>
