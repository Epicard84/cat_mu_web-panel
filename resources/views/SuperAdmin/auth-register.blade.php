<!DOCTYPE html>
<html lang="en">

<head>

    @include('SuperAdmin/components/head')
    <title>Sign up</title>

</head>

<body>
    <div id="auth">
        
<div class="container">
    <div class="row">
        <div class="col-md-7 col-sm-12 mx-auto">
            <div class="card pt-4">
                <div class="card-body">
                    <div class="text-center mb-5">
                        <img src="{{asset('SuperAdmin/assets/images/favicon.svg')}}" height="48" class='mb-4'>
                        <h3>Sign Up</h3>
                        <p>Please fill the form to register.</p>
                    </div>
                    <form action="">
                        <div class="row">
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="first-name-column">First Name</label>
                                    <input type="text" id="first-name-column" class="form-control"  name="fname-column">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="last-name-column">Last Name</label>
                                    <input type="text" id="last-name-column" class="form-control"  name="lname-column">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="username-column">Username</label>
                                    <input type="text" id="username-column" class="form-control" name="username-column">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="country-floating">Country</label>
                                    <input type="text" id="country-floating" class="form-control" name="country-floating">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="company-column">Company</label>
                                    <input type="text" id="company-column" class="form-control" name="company-column">
                                </div>
                            </div>
                            <div class="col-md-6 col-12">
                                <div class="form-group">
                                    <label for="email-id-column">Email</label>
                                    <input type="email" id="email-id-column" class="form-control" name="email-id-column">
                                </div>
                            </div>
                        </diV>

                                <a href="{{route('login')}}">Have an account? Login</a>
                        <div class="clearfix">
                            <button class="btn btn-primary float-end">Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

    </div>
    <script src="{{asset('SuperAdmin/assets/js/feather-icons/feather.min.js')}}"></script>
    <script src="{{asset('SuperAdmin/assets/js/app.js')}}"></script>
    
    <script src="{{asset('SuperAdmin/assets/js/main.js')}}"></script>
</body>

</html>
