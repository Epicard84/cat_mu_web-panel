<!DOCTYPE html>
<html lang="en">

@include('SuperAdmin/components/head')
<meta name="csrf-token" content="{{ csrf_token() }}">

{{-- @foreach ($sub_users as $su)
    {{$su->getMasterDetail->id}}
@endforeach --}}

<body>
    <div id="app">
        @include('SuperAdmin/components/sidebar')
        <div id="main">
            @include('SuperAdmin/components/header')

            <div class="container-fluid">
                <div class="page-title">
                    <h3><b>All Clients</b></h3>
                </div>

                <section class="section">
                    <div class="row">
                        <div class="card-body">

                            {{-- Datatable start --}}
                            <div class="card">

                                <div class="card-body">
                                    {{-- <div align="right" class="mb-3">
                                        <label for="" class="btn icon icon-left btn-success"
                                            data-bs-toggle="modal" data-bs-target="#addmasteruser"><i
                                                class="fa-solid fa-user-plus"></i></label>
                                    </div> --}}
                                    <table class='table table-striped table-light table-sm' id="clients">
                                        <thead>
                                            <tr>
                                                <th>Sl No</th>
                                                <th>Name</th>
                                                <th>DOB</th>
                                                <th>Gender</th>
                                                <th>Status</th>
                                                <th>MasterUser Name</th>
                                                {{-- <th style="width: 15%">Action</th> --}}
                                                <th>MasterUser Email</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
            </div>
            </section>
        </div>


        @include('SuperAdmin/components/footer')

</body>
<script>
    $(document).ready(function() {


        // Active Clients DataTable ajax
        $('#clients').DataTable({
            serverSide: true,
            ajax: "{{ route('clients') }}",
            columns: [{
                    data: 'DT_RowIndex',
                    name: 'DT_RowIndex'
                },
                {
                    data: 'client_name',
                    name: 'client_name'
                },
                {
                    data: 'client_dob',
                    name: 'client_dob'
                },
                {
                    data: 'client_gender',
                    name: 'client_gender'
                },
                {
                    data: 'client_status',
                    name: 'client_status'
                },
                {
                    data: 'client_muName',
                    name: 'client_muName'
                },
                {
                    data: 'client_muEmail',
                    name: 'client_muEmail'
                },
            ],
        });
    });
</script>
<style>


</style>

</html>