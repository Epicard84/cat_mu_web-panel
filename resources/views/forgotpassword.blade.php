@extends('layouts.auth.app')

@section('title', 'Reset')

@section('content')
    <section class="loginContainer">
        <div class="forgotpassicon"><img src="{{ asset('public/MasterUser/assets/images/forgotpass.png') }}" alt="">
        </div>
        <h2>Enter your email address!</h2>
        <p>Please enter your email address to receive a verification code.</p>
        <form action="{{ route('verify_email') }}" method="post">
            @csrf
            @if (Session::has('msg'))
                <p class="text-danger">{{ Session::get('msg') }}</p>
            @endif
            <div class="mb-4 mt-3">
                <input type="email" class="form-control" name ="email" placeholder="Enter email address">
            </div>
            @if (Session::has('errors'))
                @if (array_key_exists('email', Session::get('errors')))
                    <p class="text-danger">{{ '*' . $errors['email'][0] }}</p>
                @endif
            @endif
            <button>Next</button>
        </form>
    </section>

    
    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
        });
    </script>
@endsection