@extends('layouts.auth.app')

@section('title', 'Reset')

@section('content')
    <section class="loginContainer">
        <div class="forgotpassicon"><img src="{{ asset('public/MasterUser/assets/images/entercode.png')}}" alt=""></div>
        {{-- @if (Session::has('msg'))
            <h2 class="text-danger">{{ Session::get('msg') }}</h2>
        @else --}}
            <h2>Reset password!</h2><br>
        {{-- @endif --}}
        {{-- <p>Please enter 4 digit code sent to {{ $email }}</p> --}}
                    <form action="#" method="post" id="supasscodeForm">
                        @csrf
                        {{-- @if (Session::has('msg')) --}}
                            <h6 class="text-danger description_error"></h6>
                        {{-- @endif --}}
                        <p>Enter New 4 digit passcode</p>
                        <div class="digit-group">
                            <input type="text" id="digit-1" name="digit1" data-next="digit-2" />
                            <input type="text" id="digit-2" name="digit2" data-next="digit-3"
                                data-previous="digit-1" />
                            <input type="text" id="digit-3" name="digit3" data-next="digit-4"
                                data-previous="digit-2" />
                            <input type="text" id="digit-4" name="digit4" data-previous="digit-3" />
                        </div>
                        <p>Enter Confirm 4 digit passcode</p>
                        <div class="digit-group">
                            <input type="text" id="digit-5" name="digit5" data-next="digit-6" />
                            <input type="text" id="digit-6" name="digit6" data-next="digit-7"
                                data-previous="digit-5" />
                            <input type="text" id="digit-7" name="digit7" data-next="digit-8"
                                data-previous="digit-6" />
                            <input type="text" id="digit-8" name="digit8" data-previous="digit-7" />
                        </div>
                        <input hidden name="suID" value="{{ $user->id }}">
                        <br>
                        <button type="submit" name="next" class="next" id="next">Submit</button>
                    </form>
                </section>
            </div>
        </section>
    </div>


    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('public/MasterUser/assets/js/datepick.js') }}"></script>

    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>


    {{-- <script>

        $(document).ready(function() {
            $(".check").click(function() {
                var rid = $(this).attr("data-id");
                // alert(rid);
                $.ajax({
                    url: `{{ url('check_reminder/${rid}') }}`,
                    method: 'get',

                    success: function(res) {

                        if (res.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: res.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);


                        } else {
                            console.log(res)
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'error',
                                title: res.error
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);
                        }
                    }

                })

            });
        });
    </script> --}}

    <script>
        $(document).ready(function() {
            //New Reminder
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#supasscodeForm').submit(function(e) {
                e.preventDefault();

                $('.description_error').html("");

                var form = $('#supasscodeForm')[0];
                var formData = new FormData(form);
                console.log(formData);
                $.ajax({
                    url: "{{ route('su_reset_passcode') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location = "https://mychartspace.com/";
                            }, 1501);



                        } else {

                            // let description = response.hasOwnProperty('description');                 
                            console.log(response.error);
                            if (response.error) {
                                $('.description_error').html("*" + response.error);
                            }

                        }

                    }
                });

            });
        });
    </script>
    <script>
        $('.digit-group').find('input').each(function() {
            $(this).attr('maxlength', 1);
            $(this).on('keyup', function(e) {
                var parent = $($(this).parent());

                if (e.keyCode === 8 || e.keyCode === 37) {
                    var prev = parent.find('input#' + $(this).data('previous'));

                    if (prev.length) {
                        $(prev).select();
                    }
                } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (
                        e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                    var next = parent.find('input#' + $(this).data('next'));

                    if (next.length) {
                        $(next).select();
                    } else {
                        if (parent.data('autosubmit')) {
                            parent.submit();
                        }
                    }
                }
            });
        });
    </script>

</body>

</html>
