<!DOCTYPE html>
<html lang="en">
    
@include('SubUser/Components/head')

    <body>
        <div class="container customContainer">
            <section class="dbMainbase">
                <button class="sideBtn" onclick="openNav()">☰</button>
                @include('SubUser/Components/sidebar')

            <div class="dbRight">
                @if (Session::has('success'))
                    <script>
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: false,
                            didOpen: (toast) => {
                                toast.addEventListener(
                                    'mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener(
                                    'mouseleave',
                                    Swal.resumeTimer
                                )
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: "MFD added successfuly"
                        })
                    </script>
                @endif



                @if (count($mfds) > 0)
                    @foreach ($mfds as $m)
                        <div class="medicationDetlbg">
                            <div class="mfdTop">
                                <span class="caltime"><img
                                        src="{{ asset('public/MasterUser/assets/images/caltime2.png') }}" alt="">
                                    {{ date('m-d-Y, H:i', strtotime($m->created_at)) }}</span>
                                <span
                                    class="dmbg mfdHe">{{ substr($m->getName->first_name, 0, 1) . substr($m->getName->last_name, 0, 1) }}</span>
                            </div>
                            <p>{{ $m->message }}</p>
                        </div>
                    @endforeach
                @else
                    <div class="medicationDetlbg">
                        <p>No MFD found</p>
                    </div>
                @endif

                {{-- @if ($mfd_btn == 0)
                @endif --}}
                {{-- <a href="{{ route('create_mfd') }}"><button class="commonButton respFullwdth disabled">Add new
                        MFD</button></a> --}}

            </div>
        </section>
    </div>



    @include('SubUser/Components/footer')

    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
</body>
</html>
