<!DOCTYPE html>
<html lang="en">

@include('SubUser/Components/head')
{{-- {{$date}} --}}
{{-- {{strtotime($date)}} --}}
{{-- {{dd( count($data->getActivities))}} --}}
{{-- {{date('d',strtotime($date))}} --}}
{{-- @foreach ($data->getActivities as $medicine) --}}
{{-- @foreach ($medicine as $m) --}}
{{-- @foreach ($medicine->getActivitySchedule as $ms) --}}
{{-- {{ 'medicine_id-' . $ms->medicine_id . ',date-' . date('d', strtotime($ms->date)) . ',medicine_status-' . $ms->complete_details }}<br> --}}
{{-- @endforeach --}}
{{-- {{$m}} --}}
{{-- @endforeach --}}
{{-- @endforeach --}}
{{-- {{$data->getActivities}} --}}
{{-- {{$patient_id}} --}}
{{-- {{$nach}} --}}
{{-- @foreach ($nach as $nc)
    {{ $nc }}
@endforeach --}}
{{-- @foreach ($note as $nt)
    {{ $nt }}
@endforeach --}}

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('SubUser/Components/sidebar')
            <div class="dbRight" id="divPrint" style="position: relative">
                <div class="happyEvnt dmTop mb-3 stickyName">
                    <a href="{{ route('su_dashboard') }}"><span><i class="fa-solid fa-angle-left"></i></span></a>
                    <div>
                @if ($preClient)
                    
                <span class="preClient" data-id="{{Crypt::encryptString($preClient->id)}}"><i class="fa-solid fa-angle-left" style="color: #ffffff;"></i></span>
                @endif
                </div>
                    {{-- <a href="{{ route('clients_details', Crypt::encryptString($data->id)) }}"> --}}
                        <h4>{{ $data->first_name }} {{ $data->last_name }}</h4>
                    {{-- </a> --}}
                    <div>
                @if ($nxtClient)
                    
                <span class="nxtClient" data-id="{{Crypt::encryptString($nxtClient->id)}}"><i class="fa-solid fa-angle-right" style="color: #ffffff;"></i></span>
                @endif
                </div>
                    <div><div><a href="{{ route('/master_details') }}"><span
                                class="dmbg">{{ strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1)) }}</span></a>
                    &nbsp;&nbsp;
                    
                    <span type="button" onclick="window.print()"/><i class="fal fa-print fa-lg" style="color: #ffffff;"></i></span>
                    </div>
                    <p>{{ Auth::user()->first_name . Auth::user()->last_name }} {{ Auth::user()->phone_number}}</p>
                    </div>
                </div>
                {{-- @if (Session::has('msg')) --}}
                <div class="cmBg">
                    <table>
                        <thead>
                            <tr>
                                <th><span class="prvNxtDate"
                                        data-id="{{ date('d F Y', strtotime($date . '-3 day')) }}"><i
                                            class="fa-solid fa-angle-left"></i></span></th>
                                <th>{{ date('F Y', strtotime($date)) }}</th>
                                {{-- @for ($d = date('d', strtotime($date)) - 2; $d <= date('d', strtotime($date)); $d++) --}}
                                @for ($d = strtotime($date . '-2 day'); $d <= strtotime($date); $d = $d + 86400)
                                    {{-- <th><span class="cmchart dateId" data-id="{{Crypt::encryptString(date('Y-m', strtotime($date)).'-'.$d)}}">{{ $d }}<span>c</span></span></th> --}}
                                    @php
                                        $ch = 0;
                                        $na = 0;
                                        //    $sh = "";
                                        foreach ($nach as $nc) {
                                            if (strtotime($nc->date) == $d) {
                                                if ($nc->type == 1) {
                                                    $ch = 1;
                                                    // $sh = "C";
                                                } elseif ($nc->type == 2) {
                                                    $na = 1;
                                                    // $sh = "N";
                                                }
                                            }
                                        }
                                        // $sh = '';
                                        // if ($na == 1 && $ch == 0) {
                                        //     $sh = 'N';
                                        // } elseif ($ch == 1 && $na == 0) {
                                        //     $sh = 'C';
                                        // } elseif ($na == 1 && $ch == 1) {
                                        //     $sh = 'A';
                                        // }
                                    @endphp

                                    <th><span class="cmchart dateId"
                                            data-id="{{ Crypt::encryptString(date('Y-m-d', $d)) }}">
                                            @if ($ch == 1)
                                                <span class="indRight">P</span>
                                            @endif
                                            {{ date('d', $d) }}
                                            {{-- @if ($sh)
                                                <span>{{ $sh }}</span>
                                            @endif --}}
                                            @if ($na == 1)
                                                <span>N</span>
                                            @endif
                                        </span></th>
                                @endfor
                                <th><span class="prvNxtDate"
                                        data-id="{{ date('d F Y', strtotime($date . '+3 day')) }}"><i
                                            class="fa-solid fa-angle-right"></i></span></th>
                                {{-- <th><span class="cmchart">5<span>c</span></span></th>
                                <th><span class="cmchart">6<span>c</span></span></th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            {{-- {{($segment2 == '#') ? 'active' : ''}} --}}
                            @foreach ($data->getActivities as $activity)
                                @if (strtotime($date) >= strtotime($activity->start_date) &&
                                        strtotime($date) <= strtotime($activity->end_date . '+2 day'))
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td data-id="{{ $activity->id }}" class="medicine">
                                            {{ $activity->activity_name }}
                                        <p style="font-size: 10pt">{{ $activity->descriptions}}</p>
                                        </td>
                                        {{-- @for ($d = date('d', strtotime($date)) - 2; $d <= date('d', strtotime($date)); $d++) --}}
                                        @for ($d = strtotime($date . '-2 day'); $d <= strtotime($date); $d = $d + 86400)
                                            <td>
                                                @php
                                                $meds = 0;
                                                
                                                @endphp
                                                @if (date('d', $d) >= date('d', strtotime($activity->start_date)) ||
                                                        date('d', $d) <= date('d', strtotime($activity->end_date)))
                                                    @foreach ($activity->getActivitySchedule as $ms)
                                                        {{-- {{ 'medicine_id-' . $ms->medicine_id . ',date-' . date('d', strtotime($ms->date)) . ',medicine_status-' . $ms->complete_details }} --}}
                                                        @if ($ms->activity_id == $activity->id && date('d-m-Y', strtotime($ms->date)) == date('d-m-Y', $d))
                                                            @php
                                                            
                                                                if ($ms->complete_details == null) {
                                                                    // $index = -1;
                                                                    $schtimes = explode(',',$ms->scheduled_time); //later
                                                                    $medtime = count($schtimes);
                                                                    for ($i = 0; $i < $medtime; $i++){
                                                                        $times[$i] = 0;
                                                                    }
                                                                } else {
                                                                    $times = explode(',', $ms->complete_details);
                                                                    $comtime = count($times);
                                                                    // $index = count($times);
                                                                    $schtimes = explode(',',$ms->scheduled_time); //later
                                                                    $medtime = count($schtimes);
                                                                    // if($comtime == 1){
                                                                    //     $times[0] = $times[0];
                                                                    //     for ($i = 1; $i < $medtime; $i++){
                                                                    //         $times[$i] = 0;
                                                                    //     }
                                                                    if($medtime > $comtime){
                                                                        for($i = 0; $i < $comtime; $i++){
                                                                            // $times[$i] = $times[$i];
                                                                            if ($times[$i] == '' || $times[$i] == null) {
                                                                            $times[$i] = 0;
                                                                        }else{
                                                                            $times[$i] = $times[$i];
                                                                        }
                                                                        }
                                                                        for($i = $comtime; $i < $medtime; $i++){
                                                                            $times[$i] = 0;
                                                                        }
                                                                    }else{
                                                                    for ($i = 0; $i < $medtime; $i++){
                                                                        if ($times[$i] == '' || $times[$i] == null) {
                                                                            $times[$i] = 0;
                                                                        }else{
                                                                            $times[$i] = $times[$i];
                                                                        }
                                                                    }
                                                                    }
                                                                }
                                                                // print_r ($index);
                                                                // $nt = '0';
                                                                // foreach ($note as $nt) {
                                                                //     if ($nt->schedule_id == $ms->id) {
                                                                //         $nt = '1';
                                                                //     }
                                                                // }
                                                                $meds = $ms->id;
                                                            @endphp

                                                            @for ($i = 0; $i < $medtime; $i++)
                                                                {{-- @foreach ($times as $t) --}}


                                                                {{-- @endforeach --}}
                                                                {{-- <div class="cmMediblock {{ $ms->status == '1' ? 'noMedicine' : '' }}">
                                                                <div class="cmMediStatement">{{ $d }}He<span
                                                                        class="cmNotes">N</span></div>
                                                                <small>12:00</small>
                                                                {{ $medicine->id . ' ' . $d }}
                                                                </div> --}}

                                                                @if ($times[$i] == 0)
                                                                <div
                                                                        class="cmMediblock noMedicine medSchId1 medSchId">
                                                                        <div class="cmMediStatement"
                                                                            data-id="{{ $ms->id }}_{{ $i }}">
                                                                            <input type="text" id="action"
                                                                                class="action" hidden value="0">
                                                                            {{ date('d', $d) }}
                                                                            {{-- @foreach ($note as $nt)
                                                                                @if ($nt->schedule_id == $ms->id)
                                                                                
                                                                                    <span class="cmNotes">N</span>
                                                                                @endif
                                                                            @endforeach --}}
                                                                    </div>
                                                                        {{-- <small>12:00</small> --}}
                                                                        @php
                                                                        $time= $schtimes[$i];
                                                                        @endphp
                                                                        <!-- <small style="color: rgb(150, 0, 0)">{{ $time }}</small> later -->
                                                                        {{-- {{ $medicine->id . ' ' . date('d', $d) . ' ' . $ms->id }} --}}
                                                                    </div>
                                                                @else
                                                                <div class="cmMediblock medSchId">
                                                                    <div class="cmMediStatement"
                                                                        data-id="{{ $ms->id }}_{{ $i }}">
                                                                        {{-- {{ strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1)) }} --}}
                                                                        {{ $times[$i] }}
                                                                        <input type="text" id="action"
                                                                            class="action" hidden value="1">
                                                                        {{-- @php
                                                                        $nt = "0";
                                                                        foreach ($note as $nt) {
                                                                            if($nt->schedule_id == $ms->id){
                                                                                $nt = "1";
                                                                            }
                                                                        }
                                                                        @endphp --}}
                                                                        {{-- @foreach ($note as $nt)
                                                                            @if ($nt->schedule_id == $ms->id)
                                                                                <span class="cmNotes">N</span>
                                                                            @endif
                                                                        @endforeach --}}
                                                                    </div>
                                                                    {{-- <small>{{ $times[$i] }} EST</small> --}}
                                                                    @php
                                                                    $time= $schtimes[$i];
                                                                    @endphp
                                                                    <!-- <small style="color: rgb(150, 0, 0)">{{ $time }}</small> later -->
                                                                </div>
                                                                @endif
                                                            @endfor
                                                        @endif
                                                    @endforeach
                                                @else
                                                    {{-- <div class="cmMediblock">
                                                    <div class="cmMediStatement">No</div>
                                                    <small>00:00</small>
                                                </div> --}}
                                                @endif
                                                @if($meds != 0)
                                                <div align="center" class="notesPage" data-id="{{ $meds }}">
                                                    <div class="noteini">
                                                        @foreach ($note as $nt)
                                                            @if ($nt->schedule_id == $meds)
                                                                {{-- @if ($nt == '1') --}}
                                                                <span class="cmNotes">N</span>
                                                            @endif
                                                        @endforeach
                                                        {{-- <span class="cmNotes">N</span> --}}
                                                    <button type="button"
                                                        class="btn btn-sm btn-outline-primary">Notes</button>
                                                    </div>
                                                </div>
                                                @endif
                                            </td>
                                        @endfor
                                        {{-- <td>
                                    <div class="cmMediblock">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                </td>
                                <td>
                                    <div class="cmMediblock">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                </td> --}}
                                        <td>&nbsp;</td>
                                    </tr>
                                    {{-- 
                            <tr>
                                <td>&nbsp;</td>
                                <td>Entresto</td>
                                <td>
                                    <div class="cmMediblock">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                </td>
                                <td>
                                    <div class="cmMediblock">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                </td>
                                <td>
                                    <div class="cmMediblock noMedicine">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>Farxiga</td>
                                <td>
                                    <div class="cmMediblock">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                    <div class="cmMediblock">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                </td>
                                <td>
                                    <div class="cmMediblock">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                    <div class="cmMediblock">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                </td>
                                <td>
                                    <div class="cmMediblock noMedicine">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                    <div class="cmMediblock noMedicine">
                                        <div class="cmMediStatement">He<span class="cmNotes">N</span></div>
                                        <small>12:00</small>
                                    </div>
                                </td>
                            </tr> --}}
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div>&nbsp;</div>
                {{-- @endif --}}
                {{-- <div class="medicineAdd mb-3"><a href="{{route('add_medication',Crypt::encryptString($data->id))}}"><button class="">Manage Medications</button></a></div> --}}

                {{-- @if (session::has('msg')) --}}
                <div class="clientMedfootbutton">
                    {{-- <button class="commonButton note testtip" data-id="{{ Crypt::encryptString($patient_id) }}"><span
                            class="alertSpan">Please select Medication Schedule</span>Notes</button> --}}
                    <!-- <button class="commonButton chart testtip" data-id="{{ Crypt::encryptString($patient_id) }}"><span
                            class="alertSpan">Please select a Date</span>Prog Notes</button> -->
                    <!-- <button class="commonButton narrative testtip"
                        data-id="{{ Crypt::encryptString($patient_id) }}"><span class="alertSpan">Please select a Date
                            </span>Narrative</button> -->
                </div>
                {{-- @endif --}}
                <input hidden id="patient_id" value="{{ Crypt::encryptString($patient_id) }}">
                <input hidden id="today" value="{{Crypt::encryptString(date('Y-m-d',strtotime( $date)))}}">
            </div>
        </section>
    </div>



    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    @include('SubUser/Components/footer')
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        $(document).ready(function() {
            $(".notesPage button").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            });

            $(".notesPage").on('click', function(e) {
                var id = $(this).attr("data-id");
                // alert(id);
                var patient_id = document.getElementById('patient_id').value;
                // alert(patient_id);
                window.location.href = `{{ url('su_activity_note/${id}/${patient_id}') }}`;
            });
        });
    </script>
    <script>
        $(document).ready(function() {
            $(".medSchId div").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            });

            $(".medSchId div").on('dblclick click', function(e) {
                // $("#medSchId div").dblclick(function() {
                var id = $(this).attr("data-id");
                // alert($(this).attr("data-id"));
                // alert(id);
                if (e.type == "dblclick") {
                    $.ajax({
                        // url: `{{ url('client_medication_update/${id}') }}`,
                        method: 'get',

                        success: function(res) {
                            // console.log(res)
                            // if(res.success){

                            // const Toast = Swal.mixin({
                            //         toast: true,
                            //         position: 'top-end',
                            //         showConfirmButton: false,
                            //         timer: 2500,
                            //         timerProgressBar: true,
                            //         didOpen: (toast) => {
                            //             toast.addEventListener(
                            //                 'mouseenter',
                            //                 Swal.stopTimer)
                            //             toast.addEventListener(
                            //                 'mouseleave',
                            //                 Swal.resumeTimer
                            //             )
                            //         }
                            //     })

                            //     Toast.fire({
                            //         icon: 'success',
                            //         title: response.success
                            //     })

                            // setTimeout(() => {

                            //     window.location.reload(true);
                            // }, 3000);
                            // }
                            if (res.hasOwnProperty('success')) {

                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2500,
                                    timerProgressBar: true,
                                    didOpen: (toast) => {
                                        toast.addEventListener(
                                            'mouseenter',
                                            Swal.stopTimer)
                                        toast.addEventListener(
                                            'mouseleave',
                                            Swal.resumeTimer
                                        )
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: res.success
                                })

                                setTimeout(function() {
                                    window.location.reload(false);
                                }, 2501);


                            } else {
                                console.log(res)
                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2500,
                                    timerProgressBar: true,
                                    didOpen: (toast) => {
                                        toast.addEventListener(
                                            'mouseenter',
                                            Swal.stopTimer)
                                        toast.addEventListener(
                                            'mouseleave',
                                            Swal.resumeTimer
                                        )
                                    }
                                })

                                Toast.fire({
                                    icon: 'error',
                                    title: res.error
                                })

                                setTimeout(function() {
                                    window.location.reload(false);
                                }, 2501);
                            }
                        }

                    })
                } else if (e.type == "click") {
                    // // alert(id);
                    // $(".cmchart").removeClass("datePick");
                    // $(".chart").addClass("testtip");
                    // $(".narrative").addClass("testtip");
                    // $(".cmchart").removeAttr("data-id");
                    // $(".medSchId div").removeClass("schSelect");
                    // $(".dateId").removeAttr("data-id");

                    // $(this).addClass("schSelect");

                    // // $(".cmchart").removeClass("datePick");
                    // // $(this).addClass("schedId");

                    // $(".note").removeClass("testtip");
                    // // $(".note").click(function() {
                    // //     // var type = "note";
                    // //     // alert('Note '+id);
                    // //     var patient_id = $(this).attr("data-id");
                    // //     window.location.href = `{{ url('medication_note/${id}/${patient_id}') }}`;
                    // // });
                    // $(".note").addClass("noteClick");

                    // $(".noteClick").click(function() {
                    //     // var type = "note";
                    //     // alert('Note '+id);
                    //     var patient_id = $(this).attr("data-id");
                    //     window.location.href = `{{ url('medication_note/${id}/${patient_id}') }}`;
                    // });
                    // $(".chart").click(function() {
                    //     Swal.fire({
                    //         icon: 'error',
                    //         title: 'Sorry, request is not available. ',
                    //         text: 'Click on Notes',
                    //         confirmButtonColor: '#3D4EC6',
                    //         // })
                    //     }).then((result) => {
                    //         // Reload the Page
                    //         location.reload();
                    //     });
                    // })
                    // $(".narrative").click(function() {
                    //     Swal.fire({
                    //         icon: 'error',
                    //         title: 'Sorry, request is not available. ',
                    //         text: 'Click on Notes',
                    //         confirmButtonColor: '#3D4EC6',
                    //         // })
                    //     }).then((result) => {
                    //         // Reload the Page
                    //         location.reload();
                    //     });

                    // })
                    var action = $(this).find('.action').val();
                    // alert(action);

                    if (action == 0) {
                        $.ajax({
                            url: `{{ url('su_client_activity_update/${id}') }}`,
                            method: 'get',

                            success: function(res) {
                                // console.log(res)
                                // if(res.success){

                                // const Toast = Swal.mixin({
                                //         toast: true,
                                //         position: 'top-end',
                                //         showConfirmButton: false,
                                //         timer: 2500,
                                //         timerProgressBar: true,
                                //         didOpen: (toast) => {
                                //             toast.addEventListener(
                                //                 'mouseenter',
                                //                 Swal.stopTimer)
                                //             toast.addEventListener(
                                //                 'mouseleave',
                                //                 Swal.resumeTimer
                                //             )
                                //         }
                                //     })

                                //     Toast.fire({
                                //         icon: 'success',
                                //         title: response.success
                                //     })

                                // setTimeout(() => {

                                //     window.location.reload(true);
                                // }, 3000);
                                // }
                                if (res.hasOwnProperty('success')) {

                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2000,
                                        timerProgressBar: false,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })

                                    Toast.fire({
                                        icon: 'success',
                                        title: res.success
                                    })

                                    setTimeout(function() {
                                        window.location.reload(false);
                                    }, 2501);


                                } else {
                                    console.log(res)
                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2000,
                                        timerProgressBar: false,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })

                                    Toast.fire({
                                        icon: 'error',
                                        title: res.error
                                    })

                                    setTimeout(function() {
                                        window.location.reload(false);
                                    }, 2501);
                                }
                            }

                        })
                    } else {
                        $.ajax({
                            url: `{{ url('su_client_activity_undo/${id}') }}`,
                            method: 'get',

                            success: function(res) {

                                if (res.hasOwnProperty('success')) {

                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2000,
                                        timerProgressBar: false,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })

                                    Toast.fire({
                                        icon: 'success',
                                        title: res.success
                                    })

                                    setTimeout(function() {
                                        window.location.reload(false);
                                    }, 2501);


                                } else {
                                    console.log(res)
                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2000,
                                        timerProgressBar: false,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })

                                    Toast.fire({
                                        icon: 'error',
                                        title: res.error
                                    })

                                    setTimeout(function() {
                                        window.location.reload(false);
                                    }, 2501);
                                }
                            }

                        })
                    }
                }
            });

            //Medicine Details
            $(".medicine").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    var med_id = $(this).attr("data-id");
                    // alert(med_id);
                    // window.location.href = `{{ url('/su_activity_details/${med_id}') }}`;
                }
            });

            //Previous and Next date
            $(".prvNxtDate").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
                click: function() {
                    var date = $(this).attr("data-id");
                    var patient_id = document.getElementById("patient_id").value
                    // alert(date+' '+patient_id);
                    // window.location.href = `{{ url('/client_med_prenxt/${patient_id}/${date}') }}`;
                    window.location.href = `{{ url('/su_client_medication/${patient_id}/${date}') }}`;
                }
            });

            //Chart Details
            $(".chart").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
                
                click: function() {
                    var patient_id = $(this).attr("data-id");
                    var today = document.getElementById('today').value;
                    // alert(today);
                    window.location.href =
                        `{{ url('/su_medication_chart/${today}/${patient_id}') }}`;
                }
            });

            //Narrative Details
            $(".narrative").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    var patient_id = $(this).attr("data-id");
                    var today = document.getElementById('today').value;
                    window.location.href =
                        `{{ url('/su_medication_narrative/${today}/${patient_id}') }}`;
                }
            });

            // Date + Chart
            $(".dateId").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    $(".cmchart").removeClass("datePick");
                    $(".medSchId div").removeClass("schSelect");
                    $(".note").addClass("testtip");
                    $(".note").removeClass("noteClick");
                    $(this).addClass("datePick");

                    var date = $(this).attr("data-id");
                    // alert(date);
                    $(".chart").removeClass("testtip");
                    $(".narrative").removeClass("testtip");
                    $(".chart").click(function() {
                        // var type = "note";
                        var patient_id = $(this).attr("data-id");
                        // alert('Chart '+patient_id);
                        window.location.href =
                            `{{ url('/su_medication_chart/${date}/${patient_id}') }}`;
                    });
                    $(".narrative").click(function() {
                        // var type = "note";
                        // alert('Note '+id);
                        var patient_id = $(this).attr("data-id");
                        window.location.href =
                            `{{ url('/su_medication_narrative/${date}/${patient_id}') }}`;
                    });
                    $(".note").click(function() {
                        // alert('Sorry, your request is not available. Click on Charts or Narrative');
                        Swal.fire({
                            icon: 'error',
                            title: 'Sorry, request is not available. ',
                            text: 'Click on Charts or Narrative',
                            confirmButtonColor: '#3D4EC6',
                            // })
                        }).then((result) => {
                            // Reload the Page
                            location.reload();
                        });
                    })
                }
            });
            //Previous Patient
            $(".preClient").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
                click: function() {
                    var patient_id = $(this).attr("data-id");
                    
                    // alert(patient_id);
                    var date = "0";
                    window.location.href = `{{ url('/su_client_medication/${patient_id}/${date}') }}`;
                }
            });
            //Next Patient
            $(".nxtClient").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
                click: function() {
                    var patient_id = $(this).attr("data-id");
                    
                    // alert(patient_id);
                    var date = "0";
                    window.location.href = `{{ url('/su_client_medication/${patient_id}/${date}') }}`;
                }
            });
        });
    </script>
    {{-- <script>
        var button = document.getElementById("pdfButton");
        var makepdf = document.getElementById("divPrint");
        button.addEventListener("click", function () {
           var mywindow = window.open("", "PRINT", "height=600,width=800");
           mywindow.document.write(makepdf.innerHTML);
           mywindow.document.close();
           mywindow.focus();
           mywindow.print();
           return true;
        });
     </script>  --}}
</body>

</html>
