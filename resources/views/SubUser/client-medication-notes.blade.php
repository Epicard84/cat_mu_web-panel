<!DOCTYPE html>
<html lang="en">
<link rel='stylesheet' href="{{ asset('public/MasterUser/assets/css/gallery.min.css') }}">
@include('SubUser/Components/head')
{{-- {{ $date_note }} --}}
{{-- {{ $patient_id }} --}}
{{-- {{ $med_shedule->id }} --}}
{{-- {{$form}} --}}

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('SubUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt dmTop mb-3">
                    <div class="sbBack back" data-id="{{ Crypt::encryptString($patient_id) }}">
                        <span class="sbAngle back" onclick="javascript:history.go(-1)"><i
                                class="fa-solid fa-angle-left"></i></span>
                        <h4>{{ $med_name }}</h4>
                    </div>
                    <div>
                        <h4>{{$patName}}</h4>
                    </div>
                    <div class="caltimeMain"><span class="caltime"><img
                                src="{{ asset('public/MasterUser/assets/images/caltime.png') }}" alt="" />
                            {{ date('m-d-Y', strtotime($med_shedule->date)) }}</span><a
                            href="{{ route('/master_details') }}"><span
                                class="dmbg">{{ strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1)) }}</span></a>
                    </div>
                </div>
                @if ($form == 1)
                <form method="POST" enctype="multipart/form-data" id="noteForm">
                    @csrf
                    <div class="row col-12">
                        <div class="col-7">
                            <input hidden name="schedule_id" value="{{ $med_shedule->id }}">
                            <textarea class="form-control" rows="1" placeholder="Enter Note Text" name="description" id="description"></textarea>
                            <p class="description_error" style="color: red; font-size:10pt"></p>
                        </div>
                        <div class="col-4">
                            <input type="file" class="form-control" multiple name="detailsImage[]" accept=".doc,.docx,.pdf,.png,.jpeg,.jpg"/>
                        </div>
                        <div class="col-1">
                            <button type="submit" class="btn btn-sm" style="background-color: #3D4EC6; color:white">Add</button>
                        </div>
                    </div>
                    <input hidden name="patient_id" value="{{ $patient_id }}">
                    <input hidden name="type" value="0">
                </form>
                @endif

                @if (count($date_note) > 0)
                    @php
                        $i = 0;
                    @endphp
                    @foreach ($date_note as $note)
                        <div class="medicationDetlbg">
                            {{-- <div class="timeshow">{{ date('Hi', strtotime($note->created_at)) }}hrs</div> --}}
                            @if ($form == 1 && Auth::user()->id==$note->creator_id)
                                <div class="edit" data-bs-toggle="modal"
                                    data-bs-target="#notesEditModal{{ $note->id }}" data-id="{{ $note->id }}">
                                    <i class="fas fa-edit"></i></div>
                            @endif
                            <div class="clientMediNoteblock">
                                @if ($note->getName->user_type == 1)
                                    <span class="dmbg clickMUProfile"
                                        data-id="{{ $note->getName->id }}">{{ strtoupper(substr($note->getName->first_name, 0, 1)) . strtoupper(substr($note->getName->last_name, 0, 1)) }}</span>
                                    &nbsp; &nbsp;
                                @else
                                    <span
                                        class="dmbg">{{ strtoupper(substr($note->getName->first_name, 0, 1)) . strtoupper(substr($note->getName->last_name, 0, 1)) }}</span>
                                    &nbsp; &nbsp;
                                @endif

                                <div class="schedDetails">
                                    <p>{{ $note->descriptions }}</p>
                                    {{-- @if ($note->images) --}}
                                    @if ($note->images)
                                        @php
                                            $images = explode(',', $note->images);
                                            $doc = 0;
                                            $img = 0;
                                            //
                                        
                                        foreach ($images as $image){
                                        
                                            $doc1 = explode('.', $image);
                                            $type = $doc1[1];
                                            if ($type == 'pdf' || $type == 'docx' || $type == 'doc') {
                                                $doc = 1;
                                            }
                                        }
                                            // @endforeach
                                        @endphp

                                        @if ($doc != 1)
                                            <div class="cmnImgbg gallery" data-id="{{ $i }}">
                                                @php
                                                    $images = explode(',', $note->images);
                                                @endphp
                                                @foreach ($images as $image)
                                                    <a href="{{ asset('public/documents/Schedule_Details/' . $image) }}"
                                                        title="Note {{ date('m-d-Y', strtotime($med_shedule->date)) }}">
                                                        <figure><img class="img-fluid img-thumbnail"
                                                                src="{{ asset('public/documents/Schedule_Details/' . $image) }}"
                                                                alt="{{ $image }}">
                                                        </figure>
                                                    </a>
                                                @endforeach
                                            </div>
                                        @else
                                            @php
                                            $images = explode(',', $note->images);
                                            @endphp
                                            <div class="d-flex d-inline">
                                                @foreach ($images as $image)
                                                <div class="pdfbg mx-2">
                                                    
                                                    Document
                                                    &nbsp;
                                                    <a href="{{ asset('public/documents/Schedule_Details/' . $image) }}"><label
                                                            class="commonButton savebtn viewbtn"><i
                                                                class="fa-light fa-download"></i></label></a>
                                                </div>
                                                
                                                @endforeach
                                            </div>
                                            
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        @php
                            $i = $i + 1;
                        @endphp

                         <!-- Edit Notes-Modal -->
                         <div class="modal fade modalBase" id="notesEditModal{{ $note->id }}" tabindex="-1"
                            aria-labelledby="notesModalLabel{{ $note->id }}" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Note</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form enctype="multipart/form-data" id="editDetailsForm{{ $note->id }}">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="enterMfd">
                                                <input hidden name="details_id" value="{{ $note->id }}">
                                                <textarea class="form-control" name="description">{{ $note->descriptions }}</textarea>
                                                <p class="description_error_1" style="color: red; font-size:10pt"></p>
                                                <input type="file" multiple name="detailsImage[]" />
                                            </div>
                                            @if ($note->images != null)
                                                @php
                                                    $files = explode(',', $note->images);
                                                @endphp
                                                <div class="mb-3">
                                                    @foreach ($files as $f)
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox"
                                                                value="{{ $f }}" id="flexCheckChecked"
                                                                name="docs[]" checked>
                                                            <label class="form-check-label" for="flexCheckChecked">
                                                                {{ $f }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                        <div class="modal-footer">
                                            <button id="submit{{ $note->id }}" type="button"
                                                class="btn commonButton">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div>
                        <p>&nbsp; No notes found...</p>
                    </div>
                @endif

                {{-- <button class="commonButton respFullwdth" data-bs-toggle="modal" data-bs-target="#notesModal">Add
                    Notes</button> --}}

            </div>
    </div>
    </section>
    </div>

    {{-- <!-- Notes-Modal -->
    <div class="modal fade modalBase" id="notesModal" tabindex="-1" aria-labelledby="notesModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Notes</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="POST" enctype="multipart/form-data" id="#">
                    @csrf
                    <div class="modal-body">
                        <div class="enterMfd">
                            <input hidden name="patient_id" value="{{ $patient_id }}">
                            <input hidden name="type" value="0">
                            <input hidden name="schedule_id" value="{{ $med_shedule->id }}">
                            <textarea class="form-control" placeholder="Enter Note Text" name="description"></textarea>
                            <p class="description_error" style="color: red; font-size:10pt"></p>
                            <input type="file" multiple name="detailsImage[]" accept="image/*" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn commonButton">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div> --}}

    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('public/MasterUser/assets/js/gallery.min.js') }}"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        (function($) {
            $('.gallery').each(function() { // the containers for all your galleries
                $(this).magnificPopup({
                    delegate: "a",
                    type: "image",
                    tLoading: "Loading image #%curr%...",
                    mainClass: "mfp-img-mobile",
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,
                            1
                        ] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                    }
                });
            });
        })(jQuery);
    </script>
    <script>
        $(document).ready(function() {

            $('#description').keyup(function(event) {
                var textBox = event.target;
                var start = textBox.selectionStart;
                var end = textBox.selectionEnd;
                textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);
            });

            // $(".back").click(function() {
            // var patient_id = $(this).attr("data-id");
            // alert(patient_id);
            // window.location.href = `{{ url('client_medication/${patient_id}') }}`;
            // window.history.backward();
            // });

            $(".edit").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            })

            //Edit Note Form Submit
            $('.edit').click(function(e) {
                e.preventDefault();
                var uid = $(this).attr('data-id');
                id = "#editDetailsForm" + uid;
                var btnid = "#submit" + uid;
                $(btnid).on("click", function(e) {
                    e.preventDefault();
                    $('.description_error').html("");

                    var form = $(id)[0];
                    var formData = new FormData(form);
                    console.log(formData);
                    $.ajax({
                        url: "{{ route('su_edit_details_form') }}",
                        type: 'POST',
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        data: formData,
                        success: function(response) {

                            console.log(response);

                            if (response.hasOwnProperty('success')) {

                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2000,
                                    timerProgressBar: false,
                                    didOpen: (toast) => {
                                        toast.addEventListener(
                                            'mouseenter',
                                            Swal.stopTimer)
                                        toast.addEventListener(
                                            'mouseleave',
                                            Swal.resumeTimer
                                        )
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: response.success
                                })

                                setTimeout(function() {
                                    window.location.reload(false);
                                }, 2501);

                            } else {
                                console.log(response.error);
                                if (response.error) {
                                    $('.description_error_1').html("*" + response.error);
                                }
                            }
                        }
                    });
                })
            });

            //New Chart
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#noteForm').submit(function(e) {
                e.preventDefault();

                $('.description_error').html("");

                var form = $('#noteForm')[0];
                var formData = new FormData(form);
                console.log(formData);
                $.ajax({
                    url: "{{ route('su_new_details') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);

                        } else {
                            console.log(response.error);
                            if (response.error) {
                                $('.description_error').html("*" + response.error);
                            }
                        }
                    }
                });
            })
            $(".clickMUProfile").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            })
            $(".clickMUProfile").click(function() {
                var master_id = $(this).attr("data-id");
                // alert(master_id);
                window.location.href =
                    `{{ url('master_profile/${master_id}') }}`;
            });
        });
    </script>
</body>

</html>
