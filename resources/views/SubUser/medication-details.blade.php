<!DOCTYPE html>
<html lang="en">

@include('SubUser/Components/head')
{{-- {{$med_data}} --}}

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('SubUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt dmTop mb-3">
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long"></i></span>
                    <h4>{{ $med_data->medicine_name }}</h4>
                    <div class="caltimeMain"><span class="caltime"><img
                                src="{{ asset('MasterUser/assets/images/caltime.png') }}" alt="" /></span><a
                            href="{{ route('/master_details') }}"><span
                                class="dmbg">{{ strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1)) }}</span></a>
                    </div>
                </div>
                <div class="medicationDetlbg">
                    <p>{{ $med_data->descriptions }}</p>
                    <div class="row">
                        @if ($med_data->docs)
                            @php
                                $files = explode(',', $med_data->docs);
                            @endphp
                            @foreach ($files as $docs)
                                @php
                                    $doc = explode('.', $docs);
                                    $type = $doc[1];
                                @endphp
                                @if ($type == 'jpg' || $type == 'jpeg' || $type == 'png')
                                    <div class="mdbLeft mb-3"><img
                                            src="{{ asset('public/documents/Medicine_Docs/' . $docs) }}" alt=""
                                            height="200px" width="250px" /></div>
                                @else
                                    <div class="mdbRight mb-3">
                                        <div class="pdfbg">
                                            <h6>{{ $med_data->medicine_name }} Manual</h6>
                                            &nbsp;
                                            <a href="{{ asset('public/documents/Medicine_Docs/' . $docs) }}"><label
                                                    class="commonButton savebtn viewbtn"><i
                                                        class="fa-light fa-download"></i></label></a>
                                        </div>

                                    </div>
                                @endif
                            @endforeach
                        @else
                            <div class="medicationDetlBtm">
                                <p class="text-info">*No Documents Available</p>
                            </div>
                        @endif
                    </div>
                </div>
                {{-- <div class="clientMedfootbutton">
                    <button class="commonButton" onclick="window.location.href = 'add-document.html';"><i class="fa-regular fa-plus"></i> Upload Image/Documents</button>
                </div> --}}

            </div>
        </section>
    </div>



    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    @include('SubUser/Components/footer')
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        $(document).ready(function() {
            $(".happyEvnt i").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            });
        });
    </script>
</body>

</html>
