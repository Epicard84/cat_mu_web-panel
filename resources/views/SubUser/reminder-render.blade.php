@if (count($reminders) > 0)
    @foreach ($reminders as $reminder)
        <div class="reminderBlock mb-3">
            <div class="reminderdiv">
                <span><img src="{{ asset('MasterUser/assets/images/reminder.png') }}" alt=""></span>
                <div>
                    <p>{{ $reminder->reminder }}</p>
                </div>
            </div>
            <!-- @if ($reminder->status == 1)
                <span class="greenTk"><i class="fas fa-check-circle"></i></span>
            @else -->
                <!-- <span class="warnTk {{ $check == 1 ? 'check' : '' }}" data-id="{{ $reminder->id }}"><i
                        class="far fa-circle"></i></span> -->
                        <label data-id="{{ $reminder->id }}"
                                                class="deleteReminder" style="margin-left: 30px;"><i class="fas fa-trash" style="color: #3D4EC6 ;"></i></label>
            <!-- @endif -->
        </div>
    @endforeach
@else
    <div class="medicationDetlbg">
        <div class="mfdTop">
        </div>
        <p>No Reminder available...</p>
    </div>
@endif

<script>
    //Notification Seen Update
    $(".check").on({
        mouseenter: function() {
            $(this).css("cursor", "pointer");
        },

        mouseleave: function() {
            $(this).css("cursor", "auto");
        },
    });
    $(document).ready(function() {
        $(".check").click(function() {
            var rid = $(this).attr("data-id");
            // alert(rid);
            $.ajax({
                url: `{{ url('check_reminder/${rid}') }}`,
                method: 'get',

                success: function(res) {

                    if (res.hasOwnProperty('success')) {

                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1500,
                            timerProgressBar: false,
                            didOpen: (toast) => {
                                toast.addEventListener(
                                    'mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener(
                                    'mouseleave',
                                    Swal.resumeTimer
                                )
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: res.success
                        })

                        setTimeout(function() {
                            window.location.reload(false);
                        }, 2501);


                    } else {
                        console.log(res)
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1500,
                            timerProgressBar: false,
                            didOpen: (toast) => {
                                toast.addEventListener(
                                    'mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener(
                                    'mouseleave',
                                    Swal.resumeTimer
                                )
                            }
                        })

                        Toast.fire({
                            icon: 'error',
                            title: res.error
                        })

                        setTimeout(function() {
                            window.location.reload(false);
                        }, 2501);
                    }
                }

            })

        });
    });
</script>
