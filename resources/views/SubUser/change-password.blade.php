<!DOCTYPE html>
<html lang="en">

@include('SubUser/Components/head')

{{-- {{$reminders}} --}}
{{-- {{$date}} --}}

<body>

    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('SubUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt reminderTop mb-3">
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long"
                            style="color: #ffffff;"></i></span>
                    <h4>Change Passcode</h4>
                    <span></span>
                </div>
                <section class="loginContainer subUsrchpasscontainer">
                    <form action="#" method="post" id="passcodeForm">
                        @csrf
                        {{-- @if (Session::has('msg')) --}}
                            <h6 class="text-danger description_error"></h6>
                        {{-- @endif --}}
                        <p>Please enter Old 4 digit passcode</p>
                        <div class="digit-group">
                            <input type="text" id="digit-1" name="digit1" data-next="digit-2" />
                            <input type="text" id="digit-2" name="digit2" data-next="digit-3"
                                data-previous="digit-1" />
                            <input type="text" id="digit-3" name="digit3" data-next="digit-4"
                                data-previous="digit-2" />
                            <input type="text" id="digit-4" name="digit4" data-previous="digit-3" />
                        </div>
                        <p>Please enter New 4 digit passcode</p>
                        <div class="digit-group">
                            <input type="text" id="digit-5" name="digit5" data-next="digit-6" />
                            <input type="text" id="digit-6" name="digit6" data-next="digit-7"
                                data-previous="digit-5" />
                            <input type="text" id="digit-7" name="digit7" data-next="digit-8"
                                data-previous="digit-6" />
                            <input type="text" id="digit-8" name="digit8" data-previous="digit-7" />
                        </div>
                        <p>Please enter Confirm 4 digit passcode</p>
                        <div class="digit-group">
                            <input type="text" id="digit-9" name="digit9" data-next="digit-10" />
                            <input type="text" id="digit-10" name="digit10" data-next="digit-11"
                                data-previous="digit-9" />
                            <input type="text" id="digit-11" name="digit11" data-next="digit-12"
                                data-previous="digit-10" />
                            <input type="text" id="digit-12" name="digit12" data-previous="digit-11" />
                        </div>
                        <input hidden name="suID" value="{{ Auth::user()->id }}">
                        <button type="submit" name="next" class="next" id="next">Submit</button>
                    </form>
                </section>
            </div>
        </section>
    </div>


    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    @include('SubUser/Components/footer')
    <script src="{{ asset('public/MasterUser/assets/js/datepick.js') }}"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>


    {{-- <script>

        $(document).ready(function() {
            $(".check").click(function() {
                var rid = $(this).attr("data-id");
                // alert(rid);
                $.ajax({
                    url: `{{ url('check_reminder/${rid}') }}`,
                    method: 'get',

                    success: function(res) {

                        if (res.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: res.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);


                        } else {
                            console.log(res)
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'error',
                                title: res.error
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);
                        }
                    }

                })

            });
        });
    </script> --}}

    <script>
        $(document).ready(function() {
            //New Reminder
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#passcodeForm').submit(function(e) {
                e.preventDefault();

                $('.description_error').html("");

                var form = $('#passcodeForm')[0];
                var formData = new FormData(form);
                console.log(formData);
                $.ajax({
                    url: "{{ route('update_passcode') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 1501);



                        } else {

                            // let description = response.hasOwnProperty('description');                 
                            console.log(response.error);
                            if (response.error) {
                                $('.description_error').html("*" + response.error);
                            }

                        }

                    }
                });

            });
        });
    </script>
    <script>
        $('.digit-group').find('input').each(function() {
            $(this).attr('maxlength', 1);
            $(this).on('keyup', function(e) {
                var parent = $($(this).parent());

                if (e.keyCode === 8 || e.keyCode === 37) {
                    var prev = parent.find('input#' + $(this).data('previous'));

                    if (prev.length) {
                        $(prev).select();
                    }
                } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (
                        e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                    var next = parent.find('input#' + $(this).data('next'));

                    if (next.length) {
                        $(next).select();
                    } else {
                        if (parent.data('autosubmit')) {
                            parent.submit();
                        }
                    }
                }
            });
        });
    </script>

</body>

</html>
