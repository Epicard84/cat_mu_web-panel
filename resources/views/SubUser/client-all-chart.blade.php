<!DOCTYPE html>
<html lang="en">
<link rel='stylesheet' href="{{ asset('public/MasterUser/assets/css/gallery.min.css') }}">
@include('SubUser/Components/head')
{{-- {{$date_chart}}
{{$patient_id}} --}}
{{-- {{$form}} --}}
{{-- {{$date}} --}}

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('SubUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt dmTop mb-3">
                    <div class="sbBack">
                        <span class="sbAngle back" data-id="{{ Crypt::encryptString($patient_id) }}"
                            onclick="javascript:history.go(-1)"><i class="fa-solid fa-angle-left"></i></span>
                        <h6 style="color:white">Notes / Prog Notes / Dr's Orders</h6>
                    </div>
                    <div>
                        <h4>{{ $patName }}</h4>
                    </div>
                    <div class="caltimeMain"><span
                                class="dmbg">{{ strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1)) }}</span>
                    </div>
                </div>
                {{-- @if ($form == 1) --}}
                    {{-- <form method="POST" enctype="multipart/form-data" id="chartForm">
                        @csrf
                        <div class="row col-12">
                            <div class="col-7">
                                <textarea class="form-control" rows="1" placeholder="Enter Prog Notes Text" name="description" id="description"></textarea>
                                <p class="description_error" style="color: red; font-size:10pt"></p>
                            </div>
                            <div class="col-4">
                                <input type="file" class="form-control" multiple name="detailsImage[]" />
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btn-sm"
                                    style="background-color: #3D4EC6; color:white">Add</button>
                            </div>
                            <input hidden name="patient_id" value="{{ $patient_id }}">
                            <input hidden name="type" value="1"> --}}
                            {{-- <input hidden name="date" value="{{ $date }}"> --}}
                        {{-- </div>
                    </form> --}}
                {{-- @endif --}}
                @if (count($date_chart) > 0)
                    @foreach ($date_chart as $chart)
                        @php
                            $type = "";
                            if($chart->type == 0){
                                $type = "Note";
                            }elseif ($chart->type == 1) {
                                $type = "Prog Note";
                            }else{
                                $type = "Dr's Order";
                            }
                        @endphp

                        <div class="medicationDetlbg">
                            <div class="detailstype">{{$type}}</div>
                            <div class="timeshow">{{ date('m-d-Y Hi', strtotime($chart->created_at)) }}hrs</div>
                            @if ($date == $chart->date && Auth::user()->id == $chart->creator_id)
                                <div class="edit" data-bs-toggle="modal"
                                    data-bs-target="#notesEditModal{{ $chart->id }}" data-id="{{ $chart->id }}">
                                    <i class="fas fa-edit"></i>
                                </div>
                            @endif
                            <div class="clientMediNoteblock">
                                @if ($chart->getName->user_type == 1)
                                    <span class="dmbg clickMUProfile"
                                        data-id="{{ $chart->getName->id }}">{{ strtoupper(substr($chart->getName->first_name, 0, 1)) . strtoupper(substr($chart->getName->last_name, 0, 1)) }}</span>
                                    &nbsp; &nbsp;
                                @else
                                    <span
                                        class="dmbg">{{ strtoupper(substr($chart->getName->first_name, 0, 1)) . strtoupper(substr($chart->getName->last_name, 0, 1)) }}</span>
                                    &nbsp; &nbsp;
                                @endif
                                <div class="schedDetails">
                                    <p>{{ $chart->descriptions }}</p>
                                    @if ($chart->images)
                                        @php
                                            $images = explode(',', $chart->images);
                                            $doc = 0;
                                            $img = 0;
                                            
                                            foreach ($images as $image) {
                                                $doc1 = explode('.', $image);
                                                $type = $doc1[1];
                                                if ($type == 'pdf' || $type == 'docx' || $type == 'doc') {
                                                    $doc = 1;
                                                }
                                            }
                                            
                                        @endphp

                                        @if ($doc != 1)
                                            <div class="cmnImgbg gallery">
                                                @php
                                                    $images = explode(',', $chart->images);
                                                @endphp
                                                @foreach ($images as $image)
                                                    <a href="{{ asset('public/documents/Schedule_Details/' . $image) }}"
                                                        title="Chart {{ date('m-d-Y', strtotime($chart->created_at)) }}">
                                                        <figure><img class="img-fluid img-thumbnail"
                                                                src="{{ asset('public/documents/Schedule_Details/' . $image) }}"
                                                                alt="{{ $image }}"></figure>
                                                    </a>
                                                @endforeach
                                            </div>
                                        @else
                                            @php
                                                $images = explode(',', $chart->images);
                                            @endphp
                                            <div class="d-flex d-inline">
                                                @foreach ($images as $image)
                                                    <div class="pdfbg mx-2">

                                                        Document
                                                        &nbsp;
                                                        <a
                                                            href="{{ asset('public/documents/Schedule_Details/' . $image) }}"><label
                                                                class="commonButton savebtn viewbtn"><i
                                                                    class="fa-light fa-download"></i></label></a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- Edit Notes-Modal -->
                        <div class="modal fade modalBase" id="notesEditModal{{ $chart->id }}" tabindex="-1"
                            aria-labelledby="notesModalLabel{{ $chart->id }}" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Chart</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form enctype="multipart/form-data" id="editDetailsForm{{ $chart->id }}">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="enterMfd">
                                                <input hidden name="details_id" value="{{ $chart->id }}">
                                                <textarea class="form-control" name="description">{{ $chart->descriptions }}</textarea>
                                                <p class="description_error" style="color: red; font-size:10pt"></p>
                                                <input type="file" multiple name="detailsImage[]" />
                                            </div>
                                            @if ($chart->images != null)
                                                @php
                                                    $files = explode(',', $chart->images);
                                                @endphp
                                                <div class="mb-3">
                                                    @foreach ($files as $f)
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox"
                                                                value="{{ $f }}" id="flexCheckChecked"
                                                                name="docs[]" checked>
                                                            <label class="form-check-label" for="flexCheckChecked">
                                                                {{ $f }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                        <div class="modal-footer">
                                            <button id="submit{{ $chart->id }}" type="button"
                                                class="btn commonButton">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div>
                        <p>&nbsp; Not found...</p>
                    </div>
                @endif
                {{-- <button class="commonButton respFullwdth" data-bs-toggle="modal" data-bs-target="#chartModal">
                    Add Charts
                </button> --}}
            </div>
            {{-- </div> --}}

        </section>
    </div>

    {{-- <!-- Chart-Modal -->
    <div class="modal fade modalBase" id="#" tabindex="-1" aria-labelledby="chartModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Chart</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="POST" enctype="multipart/form-data" id="chartForm">
                    @csrf
                    <div class="modal-body">
                        <div class="enterMfd">
                            <input hidden name="patient_id" value="{{ $patient_id }}">
                            <input hidden name="type" value="1">
                            <input hidden name="date" value="{{ $date }}">
                            <textarea class="form-control" placeholder="Enter Chart Text" name="description"></textarea>
                            <p class="description_error" style="color: red; font-size:10pt"></p>
                            <input type="file" multiple name="detailsImage[]" accept="image/*" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn commonButton">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div> --}}


    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    {{-- @include('SubUser/Components/footer') --}}
    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('public/MasterUser/assets/js/gallery.min.js') }}"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        (function($) {
            $('.gallery').each(function() { // the containers for all your galleries
                $(this).magnificPopup({
                    delegate: "a",
                    type: "image",
                    tLoading: "Loading image #%curr%...",
                    mainClass: "mfp-img-mobile",
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,
                            1
                        ] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                    }
                });
            });
        })(jQuery);
    </script>
    <script>
        $(document).ready(function() {

            $('#description').keyup(function(event) {
                var textBox = event.target;
                var start = textBox.selectionStart;
                var end = textBox.selectionEnd;
                textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);
            });

            // $(".back").click(function() {
            //     var patient_id = $(this).attr("data-id");
            //     // alert(patient_id);
            //     window.location.href = `{{ url('client_medication/${patient_id}') }}`;
            // });

            $(".edit").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            })

            //Edit Note Form Submit
            $('.edit').click(function(e) {
                e.preventDefault();
                var uid = $(this).attr('data-id');
                id = "#editDetailsForm" + uid;
                var btnid = "#submit" + uid;
                $(btnid).on("click", function(e) {
                    e.preventDefault();
                    $('.description_error').html("");

                    var form = $(id)[0];
                    var formData = new FormData(form);
                    console.log(formData);
                    $.ajax({
                        url: "{{ route('su_edit_details_form') }}",
                        type: 'POST',
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        data: formData,
                        success: function(response) {

                            console.log(response);

                            if (response.hasOwnProperty('success')) {

                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2000,
                                    timerProgressBar: false,
                                    didOpen: (toast) => {
                                        toast.addEventListener(
                                            'mouseenter',
                                            Swal.stopTimer)
                                        toast.addEventListener(
                                            'mouseleave',
                                            Swal.resumeTimer
                                        )
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: response.success
                                })

                                setTimeout(function() {
                                    window.location.reload(false);
                                }, 2501);

                            } else {
                                console.log(response.error);
                                if (response.error) {
                                    $('.description_error').html("*" + response.error);
                                }
                            }
                        }
                    });
                })
            });

            //New Chart
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#chartForm').submit(function(e) {
                e.preventDefault();

                $('.description_error').html("");

                var form = $('#chartForm')[0];
                var formData = new FormData(form);
                console.log(formData);
                $.ajax({
                    url: "{{ route('su_new_details') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);


                            // } else if (response.hasOwnProperty('error')) {
                            //     Swal.fire({
                            //         icon: 'error',
                            //         title: 'Sorry..',
                            //         text: response.error,
                            //         confirmButtonColor: '#3D4EC6',
                            //     })

                        } else {

                            // let description = response.hasOwnProperty('description');                 
                            console.log(response.error);
                            if (response.error) {
                                $('.description_error').append("*" + response.error);
                            }

                        }

                    }
                });
                
            })
            $(".clickMUProfile").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            })
            $(".clickMUProfile").click(function() {
                var master_id = $(this).attr("data-id");
                // alert(master_id);
                window.location.href =
                    `{{ url('master_profile/${master_id}') }}`;
            });
        });
    </script>
</body>

</html>
