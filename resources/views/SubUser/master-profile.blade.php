@include('SubUser/Components/head')
<body>
    {{-- {{$masterDetails}}
{{$muLocation}}
{{$archivedClients}} --}}
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('SubUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt mb-4">
                    <div>
                        <div class="subusr mastertopSection">
                            @if ($masterDetails->profile_image_name)
                                    <div class="subusrUpload"><img
                                            src="{{ asset('images/MasterUserProfile/' . $masterDetails->profile_image_name) }}"
                                            alt="" /></div>
                                @else
                                    <div class="subusrUpload"><img
                                            src="{{ asset('public/images/MasterUserProfile/defult_image.png') }}"
                                            alt="" /></div>
                                @endif
                            <div class="masterHdn">
                                <h4>{{$masterDetails->first_name." ".$masterDetails->last_name}}</h4>
                                <p>{{$muLocation->location}}</p>
                                <div class="masterStatusToggle">
                                    <strong>Status:</strong>
                                    <div class="btn-group btn-toggle">
                                        @if($muLocation->technical_status == "online")
                                        <button class="btn btn-default active">Online</button>
                                        @else
                                        <button class="btn btn-primary active">Offline</button>
                                        @endif
                                    </div>
                                </div>                                
                            </div>
                        </div>
                        
                    </div>
                    {{-- <span class="editIcon" onclick="window.location.href = 'edit-master-user.html';"><i class="fa-light fa-pen"></i></span> --}}
                </div>
                <div class="row mb-2">
                    <div class="col-md-12">
                        <h4 class="mb-2">Personal Information</h4>
                        <div class="personalInfo autoHeight">
                            <p>Phone number: {{$masterDetails->phone_number}}</p>
                            <p>Email address: {{$masterDetails->email}}</p>
                            <p>Facility Address: {{$masterDetails->permanent_address}}.</p>
                            <p>City: {{$masterDetails->city}}.</p>
                            <p>State: {{$masterDetails->state}}</p>
                            <p>Country: {{$masterDetails->country}}</p>
                        </div>
                        {{-- <div class="masterProfButton mb-5">
                            <button class="commonButton">Add Subuser</button>
                            <button class="commonButton" onclick="window.location.href = 'add-client.html';">Add Client</button>
                        </div> --}}
                        <h4 class="mb-2 text-center mb-3">Archived Clients</h4>
                        @foreach ($archivedClients as $ac)
                            
                        <div class="evntbtns">
                            <button>{{$ac->first_name." ".$ac->last_name}}</button>
                        </div>
                        @endforeach
                    </div>
                </div>
<!--                <button class="commonButton">Add Client</button>-->
            </div>
        </section>
    </div>


    @include('SubUser/Components/footer')

    <script>
        $('.btn-toggle').click(function() {
        $(this).find('.btn').toggleClass('active');

        if ($(this).find('.btn-primary').length>0) {
        $(this).find('.btn').toggleClass('btn-primary');
        }
        if ($(this).find('.btn-danger').length>0) {
        $(this).find('.btn').toggleClass('btn-danger');
        }
        if ($(this).find('.btn-success').length>0) {
        $(this).find('.btn').toggleClass('btn-success');
        }
        if ($(this).find('.btn-info').length>0) {
        $(this).find('.btn').toggleClass('btn-info');
        }

        $(this).find('.btn').toggleClass('btn-default');

        });

        $('form').submit(function(){
        var radioValue = $("input[name='options']:checked").val();
        if(radioValue){
        alert("You selected - " + radioValue);
        };
        return false;
        });
    </script>
       

</body>

</html>
