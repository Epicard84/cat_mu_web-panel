@include('SubUser/Components/head')
{{-- {{Auth::user()}} --}}
{{-- {{$location}} --}}
{{-- {{$client}} --}}
{{-- {{$masterUser}} --}}

<body>
    {{-- <script>
        window.history.forward();
     </script> --}}
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('SubUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt dmTop">
                    <div>
                        <h4>{{ Auth::user()->first_name . ' ' . Auth::user()->last_name }}</h4>
                        <p><i class="fa-light fa-location-dot"></i>
                            <!-- {{ $location->facility_address }} -->
                            <span data-bs-toggle="tooltip" data-bs-placement="top" title="{{ $location->facility_address }}">
                                @if (strlen($location->facility_address) > 41)
                                {{substr($location->facility_address, 0, 40) . '...'}}
                                @else
                                {{ $location->facility_address }}
                                @endif
                            </span>
                        </p>
                    </div>
                    @if ($reminder > 0)
                    <div class="hasreminder">
                        * Please check Today's reminder
                    </div>
                    @endif
                    <div class="caltimeMain"><span class="caltime">Master User: </span><span class="dmbg cursorpoint masterProfile" data-id="{{ $masterUser->id }}">{{ substr($masterUser->first_name, 0, 1) . substr($masterUser->last_name, 0, 1) }}</span>
                    </div>
                </div>
                <div class="evntbtns addSubUsr">
                    @foreach ($client as $item)
                    <button><label class="dashProgicon" data-id="{{ Crypt::encryptString($item->id) }}">P</label><label class="patientId" data-id="{{ Crypt::encryptString($item->id) }}">{{ $item->first_name }}
                            {{ $item->last_name }}</label></button>
                    @endforeach
                </div>
                <input hidden id="today" value="{{ Crypt::encryptString($date) }}">
                <!--                <div class="centerAlign"><button class="commonButton" onclick="window.location.href = 'add-client.html';">Add Client</button></div>-->
            </div>
        </section>
    </div>
    @include('SubUser/Components/footer')
</body>

<script>
    // $(".archive").on({
    //     mouseenter: function() {
    //         $(this).css("cursor", "pointer");
    //     },

    //     mouseleave: function() {
    //         $(this).css("cursor", "auto");
    //     },
    // });
    $(document).ready(function() {

        // $('.archive').click(function() {
        //     const swalWithBootstrapButtons = Swal.mixin({
        //         customClass: {
        //             confirmButton: 'btn btn-success',
        //             cancelButton: 'btn btn-danger m-3'
        //         },
        //         buttonsStyling: false
        //     })

        //     swalWithBootstrapButtons.fire({
        //         title: 'Are you sure?',
        //         text: "You won't be able to revert this!",
        //         icon: 'warning',
        //         showCancelButton: true,
        //         confirmButtonText: 'Yes, Archive it!',
        //         cancelButtonText: 'No, cancel!',
        //         reverseButtons: true,
        //     }).then((result) => {
        //         if (result.isConfirmed) {

        //             $.ajax({
        //                 type: "GET",
        //                 url: "{{ route('/archive_client') }}",
        //                 data: {
        //                     'id': $(this).attr('data-id')
        //                 },
        //                 dataType: "json",
        //                 success: function(response) {
        //                     swalWithBootstrapButtons.fire(
        //                         'Deleted!',
        //                         response.success,
        //                         'success'
        //                     )
        //                 }
        //             });
        //             setTimeout(function() {
        //                 window.location.reload(false);
        //             }, 1000);



        //         } else if (
        //             /* Read more about handling dismissals below */
        //             result.dismiss === Swal.DismissReason.cancel
        //         ) {
        //             swalWithBootstrapButtons.fire(
        //                 'Cancelled',
        //                 'Your imaginary file is safe :)',
        //                 'error'
        //             )
        //         }
        //     })


        //     // console.log($(this).attr('data-id'));
        // });
        //SumonK Mouse pointer change
        $(".patientId").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });
        $(".patientId").click(function() {
            var patient_id = $(this).attr("data-id");
            var date = "0";
            // alert(patient_id);
            window.location.href =
                `{{ url('/su_client_medication/${patient_id}/${date}') }}`;
        });


        $(".masterProfile").click(function() {
            var master_id = $(this).attr("data-id");
            // alert(master_id);
            window.location.href =
                `{{ url('master_profile/${master_id}') }}`;
        });
    });
</script>
<script>
    $(".dashProgicon").on({
        mouseenter: function() {
            $(this).css("cursor", "pointer");
        },

        mouseleave: function() {
            $(this).css("cursor", "auto");
        },
    });
    $(document).ready(function() {
        $(".dashProgicon").click(function() {
            var patient_id = $(this).attr("data-id");
            var today = document.getElementById('today').value;
            // alert(today);
            window.location.href =
                `{{ url('/su_medication_all_chart/${patient_id}') }}`;
        });
    });
</script>
<script>
    $(".hasreminder").on({
        mouseenter: function() {
            $(this).css("cursor", "pointer");
        },

        mouseleave: function() {
            $(this).css("cursor", "auto");
        },
    });
    $(document).ready(function() {
        $(".hasreminder").click(function() {
            window.location.href =
                `{{ url('su_reminder') }}`;
        });
    });
</script>
{{-- <script>
    history.pushState(null, document.title, location.href);
    window.addEventListener('popstate', function(event) {
        history.pushState(null, document.title, location.href);
    });
</script> --}}



<!--    <script src="js/jquery.js" type="text/javascript"></script>-->