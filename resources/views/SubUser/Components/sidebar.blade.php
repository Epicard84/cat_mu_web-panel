<div class="dbLeftpannel" id="mySidepanel" onclick="closeNav()">
    <div class="dbLeftInn">
    <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
    <a href="{{route('su_dashboard')}}"><div class="commonLogoSection dblogo">
        <img src="{{asset('public/MasterUser/assets/images/logo.png')}}" alt="">
        <div>
            <h1>MyChartSpace</h1>
            {{-- <p>Save time, use your phone.</p> --}}
            <h5>Home</h5>
        </div>
    </div></a>
    <div class="navbar navbar-expand-lg">                    

        <div class="navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto sidenav" id="navAccordion">
                <?php
                $segment2 =  request()->segments(1)[0];
                // dd($segment2)
                ?>

                <li class="nav-item {{($segment2 == 'su_reminder') ? 'active' : ''}}">
                    <a class="nav-link" href="{{route('su_reminder')}}" >
                        <div class="reminder"><img src="{{asset('public/MasterUser/assets/images/reminder.png')}}" alt="" />@if($reminder>0)<span class="noticircle"></span>@endif</div>
                        Reminder</a>
                </li>
                {{-- <li class="nav-item {{(in_array($segment2, ['sub_users', 'subuser_profile'])) ? 'active' : ''}}">
                    <a class="nav-link" href="#" ><img src="{{asset('public/MasterUser/assets/images/subusr.png')}}" alt="" />Sub users</a>
                </li> --}}
                <li class="nav-item {{(in_array($segment2, ['mfd', 'create_mfd'])) ? 'active' : ''}}">
                    <a class="nav-link" href="{{route('su_mfd')}}"><img src="{{asset('public/MasterUser/assets/images/mfd.png')}}" alt="" />Monthly fire drill</a>
                </li>
                <li class="nav-item {{($segment2 == 'su_notifications') ? 'active' : ''}}">
                    <a class="nav-link" href="{{route('su_notifications')}}"><img src="{{asset('public/MasterUser/assets/images/notify.png')}}" alt="" />Notification</a>
                </li>
                {{-- <li class="nav-item ">
                    <a class="nav-link" href="{{route('su_login')}}"><img src="#" alt="" />SubUser Login</a>
                </li> --}}
            </ul>
        </div>
    </div>
    <div class="leftPannelFooter">
        <div class="navbar navbar-expand-lg">                    

        <div class="navbar-collapse" id="">
            <ul class="navbar-nav mr-auto sidenav" >
                <?php
                $segment2 =  request()->segments(1)[0];
                // dd($segment2)
                ?>
                <li class="nav-item {{($segment2 == 'su_change_password') ? 'active' : ''}}">
                    <a class="nav-link" href="su_change_password"><img src="{{asset('public/MasterUser/assets/images/setting.png')}}" alt="" />Settings</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{route('user_logout')}}"><img src="{{asset('public/MasterUser/assets/images/logout.png')}}" alt="" />Logout</a>
                </li>
            </ul>
        </div>
    </div>
    </div>
    </div>
</div>