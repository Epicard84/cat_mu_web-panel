@extends('layouts.auth.app')

@section('title', 'Login')

@section('content')
    <section class="loginContainer">
        <a href="{{route('/')}}">
        <div class="commonLogoSection loginLogo">
            <img src="{{ asset('public/MasterUser/assets/images/logo.png') }}" alt="">
            <div>
                <h1>MyChartSpace</h1>
                <p>Save time, use your phone.</p>
            </div>


        </div>
        </a>
        <h2>Welcome to MyChartSpace!</h2>
        {{-- <p>There are many variations of passages of Lorem Ipsum available.</p> --}}
        <br>



        @if (Session::has('password'))
            <script>
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2500,
                    timerProgressBar: true,
                    didOpen: (toast) => {
                        toast.addEventListener(
                            'mouseenter',
                            Swal.stopTimer)
                        toast.addEventListener(
                            'mouseleave',
                            Swal.resumeTimer
                        )
                    }
                })

                Toast.fire({
                    icon: 'success',
                    title: "Password updated successfuly"
                })
            </script>
        @endif
        
         @if (Session::has('success')) 
        <script>
            const Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 2500,
                timerProgressBar: true,
                didOpen: (toast) => {
                    toast.addEventListener(
                        'mouseenter',
                        Swal.stopTimer)
                    toast.addEventListener(
                        'mouseleave',
                        Swal.resumeTimer
                    )
                }
            })

            Toast.fire({
                icon: 'success',
                title: "User Created successfuly"
            })
        </script>
     @endif



        @if (Session::has('msg'))
            <p class="text-danger">{{ Session::get('msg') }}</p>
        @endif
        <form action="{{ route('user_login_auth') }}" method="POST">
            @csrf
            <div class="mb-3">
                <input type="email" class="form-control" name="email" placeholder="Enter email address">
            </div>
            @if (Session::has('errors'))
                @if (array_key_exists('email', Session::get('errors')))
                    <p class="text-danger">{{ '*' . $errors['email'][0] }}</p>
                @endif
            @endif

            <div class="form-group mb-3">
                <div class="showhidePwd" id="show_hide_password">
                    <input class="form-control" type="password" name="password" placeholder="Password">
                    <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>
                </div>
            </div>

            @if (Session::has('errors'))
                @if (array_key_exists('password', Session::get('errors')))
                    <p class="text-danger">{{ '*' . $errors['password'][0] }}</p>
                @endif
            @endif

            <p class="forgetPass mb-4"><a href="{{ route('forgot_password') }}">Forgot password?</a></p>
            <p style="font-size: 16px">*By logging in , I am accepting <a href="{{route('terms-of-service')}}" style="font-size: 16px">Terms & Conditions</a> and <a href="{{route('privacy-and-cookie-policy')}}" style="font-size: 16px">Privacy Policies</a></p>
            <button>Login</button>
            <p align="center" class="mt-3"></p>
        </form>
        <p align="center" class="mt-3 signup">No account? <a href="{{ route('/sign_up') }}">Signup</a></p>
        <div class="loginFooter loginFooterNew">
            <div class="downloadicon">
                <span>Download from: </span>
                <a href="https://play.google.com/store/apps/details?id=com.mychartspace&pli=1"><img src="{{ asset('public/MasterUser/assets/images/android.png')}}" alt=""></a>
                <a href="https://apps.apple.com/us/app/mychartspace/id6446087963"><img src="{{ asset('public/MasterUser/assets/images/ios.png')}}" alt=""></a>
            </div>
            <div class="dnLink">
                <a href="">Home</a> |
                <a href="{{route('about-us')}}">About Us</a> |
                <a href="{{route('terms-of-service')}}">Terms &amp; Condition</a> |
                <a href="{{route('privacy-and-cookie-policy')}}">Privacy Policy</a>
            </div>
        </div>
    </section>

    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('public/MasterUser/assets/js/datepicker.js') }}"></script>


    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
        });
    </script>
@endsection
