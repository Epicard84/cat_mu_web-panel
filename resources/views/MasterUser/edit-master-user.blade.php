<!DOCTYPE html>
<html lang="en">


@extends('layouts.auth.app')

@section('title', 'Profile')

@section('content')

<body onload="initAutocomplete()">
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('layouts.MasterUser.sidebar')
            <div class="dbRight">
                <form action="{{ route('/edit_master_user_post') }}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="emUploadImg">
                        <div class="imageUpload">
                            <img src="{{ $data->profile_image_name }}" id="blah" alt="Img">
                            <div class="uploadInput"><span><i class="fa-regular fa-plus"></i></span><input type="file" id="inputFile" name="image" onchange="readUrl(this)" accept="image/*"></div>
                            <!--                            <button type="button" onclick="removeImg()"><i class="fa-regular fa-circle-xmark"></i></button>-->
                        </div>
                    </div>
                    <div class="row g-3 mb-4 editMasterForm">
                        <div class="col-md-6">
                            <input type="text" name="id" id="id" value="{{ $data->id }}" hidden>
                            <input type="text" class="form-control" name="first_name" value="{{ $data->first_name }}" placeholder="First Name">
                            @if (Session::has('error'))
                            @if (array_key_exists('first_name', Session::get('error')))
                            <p class="text-danger">{{ Session::get('error')['first_name'][0] }}</p>
                            @endif
                            @endif
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="last_name" value="{{ $data->last_name }}" placeholder="Last Name">
                            @if (Session::has('error'))
                            @if (array_key_exists('last_name', Session::get('error')))
                            <p class="text-danger">{{ Session::get('error')['last_name'][0] }}</p>
                            @endif
                            @endif
                        </div>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="company_name" value="{{ $data->company_name }}" placeholder="Company Name">
                            @if (Session::has('error'))
                            @if (array_key_exists('company_name', Session::get('error')))
                            <p class="text-danger">{{ Session::get('error')['company_name'][0] }}</p>
                            @endif
                            @endif
                        </div>
                        <div class="col-md-6">
                            <input type="text" maxlength="10" class="form-control" name="phone_number" id="phone_number" value="{{ $data->phone_number }}" placeholder="Phone Number">
                            <p class="phone_number_error text-danger" hidden></p>
                            @if (Session::has('error'))
                            @if (array_key_exists('phone_number', Session::get('error')))
                            <p class="text-danger" id="phone_number_error">
                                {{ Session::get('error')['phone_number'][0] }}
                            </p>
                            @endif
                            @endif
                        </div>
                        <div class="col-md-6">
                            <input type="email" readonly class="form-control" name="email" value="{{ $data->email }}" placeholder="Email Address">
                            @if (Session::has('error'))
                            @if (array_key_exists('email', Session::get('error')))
                            <p class="text-danger">{{ Session::get('error')['email'][0] }}</p>
                            @endif
                            @endif
                        </div>
                        {{-- <div class="col-md-12">
                                <textarea class="form-control textareaheight" name="permanent_address" placeholder="Address">{{ $data->permanent_address }}</textarea>
                        @if (Session::has('error'))
                        @if (array_key_exists('permanent_address', Session::get('error')))
                        <p class="text-danger">{{ Session::get('error')['permanent_address'][0] }}</p>
                        @endif
                        @endif
                    </div> --}}
                    <div class="col-md-12">
                        <textarea id="addressInput" class="form-control textareaheight" name="facility_address" placeholder="Facility Address">{{ $data->facility_address }}</textarea>
                        @if (Session::has('error'))
                        @if (array_key_exists('facility_address', Session::get('error')))
                        <p class="text-danger">{{ Session::get('error')['facility_address'][0] }}</p>
                        @endif
                        @endif
                    </div>
                    <!-- <div class="col-md-12">
                        <input type="text" class="form-control" name="city" value="{{ $data->city }}" placeholder="City">
                        @if (Session::has('error'))
                        @if (array_key_exists('city', Session::get('error')))
                        <p class="text-danger">{{ Session::get('error')['city'][0] }}</p>
                        @endif
                        @endif
                    </div> -->
                    <!-- <div class="col-md-12">
                        <input type="text" class="form-control" name="state" value="{{ $data->state }}" placeholder="State">
                        @if (Session::has('error'))
                        @if (array_key_exists('state', Session::get('error')))
                        <p class="text-danger">{{ Session::get('error')['state'][0] }}</p>
                        @endif
                        @endif
                    </div> -->

                    <!-- <div class="row g-3 mb-4">
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="country" value="{{ $data->country }}" placeholder="Country">
                            @if (Session::has('error'))
                            @if (array_key_exists('country', Session::get('error')))
                            <p class="text-danger">{{ Session::get('error')['country'][0] }}</p>
                            @endif
                            @endif
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="zip_code" value="{{ $data->zip_code }}" placeholder="Zip Code">
                            @if (Session::has('error'))
                            @if (array_key_exists('zip_code', Session::get('error')))
                            <p class="text-danger">{{ Session::get('error')['zip_code'][0] }}</p>
                            @endif
                            @endif
                        </div>
                    </div> -->
                    <div class="col-md-12">
                        <h4 class="mb-2">Documents</h4>
                        <div class="row" style="text-align: center;">
                            <div class="col-3">
                                <h6>Operators License</h6>
                                @if ($data->operators_license)
                                <a href="{{asset('images/MasterUserProfile/' . $data->operators_license)}}"><img src="{{ asset('images/MasterUserProfile/' . $data->operators_license) }}" alt="" srcset="" class="docsimg"></a>

                                @endif
                                <input type="file" class="form-control" name="operatorLicense">
                                @if (Session::has('error'))
                                @if (array_key_exists('city', Session::get('error')))
                                <p class="text-danger">{{ Session::get('error')['operatorLicense'][0] }}</p>
                                @endif
                                @endif
                            </div>
                            <div class="col-3">
                                <h6>TB Clearance</h6>
                                @if ($data->tb_clearance)
                                <a href="{{asset('images/MasterUserProfile/' . $data->tb_clearance)}}"><img src="{{ asset('images/MasterUserProfile/' . $data->tb_clearance) }}" alt="" srcset="" class="docsimg"></a>
                                @endif
                                <input type="file" class="form-control" name="tbClearance">
                                @if (Session::has('error'))
                                @if (array_key_exists('city', Session::get('error')))
                                <p class="text-danger">{{ Session::get('error')['tbClearance'][0] }}</p>
                                @endif
                                @endif
                            </div>
                            <div class="col-3">
                                <h6>Physical Checkup</h6>
                                @if ($data->physical_checkup)
                                <a href="{{asset('images/MasterUserProfile/' . $data->physical_checkup)}}"><img src="{{ asset('images/MasterUserProfile/' . $data->physical_checkup) }}" alt="" srcset="" class="docsimg"></a>
                                @endif
                                <input type="file" class="form-control" name="physicalCheckup">
                                @if (Session::has('error'))
                                @if (array_key_exists('city', Session::get('error')))
                                <p class="text-danger">{{ Session::get('error')['physicalCheckup'][0] }}</p>
                                @endif
                                @endif
                            </div>
                            <div class="col-3">
                                <h6>Miscellaneous Doc</h6>
                                @if ($data->misc_doc)
                                <a href="{{asset('images/MasterUserProfile/' . $data->misc_doc)}}"><img src="{{ asset('images/MasterUserProfile/' . $data->misc_doc) }}" alt="" srcset="" class="docsimg"></a>
                                @endif
                                <input type="file" class="form-control" name="misc_doc">
                                @if (Session::has('error'))
                                @if (array_key_exists('city', Session::get('error')))
                                <p class="text-danger">{{ Session::get('error')['physicalCheckup'][0] }}</p>
                                @endif
                                @endif
                            </div>
                        </div>
                        <!-- <input type="file" class="form-control" name="city">
                        @if (Session::has('error'))
                        @if (array_key_exists('city', Session::get('error')))
                        <p class="text-danger">{{ Session::get('error')['city'][0] }}</p>
                        @endif
                        @endif -->
                    </div>

                    <button type="submit" class="commonButton">Update</button>
                </form>
            </div>
        </section>
    </div>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        var a = document.getElementById("blah");

        function readUrl(input) {
            if (input.files) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = (e) => {
                    a.src = e.target.result;
                }
            }
        }

        var inputFile = document.getElementById("inputFile");
        removeImg = () => {
            a.src = "images/subuser.png";
            inputFile.value = "";
        }


        $(document).ready(function() {
            //phnumber Validation
            function phone_number_validator(phnumber) {
                var filter =
                    /^[0-9]+$/;

                if (filter.test(phnumber)) {
                    return true;
                } else {
                    return false;
                }

            }

            $('#phone_number').on('keyup', function() {

                var response = phone_number_validator($(this).val());

                console.log(response);
                if ($(this).val().length == 0) {
                    $('.phone_number_error').html("");
                } else if (response == false) {
                    $('.phone_number_error').removeAttr('hidden');
                    $('#phone_number_error').html("");
                    $('.phone_number_error').html("*Please Enter a valid Phone Number");
                } else {
                    $('.phone_number_error').html("");
                }

            })

        });
    </script>

<script>
    function initAutocomplete() {
      var input = document.getElementById('addressInput');
      var autocomplete = new google.maps.places.Autocomplete(input);

      autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        console.log(place.formatted_address);
        console.log(place);
      });
    }

    // function data(){
    //   var address = document.getElementById('addressInput').value;
    //   alert(address);
    // }
  </script>

<script src="https://maps.google.com/maps/api/js?key=AIzaSyASsOMW6tK9Qo981eUEHdcPi3WjNcJwFQ0&libraries=places"></script>


</body>
@endsection

</html>