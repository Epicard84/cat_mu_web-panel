<!DOCTYPE html>
<html lang="en">

@extends('layouts.auth.app')

@section('title', 'MFD')

@section('content')
    <body>
        <div class="container customContainer">
            <section class="dbMainbase">
                <button class="sideBtn" onclick="openNav()">☰</button>
                @include('layouts.MasterUser.sidebar')

            <div class="dbRight">
                <div class="happyEvnt dmTop mb-3">
                        <h4>{{$date}}</h4> 
                    <span><img src="{{asset('MasterUser/assets/images/dateicon.png')}}" alt=""></span>
                </div>
                <form method="post" action="{{route('store_mfd')}}">
                    @csrf
                    <div class="enterMfd">
                        <textarea class="form-control" placeholder="Enter MFD" name="mfd"></textarea>
                        @if(Session::has('error'))
                            <span style="font-size: small; color:rgb(121, 0, 0)">***{{ Session::get('error') }}</span>
                        @endif
                    </div>
                    <a href="{{route('store_mfd')}}"><button class="commonButton respFullwdth" type="submit">Submit</button></a>
                </form>
                   
                </div>                
        </section>
    </div>



@include('MasterUser/Components/footer')

    <script>
        window.pressed = function(){
        var a = document.getElementById('aa');
        if(a.value == "")
        {
            fileLabel.innerHTML = "Upload image";
        }
        else
        {
            var theSplit = a.value.split('\\');
            fileLabel.innerHTML = theSplit[theSplit.length-1];
        }
    };
    </script>
</body>
@endsection
</html>
