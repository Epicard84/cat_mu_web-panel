<!DOCTYPE html>
<html lang="en">

@extends('layouts.auth.app')

@section('title', 'Profile')

@section('content')

    <body>
        <div class="container customContainer">
            <section class="dbMainbase">
                <button class="sideBtn" onclick="openNav()">☰</button>
                @include('layouts.MasterUser.sidebar')

                <div class="dbRight">
                    @if (Session::has('succcess'))
                        <script>
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: "Updated Successfully"
                            })
                        </script>
                    @endif
                    <div class="happyEvnt mb-4">
                        <span class="back" onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long"
                                style="color: #ffffff;"></i></span>
                        <div>
                            <h4>{{ $subUserDetails->first_name . ' ' . $subUserDetails->last_name }}</h4>
                        </div>
                        <div>
                        <a href="{{ route('/edit_sub_user', Crypt::encryptString($subUserDetails->id)) }}"><span
                                class="editIcon"><i class="fa-light fa-pen"></i></span></a>
                        </div>
                        <!-- <div></div> -->
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h4 class="mb-2">Personal Information</h4>
                            <div class="personalInfo">
                                {{-- <p>Date of birth: ***10th December, 1995</p>
                            <p>Gender: ***Female</p> --}}
                                {{-- <p>Address: {{ $subUserDetails->permanent_address }}</p>
                                <p>Mailing address: {{ $subUserDetails->mailing_address }}</p>
                                <p>City: {{ $subUserDetails->city }}</p>
                                <p>State: {{ $subUserDetails->state }}</p>
                                <p>Country: {{ $subUserDetails->country }}</p> --}}
                                <p>Email: <a href="mailto:{{ $subUserDetails->email }}">{{ $subUserDetails->email }}</a></p>
                                <!-- <p>Documents: <span class="editDocs" data-bs-toggle="modal"
                                        data-bs-target="#editSubUserDocsModal" data-id="{{ $subUserDetails->id }}"><i
                                            class="fas fa-edit"></i></span></p>
                                @if ($document && $document->document)
                                    @php
                                        $images = explode(',', $document->document);
                                    @endphp
                                    <div class="d-flex d-inline">
                                        @foreach ($images as $image)
                                            <div class="pdfbg mx-2">
                                                <div class="doCard">
                                                    <div class="pdfbg"><img src="{{ asset('public/MasterUser/assets/images/pdf.png') }}"
                                                            alt=""><span style="font-size: 10pt">{{$image}}</span></div>
                                                &nbsp;
                                                <a href="{{ asset('public/documents/SubUser_Docs/' . $image) }}"><label
                                                        class="commonButton savebtn viewbtn"><i
                                                            class="fa-light fa-download"></i></label></a>
                                            </div>
                                        @endforeach
                                    </div>
                                @else
                                    <p>No Documents Found</p>
                                @endif -->
                            </div>
                        </div>
                        <h4 class="mb-2">Documents</h4>
                        <div class="personalInfo autoHeight">
                            <div class="row" style="text-align: center;">
                                <div class="col-3">
                                    <h6>Operators License</h6>
                                    @if ($subUserDetails->operators_license)
                                        <a href="{{asset('images/SubUserProfile/' . $subUserDetails->operators_license)}}"><img src="{{ asset('images/SubUserProfile/' . $subUserDetails->operators_license) }}"
                                                alt="" srcset="" class="docsimg"></a>
                                               
                                        @endif
                                </div>
                                <div class="col-3">
                                    <h6>TB Clearance</h6>
                                    @if ($subUserDetails->tb_clearance)
                                            <a href="{{asset('images/SubUserProfile/' . $subUserDetails->tb_clearance)}}"><img src="{{ asset('images/SubUserProfile/' . $subUserDetails->tb_clearance) }}"
                                                alt="" srcset="" class="docsimg"></a>
                                        @endif
                                </div>
                                <div class="col-3">
                                    <h6>Physical Checkup</h6>
                                    @if ($subUserDetails->physical_checkup)
                                            <a href="{{asset('images/SubUserProfile/' . $subUserDetails->physical_checkup)}}"><img src="{{ asset('images/SubUserProfile/' . $subUserDetails->physical_checkup) }}"
                                                alt="" srcset="" class="docsimg"></a>
                                        @endif
                                </div>
                                <div class="col-3">
                                    <h6>Miscellaneous Doc</h6>
                                    @if ($subUserDetails->misc_doc)
                                            <a href="{{asset('images/SubUserProfile/' . $subUserDetails->misc_doc)}}"><img src="{{ asset('images/SubUserProfile/' . $subUserDetails->misc_doc) }}"
                                                alt="" srcset="" class="docsimg"></a>
                                        @endif
                                </div>
                            </div>  
                        </div>
                    </div>
                    <!--                <button class="commonButton">Add Client</button>-->
                </div>
            </section>
        </div>
        <!-- Edit Prog Notes Modal -->
        <div class="modal fade modalBase" id="editSubUserDocsModal" tabindex="-1" aria-labelledby="notesModalLabel"
            aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Update Documents {{ $subUserDetails->id }}</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form enctype="multipart/form-data" id="editSubUserDocs">
                        @csrf
                        <div class="modal-body">
                            <p>Documents:</p>
                            @if ($document->document != null)
                                @php
                                    $files = explode(',', $document->document);
                                @endphp
                                <div class="mb-3">
                                    @foreach ($files as $f)
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="{{ $f }}"
                                                id="flexCheckChecked" name="docs[]" checked>
                                            <label class="form-check-label" for="flexCheckChecked">
                                                {{ $f }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            @else
                                <p style="font-size: 10pt">No documents found</p>
                            @endif
                            <div class="enterMfd">
                                <input hidden name="details_id" value="{{ $subUserDetails->id }}">
                                {{-- <textarea class="form-control" name="description">{{ $chart->descriptions }}</textarea> --}}
                                <input type="file" multiple name="detailsImage[]" />
                                <p class="description_error_1" style="color: red; font-size:10pt"></p>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button id="updatedocssubmit" type="button" class="btn commonButton">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </body>
    <script>
        $(".back").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });
    </script>
        <script>
            $(document).ready(function() {
    
                $(".editDocs").on({
                        mouseenter: function() {
                            $(this).css("cursor", "pointer");
                        },
    
                        mouseleave: function() {
                            $(this).css("cursor", "auto");
                        },
                    })
    
                //Edit MFD Form Submit
                $('#updatedocssubmit').click(function(e) {
                    e.preventDefault();
                    // var uid = $(this).attr('data-id');
                    // id = "#editSubUserDocs" + uid;
                    // var btnid = "#submit" + uid;
                    // $(btnid).on("click", function(e) {
                        // e.preventDefault();
                        $('.description_error_1').html("");
                        var form = $('#editSubUserDocs')[0];
    
                        var formData = new FormData(form);
                        console.log(formData);
                        $.ajax({
                            url: "{{ route('subuser_docs_edit') }}",
                            type: 'POST',
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            data: formData,
                            success: function(response) {
    
                                console.log(response);
    
                                if (response.hasOwnProperty('success')) {
    
                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2000,
                                        timerProgressBar: false,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })
    
                                    Toast.fire({
                                        icon: 'success',
                                        title: response.success
                                    })
    
                                    setTimeout(function() {
                                        window.location.reload(false);
                                    }, 2501);
    
                                } else {
                                    console.log(response.error);
                                    if (response.error) {
                                        $('.description_error_1').html("*" + response
                                            .error);
                                    }
                                }
                            }
                        });
                    // })
                });
            });
        </script>


@endsection



</html>
