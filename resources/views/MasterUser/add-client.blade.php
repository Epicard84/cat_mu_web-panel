<!DOCTYPE html>
<html lang="en">
@include('MasterUser.Components.head')

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('layouts.MasterUser.sidebar')
            <div class="dbRight">
                <div class="happyEvnt mb-4">
                    
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long" style="color: #ffffff;"></i></span>
                    <h4>New Client</h4>
                    <span></span>
                
            </div>
                <form action="" method="post" enctype="multipart/form-data" id="create_form" autocomplete="off">
                    @csrf
                    <div class="row g-3 mb-4 editMasterForm">
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="first_name" id="first_name"
                                placeholder="First Name">
                            <p class="first_name_error text-danger"></p>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="last_name" id="last_name"
                                placeholder="Last Name">
                            <p class="last_name_error text-danger"></p>
                        </div>

                        <div class="col-md-4 pickDate">

                            <input type="text" id="datepicker" name="dob" id="dob" class="form-control"
                                placeholder="Date of Birth MM/DD/YYYY" autocomplete="off" />
                            <p class="dob_error text-danger"></p>

                        </div>

                        <div class="col-md-3">

                            {{-- <input type="text" id="datepicker" name="dob" id="dob" class="form-control"
                                placeholder="Select Date of Birth" autocomplete="off" /> --}}
                            <input type="text" id="age" class="form-control" placeholder="Age" readonly />
                            {{-- <p class="dob_error text-danger"></p> --}}

                        </div>

                        <div class="col-md-5">
                            <select class="form-control" id="gender" name="gender">
                                <option value="" hidden selected>Select Gender</option>
                                <option value="male">Male</option>
                                <option value="female">Female</option>
                            </select>
                            <p class="gender_error text-danger"></p>

                        </div>
                        <div class="guardian">
                            <div class="col-md-12">
                                <input type="text" required name="guardian_name[]" id="guardian_name"
                                    class="form-control" placeholder="Guardian name">
                                <p class="guardian_name_error text-danger"></p>
                            </div>
                            <div class="col-md-12">
                                <input type="number" required class="form-control" id="guardian_phone_number"
                                    name="guardian_phone_number[]" placeholder="Guardian phone number" >
                                <p class="guardian_phone_number_error text-danger"></p>
                            </div>
                            <div class="col-md-12">
                                <textarea class="form-control textareaheight" required class="form-control" name="guardian_address[]"
                                    id="guardian_address" placeholder="Guardian address"></textarea>
                                <p class="guardian_address_error text-danger"></p>
                            </div>
                        </div>

                        <div class="newguardian">

                        </div>
                        <div class="addguardianform">
                            <label class="commonButton add_guardian"><i class="fa fa-plus"></i>Add guardian</label>
                        </div>

                        {{-- Doctor add --}}
                        <div class="doctor">
                            <div class="col-md-12">
                                <input type="text" class="form-control" required name="doctor_name[]"
                                    id="doctor_name" placeholder="Doctor’s name">
                                <p class="doctor_name_error text-danger"></p>
                            </div>
                            <div class="col-md-12">
                                <input type="number" class="form-control" required name="doctor_phone_number[]"
                                    id="doctor_phone_number" placeholder="Doctor’s Phone number">
                                <p class="doctor_phone_number_error text-danger"></p>
                            </div>
                            <div class="col-md-12">
                                <textarea class="form-control textareaheight" required name="doctor_address[]" id="doctor_address"
                                    placeholder="Doctor’s address"></textarea>
                                <p class="doctor_address_error text-danger"></p>
                            </div>
                        </div>

                        <div class="newdoctor">

                        </div>
                        <div class="adddoctorform">
                            <label class="commonButton add_doctor"><i class="fa fa-plus"></i>Add doctor</label>
                        </div>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="additional_information"
                                placeholder="Diagnosis">
                            <p class="additional_information_error text-danger"></p>
                        </div>

                        <div class="col-md-12">
                            <div class="file addClntfile">
                                <div class="file__input upfileInput">
                                    <input class="file__input--file" id="customFile" type="file"
                                        multiple="multiple" name="file[]"
                                        accept=".doc,.docx,.pdf,.png,.jpeg,.jpg" />
                                    <label class="file__input--label" for="customFile"
                                        data-text-btn="Upload">Upload</label>
                                    <label class="acl">Documents</label>
                                </div>
                                <p id="imgerror" class="text-danger"></p>
                                <div id="file__input"></div>
                                <div id="nfile"></div>
                            </div>
                        </div>

                    </div>

                    <button type="submit" class="commonButton">Save</button>

                </form>


            </div>
        </section>
    </div>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>

    <script>
        var a = document.getElementById("blah");

        function readUrl(input) {
            if (input.files) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = (e) => {
                    a.src = e.target.result;
                }
            }
        }

        var inputFile = document.getElementById("inputFile");
        removeImg = () => {
            a.src = "images/subuser.png";
            inputFile.value = "";
        }
    </script>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/js/bootstrap-datepicker.min.js"></script>
    <link
        href="https://cdn.jsdelivr.net/npm/bootstrap-datepicker@1.9.0/dist/css/bootstrap-datepicker3.min.css"rel="stylesheet">

    {{-- <script>
        new AirDatepicker('#datepicker', {
            autoClose: false,
            dateFormat: 'MM/dd/yyyy',
            maxDate: new Date(),
        })

        // Multi DatePicker
        new AirDatepicker('#datepicker-multi', {
            range: true,
            multipleDates: true,
            multipleDatesSeparator: " - "
        })
    </script> --}}
    <script>
        $(function() {
            $("#datepicker").datepicker({
                endDate: new Date(),
                dateFormat: 'mm/dd/yyyy'
            }).on('change', function() {
                var age = getAge(this);
                // console.log(age);
                $('#age').val(age);
            });

            function getAge(dateVal) {
                var
                    birthday = new Date(dateVal.value),
                    today = new Date(),
                    ageInMilliseconds = new Date(today - birthday),
                    years = ageInMilliseconds / (24 * 60 * 60 * 1000 * 365.25);
                  
                return Math.floor(years) + ' years ';
            }
        });
    </script>
    <script>
        $(document).ready(function() {

            $('.file__input--file').on('change', function(event) {
                var files = event.target.files;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    $("<div class='file__value'><div class='file__value--text'>" + file.name +
                        "</div><div class='file__value--remove' data-id='" + file.name +
                        "' ></div></div>").appendTo('#nfile');
                }
            });

            //Click to remove item
            $('body').on('click', '.file__value', function() {
                $(this).remove();
            });





            // $('#create_form').validate({
            //     rules: {
            //         first_name: "required",
            //         last_name: "required",
            //         dob: "required",
            //         gender: "required",
            //     }
            // messages: {
            //     "first_name": "First name can't be null",
            //     "last_name": "Last name can't be null",
            //     "dob": "DOB can't be null",
            //     "gender": "Gender can't be null",
            // }
            // });
            //Form Validation 

            //phnumber Validation
            // function phone_number_validator(phnumber) {
            //     var filter =
            //         /^[0-9]+$/;

            //     if (filter.test(phnumber)) {
            //         return true;
            //     } else {
            //         return false;
            //     }

            // }



            // $('#guardian_phone_number').on('keyup', function() {

            //     var response = phone_number_validator($(this).val());

            //     if ($(this).val().length == 0) {
            //         $('.guardian_phone_number_error').html("");
            //     } else if (response == false) {
            //         $('.guardian_phone_number_error').html("*Please enter a valid Phone Number");
            //     } else {
            //         $('.guardian_phone_number_error').html("");
            //     }

            // })

            $('#create_form').validate({
                rules: {
                    doctor_name: "required",
                    doctor_phone_number: "required",
                    doctor_address: "required",
                },
                messages: {
                    "doctor_name": "The doctor name can't be null",
                    "doctor_phone_number": "The doctor phone number can't be null",
                    "doctor_address": "The doctor address can't be null",
                }
            });



            //Add Doctor 

            var doctor = 2;

            $('.add_doctor').click(function() {
                $('.newdoctor').append(`
                            <div id="doctor${doctor}">
                                    <div class="moreField">
                                        <div class="morefieldTop">
                                    <h6>Doctor</h6>
                                    <div class="extra_doctor" id="extra_doctor" data-id="${doctor}"><i class="fa-regular fa-trash-can"></i></div>
                                </div>
                                        <div class="col-md-12 mb-3">
                                        <input type="text" class="form-control" required name="doctor_name[]" id="doctor_name${doctor}" placeholder="Doctor’s name">
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <input type="number" class="form-control" required name="doctor_phone_number[]" id="doctor_phone_numbe${doctor}" placeholder="Doctor’s Phone number">
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <textarea class="form-control textareaheight" required name="doctor_address[]" id="doctor_address${doctor}" placeholder="Doctor’s address"></textarea>
                                    </div>
                                    </div>
                                    <div>`);


                var doctor_name = doctor_name + doctor;
                var doctor_phone_number = doctor_phone_number + doctor;
                var doctor_address = doctor_address + doctor;


                $('#create_form').validate({
                    rules: {
                        doctor_name: "required",
                        doctor_phone_number: "required",
                        doctor_address: "required",
                    },
                    messages: {
                        "doctor_name": "Doctor name can't be null",
                        "doctor_phone_number": "Doctor phone number can't be null",
                        "doctor_address": "Doctor address can't be null",
                    }
                });

                doctor = doctor + 1;

            });

            //Delete doctor div
            $(document).on('click', '.extra_doctor', function() {
                var id = $(this).attr('data-id');
                $('#doctor' + id).empty();
            });


            $('#create_form').validate({
                rules: {
                    guardian_name: "required",
                    guardian_phone_number: "required",
                    guardian_address: "required",
                },
                messages: {
                    "guardian_name": "Guardian name can't be null",
                    "guardian_phone_number": "Guardian phone number can't be null",
                    "guardian_address": "Guardian address can't be null",
                }
            });



            var guardian = 2

            //Add Guardian 
            $('.add_guardian').click(function() {
                $('.newguardian').append(`
                            <div id="guardian${guardian}">
                                    <div class="moreField">
                                        <div class="morefieldTop">
                                    <h6>Guardian</h6>
                                    <div class="extra_guardian" required id="extra_guardian" data-id="${guardian}"><i class="fa-regular fa-trash-can"></i></div>
                                </div>
                                        <div class="col-md-12 mb-3">
                                        <input type="text" class="form-control" required name="guardian_name[]" id="guardian_name${guardian}" placeholder="Guardian’s name" id="guardian_name${guardian}">
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <input type="number" class="form-control" required name="guardian_phone_number[]" id="guardian_phone_number${guardian}" placeholder="Guardian’s Phone number" id="guardian_phone_number${guardian}">
                                    </div>
                                    <div class="col-md-12 mb-3">
                                        <textarea class="form-control textareaheight" required name="guardian_address[]" placeholder="Guardian’s address" id="guardian_address${guardian}"></textarea>
                                    </div>
                                    </div>
                                    <div>`);


                var guardian_name = guardian_name + guardian;
                var guardian_phone_number = guardian_phone_number + guardian;
                var guardian_address = guardian_address + guardian;


                $('#create_form').validate({
                    rules: {
                        guardian_name: "required",
                        guardian_phone_number: "required",
                        guardian_address: "required",
                    },
                    messages: {
                        "guardian_name": "Guardian name can't be null",
                        "guardian_phone_number": "Guardian phone number can't be null",
                        "guardian_address": "Guardian address can't be null",
                    }
                });

                guardian = guardian + 1

            });


            //Delete doctor div
            $(document).on('click', '.extra_guardian', function() {
                var id = $(this).attr('data-id');
                $('#guardian' + id).empty();
            });


            // var doctor = 2

            // $('.add_doctor').click(function() {
            //     $('.doctor').append(`
        //     <div id="doctor${doctor}">
        //             <div class="moreField">
        //                 <div class="morefieldTop">
        //             <h6>Guardian</h6>
        //             <div class="extra_guardian" required id="extra_doctor" data-id="${doctor}"><i class="fa-regular fa-trash-can"></i></div>
        //         </div>
        //                 <div class="col-md-12 mb-3">
        //                 <input type="text" class="form-control" required name="guardian_name[]" id="doctor_name${doctor}" placeholder="doctor name" id="doctor_name${guardian}">
        //             </div>
        //             <div class="col-md-12 mb-3">
        //                 <input type="number" class="form-control" required maxlength="10" name="doctor_phone_number[]" id="doctor_phone_number${guardian}" placeholder="doctor Phone number" id="doctor_phone_number${guardian}">
        //             </div>
        //             <div class="col-md-12 mb-3">
        //                 <textarea class="form-control textareaheight" required name="doctor_address[]" placeholder="doctor address" id="doctor_address${guardian}"></textarea>
        //             </div>
        //             </div>
        //             <div>`);


            //     var doctor_name = doctor_name + doctor;
            //     var doctor_phone_number = doctor_phone_number + doctor;
            //     var doctor_address = doctor_address + doctor;


            //     $('#create_form').validate({
            //         rules: {
            //             doctor_name: "required",
            //             doctor_phone_number: "required",
            //             doctor_address: "required",
            //         },
            //         messages: {
            //             "doctor_name": "doctor name can't be null",
            //             "doctor_phone_number": "doctor phone number can't be null",
            //             "doctor_address": "doctor address can't be null",
            //         }
            //     });

            //     doctor = doctor + 1

            // });



            //Register Master user

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('#create_form').submit(function(e) {
                e.preventDefault();

                $('.first_name_error').html("");
                $('.last_name_error').html("");
                $('.dob_error').html("");
                $('.guardian_address_error').html("");
                $('.guardian_name_error').html("");
                $('.doctor_name_error').html("");
                $('.doctor_phone_number_error').html("");
                $('.doctor_address_error').html("");
                $('.gender_error').html("");
                $('#imgerror').html("");

                var form = $('#create_form')[0];
                var formData = new FormData(form);
                $.ajax({
                    url: "{{ route('new_client') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.href =
                                    'https://mychartspace.com/master_dashboard';
                            }, 2501);


                        } else if (response.hasOwnProperty('msg')) {

                            Swal.fire({
                                icon: 'error',
                                title: 'Sorry..',
                                text: response.msg,
                                confirmButtonColor: '#3D4EC6',
                            })

                        } else if (response.hasOwnProperty('error')) {
                            Swal.fire({
                                icon: 'error',
                                title: 'Sorry..',
                                text: response.error,
                                confirmButtonColor: '#3D4EC6',
                            })
                        } else if (response.hasOwnProperty('imgerror')) {
                            // Swal.fire({
                            //     icon: 'error',
                            //     title: 'Sorry..',
                            //     text: response.error,
                            //     confirmButtonColor: '#3D4EC6',
                            // })
                            $('#imgerror').html("*" + response.imgerror);

                        } else {
                            let first_name = response.hasOwnProperty('first_name');
                            let last_name = response.hasOwnProperty('last_name');
                            let dob = response.hasOwnProperty('dob');
                            let gender = response.hasOwnProperty('gender');
                            let guardian_address = response.hasOwnProperty('guardian_address');
                            let guardian_name = response.hasOwnProperty('guardian_name');
                            let doctor_name = response.hasOwnProperty('doctor_name');
                            let doctor_phone_number = response.hasOwnProperty(
                                'doctor_phone_number');
                            let doctor_address = response.hasOwnProperty('doctor_address');
                            let additional_information = response.hasOwnProperty(
                                'additional_information');
                            let guardian_phone_number = response.hasOwnProperty(
                                'guardian_phone_number');

                            // console.log(response.first_name[0]);
                            if (first_name) {
                                $('.first_name_error').html("*" + response.first_name[0]);
                            }

                            if (last_name) {
                                $('.last_name_error').html("*" + response.last_name[0]);
                            }
                            if (dob) {
                                $('.dob_error').html("*" + response.dob[0]);
                            }
                            if (gender) {
                                $('.gender_error').html("*" + response.gender[0]);
                            }

                            if (guardian_address) {
                                $('.guardian_address_error').html("*" + response
                                    .guardian_address[0]);
                            }
                            if (guardian_name) {
                                $('.guardian_name_error').html("*" + response.guardian_name[0]);
                            }
                            if (doctor_name) {
                                $('.doctor_name_error').html("*" + response.doctor_name[0]);
                            }
                            if (doctor_phone_number) {
                                $('.doctor_phone_number_error').html("*" + response
                                    .doctor_phone_number[0]);
                            }
                            if (doctor_address) {
                                $('.doctor_address_error').html("*" + response.doctor_address[
                                    0]);
                            }
                            if (additional_information) {
                                $('.additional_information_error').html("*" + response
                                    .additional_information[0]);
                            }
                            if (guardian_phone_number) {
                                $('.guardian_phone_number_error').html("*" + response
                                    .guardian_phone_number[0]);
                            }

                        }
                    }
                });

            })

        });
    </script>
    {{-- 
    <script>
        new AirDatepicker('#datepicker', {
            autoClose: false,
            dateFormat: 'MM/dd/yyyy',
            maxDate: new Date()
        })

        // Multi DatePicker
        new AirDatepicker('#datepicker-multi', {
            range: true,
            multipleDates: true,
            multipleDatesSeparator: " - ",
            
        })
    </script> --}}


    {{-- <script>
        $(document).ready(function() {

            //phnumber Validation
            function phone_number_validator(phnumber) {
                var filter =
                    /^[0-9]+$/;

                if (filter.test(phnumber)) {
                    return true;
                } else {
                    return false;
                }

            }

            $('#phone_number').on('keyup', function() {


                var response = phone_number_validator($(this).val());

                if ($(this).val().length == 0) {
                    $('.phone_number_error').html("");
                } else if (response == false) {
                    $('.phone_number_error').html("*Please Enter a valid Phone Number");
                } else {
                    $('.guardian_phone_number_error').html("");
                }

            })
            $('#doctor_phone_number').on('keyup', function() {


                var response = phone_number_validator($(this).val());

                if ($(this).val().length == 0) {
                    $('.doctor_phone_number_error').html("");
                } else if (response == false) {
                    $('.doctor_phone_number_error').html("*Please enter a valid Phone Number");
                } else {
                    $('.doctor_phone_number_error').html("");
                }

            })

            $('#guardian_phone_number').on('keyup', function() {


                var response = phone_number_validator($(this).val());

                if ($(this).val().length == 0) {
                    $('.guardian_phone_number_error').html("");
                } else if (response == false) {
                    $('.guardian_phone_number_error').html("*Please Enter a valid Phone Number");
                } else {
                    $('.guardian_phone_number_error').html("");
                }

            })



            //Register Master user

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            $('#create_form').submit(function(e) {
                e.preventDefault();

                $('.first_name_error').html("");
                $('.last_name_error').html("");
                $('.dob_error').html("");
                $('.phone_number_error').html("");
                $('.guardian_address_error').html("");
                $('.doctor_name_error').html("");
                $('.doctor_phone_number_error').html("");
                $('.doctor_address_error').html("");
                $('.additional_information_error').html("");

                var form = $('#create_form')[0];
                var formData = new FormData(form);
                $.ajax({
                    url: "{{ route('new_client') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2500,
                                timerProgressBar: true,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            });

                            setTimeout(function() {
                                window.location.href = `{{ url('master_dashboard') }}`;
                            }, 2501);


                        } else if (response.hasOwnProperty('msg')) {

                            Swal.fire({
                                icon: 'error',
                                title: 'Sorry..',
                                text: response.msg,
                                confirmButtonColor: '#3D4EC6',
                            })

                        } else {


                            let first_name = response.hasOwnProperty('first_name');
                            let last_name = response.hasOwnProperty('last_name');
                            let dob = response.hasOwnProperty('dob');
                            let gender = response.hasOwnProperty('gender');
                            let phone_number = response.hasOwnProperty('phone_number');
                            let guardian_address = response.hasOwnProperty('guardian_address');
                            let guardain_name = response.hasOwnProperty('guardain_name');
                            let doctor_name = response.hasOwnProperty('doctor_name');
                            let doctor_phone_number = response.hasOwnProperty(
                                'doctor_phone_number');
                            let doctor_address = response.hasOwnProperty('doctor_address');
                            let additional_information = response.hasOwnProperty(
                                'additional_information');
                            let guardian_phone_number = response.hasOwnProperty(
                                'guardian_phone_number');

                            if (first_name) {
                                $('.first_name_error').html("*" + response.first_name[0]);
                            }

                            if (last_name) {
                                $('.last_name_error').html("*" + response.last_name[0]);
                            }
                            if (dob) {
                                $('.dob_error').html("*" + response.dob[0]);
                            }
                            if (gender) {
                                $('.gender_error').html("*" + response.gender[0]);
                            }
                            if (phone_number) {
                                $('.phone_number_error').html("*" + response.phone_number[0]);
                            }
                            if (guardian_address) {
                                $('.guardian_address_error').html("*" + response
                                    .guardian_address[0]);
                            }
                            if (doctor_name) {
                                $('.doctor_name_error').html("*" + response.doctor_name[0]);
                            }
                            if (guardain_name) {
                                $('.guardain_name_error').html("*" + response.guardain_name[0]);
                            }
                            if (doctor_phone_number) {
                                $('.doctor_phone_number_error').html("*" + response
                                    .doctor_phone_number[0]);
                            }
                            if (doctor_address) {
                                $('.doctor_address_error').html("*" + response.doctor_address[
                                    0]);
                            }
                            if (additional_information) {
                                $('.additional_information_error').html("*" + response
                                    .additional_information[0]);
                            }
                            if (guardian_phone_number) {
                                $('.guardian_phone_number_error').html("*" + response
                                    .guardian_phone_number[0]);
                            }

                        }
                    }
                });

            })

        });
    </script> --}}

    {{-- <script type="text/javascript">
        $(document).ready(function() {
            var age = "";
            $('#dob').AirDatepicker({
                onSelect: function(value, ui) {
                    var today = new Date();
                    age = today.getFullYear() - ui.selectedYear;
                    $('#age').val(age);
                },
                changeMonth: true,
                changeYear: true
            })
        })
    </script> --}}


</body>

</html>
