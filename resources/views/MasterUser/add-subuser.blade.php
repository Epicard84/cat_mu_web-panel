{{-- <!DOCTYPE html>
<html lang="en"> --}}

@extends('layouts.auth.content')

@section('title', 'Register')

@section('content')

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('layouts.MasterUser.sidebar')

            <div class="dbRight">
                <div class="happyEvnt reminderTop mb-3">
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long" style="color: #ffffff;"></i></span>
                    <h4>New SubUser</h4>
                    <span></span>
                </div>
                <form action="" method="POST" id="create_form" autocomplete="off">
                    @csrf
                    <div class="emUploadImg">
                        <div class="imageUpload">
                            <img src="{{ asset('public/images/MasterUserProfile/defult_image.png') }}" id="blah" alt="Img">
                            <div class="uploadInput"><span><i class="fa-regular fa-plus"></i></span><input type="file" name="profile_image" id="inputFile" accept="image/*" onchange="readUrl(this)"></div>
                            <!--                            <button type="button" onclick="removeImg()"><i class="fa-regular fa-circle-xmark"></i></button>-->
                        </div>
                    </div>
                    <div class="row g-3 mb-4 editMasterForm">
                        <div class="col-md-6">
                            <input type="text" name="first_name" class="form-control" placeholder="First Name">
                            <p id="first_name_error" class="text-danger"></p>
                        </div>
                        <div class="col-md-6">
                            <input type="text" name="last_name" class="form-control" placeholder="Last Name">
                            <p id="last_name_error" class="text-danger"></p>
                        </div>
                        {{-- <div class="col-md-12">
                                <input type="text" name="company_name" class="form-control" placeholder="Company Name">
                                <p id="company_name_error" class="text-danger"></p>
                            </div> --}}
                        {{-- <div class="col-md-6">
                                <input type="text" name="phone_number" id="phone_number" class="form-control"
                                    maxlength="13" placeholder="Phone Number">
                                <p id="phone_number_error" class="text-danger phone_number_error"></p>
                            </div> --}}
                        <div class="col-md-12">
                            <input type="email" name="email" class="form-control email" placeholder="Email Address">
                            <p class="text-danger" id="email_error"></p>
                        </div>
                        {{-- <div class="col-md-12">
                                <textarea name="address" class="form-control textareaheight" placeholder="Address"></textarea>
                                <p id="address_error" class="text-danger"></p>
                            </div> --}}
                        {{-- <div class="col-md-6">
                                <textarea name="mailing_address" class="form-control textareaheight" placeholder="Mailing Address"></textarea>
                                <p id="mailing_address_error" class="text-danger"></p>
                            </div> --}}
                        {{-- <div class="col-md-12">
                                <input type="text" name="city" class="form-control" placeholder="City">
                                <p id="city_error" class="text-danger"></p>
                            </div> --}}
                        {{-- <div class="col-md-12">
                                <input type="text" name="state" class="form-control" placeholder="State">
                                <p id="state_error" class="text-danger"></p>
                            </div> --}}
                        {{-- <div class="col-md-12">
                                <input type="text" name="country" class="form-control" placeholder="Country">
                                <p id="country_error" class="text-danger"></p>
                            </div> --}}
                        {{-- <div class="form-group mb-3">
                            <div class="showhidePwd" id="show_hide_password">
                                <input class="form-control" type="password" name="password" placeholder="Password">
                                <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>
                            </div>
                        </div> --}}
                        <div class="col-md-12">
                            <div class="showhidePwd" id="show_hide_password">
                                <input type="password" name="password" id="password" class="form-control password" placeholder="Passcode" maxlength="4">
                                {{-- <a class="showpass subpass"><i class="fa-regular fa-eye-slash"></i></a> --}}
                                <p class="password_error" id="password_error"></p>
                            </div>
                            <div class="col-md-12">
                                <div class="showhidePwd" id="show_hide_conf_password">
                                    <input type="password" name="confirm_password" id="confirm_password" class="form-control" placeholder="Confirm Passcode" maxlength="4">
                                    {{-- <a class="showpass subpass"><i class="fa-regular fa-eye-slash"></i></a> --}}
                                    <p id="message" class="mt-2"></p>
                                </div>
                                <p id="passcode_error" class="text-danger"></p>
                            </div>

                            <div class="form-group mb-3">
                                <div class="fileUploadInput signupUpload">
                                    <span>
                                        <input type='file' class="form-control" name="operators_license" accept="image/*" id="operators_license" onchange="pressed2()"><label id="fileLabel2" style="font-size: 12pt; color: #1B1F2D; font-family: var(--josef-sans); font-weight:500; line-height:15px">Upload Operators License</label></span>
                                    <button>Upload</button>
                                </div>
                                <!-- @if (Session::has('error'))
                                @if (array_key_exists('operators_license', json_decode(json_encode(Session::get('error')), true)))
                                <p class="text-danger beds_error" align="left">
                                    {{ json_decode(json_encode(Session::get('error')), true)['operators_license'][0] }}
                                </p>
                                @endif

                                @endif -->
                            </div>
                            <div class="form-group mb-3">
                                <div class="fileUploadInput signupUpload">
                                    <span>
                                        <input type='file' class="form-control" name="tb_clearance" accept="image/*" id="tb_clearance" onchange="pressed3()"><label id="fileLabel3" style="font-size: 12pt; color: #1B1F2D; font-family: var(--josef-sans); font-weight:500; line-height:15px">Upload TB Clearance</label></span>
                                    <button>Upload</button>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <div class="fileUploadInput signupUpload">
                                    <span>
                                        <input type='file' class="form-control" name="physical_checkup" accept="image/*" id="physical_checkup" onchange="pressed4()"><label id="fileLabel4" style="font-size: 12pt; color: #1B1F2D; font-family: var(--josef-sans); font-weight:500; line-height:15px">Upload Physical Checkup</label></span>
                                    <button>Upload</button>
                                </div>
                            </div>
                            <div class="form-group mb-3">
                                <div class="fileUploadInput signupUpload">
                                    <span>
                                        <input type='file' class="form-control" name="misc_doc" accept="image/*" id="misc_doc" onchange="pressed5()"><label id="fileLabel5" style="font-size: 12pt; color: #1B1F2D; font-family: var(--josef-sans); font-weight:500; line-height:15px">Miscellaneous Document</label></span>
                                    <!-- <button>Upload</button> -->
                                </div>
                            </div>
                            <!-- <div class="col-md-12">
                                <div class="file addClntfile">
                                    <div class="file__input upfileInput">
                                        <input class="file__input--file" id="customFile" type="file" multiple="multiple" name="document[]" accept=".doc,.docx,.pdf,.png,.jpeg,.jpg" />
                                        <label class="file__input--label" for="customFile" data-text-btn="Upload">Upload</label>
                                        <label class="acl">Documents</label>
                                    </div>
                                    <p id="document_error" class="text-danger"></p>
                                    <div id="file__input"></div>
                                    <div id="nfile"></div>
                                </div>
                            </div> -->

                            <button type="submit" class="commonButton">Add Now</button>

                </form>
            </div>
        </section>
    </div>



    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    {{-- @include('MasterUser/Components/footer') --}}
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        // window.pressed1 = function() {
        //     var a = document.getElementById('aa');
        //     if (a.value == "") {
        //         fileLabel1.innerHTML = "Upload image";
        //     } else {
        //         var theSplit = a.value.split('\\');
        //         fileLabel1.innerHTML = theSplit[theSplit.length - 1];
        //     }
        // };
        window.pressed2 = function() {
            var b = document.getElementById('operators_license');
            if (b.value == "") {
                fileLabel2.innerHTML = "Upload image";
            } else {
                var theSplit = b.value.split('\\');
                fileLabel2.innerHTML = theSplit[theSplit.length - 1];
            }
        };
        window.pressed3 = function() {
            var c = document.getElementById('tb_clearance');
            if (c.value == "") {
                fileLabel3.innerHTML = "Upload image";
            } else {
                var theSplit = c.value.split('\\');
                fileLabel3.innerHTML = theSplit[theSplit.length - 1];
            }
        };
        window.pressed4 = function() {
            var d = document.getElementById('physical_checkup');
            if (d.value == "") {
                fileLabel4.innerHTML = "Upload image";
            } else {
                var theSplit = d.value.split('\\');
                fileLabel4.innerHTML = theSplit[theSplit.length - 1];
            }
        };
        window.pressed5 = function() {
            var e = document.getElementById('misc_doc');
            if (e.value == "") {
                fileLabel5.innerHTML = "Upload image";
            } else {
                var theSplit = e.value.split('\\');
                fileLabel5.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        var a = document.getElementById("blah");

        function readUrl(input) {
            if (input.files) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = (e) => {
                    a.src = e.target.result;
                }
            }
        }

        var inputFile = document.getElementById("inputFile");
        removeImg = () => {
            a.src = "images/subuser.png";
            inputFile.value = "";
        }
    </script>


    <script>
        $(document).ready(function() {

            $('.file__input--file').on('change', function(event) {
                var files = event.target.files;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    $("<div class='file__value'><div class='file__value--text'>" + file.name +
                        "</div><div class='file__value--remove' data-id='" + file.name +
                        "' ></div></div>").appendTo('#nfile');
                }
            });

            //Click to remove item
            $('body').on('click', '.file__value', function() {
                $(this).remove();
            });

            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });

            $("#show_hide_conf_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_conf_password input').attr("type") == "text") {
                    $('#show_hide_conf_password input').attr('type', 'password');
                    $('#show_hide_conf_password i').addClass("fa-eye-slash");
                    $('#show_hide_conf_password i').removeClass("fa-eye");
                } else if ($('#show_hide_conf_password input').attr("type") == "password") {
                    $('#show_hide_conf_password input').attr('type', 'text');
                    $('#show_hide_conf_password i').removeClass("fa-eye-slash");
                    $('#show_hide_conf_password i').addClass("fa-eye");
                }
            });



            //Register Master user

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            // $('#create_form').validate({
            //     rules: {
            //         "phone_number": {required: true}
            //     }
            // });
            $('#create_form').submit(function(e) {
                e.preventDefault();

                $('#first_name_error').html("");
                $('#last_name_error').html("");
                // $('#company_name_error').html("");
                // $('#phone_number_error').html("");
                $('#email_error').html("");
                // $('#address_error').html("");
                // $('#mailing_address_error').html("");
                // $('#city_error').html("");
                // $('#state_error').html("");
                // $('#country_error').html("");
                $('#message').html("");
                $('#passcode_error').html("");
                $('#document_error').html("");


                var form = $('#create_form')[0];
                var formData = new FormData(form);
                $.ajax({
                    url: "{{ route('new_subuser') }}",
                    type: 'post',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.href = "{{ url('/sub_users') }}";
                            }, 2501);


                        }
                        if (response.hasOwnProperty('error')) {

                            let email = response.error.hasOwnProperty('email');
                            // let phone_number = response.error.hasOwnProperty('phone_number');
                            let password = response.error.hasOwnProperty('password');
                            let first_name = response.error.hasOwnProperty('first_name');
                            let last_name = response.error.hasOwnProperty('last_name');
                            // let company_name = response.error.hasOwnProperty('company_name');
                            // let city = response.error.hasOwnProperty('city');
                            // let state = response.error.hasOwnProperty('state');
                            // let country = response.error.hasOwnProperty('country');
                            let address = response.error.hasOwnProperty('address');
                            // let mailing_address = response.error.hasOwnProperty('password');

                            if (email) {
                                $('#email_error').html("*" + response.error.email[0]);
                            }

                            // if (phone_number) {
                            //     $('#phone_number_error').html("*" + response.error.phone_number[0]);
                            // }
                            if (first_name) {
                                $('#first_name_error').html("*" + response.error.first_name[0]);
                            }
                            if (last_name) {
                                $('#last_name_error').html("*" + response.error.last_name[0]);
                            }
                            // if (city) {
                            //     $('#city_error').html("*" + response.error.city[0]);
                            // }
                            // if (company_name) {
                            //     $('#company_name_error').html("*" + response.error.company_name[0]);
                            // }
                            // if (state) {
                            //     $('#state_error').html("*" + response.error.state[0]);
                            // }
                            // if (country) {
                            //     $('#country_error').html("*" + response.error.country[0]);
                            // }
                            // if (address) {
                            //     $('#address_error').html("*" + response.error.address[0]);
                            // }
                            // if (mailing_address) {
                            //     $('#mailing_address_error').html("*" + response
                            //         .error.mailing_address[0]);
                            // }
                            if (password) {
                                $("#message").css({
                                    "color": "red",
                                });
                                $('#message').html("*" + response.error.password[0]);
                            }
                        }

                        if (response.hasOwnProperty('passcodeerror')) {

                            $('#passcode_error').html("*" + response.passcodeerror);
                        }
                        if (response.hasOwnProperty('documenterror')) {

                            $('#document_error').html("*" + response.documenterror);
                        }

                    }
                });

            })

            //password and confirm password checking
            $('#password, #confirm_password').on('keyup', function() {
                if ($('#password').val() == "" && $('#confirm_password').val() == "") {
                    $('#message').html('');
                } else {
                    if ($('#password').val() == $('#confirm_password').val()) {
                        $('#message').html('Matching').css({
                            "color": "green",
                            "font-size": "17px"
                        });
                    } else
                        $('#message').html('Not Matching').css({
                            "color": "red",
                            "font-size": "17px"
                        });

                }

            });

            //phnumber Validation
            function phone_number_validator(phnumber) {
                var filter =
                    /^[0-9+-]+$/;

                if (filter.test(phnumber)) {
                    return true;
                } else {
                    return false;
                }

            }

            $('#phone_number').on('keyup', function() {

                var response = phone_number_validator($(this).val());


                if ($(this).val().length == 0) {
                    $('.phone_number_error').html("");
                } else if (response == false) {
                    $('.phone_number_error').html("*Please Enter a valid Phone Number");
                } else {
                    $('.phone_number_error').html("");
                }

            })


        });
    </script>
</body>
@endsection

{{-- </html> --}}