<!DOCTYPE html>
<html lang="en">

@extends('layouts.auth.app')

@section('title', 'Sub users')

@section('content')
    <body>
        <div class="container customContainer">
            <section class="dbMainbase">
                <button class="sideBtn" onclick="openNav()">☰</button>
                @include('layouts.MasterUser.sidebar')
            <div class="dbRight">
                <div class="happyEvnt">
                    <div class="subusr">
                        <a href="{{ route('/master_details') }}">
                            <span
                                class="dmbg subusrHe">{{ substr($masterUser->first_name, 0, 1) . substr($masterUser->last_name, 0, 1) }}</span></a>
                        <a href="{{ route('/master_details') }}">
                            <div>
                                <h4>{{ $masterUser->first_name . ' ' . $masterUser->last_name }}</h4>
                                <p><i class="fa-light fa-location-dot"></i>
                                    {{ $location }}</p>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="evntbtns addSubUsr">
                    @foreach ($subUsers as $su)
                        @if ($su->profile_image_name)
                            <button><a href="{{ route('subuser_profile', Crypt::encryptString($su->id)) }}"><img
                                        src="{{ asset('public/images/SubUserProfile/' . $su->profile_image_name) }}"
                                        alt="{{ $su->first_name . '.' . $su->last_name . '_img' }}"
                                        width="40px" />&nbsp;
                                @else
                                    <button><a href="{{ route('subuser_profile', Crypt::encryptString($su->id)) }}"><img
                                                src="{{ asset('public/images/SubUserProfile/defult_image.png') }}"
                                                alt="{{ $su->first_name . '.' . $su->last_name . '_img' }}"
                                                width="40px" />&nbsp;
                        @endif
                        {{ $su->first_name . ' ' . $su->last_name }}</a>
                        <label class="delete" data-id="{{ $su->id }}"><i
                                class="fa-regular fa-trash-can"></i></label></button>
                    @endforeach

                </div>
                <a href="{{ route('add_subuser') }}">
                    <div class="centerAlign"><button class="commonButton">Add Subuser</button></div>
                </a>
            </div>
        </section>
    </div>

    @include('MasterUser/Components/footer')
    <script>
        $(document).ready(function() {
            $('.delete').click(function() {
                var id = $(this).attr("data-id");
                //Delete alert
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger m-3'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then((result) => {
                    if (result.isConfirmed) {

                        $.ajax({
                            type: "GET",
                            url: "{{ route('/delete_user') }}",
                            data: {
                                'id': id
                            },
                            dataType: "json",
                            success: function(response) {
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    response.success,
                                    'success'
                                )
                            }
                        });
                        setTimeout(function() {
                            window.location.reload(false);
                        }, 1000);



                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                })


            });
        });
    </script>
@endsection
</html>
