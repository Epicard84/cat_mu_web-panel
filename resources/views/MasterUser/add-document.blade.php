<!DOCTYPE html>
<html lang="en">



<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            <div class="dbLeftpannel" id="mySidepanel" onclick="closeNav()">
                <div class="dbLeftInn">
                <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">×</a>
                <div class="commonLogoSection dblogo">
                    <img src="images/logo.png" alt="">
                    <div>
                        <h1>MyChartSpace</h1>
                        <p>Save time, use your phone.</p>
                    </div>
                </div>
                <div class="navbar navbar-expand-lg">                    

                    <div class="navbar-collapse" id="navbarCollapse">
                        <ul class="navbar-nav mr-auto sidenav" id="navAccordion">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">
                                    <div class="reminder"><img src="images/reminder.png" alt="" /><span class="noticircle">5</span></div>
                                    Reminder</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><img src="images/subusr.png" alt="" />Sub users</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><img src="images/mfd.png" alt="" />Monthly fire drill</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><img src="images/notify.png" alt="" />Notification</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="leftPannelFooter">
                    <div class="navbar navbar-expand-lg">                    

                    <div class="navbar-collapse" id="">
                        <ul class="navbar-nav mr-auto sidenav" >
                            <li class="nav-item">
                                <a class="nav-link" href="#"><img src="images/setting.png" alt="" />Settings</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="#"><img src="images/logout.png" alt="" />Logout</a>
                            </li>
                        </ul>
                    </div>
                </div>
                </div>
                </div>
            </div>
            <div class="dbRight">
                <form>
                    <div class="addMeditop">                        
                        <input type="text" class="form-control fullWdth" placeholder="Add title">
                    </div>
                    <div class="upFile mb-3">
                            <div class="file">
                              <div class="file__input upfileInput">
                                  <img src="images/selectupload.png" alt="">
                                <input class="file__input--file" id="customFile" type="file" multiple="multiple" name="files[]"/>
                                <label class="file__input--label" for="customFile" data-text-btn="Upload">Select file to upload</label>
                              </div>
                                <div class="secureOnly">
                                    <h6>Documents and images only</h6>
                                    <strong><i class="fa-regular fa-lock-keyhole"></i> Secured</strong>
                                </div>
                                <div id="file__input"></div>
                            </div>
                    </div>
                    
                    <button class="commonButton">Add more</button>
                    <button class="commonButton">Save</button>
                </form>
                
            </div>
        </section>
    </div>



<!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    <script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }

    </script>
    <script>
       $(document).ready(function() {
        
        
	$('.file__input--file').on('change',function(event){
		var files = event.target.files;
		for (var i = 0; i <files.length; i++) {
			var file = files[i];
			$("<div class='file__value'><div class='file__value--text'>" + file.name + "</div><div class='file__value--remove' data-id='" + file.name + "' ></div></div>").insertAfter('#file__input');
		}	
	});
	
	//Click to remove item
	$('body').on('click', '.file__value', function() {
		$(this).remove();
	});
	
	
	
});
    </script>
</body>

</html>
