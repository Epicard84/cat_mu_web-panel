<!DOCTYPE html>
<html lang="en">

@include('MasterUser/Components/head')

{{-- {{$reminders}} --}}
{{-- {{$date}} --}}

<body>

    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('MasterUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt reminderTop mb-3">
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long"
                            style="color: #ffffff;"></i></span>
                    <h4>Reminder</h4>
                    <span></span>
                </div>
                <form method="POST" enctype="multipart/form-data" id="reminderForm" autocomplete="off">
                    @csrf
                    <div class="row col-12">
                        <div class="col-7">

                            <textarea class="form-control" rows="1" placeholder="Enter reminder details" name="reminder_details"
                                id="reminder" required></textarea>
                            <p class="description_error" style="color: red; font-size:10pt"></p>
                        </div>
                        <div class="col-4">
                            <input type="text" class="form-control" name="reminder_date" id="datepicker"
                                placeholder="Select reminder date" required/>
                        </div>
                        <div class="col-1">
                            <button type="submit" class="btn btn-sm"
                                style="background-color: #3D4EC6; color:white">Save</button>
                        </div>
                    </div>
                </form>
                <div class="row">
                    <div class="col-md-4">
                        <div class="teacherCalendar reminderCal">
                            <div id="calendar"></div>
                        </div>
                    </div>
                    <div class="col-md-8 bladeRender">
                        @if (count($reminders) > 0)
                            @foreach ($reminders as $reminder)
                                <div class="reminderBlock mb-3">
                                    <div class="reminderdiv">
                                        <span><img src="{{ asset('MasterUser/assets/images/reminder.png') }}"
                                                alt=""></span>
                                        <div>
                                            <p>{{ $reminder->reminder }}</p>
                                        </div>
                                    </div>
                                    {{-- @if ($reminder->status == 1)
                                        <span class="greenTk"><i class="fas fa-check-circle"></i></span>
                                    @else --}}
                                        {{-- <span class="warnTk {{ $check == 1 ? 'check' : '' }}"
                                            data-id="{{ $reminder->id }}"><i class="far fa-circle"></i></span> --}}
                                            <label data-id="{{ $reminder->id }}"
                                                class="deleteReminder" style="margin-left: 30px;"><i class="fas fa-trash" style="color: #3D4EC6 ;"></i></label>
                                    {{-- @endif --}}
                                </div>
                            @endforeach
                        @else
                            <div class="medicationDetlbg">
                                <div class="mfdTop">
                                </div>
                                <p>No Reminder available...</p>
                            </div>
                        @endif
                        {{-- <div class="reminderBlock mb-3">
                    <span><img src="images/reminder.png" alt=""></span>
                    <div>
                        <p>One Medicine due for today evening. Please check.</p>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
                    </div>
                </div>
                <div class="reminderBlock mb-3">
                    <span><img src="images/reminder.png" alt=""></span>
                    <div>
                        <p>One Medicine due for today evening. Please check.</p>
                        <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum.</p>
                    </div>
                </div> --}}
                        <!--                <button class="commonButton">Add Client</button>-->
                    </div>
                </div>

            </div>
        </section>
    </div>


    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    @include('MasterUser/Components/footer')
    <script src="{{ asset('public/MasterUser/assets/js/datepick.js') }}"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>

    {{-- <script>
        $(".reminderBlock").click(function(){
          $(this).hide('slow');
        });
    </script> --}}
    <script>
        //Reminder Seen Update
        $(".check").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });
        $(document).ready(function() {
            $(".check").click(function() {
                var rid = $(this).attr("data-id");
                // alert(rid);
                $.ajax({
                    url: `{{ url('check_reminder/${rid}') }}`,
                    method: 'get',

                    success: function(res) {

                        if (res.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: res.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);


                        } else {
                            console.log(res)
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'error',
                                title: res.error
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);
                        }
                    }

                })

            });
        });
    </script>
    <script>
        $(document).ready(function() {

            $('#reminder').keyup(function(event) {
                var textBox = event.target;
                var start = textBox.selectionStart;
                var end = textBox.selectionEnd;
                textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);
            });


        });

        new AirDatepicker('#datepicker', {
            autoClose: false,
            dateFormat: 'MM/dd/yyyy',
            minDate: new Date(),
        })
    </script>
    <script>
        $(document).ready(function() {
            //New Reminder
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#reminderForm').submit(function(e) {
                e.preventDefault();

                $('.description_error').html("");

                var form = $('#reminderForm')[0];
                var formData = new FormData(form);
                console.log(formData);
                $.ajax({
                    url: "{{ route('new_reminder') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 1501);



                        } else {

                            // let description = response.hasOwnProperty('description');                 
                            console.log(response.error);
                            if (response.error) {
                                $('.description_error').html("*" + response.error);
                            }

                        }

                    }
                });

            });
            $('#calendar').on('changeDate', function(event) {

                var date = event.format('yyyy-mm-dd');
                if (!date) {
                    let searchDate = new Date();
                    date = searchDate.format("yyyy-mm-dd");

                }
                $.ajax({
                    type: "get",
                    url: `{{ url('mu_filter_reminder/${date}') }}`,

                    dataType: "json",
                    success: function(response) {
                        console.log(response.html);
                        $('.bladeRender').empty();
                        $('.bladeRender').html(response.html);
                    }
                });


            });
        });
    </script>
    
    <script>
        $(".deleteReminder").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });
        $(document).ready(function() {

            $('.deleteReminder').click(function() {
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger m-3'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, Delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then((result) => {
                    if (result.isConfirmed) {

                        $.ajax({
                            type: "GET",
                            url: "{{ route('mu_delete_reminder') }}",
                            data: {
                                'id': $(this).attr('data-id')
                            },
                            dataType: "json",
                            success: function(response) {
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    response.success,
                                    'success'
                                )
                            }
                        });
                        setTimeout(function() {
                            window.location.reload(false);
                        }, 1000);



                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your reminder is safe :)',
                            'error'
                        )
                    }
                })


                // console.log($(this).attr('data-id'));
            });
            // //SumonK Mouse pointer change
            // $(".patientId").on({
            //     mouseenter: function() {
            //         $(this).css("cursor", "pointer");
            //     },

            //     mouseleave: function() {
            //         $(this).css("cursor", "auto");
            //     },
            // });
            // $(".patientId").click(function() {
            //     var patient_id = $(this).attr("data-id");
            //     var date = "0";
            //     // alert(patient_id);
            //     window.location.href =
            //         `{{ url('/client_medication/${patient_id}/${date}') }}`;
            // });
        });
    </script>

</body>

</html>
