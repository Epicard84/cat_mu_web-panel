<!DOCTYPE html>
<html lang="en">

@extends('layouts.auth.app')

@section('title', 'MFD')

@section('content')

    <body>
        <div class="container customContainer">
            <section class="dbMainbase">
                <button class="sideBtn" onclick="openNav()">☰</button>
                @include('layouts.MasterUser.sidebar')


                <div class="dbRight">
                    <div class="happyEvnt reminderTop mb-3">
                        <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long"
                                style="color: #ffffff;"></i></span>
                        <h4>MFD</h4>
                        <span></span>
                    </div>
                    <form method="POST" enctype="multipart/form-data" id="mfdForm" autocomplete="off">
                        @csrf
                        <div class="row col-12">
                            <div class="col-7">

                                <textarea class="form-control mfd" rows="1" placeholder="Enter MFD details" name="mfd_details" id="mfd"></textarea>
                                <p class="description_error" style="color: red; font-size:10pt"></p>
                            </div>
                            <div class="col-4">
                                <input type="text" class="form-control" name="mfd_date" id="datepicker"
                                    placeholder="Select MFD date" />
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btn-sm"
                                    style="background-color: #3D4EC6; color:white">Save</button>
                            </div>
                        </div>
                    </form>
                    {{-- @if (Session::has('success'))
                    <script>
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 2000,
                            timerProgressBar: false,
                            didOpen: (toast) => {
                                toast.addEventListener(
                                    'mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener(
                                    'mouseleave',
                                    Swal.resumeTimer
                                )
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: "MFD added successfuly"
                        })
                    </script>
                @endif --}}



                    @if (count($mfds) > 0)
                        @foreach ($mfds as $m)
                            <div class="medicationDetlbg">
                                <div class="mfdTop">
                                    <span class="caltime"><img
                                            src="{{ asset('public/MasterUser/assets/images/caltime2.png') }}"
                                            alt="">
                                        {{ $m->date }}</span>
                                    <div class="editmfd" data-bs-toggle="modal"
                                        data-bs-target="#mfdEditModal{{ $m->id }}" data-id="{{ $m->id }}">
                                        <i class="fas fa-edit"></i>
                                    </div>
                                    <span
                                        class="dmbg mfdHe">{{ substr($masterUser->first_name, 0, 1) . substr($masterUser->last_name, 0, 1) }}</span>
                                </div>
                                <p>{{ $m->message }}</p>
                            </div>
                            <!-- Edit MFD-Modal -->
                            <div class="modal fade modalBase" id="mfdEditModal{{ $m->id }}" tabindex="-1"
                                aria-labelledby="notesModalLabel{{ $m->id }}" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Edit MFD</h5>
                                            <button type="button" class="btn-close" data-bs-dismiss="modal"
                                                aria-label="Close"></button>
                                        </div>
                                        <form enctype="multipart/form-data" id="editmfdForm{{ $m->id }}">
                                            @csrf
                                            <div class="modal-body">
                                                <div class="enterMfd">
                                                    <input hidden name="mfd_id" value="{{ $m->id }}">
                                                    <textarea class="form-control mfd" id="mfd" name="mfd_details">{{ $m->message }}</textarea>
                                                    <p class="description_error_1" style="color: red; font-size:10pt"></p>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <button id="submit{{ $m->id }}" type="button"
                                                    class="btn commonButton">Update</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @else
                        <div class="medicationDetlbg">
                            <p>No MFD found</p>
                        </div>
                    @endif

                    @if ($mfd_btn == 0)
                    @endif
                    {{-- <a href="{{ route('create_mfd') }}"><button class="commonButton respFullwdth disabled">Add new
                        MFD</button></a> --}}

                </div>
            </section>
        </div>



        @include('MasterUser/Components/footer')

        <script>
            window.pressed = function() {
                var a = document.getElementById('aa');
                if (a.value == "") {
                    fileLabel.innerHTML = "Upload image";
                } else {
                    var theSplit = a.value.split('\\');
                    fileLabel.innerHTML = theSplit[theSplit.length - 1];
                }
            };
        </script>
        <script>
            $(document).ready(function() {

                //MFD 1st letter capital
                $('.mfd').keyup(function(event) {
                    var textBox = event.target;
                    var start = textBox.selectionStart;
                    var end = textBox.selectionEnd;
                    textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                    textBox.setSelectionRange(start, end);
                });

                new AirDatepicker('#datepicker', {
                    autoClose: false,
                    dateFormat: 'MM/dd/yyyy',
                    // minDate: new Date(),
                })

                $(".editmfd").on({
                    mouseenter: function() {
                        $(this).css("cursor", "pointer");
                    },

                    mouseleave: function() {
                        $(this).css("cursor", "auto");
                    },
                })

                //Edit MFD Form Submit
                $('.editmfd').click(function(e) {
                    e.preventDefault();
                    var uid = $(this).attr('data-id');
                    id = "#editmfdForm" + uid;
                    var btnid = "#submit" + uid;
                    $(btnid).on("click", function(e) {
                        e.preventDefault();
                        $('.description_error_1').html("");

                        var form = $(id)[0];
                        var formData = new FormData(form);
                        console.log(formData);
                        $.ajax({
                            url: "{{ route('mfd_edit_details') }}",
                            type: 'POST',
                            dataType: 'json',
                            processData: false,
                            contentType: false,
                            data: formData,
                            success: function(response) {

                                console.log(response);

                                if (response.hasOwnProperty('success')) {

                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2000,
                                        timerProgressBar: false,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })

                                    Toast.fire({
                                        icon: 'success',
                                        title: response.success
                                    })

                                    setTimeout(function() {
                                        window.location.reload(false);
                                    }, 2501);

                                } else {
                                    console.log(response.error);
                                    if (response.error) {
                                        $('.description_error_1').html("*" + response
                                            .error);
                                    }
                                }
                            }
                        });
                    })
                });

                //New Reminder
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });

                $('#mfdForm').submit(function(e) {
                    e.preventDefault();

                    $('.description_error').html("");

                    var form = $('#mfdForm')[0];
                    var formData = new FormData(form);
                    console.log(formData);
                    $.ajax({
                        url: "{{ route('store_mfd') }}",
                        type: 'POST',
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        data: formData,
                        success: function(response) {

                            console.log(response);

                            if (response.hasOwnProperty('success')) {

                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    timerProgressBar: false,
                                    didOpen: (toast) => {
                                        toast.addEventListener(
                                            'mouseenter',
                                            Swal.stopTimer)
                                        toast.addEventListener(
                                            'mouseleave',
                                            Swal.resumeTimer
                                        )
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: response.success
                                })

                                setTimeout(function() {
                                    window.location.reload(false);
                                }, 1501);



                            } else {

                                // let description = response.hasOwnProperty('description');                 
                                console.log(response.error);
                                if (response.error) {
                                    $('.description_error').html("*" + response.error);
                                }

                            }

                        }
                    });

                });
                $('#calendar').on('changeDate', function(event) {

                    var date = event.format('yyyy-mm-dd');
                    if (!date) {
                        let searchDate = new Date();
                        date = searchDate.format("yyyy-mm-dd");

                    }
                    $.ajax({
                        type: "get",
                        url: `{{ url('mu_filter_reminder/${date}') }}`,

                        dataType: "json",
                        success: function(response) {
                            console.log(response.html);
                            $('.bladeRender').empty();
                            $('.bladeRender').html(response.html);
                        }
                    });


                });
            });
        </script>
    </body>
@endsection

</html>
