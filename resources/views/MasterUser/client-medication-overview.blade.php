<!DOCTYPE html>
<html lang="en">

@include('MasterUser/Components/head')
{{-- {{ $date }} --}}
{{-- {{strtotime($date)}} --}}
{{-- {{dd( count($data->getMedicines))}} --}}
{{-- {{date('d',strtotime($date))}} --}}
{{-- @foreach ($data->getMedicines as $medicine) --}}
{{-- @foreach ($medicine as $m) --}}
{{-- @foreach ($medicine->getMedicineSchedule as $ms) --}}
{{-- {{ 'medicine_id-' . $ms->medicine_id . ',date-' . date('d', strtotime($ms->date)) . ',medicine_status-' . $ms->complete_details }}<br> --}}
{{-- @endforeach --}}
{{-- {{$m}} --}}
{{-- @endforeach --}}
{{-- @endforeach --}}
{{-- {{$data->getMedicines}} --}}
{{-- {{$patient_id}} --}}
{{-- {{$nach}} --}}
{{-- @foreach ($nach as $nc)
    {{ $nc }}
@endforeach --}}
{{-- @foreach ($note as $nt)
    {{ $nt }}
@endforeach --}}
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('MasterUser/Components/sidebar')
            <div class="dbRight" id="divPrint" style="position: relative">
                <div class="happyEvnt dmTop mb-3 stickyName">
                    <span class="backarrow" onclick="javascript:history.go(-1)"><i
                            class="fa-light fa-arrow-left-long"></i></span>
                    <div>
                        @if ($preClient)
                            <span class="preClient" data-id="{{ Crypt::encryptString($preClient->id) }}"><i
                                    class="fa-solid fa-angle-left" style="color: #ffffff;"></i></span>
                        @endif
                    </div>
                    <a href="{{ route('clients_details', Crypt::encryptString($data->id)) }}">
                        <h4>{{ $data->first_name }} {{ $data->last_name }}</h4>
                    </a>
                    <div>
                        @if ($nxtClient)
                            <span class="nxtClient" data-id="{{ Crypt::encryptString($nxtClient->id) }}"><i
                                    class="fa-solid fa-angle-right" style="color: #ffffff;"></i></span>
                        @endif
                    </div>
                    <div>
                        <div>
                            {{-- <a href="{{ route('/master_details') }}"><span
                                    class="dmbg">{{ strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1)) }}</span></a> --}}
                            <span class="dmbg1">Overview</span>
                            &nbsp;

                            <span type="button" onclick="ExportToExcel('xlsx')" /><i class="fas fa-download fa-lg"
                                style="color: #ffffff;"></i></span>
                        </div>
                        <p>{{ Auth::user()->first_name . Auth::user()->last_name }} {{ Auth::user()->phone_number }}</p>
                    </div>
                </div>
                {{-- @if (Session::has('msg')) --}}
                <div class="cmBg">
                <div class="overviewTablebg overcustom" style="">
                    <table id="medicineTable">
                        <thead>
                            <tr>
                                <th><span class="prvNxtDate" data-id="{{ date('d F Y', strtotime($date . '-30 day')) }}"
                                        data-toggle="tooltip" data-placement="top"
                                        title="Before {{ date('d F Y', strtotime($date . '-29 day')) }}"><i
                                            class="fa-solid fa-angle-left"></i></span></th>
                                {{-- <th>{{ date('F Y', strtotime($date)) }}</th> --}}
                                <th>{{ date('Y', strtotime($date)) }}</th>
                                {{-- @for ($d = date('d', strtotime($date)) - 2; $d <= date('d', strtotime($date)); $d++) --}}
                                @for ($d = strtotime($date . '-29 day'); $d <= strtotime($date); $d = $d + 86400)
                                    {{-- <th><span class="cmchart dateId" data-id="{{Crypt::encryptString(date('Y-m', strtotime($date)).'-'.$d)}}">{{ $d }}<span>c</span></span></th> --}}
                                    @php
                                        $ch = 0;
                                        $na = 0;
                                        //    $sh = "";
                                        foreach ($nach as $nc) {
                                            if (strtotime($nc->date) == $d) {
                                                if ($nc->type == 1) {
                                                    $ch = 1;
                                                    // $sh = "C";
                                                } elseif ($nc->type == 2) {
                                                    $na = 1;
                                                    // $sh = "N";
                                                }
                                            }
                                        }
                                        // $sh = '';
                                        // if ($na == 1 && $ch == 0) {
                                        //     $sh = 'N';
                                        // } elseif ($ch == 1 && $na == 0) {
                                        //     $sh = 'C';
                                        // } elseif ($na == 1 && $ch == 1) {
                                        //     $sh = 'A';
                                        // }
                                    @endphp

                                    <th><span class="cmchart dateId"
                                            data-id="{{ Crypt::encryptString(date('Y-m-d', $d)) }}">
                                            {{-- @if ($ch == 1)
                                                <span class="indRight">P</span>
                                            @endif --}}
                                            {{ date(' m/d ', $d) }}
                                            {{-- @if ($na == 1)
                                                <span>N</span>
                                            @endif --}}
                                        </span>
                                    </th>
                                @endfor
                                <th><span class="prvNxtDate"
                                        data-id="{{ date('d F Y', strtotime($date . '+30 day')) }}"
                                        data-toggle="tooltip" data-placement="top"
                                        title="After {{ date('d F Y', strtotime($date)) }}">&nbsp;<i
                                            class="fa-solid fa-angle-right"></i></span></th>
                                {{-- <th><span class="cmchart">5<span>c</span></span></th>
                                <th><span class="cmchart">6<span>c</span></span></th> --}}
                            </tr>
                        </thead>
                        <tbody>
                            <div>
                                {{-- <button onclick="ExportToExcel('xlsx')">Export table to excel</button> --}}
                            </div>
                            {{-- {{($segment2 == '#') ? 'active' : ''}} --}}
                            @foreach ($data->getMedicines as $medicine)
                                @if (strtotime($date) >= strtotime($medicine->start_date) &&
                                        strtotime($date) <= strtotime($medicine->end_date . '+29 day'))
                                    <tr>
                                        <td>&nbsp;</td>
                                        <td data-id="{{ $medicine->id }}" class="medicine">
                                            {{ $medicine->medicine_name }}
                                            @if($medicine->time_days == 1 && $medicine->scheduled_time == "00:00")
                                            <p style="color: #3D4EC6;">*</p>
                                            
                                            @endif
                                            <p style="font-size: 10pt">{{ $medicine->descriptions }}</p>
                                        </td>
                                        {{-- @for ($d = date('d', strtotime($date)) - 2; $d <= date('d', strtotime($date)); $d++) --}}
                                        @for ($d = strtotime($date . '-29 day'); $d <= strtotime($date); $d = $d + 86400)
                                            <td>
                                                @php
                                                    $meds = 0;
                                                    
                                                @endphp
                                                {{-- @if (date('d', $d) >= date('d', strtotime($medicine->start_date)) ||
                                                        date('d', $d) <= date('d', strtotime($medicine->end_date))) --}}
                                                        @if ($d >= strtotime($medicine->start_date) ||
                                                        $d <= strtotime($medicine->end_date))
                                                    @foreach ($medicine->getMedicineSchedule as $ms)
                                                        {{-- {{ 'medicine_id-' . $ms->medicine_id . ',date-' . date('d', strtotime($ms->date)) . ',medicine_status-' . $ms->complete_details }} --}}
                                                        @if ($ms->medicine_id == $medicine->id && date('d-m-Y', strtotime($ms->date)) == date('d-m-Y', $d))
                                                            @php
                                                                
                                                                if ($ms->complete_details == null) {
                                                                    // $index = -1;
                                                                    $schtimes = explode(',', $ms->scheduled_time); //later
                                                                    $medtime = count($schtimes);
                                                                    for ($i = 0; $i < $medtime; $i++) {
                                                                        $times[$i] = 0;
                                                                    }
                                                                } else {
                                                                    $times = explode(',', $ms->complete_details);
                                                                    $comtime = count($times);
                                                                    // $index = count($times);
                                                                    $schtimes = explode(',', $ms->scheduled_time); //later
                                                                    $medtime = count($schtimes);
                                                                    // if($comtime == 1){
                                                                    //     $times[0] = $times[0];
                                                                    //     for ($i = 1; $i < $medtime; $i++){
                                                                    //         $times[$i] = 0;
                                                                    //     }
                                                                    if ($medtime > $comtime) {
                                                                        for ($i = 0; $i < $comtime; $i++) {
                                                                            // $times[$i] = $times[$i];
                                                                            if ($times[$i] == '' || $times[$i] == null) {
                                                                                $times[$i] = 0;
                                                                            } else {
                                                                                $times[$i] = $times[$i];
                                                                            }
                                                                        }
                                                                        for ($i = $comtime; $i < $medtime; $i++) {
                                                                            $times[$i] = 0;
                                                                        }
                                                                    } else {
                                                                        for ($i = 0; $i < $medtime; $i++) {
                                                                            if ($times[$i] == '' || $times[$i] == null) {
                                                                                $times[$i] = 0;
                                                                            } else {
                                                                                $times[$i] = $times[$i];
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                                // print_r ($index);
                                                                // $nt = '0';
                                                                // foreach ($note as $nt) {
                                                                //     if ($nt->schedule_id == $ms->id) {
                                                                //         $nt = '1';
                                                                //     }
                                                                // }
                                                                $meds = $ms->id;
                                                            @endphp

                                                            @for ($i = 0; $i < $medtime; $i++)
                                                                {{-- @foreach ($times as $t) --}}


                                                                {{-- @endforeach --}}
                                                                {{-- <div class="cmMediblock {{ $ms->status == '1' ? 'noMedicine' : '' }}">
                                                                <div class="cmMediStatement">{{ $d }}He<span
                                                                        class="cmNotes">N</span></div>
                                                                <small>12:00</small>
                                                                {{ $medicine->id . ' ' . $d }}
                                                                </div> --}}

                                                                @if ($times[$i] == 0)
                                                                    <div
                                                                        class="cmMediblock noMedicine medSchId1 medSchId">
                                                                        <div class="cmMediStatement"
                                                                            data-id="{{ $ms->id }}_{{ $i }}">
                                                                            {{-- <input type="text" id="action"
                                                                                class="action" hidden value="0"> --}}
                                                                            {{-- {{ date('d', $d) }} --}}
                                                                            @php
                                                                                $in = 'Not Given';
                                                                                if ($schtimes[$i] == '00:00') {
                                                                                    $in = 'PRN-X';
                                                                                }
                                                                            @endphp
                                                                            {{ $in }}
                                                                            {{-- @foreach ($note as $nt)
                                                                                @if ($nt->schedule_id == $ms->id)
                                                                                
                                                                                    <span class="cmNotes">N</span>
                                                                                @endif
                                                                            @endforeach --}}
                                                                        </div>
                                                                        {{-- <small>12:00</small> --}}
                                                                        @php
                                                                            $time = $schtimes[$i];
                                                                        @endphp
                                                                        @if ($time != "00:00")
                                                                        <small
                                                                        style="color: rgb(150, 0, 0)">{{ $time }}</small>
                                                                        @endif
                                                                        <!-- later -->
                                                                        {{-- {{ $medicine->id . ' ' . date('d', $d) . ' ' . $ms->id }} --}}
                                                                    </div>
                                                                @else
                                                                    <div class="cmMediblock medSchId">
                                                                        <div class="cmMediStatement"
                                                                            data-id="{{ $ms->id }}_{{ $i }}">
                                                                             {{-- {{ strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1)) }} --}}
                                                                             @php
                                                                            $time = $schtimes[$i];
                                                                        @endphp
                                                                             @if ($time != "00:00")
                                                                            {{ $times[$i] }}
                                                                            @else
                                                                            <p style="font-size: 8px; line-height: 12px; margin:0; color:white">PRN-{{ $times[$i] }}</p>
                                                                            @endif
                                                                            {{-- <input type="text" id="action"
                                                                                class="action" hidden value="1"> --}}
                                                                            {{-- @php
                                                                        $nt = "0";
                                                                        foreach ($note as $nt) {
                                                                            if($nt->schedule_id == $ms->id){
                                                                                $nt = "1";
                                                                            }
                                                                        }
                                                                        @endphp --}}
                                                                            {{-- @foreach ($note as $nt)
                                                                            @if ($nt->schedule_id == $ms->id)
                                                                                <span class="cmNotes">N</span>
                                                                            @endif
                                                                        @endforeach --}}
                                                                        </div>
                                                                        {{-- <small>{{ $times[$i] }} EST</small> --}}
                                                                        {{-- @php
                                                                            $time = $schtimes[$i];
                                                                        @endphp --}}
                                                                        @if ($time != "00:00")
                                                                        <small
                                                                            style="color: rgb(150, 0, 0)">{{ $time }}</small>
                                                                            @endif
                                                                        <!-- later -->
                                                                    </div>
                                                                @endif
                                                            @endfor
                                                        @endif
                                                    @endforeach
                                                @else
                                                    {{-- <div class="cmMediblock">
                                                    <div class="cmMediStatement">No</div>
                                                    <small>00:00</small>
                                                </div> --}}
                                                @endif
                                                @if ($meds != 0)
                                                    <div align="center" class="notesPage"
                                                        data-id="{{ $meds }}">
                                                        <div class="noteini">
                                                            @foreach ($note as $nt)
                                                                @if ($nt->schedule_id == $meds)
                                                                    <span class="cmNotes">N</span>
                                                                @endif
                                                            @endforeach
                                                            <button type="button"
                                                                class="btn btn-sm btn-outline-primary">Notes</button>
                                                        </div>
                                                    </div>
                                                @endif
                                            </td>
                                        @endfor

                                        <td>&nbsp;</td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

                <br>

                <div class="clientMedfootbutton">
                    {{-- <button class="commonButton note testtip"
                        data-id="{{ Crypt::encryptString($patient_id) }}">Notes</button> --}}
                    <button class="commonButton chart testtip" data-id="{{ Crypt::encryptString($patient_id) }}">Prog
                        Notes</button>
                    <button class="commonButton narrative testtip"
                        data-id="{{ Crypt::encryptString($patient_id) }}">Dr's Order</button>
                </div>
                {{-- @endif --}}
                <input hidden id="patient_id" value="{{ Crypt::encryptString($patient_id) }}">
                <input hidden id="patient_name" value="{{ $data->first_name . '.' . $data->last_name }}">
                <input hidden id="today" value="{{ Crypt::encryptString(date('Y-m-d', strtotime($date))) }}">
            </div>
        </section>
    </div>
    <div id="previewImage">

    </div>


    <script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>

    @include('MasterUser/Components/footer')
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        $(document).ready(function() {
            $(".notesPage button").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            });

            $(".notesPage").on('click', function(e) {
                var id = $(this).attr("data-id");
                // alert(id);
                var patient_id = document.getElementById('patient_id').value;
                // alert(patient_id);
                window.location.href = `{{ url('medication_note/${id}/${patient_id}') }}`;
            });
        });
    </script>
    <script>
        $(document).ready(function() {

            //Medicine Details
            $(".medicine").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    var med_id = $(this).attr("data-id");
                    // alert(med_id);
                    window.location.href = `{{ url('/medication_details/${med_id}') }}`;
                }
            });

            //Previous and Next date
            $(".prvNxtDate").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
                click: function() {
                    var date = $(this).attr("data-id");
                    var patient_id = document.getElementById("patient_id").value
                    // alert(date+' '+patient_id);
                    // window.location.href = `{{ url('/client_med_prenxt/${patient_id}/${date}') }}`;
                    window.location.href = `{{ url('/client_medication_overview/${patient_id}/${date}') }}`;
                }
            });

            //Chart Details
            $(".chart").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    var patient_id = $(this).attr("data-id");
                    var today = document.getElementById('today').value;
                    // alert(today);
                    window.location.href =
                        `{{ url('/sr_medication_chart/${today}/${patient_id}') }}`;
                }
            });

            //Narrative Details
            $(".narrative").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    var patient_id = $(this).attr("data-id");
                    var today = document.getElementById('today').value;
                    window.location.href =
                        `{{ url('/sr_medication_narrative/${today}/${patient_id}') }}`;
                }
            });

            //Note Details
            $(".note").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    var patient_id = $(this).attr("data-id");
                    var today = document.getElementById('today').value;
                    window.location.href =
                        `{{ url('/medication_note/${today}/${patient_id}') }}`;
                }
            });

            // Date + Chart
            // $(".dateId").on({
            //     mouseenter: function() {
            //         $(this).css("cursor", "pointer");
            //     },

            //     mouseleave: function() {
            //         $(this).css("cursor", "auto");
            //     },

            //     click: function() {
            //         $(".cmchart").removeClass("datePick");
            //         $(".medSchId div").removeClass("schSelect");
            //         $(".note").addClass("testtip");
            //         $(".note").removeClass("noteClick");
            //         $(this).addClass("datePick");

            //         var date = $(this).attr("data-id");
            //         // alert(date);
            //         $(".chart").removeClass("testtip");
            //         $(".narrative").removeClass("testtip");
            //         $(".chart").click(function() {
            //             // var type = "note";
            //             var patient_id = $(this).attr("data-id");
            //             // alert('Chart '+patient_id);
            //             window.location.href =
            //                 `{{ url('/sr_medication_chart/${date}/${patient_id}') }}`;
            //         });
            //         $(".narrative").click(function() {
            //             // var type = "note";
            //             // alert('Note '+id);
            //             var patient_id = $(this).attr("data-id");
            //             window.location.href =
            //                 `{{ url('/sr_medication_narrative/${date}/${patient_id}') }}`;
            //         });
            //         $(".note").click(function() {
            //             // alert('Sorry, your request is not available. Click on Charts or Narrative');
            //             Swal.fire({
            //                 icon: 'error',
            //                 title: 'Sorry, request is not available. ',
            //                 text: 'Click on Charts or Narrative',
            //                 confirmButtonColor: '#3D4EC6',
            //                 // })
            //             }).then((result) => {
            //                 // Reload the Page
            //                 location.reload();
            //             });
            //         })
            //     }
            // });

            //Previous Patient
            $(".preClient").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
                click: function() {
                    var patient_id = $(this).attr("data-id");

                    // alert(patient_id);
                    var date = "0";
                    window.location.href = `{{ url('/client_medication_overview/${patient_id}/${date}') }}`;
                }
            });
            //Next Patient
            $(".nxtClient").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
                click: function() {
                    var patient_id = $(this).attr("data-id");

                    // alert(patient_id);
                    var date = "0";
                    window.location.href = `{{ url('/client_medication_overview/${patient_id}/${date}') }}`;
                }
            });

            $(".backarrow").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },
                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            });

            //PDF Patient Medication
            $(".pdfPrint").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
                click: function() {
                    var patient_id = $(this).attr("data-id");

                    // alert(patient_id);
                    var date = "0";
                    window.location.href = `{{ url('/client_medication_overview/${patient_id}/${date}') }}`;
                }
            });
        });
    </script>

    <script>
        function ExportToExcel(type, fn, dl) {
            var elt = document.getElementById('medicineTable');
            // var patient_name = $(this).attr("data-id");
            var patient_name = document.getElementById("patient_name").value;
            console.log(patient_name);
            var wb = XLSX.utils.table_to_book(elt, {
                sheet: "sheet1"
            });
            return dl ?
                XLSX.write(wb, {
                    bookType: type,
                    bookSST: true,
                    type: 'base64'
                }) :
                XLSX.writeFile(wb, fn || (`${patient_name}_client_medication_ckecklist.` + (type || 'xlsx')));
        }
    </script>
</body>

</html>
