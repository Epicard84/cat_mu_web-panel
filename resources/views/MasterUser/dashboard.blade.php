<!DOCTYPE html>
<html lang="en">
@extends('layouts.auth.app')

@section('title', 'Dashboard')

@section('content')

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('layouts.MasterUser.sidebar')
            @if (Session::has('success'))
            <script>
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: false,
                    didOpen: (toast) => {
                        toast.addEventListener(
                            'mouseenter',
                            Swal.stopTimer)
                        toast.addEventListener(
                            'mouseleave',
                            Swal.resumeTimer
                        )
                    }
                })

                Toast.fire({
                    icon: 'success',
                    title: {
                        {
                            Session::get('success')
                        }
                    }
                })
            </script>
            @endif
            @if (Session::has('password'))
            <script>
                const Toast = Swal.mixin({
                    toast: true,
                    position: 'top-end',
                    showConfirmButton: false,
                    timer: 2000,
                    timerProgressBar: false,
                    didOpen: (toast) => {
                        toast.addEventListener(
                            'mouseenter',
                            Swal.stopTimer)
                        toast.addEventListener(
                            'mouseleave',
                            Swal.resumeTimer
                        )
                    }
                })

                Toast.fire({
                    icon: 'success',
                    title: {
                        {
                            Session::get('password')
                        }
                    }
                })
            </script>
            @endif

            <div class="dbRight">
                <div class="happyEvnt">
                    <div class="subusr mastertopSection">
                        @if (Auth::user()->profile_image_name)
                        <div class="subusrUpload"><img src="{{ asset('images/MasterUserProfile/' . Auth::user()->profile_image_name) }}" width="40px" /></div>
                        @else
                        <div class="subusrUpload"><img src="{{ asset('public/images/MasterUserProfile/defult_image.png') }}" width="40px" />
                        </div>
                        @endif
                        <div>
                            <a href="{{ route('/master_details') }}">
                                <h4>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h4>
                            </a>
                            <!-- <p><i class="fa-light fa-location-dot"></i>
                                    {{ Auth::user()->facility_address }}</p> -->
                            <!-- <p><i class="fa-light fa-location-dot"></i>
                                    {{ Auth::user()->city.", ".Auth::user()->country }}</p> -->
                            <p><i class="fa-light fa-location-dot"></i>
                                <span data-bs-toggle="tooltip" data-bs-placement="top" title="{{ Auth::user()->facility_address }}">
                                    @if (strlen(Auth::user()->facility_address) > 41)
                                    {{substr(Auth::user()->facility_address, 0, 40) . '...'}}
                                    @else
                                    {{ Auth::user()->facility_address }}
                                    @endif
                                </span>
                            </p>
                        </div>
                    </div>
                    @if ($reminder > 0)
                    <div class="hasreminder">
                        * Please check Today's reminder
                    </div>
                    @endif
                </div>
                <div class="evntbtns addSubUsr">
                    @foreach ($clients as $item)
                    <button><label class="dashProgicon" data-id="{{ Crypt::encryptString($item->id) }}">O</label><label class="patientId" data-id="{{ Crypt::encryptString($item->id) }}">{{ $item->first_name }}
                            {{ $item->last_name }}</label> <label data-id="{{ $item->id }}" class="delete archive"><i class="fa-solid fa-box-archive"></i></label></button>
                    @endforeach
                </div>
                @if ($total <= 4) <a href="{{ route('add_client') }}">
                    <div class="centerAlign"><button class="commonButton">Add Client</button></div>
                    </a>
                    @endif
                    <input hidden id="today" value="{{ Crypt::encryptString($date) }}">

            </div>
        </section>
    </div>
</body>
<script>
    $(".archive").on({
        mouseenter: function() {
            $(this).css("cursor", "pointer");
        },

        mouseleave: function() {
            $(this).css("cursor", "auto");
        },
    });
    $(document).ready(function() {

        $('.archive').click(function() {
            const swalWithBootstrapButtons = Swal.mixin({
                customClass: {
                    confirmButton: 'btn btn-success',
                    cancelButton: 'btn btn-danger m-3'
                },
                buttonsStyling: false
            })

            swalWithBootstrapButtons.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Yes, Archive it!',
                cancelButtonText: 'No, cancel!',
                reverseButtons: true,
            }).then((result) => {
                if (result.isConfirmed) {

                    $.ajax({
                        type: "GET",
                        url: "{{ route('/archive_client') }}",
                        data: {
                            'id': $(this).attr('data-id')
                        },
                        dataType: "json",
                        success: function(response) {
                            swalWithBootstrapButtons.fire(
                                'Deleted!',
                                response.success,
                                'success'
                            )
                        }
                    });
                    setTimeout(function() {
                        window.location.reload(false);
                    }, 1000);



                } else if (
                    /* Read more about handling dismissals below */
                    result.dismiss === Swal.DismissReason.cancel
                ) {
                    swalWithBootstrapButtons.fire(
                        'Cancelled',
                        'Your imaginary file is safe :)',
                        'error'
                    )
                }
            })


            // console.log($(this).attr('data-id'));
        });
        //SumonK Mouse pointer change
        $(".patientId").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });
        $(".patientId").click(function() {
            var patient_id = $(this).attr("data-id");
            var date = "0";
            // alert(patient_id);
            window.location.href =
                `{{ url('/client_medication/${patient_id}/${date}') }}`;
        });
    });
</script>
<script>
    $(".dashProgicon").on({
        mouseenter: function() {
            $(this).css("cursor", "pointer");
        },

        mouseleave: function() {
            $(this).css("cursor", "auto");
        },
    });
    $(document).ready(function() {
        $(".dashProgicon").click(function() {
            var patient_id = $(this).attr("data-id");
            var today = document.getElementById('today').value;
            // alert(today);
            window.location.href =
                `{{ url('/medication_all_chart/${patient_id}') }}`;
        });
    });
</script>
<script>
    $(".hasreminder").on({
        mouseenter: function() {
            $(this).css("cursor", "pointer");
        },

        mouseleave: function() {
            $(this).css("cursor", "auto");
        },
    });
    $(document).ready(function() {
        $(".hasreminder").click(function() {
            window.location.href =
                `{{ url('mu_reminder') }}`;
        });
    });
</script>


@endsection

</html>