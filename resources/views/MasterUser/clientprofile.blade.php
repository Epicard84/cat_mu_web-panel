<!DOCTYPE html>
<html lang="en">

@include('MasterUser/Components/head')

<body>

    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('MasterUser/Components/sidebar')
            <div class="dbRight">
                @if (Session::has('success'))
                <script>
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1500,
                        // timerProgressBar: true,
                        didOpen: (toast) => {
                            toast.addEventListener(
                                'mouseenter',
                                Swal.stopTimer)
                            toast.addEventListener(
                                'mouseleave',
                                Swal.resumeTimer
                            )
                        }
                    })
    
                    Toast.fire({
                        icon: 'success',
                        title: 'Updated Successfully'
                    })
                </script>
            @endif
                <div class="happyEvnt mb-4">
                    
                        <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long" style="color: #ffffff;"></i></span>
                        <h4>{{ $data->first_name }} {{ $data->last_name }}</h4>
                    
                    <div>
                    <a href="{{ route('update_client', Crypt::encryptString($data->id)) }}"><span class="editIcon"><i
                        class="fa-light fa-pen"></i></span></a>
                    &nbsp;&nbsp;
                    <span type="button" onclick="window.print()"/><i class="fal fa-print fa-lg" style="color: #ffffff;"></i></span>
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="col-md-7">
                        <h4>Personal Information</h4>
                        <div class="personalInfo">
                            <p>Date of birth: {{ date('jS F, Y', strtotime($data->dob)) }}</p>
                            <p>Gender: {{ $data->gender }}</p>
                            @foreach ($guardian as $item)
                            <div class="mt-2">
                                <p>Gurdian name: {{ $item->guardian_name }}</p>
                                <p>Gurdian number: {{ $item->guardian_phone_number }}</p>
                                <p>Gurdian address: {{ $item->guardian_address }}</p>
                            </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-md-5">
                        <h4>Doctor’s Information</h4>
                        <div class="doctorInfo">
                            @foreach ($doctor as $item)
                                <h3>Dr. {{ $item->doctor_name }}</h3>
                                <p>Address: {{ $item->doctor_address }}</p>
                                <p>Mobile number: {{ $item->doctor_phone_number }}</p>
                            @endforeach
                        </div>
                    </div>
                </div>
                <h3>Diagnosis</h3>
                <div class="medicationDetlbg">
                    <div class="medicationDetlbg">
                        @if (is_null($data->additional_misc_information))
                            <p class="text-danger">No information found </p>
                        @else
                            <p>{{ $data->additional_misc_information }}</p>
                        @endif
                    </div>
                </div>
                <h3>Documents</h3>
                <div class="doCardbg mb-3">
                    @if ($data->documents)
                        @foreach (explode(',', $data->documents) as $key => $item)
                            <div class="doCard">
                                <div class="pdfbg"><img src="{{ asset('public/MasterUser/assets/images/pdf.png') }}"
                                        alt=""><span style="font-size: 10pt">{{$item}}</span></div>
                                <a href="{{ url('/document_download/' . $item) }}" download="hello"><button
                                        class="commonButton"><i class="fa-light fa-download"></i></button></a>
                            </div>
                        @endforeach
                    @else
                        <p align="center text-danger">No document found</p>
                    @endif
                </div>
                <!--                <button class="commonButton">Add Client</button>-->
                <h3>Archived Medicines</h3>
                <div class="archivedMedicineTable">
                    <table>
                        <thead>
                            <tr>
                                <th scope="col">Sl No</th>
                                <th scope="col">Medicine Name</th>
                                <th scope="col">Start Date</th>
                                <th scope="col">End Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $count = 0;
                            @endphp
                            @foreach ($archivedMeds as $am)
                                @php
                                    $count = $count+1;
                                @endphp
                            <tr>
                                <td>{{$count}}</td>
                                <td>{{$am->medicine_name}}</td>
                                <td>{{date('m-d-Y',strtotime($am->start_date))}}</td>
                                <td>{{date('m-d-Y',strtotime($am->end_date))}}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </section>
    </div>



    
    @include('MasterUser/Components/footer')
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    

</body>

</html>
