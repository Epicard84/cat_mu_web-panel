<!DOCTYPE html>
<html lang="en">

@include('MasterUser/Components/head')

{{-- {{$reminders}} --}}
{{-- {{$date}} --}}

<body>

    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('MasterUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt reminderTop mb-3">
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long"
                            style="color: #ffffff;"></i></span>
                    <h4>SubUser Login</h4>
                    <span></span>
                </div>
                <section class="loginContainer subUsrlogincontainer">
                    <div class="forgotpassicon"><img src="{{ asset('public/MasterUser/assets/images/entercode.png') }}"
                            alt=""></div>
                    <h2>Enter code!</h2>
                    <p>Please enter 4 digit passcode to login</p>
                    <form action="{{ route('su_login_post') }}" method="post">
                        @csrf
                        <div class="digit-group">
                            <input type="text" id="digit-1" name="digit1" data-next="digit-2" />
                            <input type="text" id="digit-2" name="digit2" data-next="digit-3"
                                data-previous="digit-1" />
                            <input type="text" id="digit-3" name="digit3" data-next="digit-4"
                                data-previous="digit-2" />
                            <input type="text" id="digit-4" name="digit4" data-next="digit-5"
                                data-previous="digit-3" />
                        </div>
                        <input hidden name="muID" value="{{ Auth::user()->id }}">
                        @if (Session::has('msg'))
                            <h6 class="text-danger">{{ Session::get('msg') }}</h6>
                        @endif
                        <button type="submit" name="next" class="next" id="next">Submit</button>
                    </form>
                </section>

            </div>
        </section>
    </div>


    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    @include('MasterUser/Components/footer')
    <script src="{{ asset('public/MasterUser/assets/js/datepick.js') }}"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>

    {{-- <script>
        $(".reminderBlock").click(function(){
          $(this).hide('slow');
        });
    </script> --}}
    {{-- <script>
        //Reminder Seen Update
        $(".check").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });
        $(document).ready(function() {
            $(".check").click(function() {
                var rid = $(this).attr("data-id");
                // alert(rid);
                $.ajax({
                    url: `{{ url('check_reminder/${rid}') }}`,
                    method: 'get',

                    success: function(res) {

                        if (res.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: res.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);


                        } else {
                            console.log(res)
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'error',
                                title: res.error
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);
                        }
                    }

                })

            });
        });
    </script> --}}
    {{-- <script>
        $(document).ready(function() {

            $('#reminder').keyup(function(event) {
                var textBox = event.target;
                var start = textBox.selectionStart;
                var end = textBox.selectionEnd;
                textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);
            });


        });

        new AirDatepicker('#datepicker', {
            autoClose: false,
            dateFormat: 'MM/dd/yyyy',
            minDate: new Date(),
        })
    </script> --}}
    {{-- <script>
        $(document).ready(function() {
            //New Reminder
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#reminderForm').submit(function(e) {
                e.preventDefault();

                $('.description_error').html("");

                var form = $('#reminderForm')[0];
                var formData = new FormData(form);
                console.log(formData);
                $.ajax({
                    url: "{{ route('new_reminder') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 1500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 1501);



                        } else {

                            // let description = response.hasOwnProperty('description');                 
                            console.log(response.error);
                            if (response.error) {
                                $('.description_error').html("*" + response.error);
                            }

                        }

                    }
                });

            });
            $('#calendar').on('changeDate', function(event) {

                var date = event.format('yyyy-mm-dd');
                if (!date) {
                    let searchDate = new Date();
                    date = searchDate.format("yyyy-mm-dd");

                }
                $.ajax({
                    type: "get",
                    url: `{{ url('mu_filter_reminder/${date}') }}`,

                    dataType: "json",
                    success: function(response) {
                        console.log(response.html);
                        $('.bladeRender').empty();
                        $('.bladeRender').html(response.html);
                    }
                });


            });
        });
    </script> --}}
    <script>
        $('.digit-group').find('input').each(function() {
            $(this).attr('maxlength', 1);
            $(this).on('keyup', function(e) {
                var parent = $($(this).parent());

                if (e.keyCode === 8 || e.keyCode === 37) {
                    var prev = parent.find('input#' + $(this).data('previous'));

                    if (prev.length) {
                        $(prev).select();
                    }
                } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (
                        e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                    var next = parent.find('input#' + $(this).data('next'));

                    if (next.length) {
                        $(next).select();
                    } else {
                        if (parent.data('autosubmit')) {
                            parent.submit();
                        }
                    }
                }
            });
        });
    </script>


    <script>
        $(document).ready(function() {
            function disableBack() {
                window.history.forward()
            }
            window.onload = disableBack();
            window.onpageshow = function(e) {
                if (e.persisted)
                    disableBack();
            }
        });
    </script>



</body>

</html>
