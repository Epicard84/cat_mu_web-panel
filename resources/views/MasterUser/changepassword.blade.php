@extends('layouts.auth.app')

@section('title', 'Password')

@section('content')
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('MasterUser/Components/sidebar')
            <div class="dbRight">
                <!--
                        <div class="happyEvnt dmTop mb-3">
                            <div class="sbBack">
                                <h4>Change password</h4>
                            </div>
                        </div>
        -->
                <div class="changePasswordInner">
                    <div class="forgotpassicon"><img src="{{ asset('public/MasterUser/assets/images/keypassword.png') }}"
                            alt="">
                        <h3>Change Password!</h3>
                        <p>Your new password must be deferent from the previous password.</p>
                        <form action="{{ route('update_password') }}" method="post">
                            @csrf
                            <div class="form-group mb-3 mt-4">
                                <div class="showhidePwd" id="show_hide_password">
                                    <input class="form-control" type="password" placeholder="Old Password"
                                        name="old_password" id="old_password">
                                    <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>

                                    @if (Session::has('wrong_error'))
                                        <p align='left' class="text-danger password">
                                            {{ Session::get('wrong_error') }}
                                        </p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group mb-3 mt-4">
                                <div class="showhidePwd" id="show_hide_password">
                                    <input class="form-control" type="password" placeholder="New Password" name="password"
                                        id="password">
                                    <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>
                                    @if (Session::has('error'))
                                        @if (array_key_exists('password', json_decode(json_encode(Session::get('error')), true)))
                                            <p align='left' class="text-danger password">
                                                {{ json_decode(json_encode(Session::get('error')), true)['password'][0] }}
                                            </p>
                                        @endif
                                    @endif
                                </div>
                            </div>
                            <div class="form-group mb-5">
                                <div class="showhidePwd" id="show_hide_password2">
                                    <input class="form-control" type="password" placeholder="Confirm Password"
                                        name="confirm_password" id="confirm_password">
                                    <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>
                                    @if (Session::has('error'))
                                        @if (array_key_exists('confirm_password', json_decode(json_encode(Session::get('error')), true)))
                                            <p align='left' class="text-danger password">
                                                {{ json_decode(json_encode(Session::get('error')), true)['confirm_password'][0] }}
                                            </p>
                                        @endif
                                    @endif
                                </div>
                                <p class="checking mt-1" id="checking" align="left"></p>
                            </div>
                            <button class="commonButton" disabled='disabled' id="submit" type="submit">Change
                                Password</button>
                        </form>
                    </div>
                    {{-- <div>
                        <form id="reasonForm" autocomplete="off" method="POST">
                            @csrf
                            <input class="form-control" type="text" placeholder="Enter your reason" name="reason"
                                id="reason">
                            <p class="description_error" style="color: red; font-size:10pt"></p>
                            <button class="commonButton" type="submit">Submit Request</button>
                        </form>
                    </div> --}}

                </div>
        </section>
    </div>



    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    @include('MasterUser/Components/footer')
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>

    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
            $("#show_hide_password2 a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password2 input').attr("type") == "text") {
                    $('#show_hide_password2 input').attr('type', 'password');
                    $('#show_hide_password2 i').addClass("fa-eye-slash");
                    $('#show_hide_password2 i').removeClass("fa-eye");
                } else if ($('#show_hide_password2 input').attr("type") == "password") {
                    $('#show_hide_password2 input').attr('type', 'text');
                    $('#show_hide_password2 i').removeClass("fa-eye-slash");
                    $('#show_hide_password2 i').addClass("fa-eye");
                }
            });

            $(this).keyup(function() {
                $('.password').html('');
                $('.password').hide();
                var password = $("#password").val();
                var confirmPassword = $("#confirm_password").val();

                console.log(password, confirmPassword);


                if (password == '' && confirmPassword == '') {
                    $("#checking").html("");

                } else {

                    if (password != confirmPassword) {
                        $("#checking").html(`<p class="text-danger">Not Matching</p>`);

                    } else {

                        $("#checking").html(`<p class="text-success">Matching</p>`);

                        if (password.length >= 5) {
                            $("#submit").removeAttr("disabled");
                        } else {
                            $("#checking").html(`<p class="text-danger">Length must be 5</p>`);
                        }
                    }

                }

                // $(".from-control").css("border-bottom-color", "#fff");
                // $("#divCheckPasswordMatch").html("Passwords match.");
            });
        });
    </script>
    <script>
        $(document).ready(function() {

            $('#reason').keyup(function(event) {
                var textBox = event.target;
                var start = textBox.selectionStart;
                var end = textBox.selectionEnd;
                textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);
            });

            // $(".edit").on({
            //     mouseenter: function() {
            //         $(this).css("cursor", "pointer");
            //     },

            //     mouseleave: function() {
            //         $(this).css("cursor", "auto");
            //     },
            // })

            //Account deactivation request
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#reasonForm').submit(function(e) {
                e.preventDefault();

                $('.description_error').html("");

                var form = $('#reasonForm')[0];
                var formData = new FormData(form);
                console.log(formData);
                $.ajax({
                    url: "{{ route('disable_reason') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2500,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);

                        } else {
                            console.log(response.error);
                            if (response.error) {
                                $('.description_error').html("*" + response.error);
                            }
                        }
                    }
                });
            })
        });
    </script>
@endsection
