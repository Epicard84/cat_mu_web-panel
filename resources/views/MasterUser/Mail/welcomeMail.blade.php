<!DOCTYPE html>
<html lang="en">
@include('MasterUser/Components/head')

<body>
    <section class="loginContainer">
        <div class="commonLogoSection loginLogo">
            <img src="{{ asset('public/MasterUser/assets/images/logo.png') }}" alt="">
            <div>
                <h1>MyChartSpace</h1>
                <p>Save time, use your phone.</p>
            </div>
        </div>
        <h2>Welcome to MyChartSpace!</h2>
        <p>Hi, {{$user['first_name']}} Your login credential is.</p>
        
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th scope="col">Email ::</th>
                        <td scope="col">{{$user['email']}}</td>
                    </tr>
                    <tr>
                        <th scope="col">Password ::</th>
                        <td scope="col">{{$password}}</td>
                    </tr>
                </thead>
            </table>
        </div>
        <div>
        </div>
    </section>
</body>
@include('MasterUser/Components/footer')

</html>
