@if (count($date_chart) > 0)
    @foreach ($date_chart as $chart)
        @php
            $type = '';
            if ($chart->type == 0) {
                $type = 'Note';
            } elseif ($chart->type == 1) {
                $type = 'Prog Note';
            } else {
                $type = "Dr's Order";
            }
        @endphp

        <div class="medicationDetlbg">
            <div class="detailstype">
                @if ($chart->type == 0)
                    <b>*{{ $chart->getMedSch->getMedName->medicine_name }}*</b> &nbsp;&nbsp;
                @endif
                {{ $type }}
            </div>
            <div class="timeshow">{{ date('m-d-Y', strtotime($chart->created_at)) }}</div>
            {{-- @if ($date == $chart->date) --}}
            <div class="edit"> <span class="editText" data-bs-toggle="modal"
                    data-bs-target="#notesEditModal{{ $chart->id }}" data-id="{{ $chart->id }}">
                    <i class="fas fa-edit"></i></span> &nbsp; &nbsp;
                <span class="delete" data-id="{{ $chart->id }}">
                    <i class="fas fa-trash"></i></span>
            </div>
            {{-- @endif --}}
            <div class="clientMediNoteblock">
                @if ($chart->getName->user_type == 1)
                    <a href="{{ route('/master_details') }}">
                    @elseif($chart->getName->user_type == 2)
                        @php
                            $id = Crypt::encryptString($chart->getName->id);
                        @endphp
                        <a href="{{ route('subuser_profile', $id) }}">
                @endif
                <span
                    class="dmbg">{{ strtoupper(substr($chart->getName->first_name, 0, 1)) . strtoupper(substr($chart->getName->last_name, 0, 1)) }}</span></a>
                &nbsp; &nbsp;
                <div class="schedDetails">
                    <p>{{ $chart->descriptions }}</p>
                    @if ($chart->images)
                        @php
                            $images = explode(',', $chart->images);
                            $doc = 0;
                            $img = 0;
                            
                            foreach ($images as $image) {
                                $doc1 = explode('.', $image);
                                $type = $doc1[1];
                                if ($type == 'pdf' || $type == 'docx' || $type == 'doc') {
                                    $doc = 1;
                                }
                            }
                            
                        @endphp

                        @if ($doc != 1)
                            <div class="cmnImgbg gallery">
                                @php
                                    $images = explode(',', $chart->images);
                                @endphp
                                @foreach ($images as $image)
                                    <a href="{{ asset('public/documents/Schedule_Details/' . $image) }}"
                                        title="Chart {{ date('m-d-Y', strtotime($chart->created_at)) }}">
                                        <figure><img class="img-fluid img-thumbnail"
                                                src="{{ asset('public/documents/Schedule_Details/' . $image) }}"
                                                alt="{{ $image }}"></figure>
                                    </a>
                                @endforeach
                            </div>
                        @else
                            @php
                                $images = explode(',', $chart->images);
                            @endphp
                            <div class="d-flex d-inline">
                                @foreach ($images as $image)
                                    <div class="pdfbg mx-2">

                                        Document
                                        &nbsp;
                                        <a href="{{ asset('public/documents/Schedule_Details/' . $image) }}"><label
                                                class="commonButton savebtn viewbtn"><i
                                                    class="fa-light fa-download"></i></label></a>
                                    </div>
                                @endforeach
                            </div>
                        @endif
                    @endif
                </div>
            </div>
            @if ($chart->signature)
                <div class="signBg">
                    <div>
                        <em>Signature</em>
                        <div class="sign">
                            <img src="{{ asset('public/documents/Schedule_Details/' . $chart->signature) }}">
                        </div>
                    </div>
                </div>
            @endif
        </div>
        <!-- Edit Notes-Modal -->
        <div class="modal fade modalBase" id="notesEditModal{{ $chart->id }}" tabindex="-1"
            aria-labelledby="notesModalLabel{{ $chart->id }}" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Edit Chart</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <form enctype="multipart/form-data" id="editDetailsForm{{ $chart->id }}">
                        @csrf
                        <div class="modal-body">
                            <div class="enterMfd">
                                <input hidden name="details_id" value="{{ $chart->id }}">
                                <textarea class="form-control" name="description">{{ $chart->descriptions }}</textarea>
                                <p class="description_error" style="color: red; font-size:10pt">
                                </p>
                                <input type="file" multiple name="detailsImage[]" />
                            </div>
                            @if ($chart->images != null)
                                @php
                                    $files = explode(',', $chart->images);
                                @endphp
                                <div class="mb-3">
                                    @foreach ($files as $f)
                                        <div class="form-check">
                                            <input class="form-check-input" type="checkbox" value="{{ $f }}"
                                                id="flexCheckChecked" name="docs[]" checked>
                                            <label class="form-check-label" for="flexCheckChecked">
                                                {{ $f }}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            @endif
                        </div>
                        <div class="modal-footer">
                            <button id="submit{{ $chart->id }}" type="button"
                                class="btn commonButton">Update</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    @endforeach
@else
    <div>
        <p>&nbsp; Not found...</p>
    </div>
@endif

<script>
    $(document).ready(function() {



        //Delete Details
        $(".delete").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });

        $(".delete").click(function(e) {
            var id = $(this).attr("data-id");
            // alert(id);

            $.ajax({
                url: `{{ url('details_delete/${id}') }}`,
                method: 'get',

                success: function(res) {

                    if (res.hasOwnProperty('success')) {

                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 1500,
                            timerProgressBar: false,
                            didOpen: (toast) => {
                                toast.addEventListener(
                                    'mouseenter',
                                    Swal.stopTimer)
                                toast.addEventListener(
                                    'mouseleave',
                                    Swal.resumeTimer
                                )
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: res.success
                        })

                        setTimeout(function() {
                            window.location.reload(false);
                        }, 1501);


                    }
                }
            });
        });
    });
</script>
