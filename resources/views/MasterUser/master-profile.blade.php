<!DOCTYPE html>
<html lang="en">

@extends('layouts.auth.app')

@section('title', 'Profile')

@section('content')

    <body>
        <div class="container customContainer">
            <section class="dbMainbase">
                <button class="sideBtn" onclick="openNav()">☰</button>
                @include('layouts.MasterUser.sidebar')
                <div class="dbRight">
                    @if (Session::has('success'))
                        <script>
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: "Updated successfuly"
                            })
                        </script>
                    @endif
                    <div class="happyEvnt mb-4">
                        <div>
                            <div class="subusr mastertopSection">
                                @if (Auth::user()->profile_image_name)
                                    <div class="subusrUpload"><img
                                            src="{{ asset('images/MasterUserProfile/' . Auth::user()->profile_image_name) }}"
                                            alt="" /></div>
                                @else
                                    <div class="subusrUpload"><img
                                            src="{{ asset('public/images/MasterUserProfile/defult_image.png') }}"
                                            alt="" /></div>
                                @endif
                                <div class="masterHdn">
                                    <h4>{{ Auth::user()->first_name }} {{ Auth::user()->last_name }}</h4>
                                    <p><i class="fa-light fa-location-dot"></i> {{ $location }}</p>
                                    <div class="masterStatusToggle">
                                        <strong>Status:</strong>
                                        <div class="btn-group btn-toggle">


                                            @if (Auth::user()->status == 'online')
                                                <button class="btn btn-default active b " disabled data-id="online"
                                                    id="online">Online</button>
                                                <button class="btn b" data-id="offline"
                                                    id="offline">Offline</button>
                                            @else
                                                <button class="btn btn-default b" data-id="online"
                                                    id="online">Online</button>
                                                <button class="btn active b" disabled data-id="offline"
                                                    id="offline">Offline</button>
                                            @endif


                                            {{-- 
                                        <input type="radio" name ="status" class="btn btn-default " value="online">
                                        <input type="radio" name ="status" class="btn btn-primary active" value="offline">
                                        
                                        
                                        <button class="btn btn-default">Online</button>
                                        <button class="btn btn-primary active">Offline</button> --}}
                                        </div>
                                        {{-- <input type="radio" name="status" id="" value="online">
                                    <input type="radio" name="status" id=""value="offline"> --}}
                                    </div>
                                </div>
                            </div>

                        </div>
                        <a href="{{ Route('/edit_master_user') }}"><span class="editIcon"><i
                                    class="fa-light fa-pen"></i></span></a>
                    </div>
                    <div class="row mb-2">
                        <div class="col-md-12">
                            <h4 class="mb-2">Personal Information</h4>
                            <div class="personalInfo autoHeight">
                                <p>Phone number: <a href="tel:{{ Auth::user()->phone_number }}">{{ Auth::user()->phone_number }}</a></p>
                                <p>Email address: <a href="mailto:{{ Auth::user()->email }}">{{ Auth::user()->email }}</a></p>
                                <p>Facility address: {{ Auth::user()->facility_address }}</p>
                                <p>City: {{ Auth::user()->city }}</p>
                                <p>State: {{ Auth::user()->state }}</p>
                                <p>Country: {{ Auth::user()->country }}</p>
                                <p>Zip Code: {{ Auth::user()->zip_code }}</p>
                            </div>
                            <h4 class="mb-2">Documents</h4>
                            <div class="personalInfo autoHeight">
                                <div class="row" style="text-align: center;">
                                    <div class="col-3">
                                        <h6>Operators License</h6>
                                        @if (Auth::user()->operators_license)
                                            <a href="{{asset('images/MasterUserProfile/' . Auth::user()->operators_license)}}"><img src="{{ asset('images/MasterUserProfile/' . Auth::user()->operators_license) }}"
                                                    alt="" srcset="" class="docsimg"></a>
                                                   
                                            @endif
                                    </div>
                                    <div class="col-3">
                                        <h6>TB Clearance</h6>
                                        @if (Auth::user()->tb_clearance)
                                                <a href="{{asset('images/MasterUserProfile/' . Auth::user()->tb_clearance)}}"><img src="{{ asset('images/MasterUserProfile/' . Auth::user()->tb_clearance) }}"
                                                    alt="" srcset="" class="docsimg"></a>
                                            @endif
                                    </div>
                                    <div class="col-3">
                                        <h6>Physical Checkup</h6>
                                        @if (Auth::user()->physical_checkup)
                                                <a href="{{asset('images/MasterUserProfile/' . Auth::user()->physical_checkup)}}"><img src="{{ asset('images/MasterUserProfile/' . Auth::user()->physical_checkup) }}"
                                                    alt="" srcset="" class="docsimg"></a>
                                            @endif
                                    </div>
                                    <div class="col-3">
                                        <h6>Miscellaneous Doc</h6>
                                        @if (Auth::user()->misc_doc)
                                                <a href="{{asset('images/MasterUserProfile/' . Auth::user()->misc_doc)}}"><img src="{{ asset('images/MasterUserProfile/' . Auth::user()->misc_doc) }}"
                                                    alt="" srcset="" class="docsimg"></a>
                                            @endif
                                    </div>
                                </div>  
                            </div>
                            <div class="masterProfButton mb-5">
                                <a href="{{ route('add_subuser') }}" class="commonButton">Add Subuser
                                </a>
                                @if ($total < 5)
                                    <a href="{{ route('add_client') }}" class="commonButton">Add Client</a>
                                @endif
                            </div>
                            <h4 class="mb-2 text-center mb-3">Archived Clients</h4>
                            <div class="evntbtns addSubUsr">
                                @if($client)
                                @foreach ($client as $item)
                                    <button><label class="patientID" data-id="{{ Crypt::encryptString($item->id) }}"> {{ $item->first_name }}
                                        {{ $item->last_name }} </label><label class="delete restoreClient" data-id="{{ Crypt::encryptString($item->id) }}"><i class="fas fa-undo"></i></label></button>
                                @endforeach
                                @else
                                <p>No Archived Clients found.</p>
                                @endif
                            </div>
                            <h4 class="mb-2 text-center mb-3">Archived SubUsers</h4>
                            <div class="evntbtns addSubUsr">
                                @if($archiveSubusers)
                                @foreach ($archiveSubusers as $ac)
                                    <button><label class="" data-id="{{ Crypt::encryptString($ac->id) }}"> {{ $ac->first_name }}
                                        {{ $ac->last_name }} </label><span class="delete"><label class="restoreSubuser" data-id="{{ Crypt::encryptString($ac->id) }}"><i class="fas fa-undo"></i></label>&nbsp;&nbsp;&nbsp;<label class="deleteSubuser" data-id="{{ Crypt::encryptString($ac->id) }}"><i class="fas fa-trash-can"></i></label></span></button>
                                @endforeach
                                @else
                                <p>No Archived SubUsers found.</p>
                                @endif
                            </div>
                        </div>
                    </div>
                    <!--                <button class="commonButton">Add Client</button>-->
                </div>
            </section>
        </div>



        <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
        @include('MasterUser/Components/footer')
        <script>
            function openNav() {
                document.getElementById("mySidepanel").style.width = "90%";
            }

            function closeNav() {
                document.getElementById("mySidepanel").style.width = "0";
            }
        </script>
           <script>
            $(document).ready(function() {
                
                jQuery('.btn-toggle button').click(function(event) {
                    var id = $(this).attr('data-id');
                    jQuery('.active').removeClass('active');
    
    
                    // $(id).removeAttr('disabled');
                    // console.log(id);
                    if (id == "online") {
                        $('#offline').removeAttr("disabled");
                        $('#online').attr("disabled",true);
                    } else {
                        
                        $('#online').removeAttr("disabled");
                        $('#offline').attr("disabled",true);
                    }
    
    
    
                    jQuery(this).addClass('active');
                    
                    
          
                        $.ajax({
                            type: "GET",
                            url: "{{ route('status') }}",
                            data: {
                                'status': $(this).attr('data-id')
                            },
                            dataType: "json",
                            success: function(response) {
                                if (response == "success") {
                                    const Toast = Swal.mixin({
                                        toast: true,
                                        position: 'top-end',
                                        showConfirmButton: false,
                                        timer: 2000,
                                        timerProgressBar: false,
                                        didOpen: (toast) => {
                                            toast.addEventListener(
                                                'mouseenter',
                                                Swal.stopTimer)
                                            toast.addEventListener(
                                                'mouseleave',
                                                Swal.resumeTimer
                                            )
                                        }
                                    })
    
                                    Toast.fire({
                                        icon: 'success',
                                        title: "Satus updated successfuly"
                                    })
    
    
                                }
    
                            }
                        });
                    
    
                });
    
    
    
                // $('.btn-toggle').click(function() {
                //     $(this).find('.btn').toggleClass('active');
                //     if ($(this).find('.btn-primary').length > 0) {
                //         $(this).find('.btn').toggleClass('btn-primary');
                //     }
                //     if ($(this).find('.btn-danger').length > 0) {
                //         $(this).find('.btn').toggleClass('btn-danger');
                //     }
                //     if ($(this).find('.btn-success').length > 0) {
                //         $(this).find('.btn').toggleClass('btn-success');
                //     }
                //     if ($(this).find('.btn-info').length > 0) {
                //         $(this).find('.btn').toggleClass('btn-info');
                //     }
    
                //     $(this).find('.btn').toggleClass('btn-default');
    
                // });
    
                // $('form').submit(function() {
                //     var radioValue = $("input[name='options']:checked").val();
                //     if (radioValue) {
                //         alert("You selected - " + radioValue);
                //     };
                //     return false;
                // });
            });
        </script>
        <script>
            $(document).ready(function() {
                $(".patientID").click(function() {
                    var patient_id = $(this).attr("data-id");
                    var date = "0";
                    // alert(patient_id);
                    window.location.href =
                        `{{ url('/archived_client_medication/${patient_id}/${date}') }}`;
                });

                $(".restoreClient").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    var patient_id = $(this).attr("data-id");
                    // alert(patient_id);
                    $.ajax({
                        url: `{{ url('client_restore/${patient_id}') }}`,
                        method: 'get',

                        success: function(res) {

                            if (res.hasOwnProperty('success')) {

                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    timerProgressBar: false,
                                    didOpen: (toast) => {
                                        toast.addEventListener(
                                            'mouseenter',
                                            Swal.stopTimer)
                                        toast.addEventListener(
                                            'mouseleave',
                                            Swal.resumeTimer
                                        )
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: res.success
                                })

                                setTimeout(function() {
                                    window.location.href =
                                        `{{ url('/master_dashboard') }}`;
                                }, 1501);


                            }
                        }

                    })
                }

                
            });
            $(".restoreSubuser").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },

                click: function() {
                    var subusert_id = $(this).attr("data-id");
                    // alert(patient_id);
                    $.ajax({
                        url: `{{ url('subuser_restore/${subusert_id}') }}`,
                        method: 'get',

                        success: function(res) {

                            if (res.hasOwnProperty('success')) {

                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 1500,
                                    timerProgressBar: false,
                                    didOpen: (toast) => {
                                        toast.addEventListener(
                                            'mouseenter',
                                            Swal.stopTimer)
                                        toast.addEventListener(
                                            'mouseleave',
                                            Swal.resumeTimer
                                        )
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: res.success
                                })

                                setTimeout(function() {
                                    window.location.href =
                                        `{{ url('/sub_users') }}`;
                                }, 1501);


                            }
                        }

                    })
                }

                
            });
            
            });
        </script>
    <script>
        $(document).ready(function() {
            $(".deleteSubuser").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            });
            $('.deleteSubuser').click(function() {
                var id = $(this).attr("data-id");
                //Delete alert
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger m-3'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this Subuser!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then((result) => {
                    if (result.isConfirmed) {

                        $.ajax({
                            type: "GET",
                            url: "{{ route('/delete_subuser') }}",
                            data: {
                                'id': id
                            },
                            dataType: "json",
                            success: function(response) {
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    response.success,
                                    'success'
                                )
                            }
                        });
                        setTimeout(function() {
                            window.location.reload(false);
                        }, 1500);



                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                })


            });
        });
    </script>

    </body>
@endsection

</html>
