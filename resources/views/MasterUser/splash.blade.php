<!DOCTYPE html>
<html lang="en">

@include('MasterUser/Components/head')

<body>
    <section class="splashContainer">
        <div class="commonLogoSection splashLogo">
            <img src="{{asset('MasterUser/assets/images/logo.png')}}" alt="">
            <div>
                <h1>MyChartSpace</h1>
                <p>Save time, use your phone.</p>
            </div>
        </div>
        <div class="appVersion">App version : AP1.123</div>
    </section>

</body>

</html>
