<!DOCTYPE html>
<html lang="en">

@include('MasterUser/Components/head')
{{-- {{$getNotifications}} --}}
{{-- @foreach ($getNotifications as $gn)
    {{ $gn->getNotiDetails }}
@endforeach --}}

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('MasterUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt reminderTop mb-3">
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long"
                            style="color: #ffffff;"></i></span>
                    <h4>Notification</h4>
                    <span></span>
                </div>
                @if (count($getNotifications) > 0)
                    
                @foreach ($getNotifications as $gn)
                <div class="medicationDetlbg" data-id="{{ $gn->id }}">
                    <div class="mfdTop">
                        <span class="caltime"><img src="{{ asset('public/MasterUser/assets/images/reminder.png')}}" alt=""> {{date('m-d-Y Hi',$gn->getNotiDetails->cretaed_at)}}hrs</span>
                        {{-- <span class="notiOnline">&nbsp;</span> --}}
                    </div>
                    <p>{{ $gn->getNotiDetails->notification }}</p>
                </div>
                @endforeach
                @else
                <div class="medicationDetlbg">
                    <div class="mfdTop">
                    </div>
                    <p>No Notification available...</p>
                </div>
                @endif

                {{-- <div class="medicationDetlbg passThrough" onclick="window.location.href = 'client-medication.html';">
                    <div class="mfdTop">
                        <span class="caltime"><img src="images/reminder.png" alt=""> 2 hour ago</span>
                        <span class="notiOffline">&nbsp;</span>
                    </div>
                    <p>There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary, making this the first true generator on the Internet. It uses a dictionary of over 200 Latin words, combined with a handful of model sentence structures, to generate Lorem Ipsum.</p>
                </div> --}}
                <!--                <button class="commonButton">Add Client</button>-->
            </div>
        </section>
    </div>



    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    {{-- <script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }

    </script> --}}
    @include('MasterUser/Components/footer')

    <script>
        $(".reminderBlock").click(function() {
            $(this).hide('slow');
        });
    </script>
    <script>
        //Medicine Details
        $(".medicationDetlbg").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },

            click: function() {
                var noti_id = $(this).attr("data-id");
                $(this).addClass("passThrough");
                // alert(noti_id);
                // window.location.href = `{{ url('/notification_seen/${noti_id}') }}`;
                $.ajax({
                    url: `{{ url('/notification_seen/${noti_id}') }}`,
                    method: 'get',

                    // success: function(res) {
                });
            }
        });
    </script>

</body>

</html>
