<!DOCTYPE html>
<html lang="en">

@extends('layouts.auth.app')

@section('title', 'Update')

@section('content')

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('layouts.MasterUser.sidebar')
            <div class="dbRight">
                <form action="{{ route('/edit_sub_user_post') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="emUploadImg">
                        <div class="imageUpload">

                            <img src="{{ asset($data->profile_image_name) }}" id="blah" alt="Img">
                            <div class="uploadInput"><span><i class="fa-regular fa-plus"></i></span><input type="file" id="inputFile" name="profile_image" accept="image/*" onchange="readUrl(this)"></div>

                        </div>
                    </div>
                    <input type="text" name="id" hidden value="{{ $data->id }}">
                    <div class="row g-3 mb-4 editMasterForm">
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{ $data->first_name }}">

                            @if (Session::has('error'))
                            @if ( array_key_exists('first_name',(json_decode(json_encode(Session::get('error')),true))))
                            <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['first_name'][0] }}</p>
                            @endif
                            @endif

                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{ $data->last_name }}">
                            @if (Session::has('error'))

                            @if ( array_key_exists('last_name',(json_decode(json_encode(Session::get('error')),true))))
                            <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['last_name'][0] }}</p>
                            @endif
                            @endif
                        </div>
                        <!-- <div class="col-md-12">
                            <input type="text" class="form-control" name="company_name" placeholder="Company Name"
                                value="{{ $data->company_name }}">
                                @if (Session::has('error'))

                                @if ( array_key_exists('company_name',(json_decode(json_encode(Session::get('error')),true))))
                                    <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['company_name'][0] }}</p>
                                @endif
                            @endif
                        </div> -->
                        <!-- <div class="col-md-6">
                            <input type="text" class="form-control" name="phone_number" placeholder="Phone Number"
                                value="{{ $data->phone_number }}">
                                @if (Session::has('error'))

                                @if ( array_key_exists('phone_number',(json_decode(json_encode(Session::get('error')),true))))
                                    <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['phone_number'][0] }}</p>
                                @endif
                            @endif
                        </div> -->
                        <div class="col-md-12">
                            <input type="email" readonly name="email" class="form-control" placeholder="Email Address" value="{{ $data->email }}">
                        </div>
                        <!-- <div class="col-md-12">
                            <textarea class="form-control textareaheight" name = "address" placeholder="Address">{{ $data->permanent_address }}</textarea>
                                @if (Session::has('error'))

                                @if ( array_key_exists('address',(json_decode(json_encode(Session::get('error')),true))))
                                    <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['address'][0] }}</p>
                                @endif
                            @endif
                        </div> -->
                        <!-- <div class="col-md-12">
                            <textarea type="text" class="form-control textareaheight" name = "mailing_address" placeholder="Mailing Address"
                                >{{ $data->mailing_address }}</textarea>
                                @if (Session::has('error'))

                                @if ( array_key_exists('mailing_address',(json_decode(json_encode(Session::get('error')),true))))
                                    <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['mailing_address'][0] }}</p>
                                @endif
                            @endif
                        </div> -->
                        <!-- <div class="col-md-12">
                            <input type="text" class="form-control" placeholder="City" name="city" value="{{ $data->city }}">
                            @if (Session::has('error'))

                            @if ( array_key_exists('city',(json_decode(json_encode(Session::get('error')),true))))
                            <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['city'][0] }}</p>
                            @endif
                            @endif
                        </div> -->
                        <!-- <div class="col-md-12">
                            <input type="text" class="form-control" name="state" placeholder="State" value="{{ $data->state }}">
                            @if (Session::has('error'))

                            @if ( array_key_exists('state',(json_decode(json_encode(Session::get('error')),true))))
                            <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['state'][0] }}</p>
                            @endif
                            @endif
                        </div> -->
                        <!-- <div class="col-md-12">
                            <input type="text" class="form-control" name="country" placeholder="Country" value="{{ $data->country }}">
                            @if (Session::has('error'))

                            @if ( array_key_exists('country',(json_decode(json_encode(Session::get('error')),true))))
                            <p class="text-danger">{{ json_decode(json_encode(Session::get('error')),true)['country'][0] }}</p>
                            @endif
                            @endif
                        </div> -->

                        <div class="col-md-12">
                        <h4 class="mb-2">Documents</h4>
                        <div class="row" style="text-align: center;">
                            <div class="col-3">
                                <h6>Operators License</h6>
                                @if ($data->operators_license)
                                <a href="{{asset('images/SubUserProfile/' . $data->operators_license)}}"><img src="{{ asset('images/SubUserProfile/' . $data->operators_license) }}" alt="" srcset="" class="docsimg"></a>

                                @endif
                                <input type="file" class="form-control" name="operatorLicense">
                                @if (Session::has('error'))
                                @if (array_key_exists('city', Session::get('error')))
                                <p class="text-danger">{{ Session::get('error')['operatorLicense'][0] }}</p>
                                @endif
                                @endif
                            </div>
                            <div class="col-3">
                                <h6>TB Clearance</h6>
                                @if ($data->tb_clearance)
                                <a href="{{asset('images/SubUserProfile/' . $data->tb_clearance)}}"><img src="{{ asset('images/SubUserProfile/' . $data->tb_clearance) }}" alt="" srcset="" class="docsimg"></a>
                                @endif
                                <input type="file" class="form-control" name="tbClearance">
                                @if (Session::has('error'))
                                @if (array_key_exists('city', Session::get('error')))
                                <p class="text-danger">{{ Session::get('error')['tbClearance'][0] }}</p>
                                @endif
                                @endif
                            </div>
                            <div class="col-3">
                                <h6>Physical Checkup</h6>
                                @if ($data->physical_checkup)
                                <a href="{{asset('images/SubUserProfile/' . $data->physical_checkup)}}"><img src="{{ asset('images/SubUserProfile/' . $data->physical_checkup) }}" alt="" srcset="" class="docsimg"></a>
                                @endif
                                <input type="file" class="form-control" name="physicalCheckup">
                                @if (Session::has('error'))
                                @if (array_key_exists('city', Session::get('error')))
                                <p class="text-danger">{{ Session::get('error')['physicalCheckup'][0] }}</p>
                                @endif
                                @endif
                            </div>
                            <div class="col-3">
                                <h6>Miscellaneous Doc</h6>
                                @if ($data->misc_doc)
                                <a href="{{asset('images/SubUserProfile/' . $data->misc_doc)}}"><img src="{{ asset('images/SubUserProfile/' . $data->misc_doc) }}" alt="" srcset="" class="docsimg"></a>
                                @endif
                                <input type="file" class="form-control" name="misc_doc">
                                @if (Session::has('error'))
                                @if (array_key_exists('city', Session::get('error')))
                                <p class="text-danger">{{ Session::get('error')['physicalCheckup'][0] }}</p>
                                @endif
                                @endif
                            </div>
                        </div>
                        <!-- <input type="file" class="form-control" name="city">
                        @if (Session::has('error'))
                        @if (array_key_exists('city', Session::get('error')))
                        <p class="text-danger">{{ Session::get('error')['city'][0] }}</p>
                        @endif
                        @endif -->
                    </div>
                    </div>

                    <button type="submit" class="commonButton">Update</button>

                </form>


            </div>
        </section>
    </div>



    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    <script src="js/bootstrap.bundle.min.js" type="text/javascript"></script>
    <script src="js/jquery-3.4.1.min.js"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        var a = document.getElementById("blah");

        function readUrl(input) {
            if (input.files) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = (e) => {
                    a.src = e.target.result;
                }
            }
        }

        var inputFile = document.getElementById("inputFile");
        removeImg = () => {
            a.src = "images/subuser.png";
            inputFile.value = "";
        }
    </script>




</body>
@endsection

</html>