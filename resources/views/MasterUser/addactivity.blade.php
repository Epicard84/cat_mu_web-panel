<!DOCTYPE html>
<html lang="en">

@include('MasterUser/Components/head')

{{-- {{$medicine_details}} --}}
<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('MasterUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt dmTop mb-3 stickyName">
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long"></i></span>
                    <div style="color: white">
                        {{ $cname }}
                    </div>
                    <div>
                    </div>
                    {{-- <a href="{{ route('clients_details', Crypt::encryptString($data->id)) }}">
                        <h4>{{ $data->first_name }} {{ $data->last_name }}</h4>
                    </a> --}}
                    {{-- <div><a href="{{ route('/master_details') }}"><span class="dmbg">{{
                                strtoupper(substr(Auth::user()->first_name, 0, 1)) .
                                strtoupper(substr(Auth::user()->last_name, 0, 1)) }}</span></a>
                    </div> --}}

                </div>

                {{-- Existing medicine details --}}
                <input type="text" value="{{ $id }}" id="c_id" hidden name="c_id">
                <input type="text" value="{{ $medicine_count }}" id="medicine_count" hidden name="medicine_count">

                @foreach ($medicine_details as $key => $item)
                    <div>
                        <form method="post" action=" " id="{{ $key }}" enctype="multipart/form-data">
                            @csrf
                            <input type="text" value="{{ $id }}" hidden name="client_id">
                            <input type="text" value="{{ $item->id }}" hidden name="med_id">
                            <div class="moreField med1">
                                <div class="morefieldTop">
                                    <h6>Activity</h6>
                                    {{-- <div data-id="{{ $item->id }}" class="medicine"><i class="fa-regular fa-trash"></i></div> --}}
                                    <div>
                                        @if ($item->status == 0)
                                            <span data-id="{{ $item->id }}" class="archive_medicine"><i
                                                    class="fa-regular fa-folder"></i></span>
                                        @else
                                            <span style="font-size: 12pt">Archived</span>
                                        @endif
                                        &nbsp;&nbsp;

                                        <span data-id="{{ $item->id }}" class="delete_medicine"><i
                                                class="fa-regular fa-trash"></i></span>

                                    </div>
                                </div>
                                <div class="form-group mb-3">
                                    <input type="text" class="form-control name" name="name"
                                        value="{{ $item->activity_name }}" readonly placeholder="Medicine">

                                </div>

                                <div class="row">

                                    <div class="col-9">
                                        <h6>Start Date</h6>
                                        <div class="row">
                                            <div class="col-md-6 mb-3">
                                                <input type="date" name="start_date" readonly class="form-control"
                                                    value="{{ $item->start_date }}">
                                            </div>

                                        </div>
                                    </div>

                                    {{-- <div class="col-3">
                                    <h6>Archive Medication</h6>

                                    @if ($item->status == '0')
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" value="0" checked type="checkbox" name="status"
                                            id="checkbox{{ $key }}">
                                    </div>
                                    @else
                                    <div class="form-check form-switch">
                                        <input class="form-check-input" type="checkbox" name="status" value="0"
                                            id="checkbox{{ $key }}">
                                    </div>
                                    @endif
                                </div> --}}
                                </div>

                                <div class="form-group mb-3">
                                    <select class="form-control" name="time">
                                        @switch($item->time_days)
                                            @case(1)
                                                <option value="1" selected hidden>1 times</option>
                                            @break

                                            @case(2)
                                                <option value="2" selected hidden>2 times</option>
                                            @break

                                            @case(3)
                                                <option value="3" selected hidden>3 times</option>
                                            @break

                                            @case(4)
                                                <option value="4" selected hidden>4 times</option>
                                            @break

                                            @case(5)
                                                <option value="5" selected hidden>5 times</option>
                                            @break

                                            @case(6)
                                                <option value="6" selected hidden>6 times</option>
                                            @break

                                            @case(7)
                                                <option value="7" selected hidden>7 times</option>
                                            @break

                                            @case(8)
                                                <option value="8" selected hidden>8 times</option>
                                            @break
                                        @endswitch

                                        <option value="1" data-id="{{ $key }}">1 times</option>
                                        <option value="2" data-id="{{ $key }}">2 times</option>
                                        <option value="3" data-id="{{ $key }}">3 times</option>
                                        <option value="4" data-id="{{ $key }}">4 times</option>
                                        <option value="5" data-id="{{ $key }}">5 times</option>
                                        <option value="6" data-id="{{ $key }}">6 times</option>
                                        <option value="7" data-id="{{ $key }}">7 times</option>
                                        <option value="8" data-id="{{ $key }}">8 times</option>
                                    </select>
                                    <p class="text-danger">*Time is based on HST Timezone</p>
                                </div>
                                <div id="timing{{ $key }}">
                                    @foreach (explode(',', $item->scheduled_time) as $schedule)
                                        {{-- <input type="time" class="form-control mb-2" name="scheduleTime[]"
                                    value="{{ $schedule }}"> --}}
                                        <select class="form-control mb-2" name="scheduleTime[]">
                                            <option value="01:00" {{ $schedule == '01:00' ? 'selected' : '' }}>01:00
                                            </option>
                                            <option value="02:00" {{ $schedule == '02:00' ? 'selected' : '' }}>02:00
                                            </option>
                                            <option value="03:00" {{ $schedule == '03:00' ? 'selected' : '' }}>03:00
                                            </option>
                                            <option value="04:00" {{ $schedule == '04:00' ? 'selected' : '' }}>04:00
                                            </option>
                                            <option value="05:00" {{ $schedule == '05:00' ? 'selected' : '' }}>05:00
                                            </option>
                                            <option value="06:00" {{ $schedule == '06:00' ? 'selected' : '' }}>06:00
                                            </option>
                                            <option value="07:00" {{ $schedule == '07:00' ? 'selected' : '' }}>07:00
                                            </option>
                                            <option value="08:00" {{ $schedule == '08:00' ? 'selected' : '' }}>08:00
                                            </option>
                                            <option value="09:00" {{ $schedule == '09:00' ? 'selected' : '' }}>09:00
                                            </option>
                                            <option value="10:00" {{ $schedule == '10:00' ? 'selected' : '' }}>10:00
                                            </option>
                                            <option value="11:00" {{ $schedule == '11:00' ? 'selected' : '' }}>11:00
                                            </option>
                                            <option value="12:00" {{ $schedule == '12:00' ? 'selected' : '' }}>12:00
                                            </option>
                                            <option value="13:00" {{ $schedule == '13:00' ? 'selected' : '' }}>13:00
                                            </option>
                                            <option value="14:00" {{ $schedule == '14:00' ? 'selected' : '' }}>14:00
                                            </option>
                                            <option value="15:00" {{ $schedule == '15:00' ? 'selected' : '' }}>15:00
                                            </option>
                                            <option value="16:00" {{ $schedule == '16:00' ? 'selected' : '' }}>16:00
                                            </option>
                                            <option value="17:00" {{ $schedule == '17:00' ? 'selected' : '' }}>17:00
                                            </option>
                                            <option value="18:00" {{ $schedule == '18:00' ? 'selected' : '' }}>18:00
                                            </option>
                                            <option value="19:00" {{ $schedule == '19:00' ? 'selected' : '' }}>19:00
                                            </option>
                                            <option value="20:00" {{ $schedule == '20:00' ? 'selected' : '' }}>20:00
                                            </option>
                                            <option value="21:00" {{ $schedule == '21:00' ? 'selected' : '' }}>21:00
                                            </option>
                                            <option value="22:00" {{ $schedule == '22:00' ? 'selected' : '' }}>22:00
                                            </option>
                                            <option value="23:00" {{ $schedule == '23:00' ? 'selected' : '' }}>23:00
                                            </option>
                                            <option value="24:00" {{ $schedule == '24:00' ? 'selected' : '' }}>24:00
                                            </option>
                                        </select>
                                    @endforeach
                                </div>
                                <p id="schedule_error{{ $key }}" hidden class="text-danger schedule_error">
                                </p>
                                <div class="form-group mb-3">
                                    <textarea class="form-control description" name="description" placeholder="Description">{{ $item->descriptions }}</textarea>
                                    <p id="des_error{{ $key }}" class="text-danger des_error"
                                        name="des_error">
                                    </p>
                                </div>
                                @if ($item->docs)
                                    <div class="doCardbg mb-3 docardmed">
                                        @foreach (explode(',', $item->docs) as $file)
                                            <div class="doCard meddoc">
                                                {{-- <span class="cls"><i class="fa-regular fa-xmark"></i></span> --}}
                                                <div class="pdfbg"><img
                                                        src="{{ asset('public/MasterUser/assets/images/pdf.png') }}"
                                                        alt=""><span>Document</span></div>
                                                <a href="{{ url('/medicine_download/' . $file) }}"><label
                                                        class="commonButton savebtn viewbtn"><i
                                                            class="fa-light fa-download"></i></label></a>
                                            </div>
                                            &nbsp;
                                        @endforeach
                                    </div>
                                @else
                                    <p class="text-info">No documents available</p>
                                @endif
                                @if ($item->status == 0)
                                    <button class="commonButton savebtn">Update</button>
                                @endif
                            </div>
                        </form>
                    </div>
                @endforeach
                <div class="add_medicine" name="add_medicine" id="add_medicine">

                </div>
                <div class="medicineAdd" id="medicineAddbtn"><button class="add_button" id="add_button"><i
                            class="fa-regular fa-plus"></i>
                        Add
                        New Activity</button></div>

            </div>
        </section>
    </div>



    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    <script src="{{ asset('MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>

    <script>
        $(document).ready(function() {


            $(document).on('change', 'select', function() {


                var count_id = $(this).find(":selected").attr('data-id'); //Get div id using data-id
                div_id = "#timing" + count_id; //create div id for set html value
                var error_id = "#schedule_error" + count_id;
                var id = $(this).find(":selected").val();
                $(div_id).html(''); // Div empty

                $(error_id).removeAttr('hidden');
                $(error_id).text('');

                console.log(div_id);

                for (let index = 0; index < id; index++) {

                    $(div_id).append(`<select class="form-control mb-2" name="scheduleTime[]">
    <option value="01:00">01:00</option>
    <option value="02:00">02:00</option>
    <option value="03:00">03:00</option>
    <option value="04:00">04:00</option>
    <option value="05:00">05:00</option>
    <option value="06:00">06:00</option>
    <option value="07:00">07:00</option>
    <option value="08:00">08:00</option>
    <option value="09:00">09:00</option>
    <option value="10:00">10:00</option>
    <option value="11:00">11:00</option>
    <option value="12:00">12:00</option>
    <option value="13:00">13:00</option>
    <option value="14:00">14:00</option>
    <option value="15:00">15:00</option>
    <option value="16:00">16:00</option>
    <option value="17:00">17:00</option>
    <option value="18:00">18:00</option>
    <option value="19:00">19:00</option>
    <option value="20:00">20:00</option>
    <option value="21:00">21:00</option>
    <option value="22:00">22:00</option>
    <option value="23:00">23:00</option>
    <option value="24:00">24:00</option>
  </select>`);
                }

            });


            $.ajaxSetup({

                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            });

            // Current date function
            function today() {
                var today = new Date();
                var dd = today.getDate();

                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                if (dd < 10) {
                    dd = '0' + dd;
                }

                if (mm < 10) {
                    mm = '0' + mm;
                }
                today = dd + '-' + mm + '-' + yyyy;
                return today;
            }






            $(document).on("submit", "form", function(e) {

                e.preventDefault();

                var formData = new FormData(this);
                var from_id = this.id;
                $('.name_error').html("");
                $('.end_date_error').html("");
                $('.des_error').html("");
                $('.time_error').html("");
                $('.file_error').html("");
                $('.schedule_error').html("");
                $('#imgerror').html("");



                $.ajax({
                    type: "POST",
                    url: "{{ route('add_activity_post') }}",
                    processData: false,
                    contentType: false,
                    data: formData,
                    dataType: "json",
                    success: function(response) {
                        console.log(response);

                        let success = response.hasOwnProperty('success');
                        let error = response.hasOwnProperty('validation_error');
                        let schedule_error = response.hasOwnProperty('schedule_error');

                        if (success) {
                            console.log(response.success);

                            // Medicine Added Toster

                            var toastMixin = Swal.mixin({
                                toast: true,
                                icon: 'success',
                                title: 'General Title',
                                animation: false,
                                position: 'top-right',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal
                                        .stopTimer)
                                    toast.addEventListener('mouseleave', Swal
                                        .resumeTimer)
                                }
                            });


                            Swal.fire({
                                toast: true,
                                icon: 'success',
                                title: 'Posted successfully',
                                animation: false,
                                position: 'bottom',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    found
                                    toast.addEventListener('mouseenter', Swal
                                        .stopTimer)
                                    toast.addEventListener('mouseleave', Swal
                                        .resumeTimer)
                                }
                            })

                            toastMixin.fire({
                                animation: true,
                                title: response.success
                            });

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2500);


                        }
                        if (schedule_error) {
                            let schedule_error_id = "#schedule_error" + from_id;
                            console.log(schedule_error_id);
                            $(schedule_error_id).text(response.schedule_error);
                        }
                        if (imgerror) {
                            $('#imgerror').html("*" + response.imgerror);

                        }

                        if (error) {

                            //Sever Side Validation Error 

                            let name = response.validation_error.hasOwnProperty('name');
                            let start_date = response.validation_error.hasOwnProperty(
                                'start_date');
                            let file = response.validation_error.hasOwnProperty('file');
                            let time = response.validation_error.hasOwnProperty('time');
                            let end_date = response.validation_error.hasOwnProperty('end_date');
                            let description = response.validation_error.hasOwnProperty(
                                'description');
                            let scheduletime = response.validation_error.hasOwnProperty(
                                'scheduleTime');


                            if (name) {
                                $('#name_error' + from_id).append("*" + response
                                    .validation_error.name[0]);
                            }

                            if (start_date) {

                                $('#end_date_error' + from_id).html("*" + response
                                    .validation_error.start_date[0]);
                            }
                            if (file) {

                                $('#file_error' + from_id).html("*" + response.validation_error
                                    .file[0]);
                            }
                            if (time) {

                                $('#time_error' + from_id).html("*" + response.validation_error
                                    .time[0]);
                            }
                            if (description) {

                                $('#des_error' + from_id).html("*" + response.validation_error
                                    .description[0]);
                            }

                            if (scheduletime) {

                                $('#schedule_error' + from_id).html("*" + response
                                    .validation_error.scheduleTime[0]);
                            }
                        }
                    }
                });

            });


            //add new medicine page

            var count = $("#medicine_count").val();
            // console.log(count);

            $(".add_button").click(function(event) {

                $("#medicineAddbtn").attr("hidden", true);
                var client_id = $('#c_id').val();

                // console.log("hello");
                $("#name").html("");
                $("#file").html("");
                $("#date").html("");
                $("#time").html("");
                $("#file").html("");
                $("#description").html("");

                count++;

                $('.add_medicine').append(`
<div id="m${count}" classs="m${count}">
<form method="POST" action=" " id='${count}' enctype="multipart/form-data" autocomplete="off">
@csrf
<input type="text" value="${client_id}" id="client_id" hidden name="client_id">
<div class="moreField med${count}">
<div class="morefieldTop">
        <h6>Activity</h6>
        <div class="extra_medicine" id ="extra_medicine" data-id="${count}"><i class="fa-regular fa-trash-can"></i></div>
    </div>
    <div class="form-group mb-3">
        <input type="text" class="form-control" name = "name" placeholder="Activity Name">
        <p id="name_error${count}" class="text-danger name_error"></p>
    </div>
    <h6>Start Date</h6>
    <div class="col-md-6 pickDate">

    <input type="text" id="datepicker" readonly name="start_date" class="form-control"
        placeholder="Select Start date" autocomplete="off" />
        <p id="end_date_error${count}" class="text-danger end_date_error"></p>

    </div>
    <div class="form-group mb-3">
        <select class="form-control" name="time">
                        <option selected hidden value="">Select times</option>
                        <option value="1" data-id="${count}">1 times</option>
                        <option value="2" data-id="${count}">2 times</option>
                        <option value="3" data-id="${count}">3 times</option>
                        <option value="4" data-id="${count}">4 times</option>
                        <option value="5" data-id="${count}">5 times</option>
                        <option value="6" data-id="${count}">6 times</option>
                        <option value="7" data-id="${count}">7 times</option>
                        <option value="8" data-id="${count}">8 times</option>
                    </select>
                    <p id="time_error${count}" class="text-danger time_error"></p>
    </div>
    <p class="text-danger">*Time is based on HST Timezone</p>
    <div id="timing${count}">

        </div>
        <p id="schedule_error${count}" hidden class="text-danger schedule_error"></p>
    <div class="form-group mb-3">
        <textarea class="form-control description"  name = "description" placeholder="Description"></textarea>
        <p id="des_error${count}" class="text-danger des_error"></p>
    </div>
    <div class="col-md-12">
                    <div class="file addClntfile">
                      <div class="file__input upfileInput">
                        <input class="file__input--file" id="customFile" type="file" multiple="multiple" name="file[]" accept=".doc,.docx,.pdf,.png,.jpeg,.jpg"/>
                        <label class="file__input--label" for="customFile" data-text-btn="Upload">Upload</label>
                        <label class="acl">Documents</label>
                      </div>
                      <p id="imgerror" class="text-danger"></p>
                        <div id="file__input"></div>
                        <div id="nfile"></div>
                    </div>
        </div>
        <p id="file_error${count}" class="text-danger file_error"></p>
    </div>
    <button class="commonButton" type="submit">Save</button>
</div>
</form>
</div>`);


                // var form_id = "#" + count;

                // $(form_id).validate({ // initialize the plugin
                //     rules: {
                //         "scheduleTime[]": 'required',
                //         "name":'required',
                //         'start_date':'required',
                //         'time':'required',
                //         'description':'required',
                //         'file[]':'required',
                //     }
                // });

                new AirDatepicker('#datepicker', {
                    autoClose: false,
                    dateFormat: 'MM/dd/yyyy',
                    minDate: new Date(),
                })

                // Multi DatePicker
                new AirDatepicker('#datepicker-multi', {
                    range: true,
                    multipleDates: true,
                    multipleDatesSeparator: " - "
                })




                $('.file__input--file').on('change', function(event) {
                    var files = event.target.files;
                    for (var i = 0; i < files.length; i++) {
                        var file = files[i];
                        $("<div class='file__value'><div class='file__value--text'>" + file.name +
                            "</div><div class='file__value--remove' data-id='" + file.name +
                            "' ></div></div>").appendTo('#nfile');
                    }
                });

                //Click to remove item
                $('body').on('click', '.file__value', function() {
                    $(this).remove();
                });
            });




            //Delete Medicine Function 
            $('.delete_medicine').click(function(e) {

                var id = $(this).attr("data-id");

                //Delete alert
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger m-3'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, delete it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then((result) => {
                    if (result.isConfirmed) {

                        $.ajax({
                            type: "POST",
                            url: "{{ route('delete_activity') }}",
                            data: {
                                'id': id
                            },
                            dataType: "json",
                            success: function(response) {
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    response.success,
                                    'success'
                                )
                            }
                        });
                        setTimeout(function() {
                            window.location.reload(false);
                        }, 1000);



                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                })


            });

            //Delete new add medicnine form
            $(document).on('click', '.extra_medicine', function(e) {
                var id = $(this).attr('data-id');
                $('#m' + id).empty();
            });

            //Archive Medicine
            $('.archive_medicine').click(function(e) {

                var id = $(this).attr("data-id");

                //Delete alert
                const swalWithBootstrapButtons = Swal.mixin({
                    customClass: {
                        confirmButton: 'btn btn-success',
                        cancelButton: 'btn btn-danger m-3'
                    },
                    buttonsStyling: false
                })

                swalWithBootstrapButtons.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    icon: 'warning',
                    showCancelButton: true,
                    confirmButtonText: 'Yes, archive it!',
                    cancelButtonText: 'No, cancel!',
                    reverseButtons: true,
                }).then((result) => {
                    if (result.isConfirmed) {

                        $.ajax({
                            type: "POST",
                            url: "{{ route('archive_activity') }}",
                            data: {
                                'id': id
                            },
                            dataType: "json",
                            success: function(response) {
                                swalWithBootstrapButtons.fire(
                                    'Deleted!',
                                    response.success,
                                    'success'
                                )
                            }
                        });
                        setTimeout(function() {
                            window.location.reload(false);
                        }, 1000);



                    } else if (
                        /* Read more about handling dismissals below */
                        result.dismiss === Swal.DismissReason.cancel
                    ) {
                        swalWithBootstrapButtons.fire(
                            'Cancelled',
                            'Your imaginary file is safe :)',
                            'error'
                        )
                    }
                })


            });

        });
    </script>
    <script src="http://code.jquery.com/ui/1.9.2/jquery-ui.js"></script>
    <link href="http://code.jquery.com/ui/1.9.2/themes/smoothness/jquery-ui.css" rel="stylesheet" />
    <script>
        $(".happyEvnt i").on({
            mouseenter: function() {
                $(this).css("cursor", "pointer");
            },

            mouseleave: function() {
                $(this).css("cursor", "auto");
            },
        });
    </script>
</body>

</html>
