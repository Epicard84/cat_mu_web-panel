<!DOCTYPE html>
<html lang="en">
@include('MasterUser.Components.head')

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('layouts.MasterUser.sidebar')

            <div class="dbRight">
                <div class="happyEvnt mb-4">
                    <span onclick="javascript:history.go(-1)"><i class="fa-light fa-arrow-left-long" style="color: #ffffff;"></i></span>
                    <h5 style="color: white">{{ $data->first_name }} {{ $data->last_name }} - Profile Update</h5>
                    <div></div>
                </div>
                <form action="{{ route('update_client/post') }}" method="post" enctype="multipart/form-data"
                    id="create_form">
                    @csrf
                    <input type="text" value="{{ $data->id }}" name="client_id" hidden>
                    <div class="row g-3 mb-4 editMasterForm">
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="first_name" required id="first_name"
                                placeholder="First Name" value="{{ $data->first_name }}">
                            <p class="first_name_error text-danger"></p>
                        </div>
                        <div class="col-md-6">
                            <input type="text" class="form-control" name="last_name" required id="last_name"
                                placeholder="Last Name" value="{{ $data->last_name }}">
                            <p class="last_name_error text-danger"></p>
                        </div>
                        {{-- <div class="row"> --}}
                        <div class="col-md-6 pickDate">

                            <input type="text" readonly id="datepicker" name="dob" required id="dob"
                                class="form-control" placeholder="Select Date of Birth" autocomplete="off"
                                value="{{ date('m/d/Y', strtotime($data->dob)) }}" />
                            <p class="dob_error text-danger"></p>

                        </div>
                        <div class="col-md-6">

                            <select class="form-control" id="gender" required name="gender">
                                {{-- <option value="" hidden >Select Gender</option> --}}
                                @if ($data->gender == 'male')
                                    <option value="male"selected>Male</option>
                                    <option value="female">Female</option>
                                @else
                                    <option value="male">Male</option>
                                    <option value="female" selected>Female</option>
                                @endif
                            </select>


                            <p class="gender_error text-danger"></p>

                        </div>
                        {{-- </div> --}}


                        <div class="guardian">
                            <input type="text" id="total_guardian" value="{{ $total_guardian }}" hidden>
                            @foreach ($guardian as $key => $guardian)
                                <div id="guardian{{ $key }}">
                                    <div class="moreField">
                                        <div class="morefieldTop">
                                            <h6>Guardian</h6>
                                            @if ($key == 0)
                                            @else
                                                <div class="extra_guardian" required id="extra_guardian"
                                                    data-id="{{ $key }}">
                                                    <i class="fa-regular fa-trash-can"></i>
                                                </div>
                                            @endif

                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <input type="text" class="form-control"
                                                value="{{ $guardian->guardian_name }}" required name="guardian_name[]"
                                                id="guardian_name{{ $key }}" placeholder="Guardian’s name">
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <input type="number" class="form-control" required maxlength="12"
                                                name="guardian_phone_number[]"
                                                value="{{ $guardian->guardian_phone_number }}"
                                                placeholder="Guardian’s Phone number"
                                                id="guardian_phone_number{{ $key }}">
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <textarea class="form-control textareaheight" required name="guardian_address[]" placeholder="Guardian’s address"
                                                id="guardian_address{{ $key }}">{{ $guardian->guardian_address }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="newguardian">

                        </div>
                        <div class="addguardianform">
                            <label class="commonButton add_guardian"><i class="fa fa-plus"></i>Add guardian</label>
                        </div>

                        {{-- Doctor add --}}
                        <div class="doctor">
                            <input type="text" id="total_doctor" hidden value="{{ $total_doctor }}">
                            @foreach ($doctor as $doctor_key => $doctor)
                                <div id="doctor{{ $doctor_key }}">
                                    <div class="moreField">
                                        <div class="morefieldTop">
                                            <h6>Doctor</h6>

                                            @if ($doctor_key == 0)
                                            @else
                                                <div class="extra_doctor" id="extra_doctor"
                                                    data-id="{{ $doctor_key }}"><i
                                                        class="fa-regular fa-trash-can"></i></div>
                                            @endif
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <input type="text" class="form-control" required name="doctor_name[]"
                                                value="{{ $doctor->doctor_name }}"
                                                id="doctor_name{{ $doctor_key }}" placeholder="Doctor’s name">
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <input type="number" class="form-control" required maxlength="12"
                                                name="doctor_phone_number[]"
                                                value="{{ $doctor->doctor_phone_number }}"
                                                id="doctor_phone_numbe{{ $doctor_key }}"
                                                placeholder="Doctor’s Phone number">
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <textarea class="form-control textareaheight" required name="doctor_address[]"
                                                id="doctor_address{{ $doctor_key }}" placeholder="Doctor’s address">{{ $doctor->doctor_address }}</textarea>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>

                        <div class="newdoctor">

                        </div>
                        <div class="adddoctorform">
                            <label class="commonButton add_doctor"><i class="fa fa-plus"></i>Add doctor</label>
                        </div>
                        <div class="col-md-12">
                            <input type="text" class="form-control" name="additional_information"
                                placeholder="Diagnosis"
                                value="{{ $data->additional_misc_information }}">
                            <p class="additional_information_error text-danger"></p>
                        </div>
                        <div class="col-md-12">
                            {{-- <div class="fileUploadInput">
                        <span>
                            <input type='file' id="aa" name="file[]" onchange="pressed()" multiple>
                            <label id="fileLabel">Documents</label>
                        </span>
                        <button>Upload</button>
                    </div> --}}
                    <p>Documents:</p>
                    @if ($data->documents != null)
                        @php
                            $files = explode(',', $data->documents);
                        @endphp
                        <div class="mb-3">
                            @foreach ($files as $f)
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" value="{{ $f }}"
                                        id="flexCheckChecked" name="docs[]" checked>
                                    <label class="form-check-label" for="flexCheckChecked">
                                        {{ $f }}
                                    </label>
                                </div>
                            @endforeach
                        </div>
                    @else
                        <p style="font-size: 10pt">No documents found</p>
                    @endif
                            <div class="col-md-12">
                                <div class="file addClntfile ">
                                    <div class="file__input upfileInput ">
                                        <input class="file__input--file" id="customFile" type="file"
                                            multiple="multiple" name="file[]" placeholder="Upload" accept=".doc,.docx,.pdf,.png,.jpeg,.jpg"  />
                                        <label class="file__input--label" for="customFile"
                                            data-text-btn="Upload">Upload</label>
                                        <label class="acl">Documents</label>
                                    </div>
                                    <div id="file__input"></div>
                                    <div id="nfile"></div>
                                </div>
                            </div>

                        </div>

                    </div>
                    <button type="submit" class="commonButton">Update</button>

                </form>


            </div>
        </section>
    </div>

    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        $(document).ready(function() {


            $('.file__input--file').on('change', function(event) {
                var files = event.target.files;
                for (var i = 0; i < files.length; i++) {
                    var file = files[i];
                    $("<div class='file__value'><div class='file__value--text'>" + file.name +
                        "</div><div class='file__value--remove' data-id='" + file.name +
                        "' ></div></div>").appendTo('#nfile');
                }
            });

            //Click to remove item
            $('body').on('click', '.file__value', function() {
                $(this).remove();
            });



        });
    </script>

    <script>
        var a = document.getElementById("blah");

        function readUrl(input) {
            if (input.files) {
                var reader = new FileReader();
                reader.readAsDataURL(input.files[0]);
                reader.onload = (e) => {
                    a.src = e.target.result;
                }
            }
        }

        var inputFile = document.getElementById("inputFile");
        removeImg = () => {
            a.src = "images/subuser.png";
            inputFile.value = "";
        }
    </script>

    <script>
        new AirDatepicker('#datepicker', {
            autoClose: false,
            dateFormat: 'MM/dd/yyyy',
            maxDate: new Date()
        })

        // Multi DatePicker
        new AirDatepicker('#datepicker-multi', {
            range: true,
            multipleDates: true,
            multipleDatesSeparator: " - "
        })
    </script>
    <script>
        $(document).ready(function() {

            $("#create_form").submit(function(e) {
                e.preventDefault();
            }).validate({
                rules: {
                    first_name: "required",
                    last_name: "required",
                    dob: "required",
                    gender: "required",
                    guardian_name: "required",
                    guardian_phone_number: "required",
                    guardian_address: "required",
                    doctor_name: "required",
                    doctor_phone_number: "required",
                    doctor_address: "required",
                },
                submitHandler: function(form) {
                    form.submit();
                }
            });

            //Add Doctor 

            var doctor = $('#total_doctor').val();;

            $('.add_doctor').click(function() {
                $('.newdoctor').append(`
                <div id="doctor${doctor}">
                        <div class="moreField">
                            <div class="morefieldTop">
                        <h6>Doctor</h6>
                        <div class="extra_doctor" id="extra_doctor" data-id="${doctor}"><i class="fa-regular fa-trash-can"></i></div>
                    </div>
                            <div class="col-md-12 mb-3">
                            <input type="text" class="form-control" required name="doctor_name[]" id="doctor_name${doctor}" placeholder="Doctor’s name">
                        </div>
                        <div class="col-md-12 mb-3">
                            <input type="number" class="form-control" required maxlength="12" name="doctor_phone_number[]" id="doctor_phone_numbe${doctor}" placeholder="Doctor’s Phone number">
                        </div>
                        <div class="col-md-12 mb-3">
                            <textarea class="form-control textareaheight" required name="doctor_address[]" id="doctor_address${doctor}" placeholder="Doctor’s address"></textarea>
                        </div>
                        </div>
                        </div>`);


                var doctor_name = doctor_name + doctor;
                var doctor_phone_number = doctor_phone_number + doctor;
                var doctor_address = doctor_address + doctor;

                doctor = doctor + 1;

            });

            //Delete doctor div
            $(document).on('click', '.extra_doctor', function() {
                var id = $(this).attr('data-id');
                $('#doctor' + id).empty();
            });


            var guardian = $('#total_guardian').val();


            //Add Guardian 
            $('.add_guardian').click(function() {
                $('.newguardian').append(`
                <div id="guardian${guardian}">
                        <div class="moreField">
                            <div class="morefieldTop">
                        <h6>Guardian</h6>
                        <div class="extra_guardian" required id="extra_guardian" data-id="${guardian}"><i class="fa-regular fa-trash-can"></i></div>
                    </div>
                            <div class="col-md-12 mb-3">
                            <input type="text" class="form-control" required name="guardian_name[]" id="guardian_name${guardian}" placeholder="Guardian’s name" id="guardian_name${guardian}">
                        </div>
                        <div class="col-md-12 mb-3">
                            <input type="number" class="form-control" required maxlength="10" name="guardian_phone_number[]" id="guardian_phone_number${guardian}" placeholder="Guardian’s Phone number" id="guardian_phone_number${guardian}">
                        </div>
                        <div class="col-md-12 mb-3">
                            <textarea class="form-control textareaheight" required name="guardian_address[]" placeholder="Guardian’s address" id="guardian_address${guardian}"></textarea>
                        </div>
                        </div>
                        <div>`);


                var guardian_name = guardian_name + guardian;
                var guardian_phone_number = guardian_phone_number + guardian;
                var guardian_address = guardian_address + guardian;

                guardian = guardian + 1

            });


            //Delete doctor div
            $(document).on('click', '.extra_guardian', function() {
                var id = $(this).attr('data-id');
                console.log(id);
                $('#guardian' + id).empty();
            });


            //Register Master user

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });


            // $('#create_form').submit(function(e) {
            //     e.preventDefault();

            //     var form = $('#create_form')[0];
            //     var formData = new FormData(form);

            //     alert("submit");
            //     $.ajax({
            //         url: "{{ route('update_client/post') }}",
            //         type: 'POST',
            //         dataType: 'json',
            //         processData: false,
            //         contentType: false,
            //         data: formData,
            //         success: function(response) {

            //             console.log(response);

            //             if (response.hasOwnProperty('success')) {

            //                 const Toast = Swal.mixin({
            //                     toast: true,
            //                     position: 'top-end',
            //                     showConfirmButton: false,
            //                     timer: 2500,
            //                     timerProgressBar: true,
            //                     didOpen: (toast) => {
            //                         toast.addEventListener(
            //                             'mouseenter',
            //                             Swal.stopTimer)
            //                         toast.addEventListener(
            //                             'mouseleave',
            //                             Swal.resumeTimer
            //                         )
            //                     }
            //                 })

            //                 Toast.fire({
            //                     icon: 'success',
            //                     title: response.success

            //                     setTimeout(function() {
            //                         window.location.reload(false);
            //                     }, )
            //                 })
            //             }
            //         }
            //     });

            // });

        });
    </script>
</body>

</html>
