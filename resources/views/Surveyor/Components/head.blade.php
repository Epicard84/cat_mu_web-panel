<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>CAT</title>
    <link rel="icon" href="{{ asset('public/MasterUser/assets/images/favicon.ico') }}" type="image/x-icon">
    <link href="{{ asset('public/MasterUser/assets/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/MasterUser/assets/css/style.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('public/MasterUser/assets/css/media.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://site-assets.fontawesome.com/releases/v6.2.1/css/all.css">
    <link href="{{ asset('public/MasterUser/assets/css/dbstyle.css') }}" rel="stylesheet" type="text/css" />

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"
        integrity="sha512-c42qTSw/wPZ3/5LBzD+Bw5f7bSF2oxou6wEb+I/lqeaKV5FDIfMvvRp772y4jcJLKuGUOpbJMdg/BTl50fJYAw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <link rel="stylesheet" href="{{ asset('public/MasterUser/assets/css/datepicker.css') }}">
    <!-- <link href="https://code.jquery.com/ui/1.9.2/themes/smoothness/jquery-ui.css" rel="stylesheet" /> -->

    
    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('public/MasterUser/assets/js/datepicker.js') }}"></script>
    <script src="{{ asset('public/MasterUser/assets/js/validator.js') }}"></script>
    <!-- <script src="https://code.jquery.com/ui/1.9.2/jquery-ui.js"></script> -->
</head>


