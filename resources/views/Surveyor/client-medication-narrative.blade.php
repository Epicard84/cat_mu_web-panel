<!DOCTYPE html>
<html lang="en">
<link rel='stylesheet' href="{{ asset('public/MasterUser/assets/css/gallery.min.css') }}">
@include('MasterUser/Components/head')
{{-- {{count($date_narrative)}} --}}
{{-- {{$patient_id}} --}}

<body>
    <div class="container customContainer">
        <section class="dbMainbase">
            <button class="sideBtn" onclick="openNav()">☰</button>
            @include('MasterUser/Components/sidebar')
            <div class="dbRight">
                <div class="happyEvnt dmTop mb-3">
                    <div class="sbBack">
                        <span class="sbAngle back" onclick="javascript:history.go(-1)"
                            data-id="{{ Crypt::encryptString($patient_id) }}"><i
                                class="fa-solid fa-angle-left"></i></span>
                        <h4>Dr's Order</h4>
                    </div>
                    <div>
                        <h4>{{ $patName }}</h4>
                    </div>
                    <div class="caltimeMain"><span class="caltime">
                        <span type="button" onclick="window.print()" /><i class="fal fa-print fa-lg"
                                style="color: #ffffff;"></i></span> &nbsp;&nbsp;
                        {{-- <img src="{{ asset('public/MasterUser/assets/images/caltime.png') }}" alt="" /> --}}
                            {{-- {{ date('m-d-Y', strtotime($date)) }}</span><a href="{{ route('/master_details') }}"><span
                                class="dmbg">{{ strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1)) }}</span></a> --}}
                                <h6 style="color: white">Overview</h6>
                    </div>
                </div>
                {{-- @if ($form == 1)
                    <form method="POST" enctype="multipart/form-data" id="narrativeForm">
                        @csrf
                        <div class="row col-12">
                            <div class="col-7">
                                <textarea class="form-control" rows="1" placeholder="Enter Narrative Text" name="description" id="description"></textarea>
                                <p class="description_error" style="color: red; font-size:10pt"></p>
                            </div>
                            <div class="col-4">
                                <input type="file" class="form-control" multiple name="detailsImage[]"
                                    accept=".doc,.docx,.pdf,.png,.jpeg,.jpg" />
                            </div>
                            <div class="col-1">
                                <button type="submit" class="btn btn-sm"
                                    style="background-color: #3D4EC6; color:white">Add</button>
                            </div>
                            <input hidden name="patient_id" value="{{ $patient_id }}">
                            <input hidden name="type" value="2">
                        </div>
                    </form>
                @endif --}}
                @if (count($date_narrative) > 0)
                    @foreach ($date_narrative as $narrative)
                        <div class="medicationDetlbg">
                            <div class="timeshow">{{ date('m-d-Y', strtotime($narrative->created_at)) }}</div>
                            {{-- @if ($form == 1)
                                <div class="edit" data-bs-toggle="modal"
                                    data-bs-target="#notesEditModal{{ $narrative->id }}"
                                    data-id="{{ $narrative->id }}"><i class="fas fa-edit"></i></div>
                            @endif --}}
                            <div class="clientMediNoteblock">
                                @if ($narrative->getName->user_type == 1)
                                    <a href="{{ route('/master_details') }}">
                                    @elseif($narrative->getName->user_type == 2)
                                        @php
                                            $id = Crypt::encryptString($narrative->getName->id);
                                        @endphp
                                        <a href="{{ route('subuser_profile', $id) }}">
                                @endif
                                <span
                                    class="dmbg">{{ strtoupper(substr($narrative->getName->first_name, 0, 1)) . strtoupper(substr($narrative->getName->last_name, 0, 1)) }}</span></a>
                                &nbsp; &nbsp;

                                <div class="schedDetails">
                                    <p>{{ $narrative->descriptions }}</p>
                                    @if ($narrative->images)
                                        @php
                                            $images = explode(',', $narrative->images);
                                            $doc = 0;
                                            $img = 0;
                                            //
                                            
                                            foreach ($images as $image) {
                                                $doc1 = explode('.', $image);
                                                $type = $doc1[1];
                                                if ($type == 'pdf' || $type == 'docx' || $type == 'doc') {
                                                    $doc = 1;
                                                }
                                            }
                                            
                                        @endphp

                                        @if ($doc != 1)
                                            <div class="cmnImgbg gallery">
                                                {{-- <a href="./images/gal1.jpg" title="Gallery 1">
                                                <figure><img class="img-fluid img-thumbnail" src="./images/gal1.jpg"
                                                        title="Gallery 1" alt=""></figure>
                                                </a>
                                            --}}
                                                @php
                                                    $images = explode(',', $narrative->images);
                                                @endphp
                                                @foreach ($images as $image)
                                                    <a href="{{ asset('public/documents/Schedule_Details/' . $image) }}"
                                                        title="Narrative {{ date('m-d-Y', strtotime($date)) }}">
                                                        <figure><img class="img-fluid img-thumbnail"
                                                                src="{{ asset('public/documents/Schedule_Details/' . $image) }}"
                                                                alt="{{ $image }}"></figure>
                                                    </a>
                                                @endforeach
                                            </div>
                                        @else
                                            @php
                                                $images = explode(',', $narrative->images);
                                            @endphp
                                            <div class="d-flex d-inline">
                                                @foreach ($images as $image)
                                                    <div class="pdfbg mx-2">

                                                        Document
                                                        &nbsp;
                                                        <a
                                                            href="{{ asset('public/documents/Schedule_Details/' . $image) }}"><label
                                                                class="commonButton savebtn viewbtn"><i
                                                                    class="fa-light fa-download"></i></label></a>
                                                    </div>
                                                @endforeach
                                            </div>
                                        @endif
                                    @endif
                                </div>
                            </div>
                        </div>
                        <!-- Edit Narrative-Modal -->
                        <div class="modal fade modalBase" id="notesEditModal{{ $narrative->id }}" tabindex="-1"
                            aria-labelledby="notesModalLabel{{ $narrative->id }}" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Edit Narrative</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <form enctype="multipart/form-data" id="editDetailsForm{{ $narrative->id }}">
                                        @csrf
                                        <div class="modal-body">
                                            <div class="enterMfd">
                                                <input hidden name="details_id" value="{{ $narrative->id }}">
                                                <textarea class="form-control" name="description">{{ $narrative->descriptions }}</textarea>
                                                <p class="description_error_1" style="color: red; font-size:10pt"></p>
                                                <input type="file" multiple name="detailsImage[]" />
                                            </div>
                                            @if ($narrative->images != null)
                                                @php
                                                    $files = explode(',', $narrative->images);
                                                @endphp
                                                <div class="mb-3">
                                                    @foreach ($files as $f)
                                                        <div class="form-check">
                                                            <input class="form-check-input" type="checkbox"
                                                                value="{{ $f }}" id="flexCheckChecked"
                                                                name="docs[]" checked>
                                                            <label class="form-check-label" for="flexCheckChecked">
                                                                {{ $f }}
                                                            </label>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            @endif
                                        </div>
                                        <div class="modal-footer">
                                            <button id="submit{{ $narrative->id }}" type="button"
                                                class="btn commonButton">Update</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @endforeach
                @else
                    <div>
                        <p>&nbsp; No Orders found...</p>
                    </div>
                @endif

                {{-- <button class="commonButton respFullwdth" data-bs-toggle="modal" data-bs-target="#narrativeModal">
                    Add Narratives
                </button> --}}
            </div>
        </section>
    </div>

    <!-- Narrative-Modal -->
    <div class="modal fade modalBase" id="narrativeModal" tabindex="-1" aria-labelledby="narrativeModalLabel"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Add Narrative</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <form method="POST" enctype="multipart/form-data" action="#" id="narrativeForm">
                    @csrf
                    <div class="modal-body">
                        <div class="enterMfd">
                            <input hidden name="patient_id" value="{{ $patient_id }}">
                            <input hidden name="type" value="2">
                            <input hidden name="date" value="{{ $date }}">
                            <textarea class="form-control" placeholder="Enter Chart Text" name="description"></textarea>
                            <p class="description_error" style="color: red; font-size:10pt"></p>
                            <input type="file" name="detailsImage[]" multiple accept="image/*" />
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="btn commonButton">Add</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!--    <script src="js/jquery.js" type="text/javascript"></script>-->
    {{-- @include('MasterUser/Components/footer') --}}
    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script src="{{ asset('public/MasterUser/assets/js/gallery.min.js') }}"></script>
    <script>
        function openNav() {
            document.getElementById("mySidepanel").style.width = "90%";
        }

        function closeNav() {
            document.getElementById("mySidepanel").style.width = "0";
        }
    </script>
    <script>
        window.pressed = function() {
            var a = document.getElementById('aa');
            if (a.value == "") {
                fileLabel.innerHTML = "Upload image";
            } else {
                var theSplit = a.value.split('\\');
                fileLabel.innerHTML = theSplit[theSplit.length - 1];
            }
        };
    </script>
    <script>
        (function($) {
            $('.gallery').each(function() { // the containers for all your galleries
                $(this).magnificPopup({
                    delegate: "a",
                    type: "image",
                    tLoading: "Loading image #%curr%...",
                    mainClass: "mfp-img-mobile",
                    gallery: {
                        enabled: true,
                        navigateByImgClick: true,
                        preload: [0,
                            1
                        ] // Will preload 0 - before current, and 1 after the current image
                    },
                    image: {
                        tError: '<a href="%url%">The image #%curr%</a> could not be loaded.'
                    }
                });
            });
        })(jQuery);
    </script>
    <script>
        $(document).ready(function() {

            $('#description').keyup(function(event) {
                var textBox = event.target;
                var start = textBox.selectionStart;
                var end = textBox.selectionEnd;
                textBox.value = textBox.value.charAt(0).toUpperCase() + textBox.value.slice(1);
                textBox.setSelectionRange(start, end);
            });

            // $(".back").click(function() {
            //     var patient_id = $(this).attr("data-id");
            //     // alert(patient_id);
            //     window.location.href = `{{ url('client_medication/${patient_id}') }}`;
            // });

            $(".edit").on({
                mouseenter: function() {
                    $(this).css("cursor", "pointer");
                },

                mouseleave: function() {
                    $(this).css("cursor", "auto");
                },
            })

            //Edit Note Form Submit
            $('.edit').click(function(e) {
                e.preventDefault();
                var uid = $(this).attr('data-id');
                id = "#editDetailsForm" + uid;
                var btnid = "#submit" + uid;
                $(btnid).on("click", function(e) {
                    e.preventDefault();
                    $('.description_error').html("");

                    var form = $(id)[0];
                    var formData = new FormData(form);
                    console.log(formData);
                    $.ajax({
                        url: "{{ route('edit_details_form') }}",
                        type: 'POST',
                        dataType: 'json',
                        processData: false,
                        contentType: false,
                        data: formData,
                        success: function(response) {

                            console.log(response);

                            if (response.hasOwnProperty('success')) {

                                const Toast = Swal.mixin({
                                    toast: true,
                                    position: 'top-end',
                                    showConfirmButton: false,
                                    timer: 2000,
                                    timerProgressBar: false,
                                    didOpen: (toast) => {
                                        toast.addEventListener(
                                            'mouseenter',
                                            Swal.stopTimer)
                                        toast.addEventListener(
                                            'mouseleave',
                                            Swal.resumeTimer
                                        )
                                    }
                                })

                                Toast.fire({
                                    icon: 'success',
                                    title: response.success
                                })

                                setTimeout(function() {
                                    window.location.reload(false);
                                }, 2501);

                            } else {
                                console.log(response.error);
                                if (response.error) {
                                    $('.description_error_1').html("*" + response
                                    .error);
                                }
                            }
                        }
                    });
                })
            });

            //New Narrative
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            $('#narrativeForm').submit(function(e) {
                e.preventDefault();

                $('.description_error').html("");

                var form = $('#narrativeForm')[0];
                var formData = new FormData(form);
                console.log(formData);
                $.ajax({
                    url: "{{ route('new_details') }}",
                    type: 'POST',
                    dataType: 'json',
                    processData: false,
                    contentType: false,
                    data: formData,
                    success: function(response) {

                        console.log(response);

                        if (response.hasOwnProperty('success')) {

                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 2000,
                                timerProgressBar: false,
                                didOpen: (toast) => {
                                    toast.addEventListener(
                                        'mouseenter',
                                        Swal.stopTimer)
                                    toast.addEventListener(
                                        'mouseleave',
                                        Swal.resumeTimer
                                    )
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: response.success
                            })

                            setTimeout(function() {
                                window.location.reload(false);
                            }, 2501);


                            // } else if (response.hasOwnProperty('error')) {
                            //     Swal.fire({
                            //         icon: 'error',
                            //         title: 'Sorry..',
                            //         text: response.error,
                            //         confirmButtonColor: '#3D4EC6',
                            //     })

                        } else {

                            // let description = response.hasOwnProperty('description');                 
                            console.log(response.error);
                            if (response.error) {
                                $('.description_error').html("*" + response.error);
                            }

                        }

                    }
                });

            })
        });
    </script>
</body>

</html>
