@extends('layouts.auth.app')

@section('title', 'Reset')

@section('content')
    <section class="loginContainer">
        <div class="forgotpassicon"><img src="{{ asset('public/MasterUser/assets/images/keypassword.png') }}" alt="">
        </div>
        <h2>Enter new password!</h2>
        <p>Your new password must be deferent from the previous password.</p>
        <form action="{{route('verify_password')}}" method="post">
            @csrf
            <div class="form-group mb-3 mt-4">
                <div class="showhidePwd" id="show_hide_password">
                    <input class="form-control" type="password" name="password" id="password"
                        placeholder="New Password">
                    <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>
                </div>
            </div>
            <input type="email" name="email" id="email" value="{{ $email }}" hidden />
            <div class="form-group mb-5">
                <div class="showhidePwd" id="show_hide_password2">
                    <input class="form-control" type="password" name="confirm_password" id="confirm_password"
                        placeholder="Confirm Password">
                    <a class="showpass"><i class="fa-regular fa-eye-slash"></i></a>
                </div>
                <p class="checking mt-1" id="checking" align="left">

                </p>
            </div>
            <button id="submit" disabled>Change Password</button>
        </form>
    </section>

    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>
    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
            $("#show_hide_password2 a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password2 input').attr("type") == "text") {
                    $('#show_hide_password2 input').attr('type', 'password');
                    $('#show_hide_password2 i').addClass("fa-eye-slash");
                    $('#show_hide_password2 i').removeClass("fa-eye");
                } else if ($('#show_hide_password2 input').attr("type") == "password") {
                    $('#show_hide_password2 input').attr('type', 'text');
                    $('#show_hide_password2 i').removeClass("fa-eye-slash");
                    $('#show_hide_password2 i').addClass("fa-eye");
                }
            });



            $(this).keyup(function() {
                var password = $("#password").val();
                var confirmPassword = $("#confirm_password").val();
                if (password == '' && confirmPassword == '') {
                    $("#checking").html("");

                } else {

                    if (password != confirmPassword) {
                        $("#submit").attr("disabled", true);
                        $("#checking").html(`<p class="text-danger">Not Matching</p>`);

                    } else {

                        $("#checking").html(`<p class="text-success">Matching</p>`);

                        if (password.length >= 5) {
                            $("#submit").removeAttr("disabled");
                        }else{
                            $("#checking").html(`<p class="text-danger">Length must be 5</p>`);
                        }
                    }

                }

                // $(".from-control").css("border-bottom-color", "#fff");
                // $("#divCheckPasswordMatch").html("Passwords match.");
            });
        });
    </script>

@endsection
