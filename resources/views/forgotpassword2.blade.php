@extends('layouts.auth.app')

@section('title', 'Reset')

@section('content')
    <section class="loginContainer">
        <div class="forgotpassicon"><img src="{{ asset('public/MasterUser/assets/images/entercode.png')}}" alt=""></div>
        @if (Session::has('msg'))
            <h2 class="text-danger">{{ Session::get('msg') }}</h2>
        @else
            <h2>Enter code!</h2>
        @endif
        <p>Please enter 4 digit code sent to {{ $email }}</p>
        <form action="{{ route('verify_otp') }}" method="post">
            @csrf
            <div class="digit-group">
                <input type="text" id="digit1" name="digit1" data-next="digit2" />
                <input type="text" id="digit2" name="digit2" data-next="digit3" data-previous="digit1" />
                <input type="text" id="digit3" name="digit3" data-next="digit4" data-previous="digit2" />
                <input type="text" id="digit4" name="digit4" data-next="digit5" data-previous="digit3" />
                <input type="email" name="email" value="{{ $email }}" hidden />
            </div>
            <button type="submit" name="next" class="next" id="next">Next</button>
        </form>
    </section>

    <script src="{{ asset('public/MasterUser/assets/js/bootstrap.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('public/MasterUser/assets/js/jquery-3.4.1.min.js') }}"></script>

    <script>
        $(document).ready(function() {
            $("#show_hide_password a").on('click', function(event) {
                event.preventDefault();
                if ($('#show_hide_password input').attr("type") == "text") {
                    $('#show_hide_password input').attr('type', 'password');
                    $('#show_hide_password i').addClass("fa-eye-slash");
                    $('#show_hide_password i').removeClass("fa-eye");
                } else if ($('#show_hide_password input').attr("type") == "password") {
                    $('#show_hide_password input').attr('type', 'text');
                    $('#show_hide_password i').removeClass("fa-eye-slash");
                    $('#show_hide_password i').addClass("fa-eye");
                }
            });
        });
    </script>
    <script>
        $('.digit-group').find('input').each(function() {
            $(this).attr('maxlength', 1);
            $(this).on('keyup', function(e) {
                var parent = $($(this).parent());

                if (e.keyCode === 8 || e.keyCode === 37) {
                    var prev = parent.find('input#' + $(this).data('previous'));

                    if (prev.length) {
                        $(prev).select();
                    }
                } else if ((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (
                        e.keyCode >= 96 && e.keyCode <= 105) || e.keyCode === 39) {
                    var next = parent.find('input#' + $(this).data('next'));

                    if (next.length) {
                        $(next).select();
                    } else {
                        if (parent.data('autosubmit')) {
                            parent.submit();
                        }
                    }
                  
                }
            });
        });
    </script>
@endsection
