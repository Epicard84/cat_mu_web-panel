<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->bigInteger('phone_number')->nullable();
            $table->string('company_name')->nullable();
            $table->string('city')->nullable();
            $table->string('country')->nullable();
            $table->string('state')->nullable();
            $table->string('permanent_address',500)->nullable();
            $table->string('mailing_address',500)->nullable();
            $table->integer('zip_code',10)->nullable();
            $table->string('facility_address',500)->nullable();
            $table->string('status')->nullable();
            $table->timestamp('last_activity')->nullable();
            $table->string('profile_image_name')->nullable();
            $table->string('password');
            $table->integer('user_type')->nullable(); // admin = 0 or masteruser = 1 or subuser = 2
            $table->bigInteger('master_user')->nullable();
            $table->softDeletes(); 
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
