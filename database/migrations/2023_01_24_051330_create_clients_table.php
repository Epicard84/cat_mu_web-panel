<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clients', function (Blueprint $table) {
            $table->id();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->date('dob')->nullable();
            $table->bigInteger('phone_number')->nullable();
            $table->string('gender')->nullable();
            $table->string('additional_misc_information')->nullable();
            $table->string('status')->comment('1=active,0=discharged')->nullable();
            $table->string('documents',500)->nullable();
            $table->string('master_user')->nullable();
            $table->timestamp('discharged_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clients');
    }
};
