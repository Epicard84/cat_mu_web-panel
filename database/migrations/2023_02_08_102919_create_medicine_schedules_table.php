<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('medicine_schedules', function (Blueprint $table) {
            $table->id();
            $table->integer('medicine_id')->nullable();
            $table->timestamp('date')->nullable();
            $table->string('complete_details')->nullable();
            $table->string('status')->comment('0 for pending and 1 for completed');
            $table->softDeletes(); 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('medicine_schedules');
    }
};
