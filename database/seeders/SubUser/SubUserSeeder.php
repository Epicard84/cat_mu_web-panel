<?php

namespace Database\Seeders\SubUser;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;
use Illuminate\Support\Facades\Hash;

class SubUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::Create();
        for ($i = 1; $i < 10 ; $i++) { 
            $number = (Integer)$faker->phoneNumber();
            DB::table('users')->insert([
                'first_name' => $faker->firstName(),
                'last_name' =>  $faker->lastName(),
                'email' =>  $faker->email,
                'city' =>$faker->city(),
                'country' =>$faker->country(),
                'company_name' =>$faker->company(),
                'state' =>$faker->state(),
                'password' => Hash::make('password'),
                'phone_number' => $number,
                'permanent_address' => $faker->address(),
                'mailing_address' => $faker->address(),
                'master_user' => 2,
                'status' => 'online',
                'user_type' => 2,
            ]);
        }
    }
}
