<?php

namespace Database\Seeders\SuperAdmin;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Faker\Factory as Faker;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        DB::table('users')->truncate();
        $faker = Faker::Create();

        for ($i = 1; $i < 10 ; $i++) { 
            if($i == 1){
                $name = "Admin";
                $last_name = $name;
                $email = 'admin@admin.com';
                $user_type = 0;
            }else{
                $name = $faker->firstName();
                $last_name = $faker->lastName();
                $email = $faker->email;
                $user_type = 1;
            }
            $number = (Integer)$faker->phoneNumber();
            DB::table('users')->insert([
                'first_name' => $name,
                'last_name' => $last_name,
                'email' => $email,
                'city' =>$faker->city(),
                'country' =>$faker->country(),
                'company_name' =>$faker->company(),
                'state' =>$faker->state(),
                'password' => Hash::make('password'),
                'phone_number' => $number,
                'permanent_address' => $faker->address(),
                'mailing_address' => $faker->address(),
                'status' => 'online',
                'user_type' => $user_type,
            ]);
         }
    }
}
