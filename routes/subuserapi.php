<?php

use App\Http\Controllers\SubUser\AuthController;
use App\Http\Controllers\SubUser\SubUserApiController;
use Illuminate\Support\Facades\Route;
use SebastianBergmann\CodeCoverage\Report\Html\Dashboard;

Route::controller(AuthController::class)->prefix('sub_user')->group(function () {


    //Login Authentication
    Route::post('login', 'loginAuth');

    //Forgot password
    Route::post('forgot_password', 'forgotPassword');

    //OTP verification 
    Route::post('otp_verifiaction', 'otpVerification');

    //new password creation
    Route::post('reset_password', 'newPassword');
});

Route::middleware('auth:sanctum', 'verified', 'LastUserActivity')->prefix('sub_user')->controller(SubUserApiController::class)->group(function () {

    // Dashboard
    Route::get('dashboard', 'dashboard');

    //Client details
    Route::get('client_details/{id}','client_details');
    
});
