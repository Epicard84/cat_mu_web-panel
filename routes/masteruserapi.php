<?php

use App\Http\Controllers\MasterUser\API\AuthController;
use App\Http\Controllers\MasterUser\API\MasterUserApiController;
use Illuminate\Support\Facades\Route;

Route::controller(AuthController::class)->prefix('master_user')->group(function () {


        //Login Authentication
        Route::post('login', 'loginAuth');

        //Forgot password
        Route::post('forgot_password', 'forgotPassword');

        //OTP verification 
        Route::post('otp_verifiaction', 'otpVerification');

        //new password creation
        Route::post('new_password', 'newPassword');
});

// Route::middleware('auth:sanctum', 'verified', 'LastUserActivity')->prefix('master_user')->controller(MasterUserApiController::class)->group(function ()  {
        Route::middleware('auth:sanctum', 'verified')->prefix('master_user')->controller(MasterUserApiController::class)->group(function ()  {


                //Dashboard
                Route::get('dashboard', 'dashboard');

                //Sub user Details
                Route::get('sub_user_list', 'subUserDetails');

                //Sub user Profile
                Route::get('sub_user_profile/{id}', 'subUserProfile');

                //Create new Sub User
                Route::post('sub_user_register', 'subUserRegister');

                //Edit Sub Users
                Route::post('sub_user_edit', 'subUserEdit');

                //all Clients details
                Route::get('clients_list', 'clientsDetails');

                //Client Archived Medications
                Route::get('archived_medictaionlist/{id}','archivedMedicationList');

                //Create new Client
                Route::post('client_register', 'clientRegister');

                //edit client details
                Route::post('update_client','updateClient');

                //Client archive
                Route::post('client_archive', 'clientArchive');

                //Client Restore SumonK
                Route::get('/client_restore/{id}','clientRestore');

                //Archive Clients
                Route::get('archive_client','archiveClients');

                //clients medicines list 
                // Route::get('medicine_schedule/{id}', 'clientMedicineSchedule');
                
                //update medicines list 
                Route::post('update_medicine_schedule', 'updateMedicineSchedule');

                //clients medicines list 
                Route::get('medicine_details/{id}', 'clientMedicineDetails');

                //Create new Clients medicines 
                Route::post('add_new_medicine', 'addNewMedicine');

                //Client medicine delete
                Route::get('medicine_delete/{id}','medicineDelete');

                //add new MFD
                Route::post('create_mfd', 'createMfd');

                //MFD list
                Route::get('mfd_list', 'mfdList');

                //MFD Edit
                Route::post('/edit_mfd/{id}','editMfd'); //SumonK

                //notes list 
                Route::get('notes_details/{id}', 'notesDetails');

                //chates list 
                Route::get('chates_details/{id}', 'notesDetails');

                //narrates list 
                Route::get('narrates_details/{id}', 'notesDetails');

                //master user details
                Route::get('user_details', 'userDetails');

                //edit master user
                Route::post('edit', 'editMasterUser');

                //Change sub user details for client
                Route::get('Change_sub_user_details/{id}','changeUserDetails');

                //Update Sub user for Client
                Route::post('update_sub_user_details','updateSubUserDetails');

                //clients medicines list SumonK 
                Route::get('medicine_schedule/{id}/{date}', 'clientMedicineSchedule');

                //Complete Medicine Schedule SumonK
                Route::get('client_medication_update/{id}/{index}','clientMedicationUpdate');

                //Medication Undo SumonK
                Route::get('/client_medication_undo/{id}/{index}','clientMedicationUndo');

                //New medication chart narrative notes SumonK
                Route::post('/new_details','newDetails');

                //Edit Note Chart Narrative Details SumonK
                Route::post('/edit_details','editDetails');

                //Delete Details Notes Charts Narrative SumonK
                Route::get('details_delete/{id}','deleteDetails')->name('details_delete');

                //Medication chart SumonK
                Route::get('/medication_chart/{date}/{id}','medicationChart');

                //Medication Narrative SumonK
                Route::get('/medication_narrative/{date}/{id}','medicationNarrative');

                //Medication Note SumonK
                Route::get('/medication_note/{schid}/{pid}','medicationNote');

                //Notification SumonK
                Route::get('/mu_notifications','muNotifications'); //Notification List
                
                Route::get('/notification_seen/{id}','notificationSeen'); //Seen Updated

                //Get Medicine Details SumonK
                Route::get('/medication_details/{id}','medicationDetails');

                //Reminder SumonK
                Route::get('/mu_reminder/{date}','muReminder'); //Notification List
                Route::post('new_reminder','newReminder'); //New Notification
                Route::get('check_reminder/{id}','checkReminder'); //Check Reminder
                Route::get('delete_reminder/{id}','deleteReminder'); //Delete Reminder

                //SubUser Document Edit
                Route::POST('subuser_docs_edit','editDocsSubUser');

                //SubUser Delete
                Route::get('/delete_user/{id}','deleteUser'); //SumonK

                //permanent Delete Subuser
                Route::get('/delete_subuser/{id}','deleteSubuser'); //SumonK

                //Archive SubUsers
                Route::get('/archive_subuser','archiveSubuser'); //SumonK

                //SubUser Restore SumonK
                Route::get('/subuser_restore/{id}','subuserRestore'); //SumonK

                //Archived clients medicines list SumonK 
                Route::get('archived_medicine_schedule/{id}/{date}', 'archivedClientMedicineSchedule');

                //Now Date Time
                Route::get('/now_datetime','nowDateTime'); //SumonK

                //All Note Chart Narrative
                Route::get('/medication_all_chart/{id}','medicationAllChart'); //SumonK
                //Filtered All Note Chart Narrative
                Route::get('/filter_medication_all_chart/{id}/{type}', 'filterMedicationAllChart')->name('filter_medication_all_chart'); //SumonK

                //Account Disable request
                Route::post('/disable_reason','disableReason')->name('disable_reason'); //SumonK
                
                //Manage Activity SumonK
        Route::get('/add_activity/{id}', 'addActivity')->name('add_activity');
        //Add Activity
        Route::POST('/add_new_activity', 'addNewActivity'); //done
        //Client Activity 
        Route::get('/activity_schedule/{id}/{date}', 'clientActivitySchedule'); //done
        //Activity Update
        Route::get('/client_activity_update/{id}/{index}', 'clientActivityUpdate'); //done
        //Activity Undo
        Route::get('/client_activity_undo/{id}/{index}', 'clientActivityUndo'); //done
        //Activity Details
        Route::get('/activity_details/{id}', 'activityDetails')->name('activity_details');
        //Activity delete
        Route::get('activity_delete/{id}', 'activityDelete'); //done
        //Archive Activity
        Route::get('/activity_archive/{id}', 'archiveActivity'); //done

                //Activity Note SumonK
                Route::get('/activity_note/{schid}/{pid}', 'activityNote');

                //New activity chart narrative notes SumonK
                Route::post('/new_activity_details', 'newActivityDetails'); 
        
                //Edit Note Chart Narrative Details SumonK
                Route::post('/edit_activity_details', 'editActivityDetails');

        });
        

