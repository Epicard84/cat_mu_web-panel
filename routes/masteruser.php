<?php

use App\Http\Controllers\MasterUser\MasterUserController;
use App\Http\Controllers\SubUser\SubUserController;
use Illuminate\Support\Facades\Route;

// Route::middleware('auth', 'verified', 'user', 'LastUserActivity')->controller(MasterUserController::class)->group(function () {
Route::middleware('auth', 'verified', 'user')->controller(MasterUserController::class)->group(function () {
  Route::get('/master_dashboard', 'dashboard')->name('master_dashboard');
  Route::get('/clients_details/{id}', 'clientsDetails')->name('clients_details');
  Route::get('/document_download/{id}', 'documentDownload')->name('document_download');
  Route::get('/add_medication/{id}', 'addMedication')->name('add_medication');
  Route::get('/add_client', 'addClient')->name('add_client');

  //SumonK
  //Client Medication 
  // Route::get('/client_medication/{id}','clientMedication')->name('client_medication');
  Route::get('/client_medication/{id}/{date}', 'clientMedication')->name('client_medication');
  //Medication Update
  Route::get('/client_medication_update/{id}', 'clientMedicationUpdate')->name('client_medication_update');
  //Medication Undo
  Route::get('/client_medication_undo/{id}', 'clientMedicationUndo')->name('client_medication_undo');
  Route::get('/medication_details/{id}', 'medicationDetails')->name('medication_details');
  //Show medication note
  Route::get('/medication_note/{schid}/{pid}', 'medicationNote')->name('medication_note');
  //Show medication chart
  Route::get('/medication_chart/{date}/{id}', 'medicationChart')->name('medication_chart');
  //Show medication narrative
  Route::get('/medication_narrative/{date}/{id}', 'medicationNarrative')->name('medication_narrative');
  //New medication details notes charts narratives
  Route::post('/new_notes', 'newDetails')->name('new_details');
  //Edit Note Chart Narrative Details
  Route::post('/edit_details_form', 'editDetailsForm')->name('edit_details_form');
  //Delete Details
  Route::get('details_delete/{id}', 'deleteDetails')->name('details_delete');
  //Show All Charts
  Route::get('/medication_all_chart/{id}', 'medicationAllChart')->name('medication_all_chart');
  Route::get('/filter_medication_all_chart/{id}/{type}', 'filterMedicationAllChart')->name('filter_medication_all_chart');

  //Manage Activity
  Route::get('/add_activity/{id}', 'addActivity')->name('add_activity');
  //Add Activity
  Route::POST('/add_activity_post', 'addActivityPost')->name('add_activity_post'); //ajax route for add activity
  //Client Activity 
  Route::get('/client_activity/{id}/{date}', 'clientActivity')->name('client_activity');
  //Activity Update
  Route::get('/client_activity_update/{id}', 'clientActivityUpdate')->name('client_activity_update');
  //Activity Undo
  Route::get('/client_activity_undo/{id}', 'clientActivityUndo')->name('client_activity_undo');
  //Activity Details
  Route::get('/activity_details/{id}', 'activityDetails')->name('activity_details');
  //Delete Activity
  Route::post('/delete_activity', 'deleteActivity')->name('delete_activity');
  //Archive Activity
  Route::post('/archive_activity', 'archiveActivity')->name('archive_activity');
  //Show activity note
  Route::get('/activity_note/{schid}/{pid}','activityNote')->name('activity_note');
  //New Activity details notes charts narratives
  Route::post('/new_activity_notes','newActivityDetails')->name('new_activity_notes');
//Edit Note Chart Narrative Details
Route::post('/edit_activity_details_form','editActivityDetailsForm')->name('edit_activity_details_form');

  Route::post('/new_client', 'newClient')->name('new_client'); //Ajax route

  Route::get('/update_client/{id}', 'updateClient')->name('update_client');
  Route::post('/update_client/post', 'updateClientPost')->name('update_client/post');

  //delete Client 
  Route::get('/archive_client', 'archiveClient')->name('/archive_client');

  //Archived Clients SumonK
  Route::get('/archived_client_medication/{id}/{date}', 'archivedClientMedication')->name('archived_client_medication');
  Route::get('/archived_clients_details/{id}', 'archivedClientsDetails')->name('archived_clients_details');
  Route::get('/client_restore/{id}', 'clientRestore')->name('client_restore');

  //SubUsers
  Route::get('/sub_users', 'subUsers')->name('sub_users');

  //Sub Users Profile Details
  Route::get('/subuser_profile/{id}', 'subUserProfile')->name('subuser_profile');

  //MFD Monthly fire drill
  Route::get('/mfd', 'mfd')->name('mfd');
  Route::get('/create_mfd', 'createMfd')->name('create_mfd');
  Route::post('/store_mfd', 'storeMFD')->name('store_mfd');
  Route::post('/mfd_edit_details', 'mfdEditDetails')->name('mfd_edit_details');

  //Change password
  Route::get('/change_password', 'changePassword')->name('change_password');

  //Account Disable request
  Route::post('/disable_reason', 'disableReason')->name('disable_reason');

  //Update password post
  Route::post('/update_password', 'updatePassword')->name('update_password');

  Route::POST('/add_medicine', 'addMedicine')->name('add_medicine'); //ajax route for add medicine

  //Delete Medicine
  Route::post('/delete_medicine', 'deleteMedicine')->name('delete_medicine');

  //Archive Medicine
  Route::post('/archive_medicine', 'archiveMedicine')->name('archive_medicine');

  //Client Document dowload
  Route::get('/medicine_download/{id}', 'medicineDownload')->name('medicine_download');

  //Master User Details
  Route::get('/master_details', 'MasterUserDetails')->name('/master_details');

  //Edit Master user
  Route::get('/edit_master_user', 'editMasterUser')->name('/edit_master_user');

  //Edit Master user post
  Route::post('/edit_master_user_post', 'editMasterUserPost')->name('/edit_master_user_post');


  //Add Subuser View
  Route::get('/add_subuser', 'addSubUser')->name('add_subuser');

  //Add Subuser Post
  Route::POST('/new_subuser', 'newSubUser')->name('new_subuser'); //Ajax Route for add Sub user

  //SubUser Document Edit
  Route::POST('/subuser_docs_edit', 'editDocsSubUser')->name('subuser_docs_edit'); //Ajax Route for add Sub user

  //Edit Sub user
  Route::get('/edit_sub_user/{id}', 'editSubUser')->name('/edit_sub_user');

  //Edit Sub user post
  Route::POST('/edit_sub_user_post', 'editSubUserPost')->name('/edit_sub_user_post');

  Route::get('/delete_user', 'deleteUser')->name('/delete_user');
  Route::get('/subuser_restore/{id}', 'restoreSubuser')->name('subuser_restore'); //restore SubUser
  Route::get('/delete_subuser', 'deleteSubuser')->name('/delete_subuser'); //permanent Delete Subuser

  //Status Changing Online/Offline
  Route::get('/status', 'status')->name('status'); //Ajax route

  //Notification SumonK
  Route::get('mu_notifications', 'muNotifications')->name('mu_notifications'); //Notifications List

  Route::get('notification_seen/{id}', 'notificationSeen')->name('notification_seen'); //Seen

  //Reminder SumonK
  Route::post('new_reminder', 'newReminder')->name('new_reminder');
  Route::get('mu_reminder', 'muReminder')->name('mu_reminder');
  Route::get('check_reminder/{id}', 'checkReminder')->name('check_reminder');
  Route::get('mu_filter_reminder/{date}', 'muFilterReminder')->name('mu_filter_reminder');
  Route::get('mu_delete_reminder', 'deleteReminder')->name('mu_delete_reminder');

  //SubUser Login
  Route::get('su_login', 'suLogin')->name('su_login');

  //Surveyor Module
    //Surveyor Dashboard
    // Route::get('sr_dashboard', 'srDashboard')->name('sr_dashboard');
    //Client Medication 
    Route::get('/client_medication_overview/{id}/{date}', 'overviewclientMedication')->name('client_medication_overview');

    //Show medication note
    Route::get('/sr_medication_note/{schid}/{pid}', 'srMedicationNote')->name('sr_medication_note');
    //Show medication chart
    Route::get('/sr_medication_chart/{date}/{id}', 'srMedicationChart')->name('sr_medication_chart');
    //Show medication narrative
    Route::get('/sr_medication_narrative/{date}/{id}','srMedicationNarrative')->name('sr_medication_narrative');
    //Show All Charts
    Route::get('/sr_medication_all_chart/{id}', 'srMedicationAllChart')->name('su_medication_all_chart');
    //PDF Client Medication
    Route::get('/client_medication_pdf/{id}/{date}','clientMedicationPDF')->name('client_medication_pdf');
});

//SubUser Module
// Route::middleware('auth', 'verified', 'LastUserActivity')->controller(SubUserController::class)->group(function () {
Route::middleware('auth', 'verified')->controller(SubUserController::class)->group(function () {
  Route::get('su_dashboard', 'suDashboard')->name('su_dashboard');
  Route::get('master_profile/{id}', 'masterProfile')->name('master_profile');

  //Client Medication 
  Route::get('/su_client_medication/{id}/{date}', 'suClientMedication')->name('su_client_medication');
  //Medication Update
  Route::get('/su_client_medication_update/{id}', 'suClientMedicationUpdate')->name('su_client_medication_update');
  //Medication Undo
  Route::get('/su_client_medication_undo/{id}', 'suClientMedicationUndo')->name('su_client_medication_undo');
  //Medication Details
  Route::get('/su_medication_details/{id}', 'suMedicationDetails')->name('su_medication_details');

  //Client Activity 
  Route::get('/su_client_activity/{id}/{date}', 'suClientActivity')->name('su_client_activity');
  //Activity Update
  Route::get('/su_client_activity_update/{id}', 'suClientActivityUpdate')->name('su_client_activity_update');
  //Activity Undo
  Route::get('/su_client_activity_undo/{id}', 'suClientActivityUndo')->name('su_client_activity_undo');
  //Activity Details
  Route::get('/su_activity_details/{id}', 'suActivityDetails')->name('su_activity_details');
  //Show activity note
  Route::get('/su_activity_note/{schid}/{pid}', 'suActivityNote')->name('su_activity_note');
  //New activity details notes charts narratives
  Route::post('/su_new_activity_notes', 'suNewActivityDetails')->name('su_new_activity_notes');
  //Edit activity Note Chart Narrative Details
  Route::post('/su_edit_activity_details_form','suEditActivityDetailsForm')->name('su_edit_activity_details_form');

  //Show medication note
  Route::get('/su_medication_note/{schid}/{pid}', 'suMedicationNote')->name('su_medication_note');
  //Show medication chart
  Route::get('/su_medication_chart/{date}/{id}', 'suMedicationChart')->name('su_medication_chart');
  //Show All Charts
  Route::get('/su_medication_all_chart/{id}', 'suMedicationAllChart')->name('su_medication_all_chart');

  //Show medication narrative
  Route::get('/su_medication_narrative/{date}/{id}', 'suMedicationNarrative')->name('su_medication_narrative');
  //New medication details notes charts narratives
  Route::post('/su_new_notes', 'suNewDetails')->name('su_new_details');
  //Edit Note Chart Narrative Details
  Route::post('/su_edit_details_form', 'suEditDetailsForm')->name('su_edit_details_form');


  //Change Password
  Route::get('su_change_password', 'suChangePassword')->name('su_change_password');
  Route::post('update_passcode', 'updatePasscode')->name('update_passcode');

  //MFD Monthly fire drill
  Route::get('su_mfd', 'suMfd')->name('su_mfd');

  //Notification SumonK
  Route::get('su_notifications', 'suNotifications')->name('su_notifications'); //Notifications List
  Route::get('su_notification_seen/{id}', 'suNotificationSeen')->name('su_notification_seen'); //Seen

  //Reminder SumonK
  Route::post('su_new_reminder', 'suNewReminder')->name('su_new_reminder');
  Route::get('su_reminder', 'suReminder')->name('su_reminder');
  Route::get('su_check_reminder/{id}', 'suCheckReminder')->name('su_check_reminder');
  Route::get('su_filter_reminder/{date}', 'suFilterReminder')->name('su_filter_reminder');
  Route::get('su_delete_reminder', 'suDeleteReminder')->name('su_delete_reminder');
});
