<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::controller(AuthController::class)->group(function(){
    Route::middleware('guest')->group(function(){
        //Login
        Route::get('/','userLogin')->name('/');

       //Sign_up page
       Route::get('/sign_up','signUp')->name('/sign_up');
        
       //Master User register
       Route::post('/sign_up/post','userRegister')->name('/sign_up/post');


        //Login Auth
        Route::post('/user_login_auth','UserLoginAuth')->name('user_login_auth');

        //forget Password 
        Route::get('/forgot_password','forgotPassword')->name('forgot_password');

        //Email Verification
        Route::post('/verify_email','verifyEmail')->name('verify_email');

        //OTP page 
        Route::get('/otp/{email}','otp')->name('otp');

        //OTP post page 
        Route::post('/verify_otp','VerifyOtp')->name('verify_otp');

        //Create new password 
        Route::get('/new_password/{email}','newPassword')->name('new_password');

        //Create new password 
        Route::post('/verify_password','verifyPassword')->name('verify_password');

        //T&C page
        Route::get('/terms-and-conditions','tandc')->name('terms-of-service');

        //Privacy Policy page
        Route::get('/privacy-and-policy','privacyPolicy')->name('privacy-and-cookie-policy');

        //About Us
        Route::get('/about-us','aboutUs')->name('about-us');

        //SubUser Reset Password
        Route::post('su_reset_passcode','suResetPasscode')->name('su_reset_passcode');
    });
    
    //Logout
    Route::middleware('auth','verified')->get('user_logout','userLogout')->name('user_logout');

    //SubUser Login Post
    Route::post('su_login_post','suLoginPost')->name('su_login_post');
    
});
Route::fallback(function () {
    return view('notFoundPage');
});

