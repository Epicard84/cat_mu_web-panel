<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SuperAdmin\AdminController;
use App\Http\Controllers\SuperAdmin\AuthController;


Route::controller(AuthController::class)->group(function () {


    Route::middleware('guest')->group(function(){
        //Login
        Route::get('/admin', 'login')->name('/admin');
    
        //Login Authentication
        Route::post('admin_login_auth', 'loginAuth')->name('admin_login_auth');
    });

    //Logout
    Route::middleware('auth', 'verified')->get('logout', 'logout')->name('logout');
});


Route::middleware('auth','verified','admin')->controller(AdminController::class)->group(function () {
    //Admin Dashboard
    Route::get('admin_dashboard', 'adminDashboard')->name('admin_dashboard');

    //master user page
    Route::get('master_users', 'masterUsers')->name('master_users');

    //sub user page
    Route::get('all_sub_users', 'subUsers')->name('all_sub_users');

    //sub user page
    Route::get('clients', 'clients')->name('clients');

    //add master user
    Route::post('add_master_user', 'addMasterUser')->name('add_master_user');

    //master user Validaton check Ajax
    Route::get('master_user_validator', 'masterUserValidator')->name('master_user_validator');

    //Edit master user details 
    Route::post('edit_master_user', 'editMasterUser')->name('edit_master_user');

    //get master user details 
    Route::get('get_master_user_details', 'getMasterUserDetails')->name('get_master_user_details');

    //master user delete 
    Route::get('delete_master_user', 'deleteMasterUser')->name('delete_master_user');

    //master user restore 
    Route::get('restore_master_user', 'restoreMasterUser')->name('restore_master_user');

    //Get roiginal location
    Route::get('location', 'location')->name('location');

    //Get roiginal location
    Route::get('about_master_user/{id}', 'aboutMasterUserDetails')->name('about_master_user');

    //MasterUser login location Map
    Route::get('mu_login_location/{id}','muLoginLocation')->name('mu_login_location');
    
    //All MasterUser login location Map
    Route::get('all_mu_login_location','allMuLoginLocation')->name('all_mu_login_location');

    //Client details
    Route::get('client_details', 'clientDetails')->name('client_details');

    //Block Account
    Route::get('block_account','blockAccount')->name('block_account');
});
