<?php

namespace App\Http\Controllers\MasterUser;

use App\Http\Controllers\Controller;
use App\Models\Doctor;
use App\Models\Guardian;
use App\Models\Location;
use App\Models\MasterUser\Client;
use App\Models\MasterUser\Medicine;
use App\Models\MasterUser\MedicineSchedule;
use App\Models\MasterUser\MedicineDetails;
use App\Models\MasterUser\Membership;
use App\Models\MasterUser\MFD;
use App\Models\User;
use App\Services\MasterUserServices;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
/* use Stevebauman\Location\Facades\Location as FacadesLocation; */
use Stripe;
use Illuminate\Support\Facades\Validator;
use App\Models\NotificationDetail;
use App\Models\Notification;
use App\Models\Reminder;
use App\Models\ReminderDetail;
use Illuminate\Support\Facades\View;
use App\Models\AccountReason;
use App\Models\UserDocument;
use App\Models\MasterUser\Activity;
use App\Models\MasterUser\ActivitySchedule;
use App\Models\MasterUser\ActivityDetails;

use function PHPUnit\Framework\isNull;

class MasterUserController extends Controller
{
    public function current_locations($id)
    {
        $user_location = Location::where("user_id", $id)->first(); // Get Current Location from the table
        return $user_location->location;
    }

    public function dashboard()
    {
        $client = Client::where('master_user', Auth::user()->id)->where('status', 1)->get();
        $count = Client::where('master_user', Auth::user()->id)->where('status', 1)->count();
        $location = $this->current_locations(Auth::user()->id);
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        $date = date('Y-m-d');
        return view('MasterUser/dashboard', ['clients' => $client, 'location' => $location, 'total' => $count, 'reminder' => $reminder, 'date' => $date]);
    }

    //Add new Client
    public function addClient()
    {
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/add-client', ['reminder' => $reminder]);
    }

    //Add new client post
    public function newClient(Request $request)
    {
        $count = Client::where('master_user', Auth::user()->id)->where('status', 1)->count();
        if ($count < 5) {
            $validator = Validator::make($request->all(), [
                'first_name' => 'required|string',
                'last_name' => 'required|string',
                'dob' => 'required|date',
                'gender' => 'required',
            ]);

            if ($validator->fails()) {
                return $validator->errors();
            } else if (is_null($request->guardian_name[0]) || is_null($request->guardian_phone_number[0]) || is_null($request->guardian_address[0])) {
                return ['error' => "Please add guardian details"];
            } else if (is_null($request->doctor_name[0]) || is_null($request->doctor_phone_number[0]) || is_null($request->doctor_address[0])) {

                return ['error' => "Please add doctor details"];
            } else {
                if ($request->hasfile('file')) {
                    $files = $request->file('file');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }

                    if ($count ==  0) {
                        foreach ($files as $file) {
                            $name =  preg_replace('/\s+/', '', $request->first_name) . uniqid() . '.' . $file->getClientOriginalExtension();
                            $file->move(public_path() . '/documents/Client/', $name);
                            $client_docs[] = $name;
                        }
                        $file = implode(',', $client_docs);
                    } else {
                        return ['imgerror' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                    }
                } else {
                    $file = null;
                }

                $client =  Client::create([
                    'first_name' => $request->first_name,
                    'last_name' => $request->last_name,
                    'dob' => date('Y-m-d', strtotime($request->dob)),
                    'gender' => $request->gender,
                    'additional_misc_information' => $request->additional_misc_information,
                    'status' => 1,
                    'documents' => $file,
                    'master_user' => Auth::user()->id,
                    'created_at' => Carbon::now()
                ]);

                foreach ($request->doctor_name as  $key => $name) {

                    Doctor::create([
                        'client_id' => $client->id,
                        'doctor_name' => $name,
                        'doctor_phone_number' => $request->doctor_phone_number[$key],
                        'doctor_address' => $request->doctor_address[$key]
                    ]);
                }

                foreach ($request->guardian_name as  $key => $name) {

                    Guardian::create([
                        'client_id' => $client->id,
                        'guardian_name' => $name,
                        'guardian_phone_number' => $request->guardian_phone_number[$key],
                        'guardian_address' => $request->guardian_address[$key]
                    ]);
                }

                return ['success' => "Client created successfuly", 'data' => $client];
            }
        } else {
            return ['msg' => "Yor client registration limit is full"];
        }
    }

    public function clientsDetails($id)
    {
        $id = Crypt::decryptString($id);


        $client_data = Client::where('id', $id)->first();

        $guardian_details = Guardian::where('client_id', $id)->get();
        $doctor_details = Doctor::where('client_id', $id)->get();

        $archivedMeds = Medicine::where('client_id', $id)->where('status', 1)->get();

        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/clientprofile', ['data' => $client_data, 'doctor' => $doctor_details, 'guardian' => $guardian_details, 'archivedMeds' => $archivedMeds, 'reminder' => $reminder]);
    }

    //Archived Client Details SumonK
    public function archivedClientsDetails($id)
    {
        $id = Crypt::decryptString($id);


        $client_data = Client::where('id', $id)->withTrashed()->first();

        $guardian_details = Guardian::where('client_id', $id)->get();
        $doctor_details = Doctor::where('client_id', $id)->get();


        return view('MasterUser/archived-clientprofile', ['data' => $client_data, 'doctor' => $doctor_details, 'guardian' => $guardian_details]);
    }

    //update client page view
    public function updateClient($id)
    {
        $id =  Crypt::decryptString($id);

        $client_data = Client::where('id', $id)->first();

        $guardian_details = Guardian::where('client_id', $id)->get();
        $guardian_count = Guardian::where('client_id', $id)->count();
        $doctor_details = Doctor::where('client_id', $id)->get();
        $doctor_count = Doctor::where('client_id', $id)->count();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();

        return view('MasterUser/update-client', ['total_doctor' => $doctor_count, 'total_guardian' => $guardian_count, 'data' => $client_data, 'doctor' => $doctor_details, 'guardian' => $guardian_details, 'reminder' => $reminder]);
    }

    //update client Post
    public function updateClientPost(Request $request)
    {
        $previousDocs = [];
        $detailsImage = [];

        if ($request->hasfile('file')) {
            $files = $request->file('file');
            foreach ($files as $file) {
                $name =  preg_replace('/\s+/', '', $request->first_name) . uniqid() . '.' . $file->getClientOriginalExtension();
                $file->move(public_path() . '/documents/Client/', $name);
                $client_docs[] = $name;
            }
            $detailsImage = implode(',', $client_docs);
        }


        $client_details = Client::where('id', $request->client_id)->first();

        if ($client_details->documents) {
            $detailsImg = explode(',', $client_details->documents);
        }
        if ($request->docs) {
            // $detailsImg = explode(',', $details->images);
            // foreach ($detailsImg as $dimg) {
            //     $delete = 0;
            //     foreach ($request->docs as $d) {
            //         if ($d != $dimg) {
            //             $delete = 1;
            //         }
            //     }
            //     if($delete == 1){
            //     // unlink(public_path('/documents/Schedule_Details/' . $dimg));
            //     }
            // }
            $previousDocs = implode(',', $request->docs);
        }
        if ($request->docs != "" && $detailsImg != "") {
            $newDocs = $request->docs;
            $oldDocs = $detailsImg;
            // dd(array_diff($oldDocs , $newDocs));
            $diffDocs = array_diff($oldDocs, $newDocs);
            // dd($diffDocs);
            foreach ($diffDocs as $diffDoc) {
                unlink(public_path('/documents/Client/' . $diffDoc));
            }
        } elseif ($client_details->documents != "" && $request->docs == "") {
            foreach ($detailsImg as $deImg) {
                unlink(public_path('/documents/Client/' . $deImg));
            }
        }



        if ($previousDocs && $detailsImage == null) {
            $client_details->documents = $previousDocs;
        } elseif ($detailsImage && $previousDocs == null) {
            $client_details->documents = $detailsImage;
        } elseif ($detailsImage && $previousDocs) {
            $client_details->documents = $previousDocs . "," . $detailsImage;
        } else {
            $client_details->documents = null;
        }

        //docs update end

        // if (is_null($client_details->documents)) {
        //     if (is_null($file)) {
        //         $file = null;
        //     }
        // } else {
        //     if (is_null($file)) {
        //         $file = $client_details->documents;
        //     } else {
        //         $file = $client_details->documents . ',' . $file;
        //     }
        // }

        // dd($client_details);
        $client_details->first_name = $request->first_name;
        $client_details->last_name = $request->last_name;
        $client_details->dob = date('Y-m-d', strtotime($request->dob));
        $client_details->gender = $request->gender;
        $client_details->additional_misc_information = $request->additional_information;
        // $client_details->documents = $file;
        $client_details->update();


        $guardain_data = Guardian::where('client_id', $request->client_id)->delete();
        $doctor_data = Doctor::where('client_id', $request->client_id)->delete();

        foreach ($request->doctor_name as  $key => $name) {

            Doctor::create([
                'client_id' => $request->client_id,
                'doctor_name' => $name,
                'doctor_phone_number' => $request->doctor_phone_number[$key],
                'doctor_address' => $request->doctor_address[$key]
            ]);
        }

        foreach ($request->guardian_name as  $key => $name) {

            Guardian::create([
                'client_id' => $request->client_id,
                'guardian_name' => $name,
                'guardian_phone_number' => $request->guardian_phone_number[$key],
                'guardian_address' => $request->guardian_address[$key]
            ]);
        }

        Session::Flash('success', 'client updated successfuly');
        return redirect('/clients_details/' . Crypt::encryptString($request->client_id));

        // $client =  Client::create([
        //     'first_name' => $request->first_name,
        //     'last_name' => $request->last_name,
        //     'dob' => date('Y-m-d', strtotime($request->dob)),
        //     'phone_number' => $request->phone_number,
        //     'gender' => $request->gender,
        //     'additional_misc_information' => $request->additional_misc_information,
        //     'documents' => $file,
        //     'master_user' => Auth::user()->id,
        //     'created_at' => Carbon::now()
        // ]);

    }




    //achive Client
    public function archiveClient(Request $request)
    {
        $medicines = Medicine::where('client_id', $request->id)->get();
        foreach ($medicines as $med) {
            $med->status = 1;
            $med->update();
        }
        Client::where('id', $request->id)->delete();
        response()->json(['success' => 'Client Archive Successfully']);
    }

    //restore Client SumonK
    public function clientRestore($pid)
    {
        $id = Crypt::decryptString($pid);
        $client = Client::where('id', $id)->onlyTrashed()->first();
        // dd($client);
        $client->restore();
        $archiveMedicine = Medicine::where('client_id', $id)->get();
        foreach ($archiveMedicine as $am) {
            $am->status = 0;
            $am->update();
        }

        return response()->json(['success' => 'Client Restored Successfully']);
    }


    public function documentDownload(Request $request, $id)
    {

        return response()->download(public_path() . '/documents/Client/' . $id,);
    }

    // public function clientMedication($id)
    // {
    //     $id = Crypt::decryptString($id);
    //     $client_data = Client::where('id', $id)->first();
    //     // dd($client_data->getMedicines->getMedicineSchedule);
    //     $date = date('d F Y');
    //     return view('MasterUser/client-medication', ['data' => $client_data, 'date' => $date]);
    //     $medicines = Medicine::where('client_id', $client_data->id)->get();
    //     // dd($medicines);
    //     $Medicine_details = Medicine::where('medicine_name', $id)->where('start_date', Carbon::now()->subDay(2))->get();
    //     if ($Medicine_details) {
    //         foreach ($Medicine_details as $key => $value) {

    //             $medicine_shedule  = MedicineSchedule::where('medicine_id', $value->id)->get();

    //             foreach ($medicine_shedule as $key => $val) {
    //                 $val['name'] = $Medicine_details->medicine_name;
    //             }



    //             $min = Medicine::where('client_id', 1)
    //                 ->orderBy('start_date', 'asc')->first();

    //             $max = Medicine::where('client_id', 1)
    //                 ->orderBy('end_date', 'desc')->first();


    //             if ($min->start_date > Carbon::now()) {
    //                 $date = $min->start_date;
    //             } else {
    //                 dd("hi");
    //             }
    //         }
    //     } else {

    //         return view('MasterUser/client-medication', ['data' => $client_data]);
    //     }
    // }

    public function clientMedication($id, $date) //SumonK
    {
        $id = Crypt::decryptString($id);
        $client_data = Client::where('id', $id)->first();

        $fro = date('Y-m-d', strtotime($date . '-2 day'));
        $to = date('Y-m-d', strtotime($date));

        if ($date == 0) {
            $date = date('d F Y');
            $fro = date('Y-m-d', strtotime($date . '-2 day'));
            $to = date('Y-m-d');
        }

        $nach = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', "!=", 0)->get();
        // dd($nach);
        $note = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->get();
        // dd($note);
        $preClient = Client::select('id')->where('master_user', Auth::user()->id)->where('id', '<', $id)->orderByDesc('id')->first();
        $nxtClient = Client::select('id')->where('master_user', Auth::user()->id)->where('id', '>', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/client-medication', ['data' => $client_data, 'date' => $date, 'patient_id' => $id, 'nach' => $nach, 'note' => $note, 'preClient' => $preClient, 'nxtClient' => $nxtClient, 'reminder' => $reminder]);
    }

    //Archived Client
    public function archivedClientMedication($id, $date)
    {
        $id = Crypt::decryptString($id);
        $client_data = Client::where('id', $id)->withTrashed()->first();

        $fro = date('Y-m-d', strtotime($date . '-2 day'));
        $to = date('Y-m-d', strtotime($date));

        if ($date == 0) {
            $date = date('d F Y', strtotime($client_data->created_at));
            $fro = date('Y-m-d', strtotime($date . '-2 day'));
            $to = date('Y-m-d');
        }

        $nach = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', "!=", 0)->get();
        // dd($nach);
        $note = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->get();
        // dd($note);
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/archived-client-medication', ['data' => $client_data, 'date' => $date, 'patient_id' => $id, 'nach' => $nach, 'note' => $note, 'reminder' => $reminder]);
    }

    public function addMedication($id)
    {
        $id = Crypt::decryptString($id);
        $medicine_details = Medicine::where('client_id', $id)->get();
        $medicine_count = Medicine::where('client_id', $id)->count();
        $client = Client::where('id', $id)->first();
        $cname = $client->first_name . " " . $client->last_name;
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/addmedicine', ['id' => $id, 'cname' => $cname, 'medicine_details' => $medicine_details, 'medicine_count' => $medicine_count, 'reminder' => $reminder]);
    }

    public function addMedicine(Request $request)
    {

        if (is_null($request->med_id)) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'time' => 'required',
                // 'file' => 'required',
                // 'description' => 'required',
                'start_date' => 'required'
            ]);

            if ($validator->fails()) {
                $error =  $validator->errors();
                $schedule_error = "*This Schedule time field is required";

                if (is_null($request->scheduleTime)) {
                    return ["validation_error" => $error, "schedule_error" => $schedule_error];
                } else {

                    foreach ($request->scheduleTime as $value) {
                        if (is_null($value)) {
                            $schedule_error = "*This Schedule time field is required";
                        } else {
                            $schedule_error = "notnull";
                        }
                    }

                    if ($schedule_error ==  "notnull") {
                        return ["validation_error" => $error];
                    } else {
                        return ["validation_error" => $error, "schedule_error" => $schedule_error];
                    }
                }
            } else {
                if (is_null($request->scheduleTime)) {
                    return ['schedule_error' => "this Schedule time field is required"];
                } else {

                    foreach ($request->scheduleTime as $value) {
                        if (is_null($value)) {
                            return ['schedule_error' => "this Schedule time field is required"];
                        }
                    }
                }

                if ($request->hasFile('file')) {
                    $files = $request->file('file');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }

                    if ($count ==  0) {

                        foreach ($files as $key => $file) {
                            $image = $request->name . uniqid() . "." . $file->getClientOriginalExtension();
                            $file->move(public_path('documents/Medicine_Docs/'), $image);
                            $medicine_docs[] = $image;
                        }

                        $medicine_docs = implode(',', $medicine_docs);
                    } else {
                        return ['imgerror' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                    }
                } else {
                    $medicine_docs = null;
                }



                $scheduled_time = implode(',', $request->scheduleTime);

                $medicine = Medicine::create([
                    'client_id' => $request->client_id,
                    'medicine_name' => $request->name,
                    'start_date' => date('Y-m-d', strtotime($request->start_date)),
                    'end_date' => date('Y-m-d', strtotime($request->start_date)),
                    'time_days' => $request->time,
                    'scheduled_time' => $scheduled_time,
                    'status' => 0,
                    'docs' => $medicine_docs,
                    'descriptions' => $request->description
                ]);


                if ($medicine) {

                    $medicine_data = Medicine::where('id', $medicine->id)->first();



                    MedicineSchedule::create([
                        'medicine_id' => $medicine_data->id,
                        'date' => date('Y-m-d', strtotime($request->start_date)),
                        'scheduled_time' => $medicine_data->scheduled_time,
                        'status' => 0
                    ]);

                    // $date  = date('Y-m-d', strtotime($date . ' + 1 days'));
                    // }

                    // //Generate Reminder SumonK

                    // $patientDetails = Client::where('id',$request->client_id)->first();
                    // $patientName = $patientDetails->first_name." ".$patientDetails->last_name;
                    // $new_reminder = ReminderDetail::create([
                    //     'patient_id' => $patientDetails->id,
                    //     'medicine_id' => $medicine_data->id,
                    //     'reminder' => $patientName."'s scheduled medicine ".$medicine->medicine_name." is scheduled for today.",
                    // ]);

                    // $mu = Client::where('id',$patientDetails->id)->first()->master_user;
                    // $su = User::where('master_user',$mu)->get();
                    // if($new_reminder){
                    //     $reminderMU = Reminder::create([
                    //         'receiver_id' => $mu,
                    //         'reminder_id' => $new_reminder->id,
                    //     ]);
                    //     foreach($su as $s)
                    //     {
                    //         $reminderSU = Reminder::create([
                    //             'receiver_id' => $s->id,
                    //             'reminder_id' => $new_reminder->id,
                    //         ]);
                    //     }
                    // }




                    return ['success' => "Added successfuly"];
                }
            }
        } else {
            ($request->status == null) ? $status = 1 : $status = 0;
            // dd($status);

            //Medicine Creation 
            $validator = Validator::make($request->all(), [
                // 'description' => 'required',
                'name' => 'required'
            ]);

            if ($validator->fails()) {
                // $error =  $validator->errors();
                return response()->json(['success' => false, 'errors' => $validator->messages()->all()]);
                // if (is_null($request->scheduleTime)) {
                //     $schedule_error = "this field is required";
                // } else {
                //     foreach ($request->scheduleTime as $value) {
                //         if (is_null($value)) {
                //             $schedule_error = "*This Schedule time field is required";
                //         }
                //     }
                // }
                // return ["validation_error" => $error, "schedule_error" => $schedule_error];
            } else {
                foreach ($request->scheduleTime as $value) {
                    if (is_null($value)) {
                        return ['schedule_error' => "this Schedule time field is required"];
                    }
                }

                $medicine_details = Medicine::where('id', $request->med_id)->first();

                //Documents
                $previousDocs = [];
                $detailsImage = [];
                if ($request->hasfile('file')) {
                    $files = $request->file('file');
                    foreach ($files as $file) {
                        $image = $request->name . uniqid() . "." . $file->getClientOriginalExtension();
                        $file->move(public_path('documents/Medicine_Docs/'), $image);
                        $medicine_docs[] = $image;
                    }
                    $detailsImage = implode(',', $medicine_docs);
                }
                if ($medicine_details->docs) {
                    $detailsImg = explode(',', $medicine_details->docs);
                }
                if ($request->docs) {
                    $previousDocs = implode(',', $request->docs);
                }
                if ($request->docs != "" && $detailsImg != "") {
                    $newDocs = $request->docs;
                    $oldDocs = $detailsImg;
                    // dd(array_diff($oldDocs , $newDocs));
                    $diffDocs = array_diff($oldDocs, $newDocs);
                    // dd($diffDocs);
                    foreach ($diffDocs as $diffDoc) {
                        unlink(public_path('/documents/Medicine_Docs/' . $diffDoc));
                    }
                } elseif ($medicine_details->docs != "" && $request->docs == "") {
                    foreach ($detailsImg as $deImg) {
                        unlink(public_path('/documents/Medicine_Docs/' . $deImg));
                    }
                }
                if ($previousDocs && $detailsImage == null) {
                    $medicine_details->docs = $previousDocs;
                } elseif ($detailsImage && $previousDocs == null) {
                    $medicine_details->docs = $detailsImage;
                } elseif ($detailsImage && $previousDocs) {
                    $medicine_details->docs = $previousDocs . "," . $detailsImage;
                } else {
                    $medicine_details->docs = null;
                }



                $medicine_details->medicine_name = $request->name;
                $medicine_details->time_days = $request->time;
                $medicine_details->descriptions = $request->description;
                // $medicine_details->status = $status;
                $medicine_details->scheduled_time = $request->scheduled_time;
                $medicine_details->scheduled_time = implode(',', $request->scheduleTime);
                $medicine_details->update();

                return ['success' => "Updated successfuly"];

                // if (date('Y-m-d', strtotime($request->end_date)) > $medicine_details->end_date) {

                //     $previous_end_date = $medicine_details->end_date;
                //     $medicine_details->end_date = date('Y-m-d', strtotime($request->end_date));
                //     $medicine_details->descriptions = $request->description;
                //     $medicine_details->time_days = $request->time;
                //     $medicine_details->status = $request->status;
                //     $medicine_details->update();

                //     $earlier = new DateTime($previous_end_date);
                //     $later = new DateTime($request->end_date);

                //     $diff = (int) $later->diff($earlier)->format("%a"); //3

                //     $diff = $diff + 1;

                //     $date  = date('Y-m-d', strtotime($previous_end_date . ' + 1 days'));

                //     for ($i = 0; $i < $diff; $i++) {
                //         if ($i == 0) {
                //             $date = $previous_end_date;
                //         }

                //         MedicineSchedule::create([
                //             'medicine_id' => $medicine_details->id,
                //             'date' => $date,
                //             'status' => 0
                //         ]);

                //         $date  = date('Y-m-d', strtotime($date . ' + 1 days'));
                //     }

                //     return ['success' => "Updated successfuly"];
                // } else if (date('Y-m-d', strtotime($request->end_date)) == $medicine_details->end_date) {

                //     $medicine_details->descriptions = $request->description;
                //     $medicine_details->time_days = $request->time;
                //     $medicine_details->update();
                //     return ['success' => "Updated successfuly"];
                // } else {
                //     return response()->json(['end_date' => "End date must be greater than previous"]);
                // }
            }
        }
    }

    //Medicine Delete
    public function deleteMedicine(Request $request)
    {
        $medicine_id = $request->id;
        Medicine::where('id', $medicine_id)->delete();
        $medicine_shedule = MedicineSchedule::where('medicine_id', $medicine_id)->delete();

        if ($medicine_shedule) {
            return response()->json(['success' => "Medicine deleted successfuly"]);
        }
    }

    public function medicineDownload($id)
    {
        return response()->download(public_path() . '/documents/Medicine_Docs/' . $id,);
    }

    //Medicine Archive
    public function archiveMedicine(Request $request)
    {
        // $medicine_id = $request->id;
        $med = Medicine::where('id', $request->id)->first();
        $med->status = 1;
        $med->update();
        return response()->json(['success' => "Medicine archived successfuly"]);
    }

    //Medication SumonK
    public function medicationDetails($id)
    {
        // dd($id);
        $med_data = Medicine::where('id', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/medication-details', ['med_data' => $med_data, 'reminder' => $reminder]);
    }
    public function clientMedicationUpdate($id)
    {
        // dd($id);
        $id = explode('_', $id);
        $medSchId = $id[0];
        $index = $id[1];
        $medSch = MedicineSchedule::where('id', $id)->first();
        $today = date('Y-m-d');
        $fromDate = date('Y-m-d', strtotime($today . '-3 day'));

        if ($medSch->date >= $fromDate || $medSch->date == $today && $medSch->status == 0) {

            $time = $medSch->complete_details;

            // $current_time = date("h:i");
            $current_time = strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1));

            if ($time == NULL) {
                $time = explode(',', $time);
                for ($i = 0; $i < $index; $i++) {
                    $time[$i] = null;
                }
                $time[$index] = $current_time;
                $updateSchedule = implode(',', $time);
            } else {
                $time = explode(',', $time);
                $time[$index] = $current_time;
                $updateSchedule = implode(',', $time);
            }
            // if ($time) {
            //     $updateSchedule = $time . ',' . $current_time;
            // } else {
            //     $updateSchedule = $current_time;
            // }

            $medSch->complete_details = $updateSchedule;
            $medSch->update();
            // dd($updateSchedule);
            // return response()->json('success')
            // return ['success' => "Medicine Schedule updated successfuly"];
            // dd($medSch);
            $med = explode(',', $medSch->complete_details); //String to Array Conversion of medicine complete timing
            // $medCompleteCount = count($med); // Count how may time are completed
            $medCompleteCount = 0;
            foreach ($med as $m) {
                if ($m != "" || $m != null) {
                    $medCompleteCount = (int)$medCompleteCount + 1;
                }
            }
            $medSchCount = Medicine::where('id', $medSch->medicine_id)->first()->time_days; //Find how many times are scheduled
            if ($medCompleteCount == $medSchCount) {
                $medSch->status = "1"; //if the count matched status will be updated.
                $medSch->update();
                NotificationDetail::medComNote($medSch->medicine_id, $today);
            }
            // dd("medCompleteCount ".$medCompleteCount."/"."medSchCount ".$medSchCount);

            return ['success' => "Hold on! Request on process..."];
        } else {
            // return ['error' => "Sorry, You've missed Scheduled Medicine."];
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

    //Complete medicine schedule Undo SumonK
    public function clientMedicationUndo($id)
    {
        // dd($id);
        $id = explode('_', $id);
        $medSchId = $id[0];
        $index = $id[1];
        $today = date('Y-m-d');
        $fromDate = date('Y-m-d', strtotime($today . '-3 day'));
        $medSch = MedicineSchedule::where('id', $medSchId)->first();
        if ($medSch->date >= $fromDate || $medSch->date == $today) {
            if ($medSch->complete_details != "") {
                $medComDetails = $medSch->complete_details;
                $medComDetails = explode(',', $medComDetails);
                $medComDetails[$index] = "";
                $medComUpdate = implode(',', $medComDetails);
                $medSch->complete_details = $medComUpdate;
                $medSch->status = "0";
                $medSch->update();

                return ['success' => "Hold on! Request on process..."];
            } else {
                return ['error' => "Sorry, Your request can't be processed."];
            }
        } else {
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

    //Medication Note SumonK
    public function medicationNote($schid, $pid)
    {
        // dd($schid." ".$pid);
        $pid = Crypt::decryptString($pid);
        $clientArchive = Client::where('id', $pid)->first();
        $med_shedule = MedicineSchedule::where('id', $schid)->first();
        $med_details = Medicine::where('id', $med_shedule->medicine_id)->first();
        $med_name = $med_details->medicine_name;
        $date = $med_shedule->date;
        $today = date('Y-m-d');
        $fromDate = date('Y-m-d', strtotime($today . '-3 day'));
        $patDetails = Client::where('id', $pid)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date >= $fromDate || $date == $today && $clientArchive != null) {
            $form = 1;
        }
        // dd($med_name);
        // $date_note = MedicineDetails::where('date',$med_date)->where('patient_id',$pid)->where('type',0)->get();
        $date_note = MedicineDetails::where('patient_id', $pid)->where('schedule_id', $schid)->where('type', 0)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        // dd($date_note);
        return view('MasterUser/client-medication-notes', ['med_name' => $med_name, 'date_note' => $date_note, 'patient_id' => $pid, 'med_shedule' => $med_shedule, 'form' => $form, 'patName' => $patName, 'reminder' => $reminder]);
    }
    //Medication Chart SumonK
    public function medicationChart($date, $id)
    {
        $date = Crypt::decryptString($date);
        $id = Crypt::decryptString($id);
        $clientArchive = Client::where('id', $id)->first();
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        // dd($date." ".$id);
        $date_chart = MedicineDetails::where('date', $date)->where('type', 1)->where('patient_id', $id)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/client-medication-chart', ['date_chart' => $date_chart, 'patient_id' => $id, 'date' => $date, 'form' => $form, 'patName' => $patName, 'reminder' => $reminder]);
    }
    //Medication Narrative SumonK
    public function medicationNarrative($date, $id)
    {
        $date = Crypt::decryptString($date);
        $id = Crypt::decryptString($id);
        $clientArchive = Client::where('id', $id)->first();
        // dd($date." ".$id);
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        $date_narrative = MedicineDetails::where('date', $date)->where('type', 2)->where('patient_id', $id)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        // dd($date_narrative);
        return view('MasterUser/client-medication-narrative', ['date_narrative' => $date_narrative, 'patient_id' => $id, 'date' => $date, 'form' => $form, 'patName' => $patName, 'reminder' => $reminder]);
    }

    //Medication Chart SumonK
    public function medicationAllChart($id)
    {
        $id = Crypt::decryptString($id);
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        // dd($date." ".$id);
        $date = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime('-1 year'));
        $date_chart = MedicineDetails::where('patient_id', $id)->where('date', '>=', $endDate)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/client-all-chart', ['date_chart' => $date_chart, 'patient_id' => $id, 'date' => $date, 'patName' => $patName, 'reminder' => $reminder]);
    }

    //Filtered Notes/Charts/Dr Orders
    public function filterMedicationAllChart($id, $type)
    {
        // dd($id." ".$type);
        $id = Crypt::decryptString($id);
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        // dd($date." ".$id);
        $date = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime('-1 year'));
        if ($type == "4") {
            $date_chart = MedicineDetails::where('patient_id', $id)->where('date', '>=', $endDate)->orderByDesc('id')->get();
        } else {
            $date_chart = MedicineDetails::where('patient_id', $id)->where('date', '>=', $endDate)->where('type', $type)->orderByDesc('id')->get();
        }
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        $html = View::make('MasterUser/client-all-chart-render', compact("reminder", "date_chart", "date"))->render();
        return response()->json(['html' => $html]);
    }

    //Add new medication details Charts Narratives Notes SumonK
    public function newDetails(Request $request)
    {
        // dd($request);

        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
            'patient_id' => 'required',
            'type' => 'required',
            // 'date' => 'required',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'Description required'];
        } else {
            $detailsImage = null;
            $schedule_id = null;
            $date = date('Y-m-d');
            if ($request->hasfile('detailsImage')) {
                $files = $request->file('detailsImage');
                $count = 0;

                foreach ($files as $file) {
                    $file_extention = $file->getClientOriginalExtension();

                    if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                    } else {
                        $count++;
                    }
                }

                if ($count ==  0) {
                    foreach ($files as $file) {
                        $name =  $request->patient_id . '_' . $request->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/documents/Schedule_Details/'), $name);
                        $detailsImage[] = $name;
                    }
                    $detailsImage = implode(',', $detailsImage);
                } else {
                    return ['error' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                }
            }
            // else{
            //     $detailsImage = "";
            // }
            if ($request->schedule_id) {
                $schedule_id = $request->schedule_id;
                $date = MedicineSchedule::where('id', $request->schedule_id)->first()->date;
            }
            // $date = date('Y-m-d H:i:s');
            $creator_id = Auth::user()->id;

            $clientdetails =  MedicineDetails::create([
                'descriptions' => $request->description,
                'patient_id' => $request->patient_id,
                'schedule_id' => $schedule_id,
                'type' => $request->type,
                'date' => $date,
                'images' => $detailsImage,
                'creator_id' => $creator_id,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            if ($clientdetails) {
                NotificationDetail::addDetails($creator_id, $request->patient_id, $date, $request->type, $schedule_id);
            }

            return ['success' => "Added successfuly"];
        }
    }

    //Edit Note Chart Narrative
    public function editDetailsForm(Request $request)
    {
        // dd($request)
        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'Description required'];
        } else {
            $details = MedicineDetails::where('id', $request->details_id)->first();
            $details->descriptions = $request->description;
            $previousDocs = [];
            $detailsImage = [];
            if ($details->images) {
                $detailsImg = explode(',', $details->images);
            }
            if ($request->docs) {
                // $detailsImg = explode(',', $details->images);
                // foreach ($detailsImg as $dimg) {
                //     $delete = 0;
                //     foreach ($request->docs as $d) {
                //         if ($d != $dimg) {
                //             $delete = 1;
                //         }
                //     }
                //     if($delete == 1){
                //     // unlink(public_path('/documents/Schedule_Details/' . $dimg));
                //     }
                // }
                $previousDocs = implode(',', $request->docs);
            }
            if ($request->docs != "" && $detailsImg != "") {
                $newDocs = $request->docs;
                $oldDocs = $detailsImg;
                // dd(array_diff($oldDocs , $newDocs));
                $diffDocs = array_diff($oldDocs, $newDocs);
                // dd($diffDocs);
                foreach ($diffDocs as $diffDoc) {
                    unlink(public_path('/documents/Schedule_Details/' . $diffDoc));
                }
            } elseif ($details->images != "" && $request->docs == "") {
                foreach ($detailsImg as $deImg) {
                    unlink(public_path('/documents/Schedule_Details/' . $deImg));
                }
            }

            if ($request->hasfile('detailsImage')) {
                $files = $request->file('detailsImage');
                $count = 0;

                foreach ($files as $file) {
                    $file_extention = $file->getClientOriginalExtension();

                    if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                    } else {
                        $count++;
                    }
                }
                if ($count ==  0) {
                    foreach ($files as $file) {
                        $name =  $details->patient_id . '_' . $details->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/documents/Schedule_Details/'), $name);
                        $detailsImage[] = $name;
                    }
                    $detailsImage = implode(',', $detailsImage);
                    // $finalDocs = $detailsImage.",".$previousDocs;
                } else {
                    return ['error' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                }
            }

            if ($previousDocs && $detailsImage == null) {
                $details->images = $previousDocs;
            } elseif ($detailsImage && $previousDocs == null) {
                $details->images = $detailsImage;
            } elseif ($detailsImage && $previousDocs) {
                $details->images = $previousDocs . "," . $detailsImage;
            } else {
                $details->images = null;
            }
            $details->update();

            return ['success' => "Updated successfuly"];
        }
    }

    //Delete Details
    public function deleteDetails($id)
    {
        $details = MedicineDetails::where('id', $id)->delete();

        return response()->json(['success' => 'Deleted successfully']);
    }

    public function MasterUserDetails()
    {
        $count = Client::where('master_user', Auth::user()->id)->where('status', 1)->count();
        $response = MasterUserServices::archiveClients(Auth::user()->id);
        $archiveSubusers = User::where('master_user', Auth::user()->id)->onlyTrashed()->orderByDesc('id')->get();
        // dd($response);
        if (array_key_exists('success', $response)) {

            $archive_client = $response['data'];
        } else {
            $archive_client = null;
        }

        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        $location = $this->current_locations(Auth::user()->id);
        return view('MasterUser/master-profile', ['location' => $location, 'client' => $archive_client, 'total' => $count, 'archiveSubusers' => $archiveSubusers, 'reminder' => $reminder]);
    }

    public function editMasterUser()
    {
        $user_details = User::where('id', Auth::user()->id)->first();

        if ($user_details->profile_image_name) {

            $user_details['profile_image_name'] = asset('images/MasterUserProfile') . '/' . $user_details->profile_image_name;
        } else {

            $user_details['profile_image_name'] = asset('public/images/MasterUserProfile/defult_image.png');
        }
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/edit-master-user', ['data' => $user_details, 'reminder' => $reminder]);
    }

    public function editMasterUserPost(Request $request)
    {
        $response = MasterUserServices::editMasterUser($request);
        if (array_key_exists('error', $response)) {
            return back()->with(['error' => json_decode(json_encode($response['error']), true)]);
        } else {
            Session::flash('success');
            return redirect('/master_details');
        }
    }




    public function addSubUser()
    {
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/add-subuser', ['reminder' => $reminder]);
    }

    public function newSubUser(Request $request)
    {

        $response = MasterUserServices::subUserRegister($request);
        if (array_key_exists('error', $response)) {
            return response()->json(['error' => $response['error']]);
        } else {
            return $response;
        }
    }

    public function status(Request $request)
    {

        $user = User::where('id', Auth::user()->id)->first();
        $user->status = $request->status;
        $user->update();
        return response()->json('success');
    }



    public function subUsers()
    {

        $masterUser = User::where('id', Auth::user()->id)->first();
        // $masterUser = User::where('id',$id);
        $subUsers = User::where('master_user', $masterUser->id)->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/sub-users', ['subUsers' => $subUsers, 'masterUser' => $masterUser, 'location' => $this->current_locations(Auth::user()->id), 'reminder' => $reminder]);
    }

    public function subUserProfile($id)
    {
        $id = Crypt::decryptString($id);
        $subUserDetails = User::where('id', $id)->first();
        $document = UserDocument::where('user_id', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/subuser-profile', ['subUserDetails' => $subUserDetails, 'document' => $document, 'reminder' => $reminder]);
    }

    public function editSubUser($id)
    {
        $id = Crypt::decryptString($id);
        $user_details = User::where('id', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        // dd($user_details);
        if ($user_details->profile_image_name) {

            $user_details['profile_image_name'] = 'public/images/SubUserProfile/' . $user_details->profile_image_name;
        } else {

            $user_details['profile_image_name'] = 'public/images/SubUserProfile/defult_image.png';
        }
        return view('MasterUser/edit-sub-user', ['data' => $user_details, 'reminder' => $reminder]);
    }

    public function editSubUserPost(Request $request)
    {

        $response = MasterUserServices::editSubUser($request);

        if (array_key_exists('error', $response)) {
            return back()->with(['error' => $response['error']]);
        } else {


            return redirect('/subuser_profile/' . Crypt::encryptString($request->id))->with(['succcess' => "Updated Successfully"]);
        }
    }

    //Edit SubUser Document
    public function editDocsSubUser(Request $request)
    {

        $user = User::where('id', $request->details_id)->first();

        $details = UserDocument::where('user_id', $request->details_id)->first();

        $previousDocs = [];
        $detailsImage = [];

        if ($request->hasfile('detailsImage')) {
            $files = $request->file('detailsImage');
            $count = 0;

            foreach ($files as $file) {
                $file_extention = $file->getClientOriginalExtension();

                if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                } else {
                    $count++;
                }
            }
            if ($count ==  0) {
                foreach ($files as $file) {
                    $name =  $user->first_name . '_' . $user->last_name . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                    $file->move(public_path('/documents/SubUser_Docs/'), $name);
                    $detailsImage[] = $name;
                }
                $detailsImage = implode(',', $detailsImage);
                // $finalDocs = $detailsImage.",".$previousDocs;
            } else {
                return ['error' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
            }
        }

        // $details->descriptions = $request->description;

        if ($details->document) {
            $detailsImg = explode(',', $details->document);
        }
        if ($request->docs) {

            $previousDocs = implode(',', $request->docs);
        }
        if ($request->docs != "" && $detailsImg != "") {
            $newDocs = $request->docs;
            $oldDocs = $detailsImg;
            // dd(array_diff($oldDocs , $newDocs));
            $diffDocs = array_diff($oldDocs, $newDocs);
            // dd($diffDocs);
            foreach ($diffDocs as $diffDoc) {
                unlink(public_path('/documents/SubUser_Docs/' . $diffDoc));
            }
        } elseif ($details->document != "" && $request->docs == "") {
            foreach ($detailsImg as $deImg) {
                unlink(public_path('/documents/SubUser_Docs/' . $deImg));
            }
        }

        if ($previousDocs && $detailsImage == null) {
            $details->document = $previousDocs;
        } elseif ($detailsImage && $previousDocs == null) {
            $details->document = $detailsImage;
        } elseif ($detailsImage && $previousDocs) {
            $details->document = $previousDocs . "," . $detailsImage;
        } else {
            $details->document = null;
        }
        $details->update();

        return ['success' => "Updated successfuly"];
        // }
    }


    public function mfd()
    {
        $masterUser = User::where('id', Auth::user()->id)->first();

        $mfds = MFD::where('master_user_id', Auth::user()->id)->orderBy('id', 'DESC')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        // dd($mfds);
        $latest_mfd_date = 0;
        if (!$mfds) {
            $latest_mfd_date = date('Y-m', strtotime(MFD::where('master_user_id', Auth::user()->id)->orderBy('id', 'DESC')->first()->created_at));
            // dd($latest_mfd_date);
        }

        $today_date = date('Y-m');
        $mfd_btn = '0';

        if ($latest_mfd_date == $today_date) {
            $mfd_btn = '1';
        }
        return view('MasterUser/mfd', ['masterUser' => $masterUser, 'mfds' => $mfds, 'mfd_btn' => $mfd_btn, 'reminder' => $reminder]);
    }

    public function createMfd()
    {
        $date = date('d F Y');
        // dd($date);
        return view('MasterUser/enter-mfd', ['date' => $date]);
    }

    public function storeMFD(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'mfd_details' => 'required|string|min:5',
            'mfd_date' => 'required'
        ]);
        if ($validator->fails()) {
            // return back()->with('error', implode(',', $validator->messages()->all()));
            return ['error' => 'Details & Date required to proceed.'];
        } else {

            $date = date('m-d-Y', strtotime($request->mfd_date));
            $time = date('Hi') . "hrs";
            // dd($date." ".$time);
            $master_user = Auth::user()->id;

            //MFD creation 

            MFD::create([
                'master_user_id' => $master_user,
                'message' => $request->mfd_details,
                'date' => $date . " " . $time,
                'created_at' => date('m-d-Y H:i:s')
            ]);
            Reminder::mfdReminder($master_user, $request->mfd_details);
            return ['success' => "MFD added successfuly"];
        }
    }

    public function mfdEditDetails(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'mfd_details' => 'required|string|min:5',
        ]);
        if ($validator->fails()) {
            return ['error' => 'Details required to proceed.'];
        } else {
            $mfd = MFD::find($request->mfd_id);
            $mfd->message = $request->mfd_details;
            $mfd->update();

            return ['success' => "MFD updated successfuly"];
        }
    }

    public function changePassword()
    {
        $muemail = Auth::user()->email;
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/changepassword', ['muemail' => $muemail, 'reminder' => $reminder]);
    }

    public function updatePassword(Request $request)
    {
        $response = MasterUserServices::updatePassword($request);
        if (array_key_exists('error', $response)) {
            return back()->with(['error' => $response['error']]);
        }
        if (array_key_exists('wrong_error', $response)) {
            return back()->with(['wrong_error' => $response['wrong_error']]);
        } else {
            Session::flash('password', $response['success']);
            return redirect('/master_dashboard');
        }
    }

    public function deleteUser(Request $request)
    {
        $user = User::where('id', $request->id)->first();
        if ($user) {
            $user->delete();
            return response()->json(['success' => 'User Deleted Successfully']);
        }
    }

    //restore SubUser SumonK
    public function restoreSubuser($id)
    {
        $id = Crypt::decryptString($id);
        $subUser = User::where('id', $id)->onlyTrashed()->first();
        $subUser->restore();
        return response()->json(['success' => 'SubUser Restored Successfully']);
    }

    //permanent Delete SubUser SumonK
    public function deleteSubuser(Request $request)
    {
        $id = Crypt::decryptString($request->id);
        $subUser = User::where('id', $id)->onlyTrashed()->first();
        $subUser->forcedelete();
        return response()->json(['success' => 'Subuser Deleted Successfully']);
    }

    //Notification SumonK
    public function muNotifications()
    {
        $getNotifications = Notification::where('receiver_id', Auth::user()->id)->where('status', 0)->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/notifications', ['getNotifications' => $getNotifications, 'reminder' => $reminder]);
    }

    public function notificationSeen($notiId)
    {
        $notificationDetails = Notification::where('id', $notiId)->first();
        $notificationDetails->status = 1;
        $notificationDetails->update();

        return response()->json("seen");
    }

    //Reminder SumonK

    public function muReminder()
    {
        $date = date('Y-m-d');
        $check = 1;
        $reminders = Reminder::where('masteruser_id', Auth::user()->id)->where('date', $date)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/reminder', ['reminders' => $reminders, 'date' => $date, 'check' => $check, 'reminder' => $reminder]);
    }
    public function muFilterReminder($date)
    {
        $date = date('Y-m-d', strtotime($date));
        $reminders = Reminder::where('masteruser_id', Auth::user()->id)->where('date', $date)->orderByDesc('id')->get();
        $check = 0;
        if ($date == date('Y-m-d')) {
            $check = 1;
        }
        $html = View::make('MasterUser/reminder-render', compact("reminders", "check"))->render();
        return response()->json(['html' => $html]);
    }

    public function checkReminder($rid)
    {
        $date = date('Y-m-d');
        $ReminderDetails = Reminder::where('id', $rid)->first();
        if ($ReminderDetails->date == $date) {
            $ReminderDetails->status = 1;
            $ReminderDetails->update();

            return ['success' => "Hold on! Request on process..."];
        } else {
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

    //Delete Reminder
    public function deleteReminder(Request $request)
    {
        Reminder::where('id', $request->id)->delete();

        response()->json(['success' => 'Reminder deleted successfully']);
    }

    public function newReminder(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'reminder_details' => 'required',
            'reminder_date' => 'required',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'Details required'];
        } else {
            $creator_id = Auth::user()->id;

            $reminderdetails =  Reminder::create([
                'masteruser_id' => Auth::user()->id,
                'reminder' => $request->reminder_details,
                'date' => date('Y-m-d', strtotime($request->reminder_date)),
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            return ['success' => "Added successfuly"];
        }
    }

    //SubUser Login Page
    public function suLogin()
    {
        // $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        $reminder = 0;
        return view('MasterUser/subuser-login', ['reminder' => $reminder]);
    }

    //Account Disable Request Submit
    public function disableReason(Request $request)
    {
        // dd($request);
        $validator = Validator::make($request->all(), [
            'reason' => 'required',
        ]);

        if ($validator->fails()) {
            return ['error' => 'Reason required'];
        } else {
            // $creator_id = Auth::user()->id;

            $reasondetails =  AccountReason::create([
                'masteruser_id' => Auth::user()->id,
                'reason' => $request->reason,
                'date' => date('m-d-Y'),
                // 'status' => 0,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            return ['success' => "Your request has been submitted successfuly"];
        }
    }

    //Client Activity Page
    public function clientActivity($id, $date) //SumonK
    {
        $id = Crypt::decryptString($id);
        $client_data = Client::where('id', $id)->first();

        $fro = date('Y-m-d', strtotime($date . '-2 day'));
        $to = date('Y-m-d', strtotime($date));

        if ($date == 0) {
            $date = date('d F Y');
            $fro = date('Y-m-d', strtotime($date . '-2 day'));
            $to = date('Y-m-d');
        }

        // $nach = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', "!=", 0)->get();
        $nach = [];
        // dd($nach);
        $note = ActivityDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->get();
        // $note = [];
        // dd($note);
        $preClient = Client::select('id')->where('master_user', Auth::user()->id)->where('id', '<', $id)->orderByDesc('id')->first();
        $nxtClient = Client::select('id')->where('master_user', Auth::user()->id)->where('id', '>', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/client-activity', ['data' => $client_data, 'date' => $date, 'patient_id' => $id, 'nach' => $nach, 'note' => $note, 'preClient' => $preClient, 'nxtClient' => $nxtClient, 'reminder' => $reminder]);
    }

    //Client Activity Check Update
    public function clientActivityUpdate($id)
    {
        // dd($id);
        $id = explode('_', $id);
        $medSchId = $id[0];
        $index = $id[1];
        $medSch = ActivitySchedule::where('id', $id[0])->first();
        $today = date('Y-m-d');

        if ($medSch->date == $today && $medSch->status == 0) {

            $time = $medSch->complete_details;

            // $current_time = date("h:i");
            $current_time = strtoupper(substr(Auth::user()->first_name, 0, 1)) . strtoupper(substr(Auth::user()->last_name, 0, 1));

            if ($time == NULL) {
                $time = explode(',', $time);
                for ($i = 0; $i < $index; $i++) {
                    $time[$i] = null;
                }
                $time[$index] = $current_time;
                $updateSchedule = implode(',', $time);
            } else {
                $time = explode(',', $time);
                $time[$index] = $current_time;
                $updateSchedule = implode(',', $time);
            }
            // if ($time) {
            //     $updateSchedule = $time . ',' . $current_time;
            // } else {
            //     $updateSchedule = $current_time;
            // }

            $medSch->complete_details = $updateSchedule;
            $medSch->update();
            // dd($updateSchedule);
            // return response()->json('success')
            // return ['success' => "Medicine Schedule updated successfuly"];
            // dd($medSch);
            // $med = explode(',', $medSch->complete_details); //String to Array Conversion of medicine complete timing
            // // $medCompleteCount = count($med); // Count how may time are completed
            // $medCompleteCount = 0;
            // foreach ($med as $m) {
            //     if ($m != "" || $m != null) {
            //         $medCompleteCount = (int)$medCompleteCount + 1;
            //     }
            // }
            // $medSchCount = Medicine::where('id', $medSch->medicine_id)->first()->time_days; //Find how many times are scheduled
            // if ($medCompleteCount == $medSchCount) {
            //     $medSch->status = "1"; //if the count matched status will be updated.
            //     $medSch->update();
            //     NotificationDetail::medComNote($medSch->medicine_id, $today);
            // }
            // dd("medCompleteCount ".$medCompleteCount."/"."medSchCount ".$medSchCount);

            return ['success' => "Hold on! Request on process..."];
        } else {
            // return ['error' => "Sorry, You've missed Scheduled Medicine."];
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

    //Checked Activity schedule Undo SumonK
    public function clientActivityUndo($id)
    {
        // dd($id);
        $id = explode('_', $id);
        $medSchId = $id[0];
        $index = $id[1];
        $today = date('Y-m-d');
        $medSch = ActivitySchedule::where('id', $medSchId)->first();
        if ($medSch->date == $today) {
            if ($medSch->complete_details != "") {
                $medComDetails = $medSch->complete_details;
                $medComDetails = explode(',', $medComDetails);
                $medComDetails[$index] = "";
                $medComUpdate = implode(',', $medComDetails);
                $medSch->complete_details = $medComUpdate;
                $medSch->status = "0";
                $medSch->update();

                return ['success' => "Hold on! Request on process..."];
            } else {
                return ['error' => "Sorry, Your request can't be processed."];
            }
        } else {
            return ['error' => "Sorry, Your request can't be processed."];
        }
    }

    // //Activity Details SumonK
    // public function activityDetails($id)
    // {
    //     // dd($id);
    //     $med_data = Medicine::where('id', $id)->first();
    //     return view('MasterUser/medication-details', ['med_data' => $med_data]);
    // }

    //Manage Activity
    public function addActivity($id)
    {
        $id = Crypt::decryptString($id);
        $medicine_details = Activity::where('client_id', $id)->get();
        $medicine_count = Activity::where('client_id', $id)->count();
        $client = Client::where('id', $id)->first();
        $cname = $client->first_name . " " . $client->last_name;
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/addactivity', ['id' => $id, 'cname' => $cname, 'medicine_details' => $medicine_details, 'medicine_count' => $medicine_count, 'reminder' => $reminder]);
    }

    //Add new Activity
    public function addActivityPost(Request $request)
    {
        // dd($request);
        if (is_null($request->med_id)) {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'time' => 'required',
                // 'file' => 'required',
                // 'description' => 'required',
                'start_date' => 'required'
            ]);

            if ($validator->fails()) {
                $error =  $validator->errors();
                $schedule_error = "*This Schedule time field is required";

                if (is_null($request->scheduleTime)) {
                    return ["validation_error" => $error, "schedule_error" => $schedule_error];
                } else {

                    foreach ($request->scheduleTime as $value) {
                        if (is_null($value)) {
                            $schedule_error = "*This Schedule time field is required";
                        } else {
                            $schedule_error = "notnull";
                        }
                    }

                    if ($schedule_error ==  "notnull") {
                        return ["validation_error" => $error];
                    } else {
                        return ["validation_error" => $error, "schedule_error" => $schedule_error];
                    }
                }
            } else {
                if (is_null($request->scheduleTime)) {
                    return ['schedule_error' => "this Schedule time field is required"];
                } else {

                    foreach ($request->scheduleTime as $value) {
                        if (is_null($value)) {
                            return ['schedule_error' => "this Schedule time field is required"];
                        }
                    }
                }

                if ($request->hasFile('file')) {
                    $files = $request->file('file');
                    $count = 0;

                    foreach ($files as $file) {
                        $file_extention = $file->getClientOriginalExtension();

                        if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                        } else {
                            $count++;
                        }
                    }

                    if ($count ==  0) {

                        foreach ($files as $key => $file) {
                            $image = $request->name . uniqid() . "." . $file->getClientOriginalExtension();
                            $file->move(public_path('documents/Medicine_Docs/'), $image);
                            $medicine_docs[] = $image;
                        }

                        $medicine_docs = implode(',', $medicine_docs);
                    } else {
                        return ['imgerror' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                    }
                } else {
                    $medicine_docs = null;
                }



                $scheduled_time = implode(',', $request->scheduleTime);

                $medicine = Activity::create([
                    'client_id' => $request->client_id,
                    'activity_name' => $request->name,
                    'start_date' => date('Y-m-d', strtotime($request->start_date)),
                    'end_date' => date('Y-m-d', strtotime($request->start_date)),
                    'time_days' => $request->time,
                    'scheduled_time' => $scheduled_time,
                    'status' => 0,
                    'docs' => $medicine_docs,
                    'descriptions' => $request->description
                ]);


                if ($medicine) {

                    $activity_data = Activity::where('id', $medicine->id)->first();



                    ActivitySchedule::create([
                        'activity_id' => $activity_data->id,
                        'date' => date('Y-m-d', strtotime($request->start_date)),
                        'scheduled_time' => $activity_data->scheduled_time,
                        'status' => 0
                    ]);

                    return ['success' => "Added successfuly"];
                }
            }
        } else {
            // ($request->status == null) ? $status = 1 : $status = 0;
            // dd($status);

            //Medicine Creation 
            $validator = Validator::make($request->all(), [
                // 'description' => 'required',
            ]);

            if ($validator->fails()) {
                $error =  $validator->errors();
                if (is_null($request->scheduleTime)) {
                    $schedule_error = "this field is required";
                } else {
                    foreach ($request->scheduleTime as $value) {
                        if (is_null($value)) {
                            $schedule_error = "*This Schedule time field is required";
                        }
                    }
                }
                return ["validation_error" => $error, "schedule_error" => $schedule_error];
            } else {
                foreach ($request->scheduleTime as $value) {
                    if (is_null($value)) {
                        return ['schedule_error' => "this Schedule time field is required"];
                    }
                }
                $medicine_details = Activity::where('id', $request->med_id)->first();
                $medicine_details->time_days = $request->time;
                $medicine_details->descriptions = $request->description;
                // $medicine_details->status = $status;
                $medicine_details->scheduled_time = $request->scheduled_time;
                $medicine_details->scheduled_time = implode(',', $request->scheduleTime);
                $medicine_details->update();

                return ['success' => "Updated successfuly"];
            }
        }
    }

    //Activity Delete
    public function deleteActivity(Request $request)
    {
        $medicine_id = $request->id;
        Activity::where('id', $medicine_id)->delete();
        $medicine_shedule = MedicineSchedule::where('medicine_id', $medicine_id)->delete();

        if ($medicine_shedule) {
            return response()->json(['success' => "Activity deleted successfully"]);
        }
    }

    //Activity Archive
    public function archiveActivity(Request $request)
    {
        // $medicine_id = $request->id;
        $med = Activity::where('id', $request->id)->first();
        $med->status = 1;
        $med->update();
        return response()->json(['success' => "Activity archived successfully"]);
    }

    //Activity Note SumonK
    public function activityNote($schid, $pid)
    {
        // dd($schid." ".$pid);
        $pid = Crypt::decryptString($pid);
        $clientArchive = Client::where('id', $pid)->first();
        $act_shedule = ActivitySchedule::where('id', $schid)->first();
        $act_details = Activity::where('id', $act_shedule->activity_id)->first();
        $act_name = $act_details->activity_name;
        $date = $act_shedule->date;
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $pid)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        // dd($med_name);
        // $date_note = MedicineDetails::where('date',$med_date)->where('patient_id',$pid)->where('type',0)->get();
        $date_note = ActivityDetails::where('patient_id', $pid)->where('schedule_id', $schid)->where('type', 0)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        // dd($date_note);
        return view('MasterUser/client-activity-notes', ['act_name' => $act_name, 'date_note' => $date_note, 'patient_id' => $pid, 'act_shedule' => $act_shedule, 'form' => $form, 'patName' => $patName, 'reminder' => $reminder]);
    }

    //Add new activity details Charts Narratives Notes SumonK
    public function newActivityDetails(Request $request)
    {
        // dd($request);

        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
            'patient_id' => 'required',
            'type' => 'required',
            // 'date' => 'required',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'Description required'];
        } else {
            $detailsImage = null;
            $schedule_id = null;
            $date = date('Y-m-d');
            if ($request->hasfile('detailsImage')) {
                $files = $request->file('detailsImage');
                $count = 0;

                foreach ($files as $file) {
                    $file_extention = $file->getClientOriginalExtension();

                    if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                    } else {
                        $count++;
                    }
                }

                if ($count ==  0) {
                    foreach ($files as $file) {
                        $name =  $request->patient_id . '_' . $request->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/documents/Schedule_Details/'), $name);
                        $detailsImage[] = $name;
                    }
                    $detailsImage = implode(',', $detailsImage);
                } else {
                    return ['error' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                }
            }
            // else{
            //     $detailsImage = "";
            // }
            if ($request->schedule_id) {
                $schedule_id = $request->schedule_id;
                $date = ActivitySchedule::where('id', $request->schedule_id)->first()->date;
            }
            // $date = date('Y-m-d H:i:s');
            $creator_id = Auth::user()->id;

            $clientdetails =  ActivityDetails::create([
                'descriptions' => $request->description,
                'patient_id' => $request->patient_id,
                'schedule_id' => $schedule_id,
                'type' => $request->type,
                'date' => $date,
                'images' => $detailsImage,
                'creator_id' => $creator_id,
                'created_at' => date('Y-m-d H:i:s'),
            ]);

            // if ($clientdetails) {
            //     NotificationDetail::addDetails($creator_id, $request->patient_id, $date, $request->type, $schedule_id);
            // }

            return ['success' => "Added successfuly"];
        }
    }

    //Edit Activity Note Chart Narrative
    public function editActivityDetailsForm(Request $request)
    {
        // dd($request)
        $validator = Validator::make($request->all(), [
            'description' => 'required|string',
        ]);

        if ($validator->fails()) {
            // return ['error' => $validator->errors()];
            return ['error' => 'Description required'];
        } else {
            $details = ActivityDetails::where('id', $request->details_id)->first();
            $details->descriptions = $request->description;
            $previousDocs = [];
            $detailsImage = [];
            if ($details->images) {
                $detailsImg = explode(',', $details->images);
            }
            if ($request->docs) {
                // $detailsImg = explode(',', $details->images);
                // foreach ($detailsImg as $dimg) {
                //     $delete = 0;
                //     foreach ($request->docs as $d) {
                //         if ($d != $dimg) {
                //             $delete = 1;
                //         }
                //     }
                //     if($delete == 1){
                //     // unlink(public_path('/documents/Schedule_Details/' . $dimg));
                //     }
                // }
                $previousDocs = implode(',', $request->docs);
            }
            if ($request->docs != "" && $detailsImg != "") {
                $newDocs = $request->docs;
                $oldDocs = $detailsImg;
                // dd(array_diff($oldDocs , $newDocs));
                $diffDocs = array_diff($oldDocs, $newDocs);
                // dd($diffDocs);
                foreach ($diffDocs as $diffDoc) {
                    unlink(public_path('/documents/Schedule_Details/' . $diffDoc));
                }
            } elseif ($details->images != "" && $request->docs == "") {
                foreach ($detailsImg as $deImg) {
                    unlink(public_path('/documents/Schedule_Details/' . $deImg));
                }
            }

            if ($request->hasfile('detailsImage')) {
                $files = $request->file('detailsImage');
                $count = 0;

                foreach ($files as $file) {
                    $file_extention = $file->getClientOriginalExtension();

                    if ($file_extention == "jpg" || $file_extention == "png" || $file_extention == "jpeg" || $file_extention == "pdf" || $file_extention == "doc" || $file_extention == "docx") {
                    } else {
                        $count++;
                    }
                }
                if ($count ==  0) {
                    foreach ($files as $file) {
                        $name =  $details->patient_id . '_' . $details->type . '_' . uniqid() . '.' . $file->getClientOriginalExtension();
                        $file->move(public_path('/documents/Schedule_Details/'), $name);
                        $detailsImage[] = $name;
                    }
                    $detailsImage = implode(',', $detailsImage);
                    // $finalDocs = $detailsImage.",".$previousDocs;
                } else {
                    return ['error' => "Invalid file type. Only PNG/JPG/JPEG/PDF/DOC/DOCX file accepted"];
                }
            }

            if ($previousDocs && $detailsImage == null) {
                $details->images = $previousDocs;
            } elseif ($detailsImage && $previousDocs == null) {
                $details->images = $detailsImage;
            } elseif ($detailsImage && $previousDocs) {
                $details->images = $previousDocs . "," . $detailsImage;
            } else {
                $details->images = null;
            }
            $details->update();

            return ['success' => "Updated successfuly"];
        }
    }

    //Surveyor Module
    public function srDashboard()
    {
        $client = Client::where('master_user', Auth::user()->id)->where('status', 1)->get();
        // dd($client);
        $count = Client::where('master_user', Auth::user()->id)->where('status', 1)->count();
        $location = $this->current_locations(Auth::user()->id);
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        $date = date('Y-m-d');
        return view('Surveyor/dashboard', ['client' => $client, 'location' => $location, 'total' => $count, 'reminder' => $reminder, 'date' => $date]);
    }

    public function overviewclientMedication($id, $date) //SumonK
    {
        $id = Crypt::decryptString($id);
        $client_data = Client::where('id', $id)->first();

        $fro = date('Y-m-d', strtotime($date . '-30 day'));
        $to = date('Y-m-d', strtotime($date));

        if ($date == 0) {
            $date = date('d F Y');
            $fro = date('Y-m-d', strtotime($date . '-30 day'));
            $to = date('Y-m-d');
        }

        $nach = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', "!=", 0)->get();
        // dd($nach);
        $note = MedicineDetails::whereBetween('date', [$fro, $to])->where('patient_id', $id)->where('type', 0)->get();
        // dd($note);
        $preClient = Client::select('id')->where('master_user', Auth::user()->id)->where('id', '<', $id)->orderByDesc('id')->first();
        $nxtClient = Client::select('id')->where('master_user', Auth::user()->id)->where('id', '>', $id)->first();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('MasterUser/client-medication-overview', ['data' => $client_data, 'date' => $date, 'patient_id' => $id, 'nach' => $nach, 'note' => $note, 'preClient' => $preClient, 'nxtClient' => $nxtClient, 'reminder' => $reminder]);
    }

    //Medication Note SumonK
    // public function srMedicationNote($schid, $pid)
    // {
    //     // dd($schid." ".$pid);
    //     $pid = Crypt::decryptString($pid);
    //     $clientArchive = Client::where('id', $pid)->first();
    //     $med_shedule = MedicineSchedule::where('id', $schid)->first();
    //     $med_details = Medicine::where('id', $med_shedule->medicine_id)->first();
    //     $med_name = $med_details->medicine_name;
    //     $date = $med_shedule->date;
    //     $today = date('Y-m-d');
    //     $patDetails = Client::where('id', $pid)->first();
    //     $patName = $patDetails->first_name . " " . $patDetails->last_name;
    //     $form = 0;
    //     if ($date == $today && $clientArchive != null) {
    //         $form = 1;
    //     }
    //     // dd($med_name);
    //     // $date_note = MedicineDetails::where('date',$med_date)->where('patient_id',$pid)->where('type',0)->get();
    //     $date_note = MedicineDetails::where('patient_id', $pid)->where('schedule_id', $schid)->where('type', 0)->orderByDesc('id')->get();
    //     $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
    //     // dd($date_note);
    //     return view('Surveyor/client-medication-notes', ['med_name' => $med_name, 'date_note' => $date_note, 'patient_id' => $pid, 'med_shedule' => $med_shedule, 'form' => $form, 'patName' => $patName, 'reminder' => $reminder]);
    // }
    public function srMedicationNote($date, $id)
    {
        $date = Crypt::decryptString($date);
        $id = Crypt::decryptString($id);
        $clientArchive = Client::where('id', $id)->first();
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        // dd($date." ".$id);
        // $date_chart = MedicineDetails::where('date', $date)->where('type', 1)->where('patient_id', $id)->orderByDesc('id')->get();
        $date_note = MedicineDetails::where('type', 0)->where('patient_id', $id)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('Surveyor/client-medication-notes', ['date_note' => $date_note, 'patient_id' => $id, 'date' => $date, 'form' => $form, 'patName' => $patName, 'reminder' => $reminder]);
    }

    //Medication Chart SumonK
    public function srMedicationChart($date, $id)
    {
        $date = Crypt::decryptString($date);
        $id = Crypt::decryptString($id);
        $clientArchive = Client::where('id', $id)->first();
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        // dd($date." ".$id);
        // $date_chart = MedicineDetails::where('date', $date)->where('type', 1)->where('patient_id', $id)->orderByDesc('id')->get();
        $date_chart = MedicineDetails::where('type', 1)->where('patient_id', $id)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('Surveyor/client-medication-chart', ['date_chart' => $date_chart, 'patient_id' => $id, 'date' => $date, 'form' => $form, 'patName' => $patName, 'reminder' => $reminder]);
    }

    //Medication Narrative SumonK
    public function srMedicationNarrative($date, $id)
    {
        $date = Crypt::decryptString($date);
        $id = Crypt::decryptString($id);
        $clientArchive = Client::where('id', $id)->first();
        // dd($date." ".$id);
        $today = date('Y-m-d');
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        $form = 0;
        if ($date == $today && $clientArchive != null) {
            $form = 1;
        }
        // $date_narrative = MedicineDetails::where('date', $date)->where('type', 2)->where('patient_id', $id)->orderByDesc('id')->get();
        $date_narrative = MedicineDetails::where('type', 2)->where('patient_id', $id)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        // dd($date_narrative);
        return view('Surveyor/client-medication-narrative', ['date_narrative' => $date_narrative, 'patient_id' => $id, 'date' => $date, 'form' => $form, 'patName' => $patName, 'reminder' => $reminder]);
    }

    //Medication Chart SumonK
    public function srMedicationAllChart($id)
    {
        $id = Crypt::decryptString($id);
        $patDetails = Client::where('id', $id)->first();
        $patName = $patDetails->first_name . " " . $patDetails->last_name;
        // dd($date." ".$id);
        $date = date('Y-m-d');
        $endDate = date('Y-m-d', strtotime('-1 year'));
        $date_chart = MedicineDetails::where('patient_id', $id)->where('date', '>=', $endDate)->orderByDesc('id')->get();
        $reminder = Reminder::where('date', date('Y-m-d'))->where('masteruser_id', Auth::user()->id)->where('status', 0)->count();
        return view('Surveyor/client-all-chart', ['date_chart' => $date_chart, 'patient_id' => $id, 'date' => $date, 'patName' => $patName, 'reminder' => $reminder]);
    }
}
